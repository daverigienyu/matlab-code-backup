%% Execute inside coupled_tgv_recon/ directory
thisdir = pwd;
addpath(pwd);
cd('../coupled_tgv_recon');

%% Run test scripts
figsave = @(p) savefigall(gcf, fullfile(thisdir, p));
profile on
for vec_norm = {'sep','frob','nuc'}
    vnorm = vec_norm{1};
    tgv_recon_test_script_mod(vnorm);  figsave(sprintf('tgv_%s', vnorm));
end
profile viewer

%% Return to execution directory
cd(thisdir);
rmpath(pwd);