function varargout = regexpdir(rootdir, varargin)
%REGEXPDIR   List directory based on regular expressions
%   D = REGEXPDIR(rootdir, pattern, maxDepth) lists files in the root
%   directory that match the regular expression 'pattern', recursively with
%   a maximum recursion depth of 'maxDepth'. The output is similar to the
%   built-in function DIR except that an additional field 'path' is added
%   to the resulting struct array that contains the full filepath to that
%   element.

%==========================================================================
%                               PARSE INPUTS
%--------------------------------------------------------------------------
    p = inputParser;
    addRequired(p,  'rootdir', @isdir);
    addOptional(p,  'pattern', '.*', @ischar);
    addOptional(p,  'maxDepth', 1);
    addParameter(p, 'ignoreCase', false,  @islogical);    
    addParameter(p, 'recursionDepth', 1);
    addParameter(p, 'skipDirs', {});
    addParameter(p, 'skipDicom', true);

    p.parse(rootdir, varargin{:});
    rootdir        = p.Results.rootdir;
    pattern        = p.Results.pattern;
    maxDepth       = p.Results.maxDepth;
    recursionDepth = p.Results.recursionDepth;
    skipDirs       = p.Results.skipDirs;
    
    % For backwards compatibility, handle boolean maxDepth
    if islogical(maxDepth)
        switch maxDepth
            case true
                maxDepth = inf;
            otherwise
                maxDepth = 1;
        end
    end
    
    if p.Results.ignoreCase
        regexpfun = @regexpi;
    else
        regexpfun = @regexp;
    end
%__________________________________________________________________________

    fulllist = dir(rootdir);
    fulllist = fulllist(3:end); % Remove . and .. 
    
    ind = cellfun(@isempty, regexpfun({fulllist.name}, pattern));
    D =  fulllist(~ind);
    
    for i = 1:1:numel(D)
       D(i).path = fullfile(rootdir, D(i).name);        
       [D(i).folder, ~, D(i).extension] = fileparts(D(i).path);
    end
    
    if isempty(D)
        D = [];
    end
    
    if (recursionDepth < maxDepth)
        indDir   = [fulllist.isdir]';
        dirList  = fulllist(indDir);
        
        for iDir = 1:1:numel(dirList)
           dirpath = fullfile(rootdir, dirList(iDir).name);
           [parent, basename] = fileparts(dirpath);
           if any(strcmp(skipDirs,basename))
               fprintf('\nSkipping %s', dirpath);
               continue;
           end
           Dnew = regexpdir(dirpath, pattern, maxDepth, ...
                            'recursionDepth', recursionDepth + 1, ...
                            'ignoreCase', p.Results.ignoreCase, 'skipDirs', skipDirs);
           D = cat(1, D(:), Dnew(:));
        end
    end
    
    if nargout == 1
        varargout{1} = D;
    elseif nargout == 0
        % simply print the filenames to screen
        fprintf('\n\n');
        for i = 1:1:numel(D)
            fprintf('\n%s', D(i).name);
        end
        fprintf('\n\n');
    end
        
end