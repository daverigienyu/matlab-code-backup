function [output_fun] =  funwrap_io(fun)
%
%     This function is a wrapper that can convert any function "fun" to
%     read and write from .MAT files for batch processing
% 
%     For example ...
% 
%     The built-in function x2 = diff(x) normally expects a vector x as input
%     and outputs a vector x2 with numel(x2) = (numel(x)-1)
% 
%     We can generate a new function like
% 
%     diff_io = funwrap_io(diff)
% 
%     now suppose we have a .MAT file:
% 
%     input.mat/
%         fun_args = { [1,2,3,4,5] }
% 
%     calling the function diff_io('input.mat', 'output.mat') will generate a
%     new .MAT file
% 
%     output.mat/
%         fun_results = { [1, 1, 1, 1] }
    
        

      output_fun = @(inputpath, outputpath) funwrap_helper(fun, inputpath, outputpath);

end

function funwrap_helper(fun, inputpath, outputpath)

    input              = load(inputpath);
    fun_results        = {};
    num_args_out       = nargout(fun);
    
    if num_args_out > 0    
        fun_results{1:num_args_out} = fun(input.fun_args{:});
    else
        fun(input.fun_args{:});
    end
    
    output.fun_results = fun_results;
    save(outputpath, '-struct' ,'output');
    
end

