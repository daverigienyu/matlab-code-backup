function [] = filewrite(textstr, filename)

    if nargin < 2
        filename = uiputfile('*.txt', 'Save Text File As...');
    end
    
    % Open file
    fid = fopen(filename,'w+');
    
    fprintf(fid, '%s', textstr);

    % Close file (safe from ctrl C)
    finishup = onCleanup(@() myCleanupFun(fid));
     

end

function myCleanupFun(fid)
    fclose(fid);
end