These files allow you to enable symbolic links in windows (like linux)

This is VERY useful if you need to copy very large files many times for
data processing/reconstruction. For example, PET listmode data can reside
in one raw data folder, and symlinks to it can be placed all over the filesystem
as needed to perform different reconstructions. This greatly reduces the footprint
of all of this data on the hard drive and saves a ton of time from copying files

The query script will just tell you how symlink evaluation is handled currently
on your system.

The "enable..." script will enable all types of symlinks (between local and remote directories)
but MUST BE RUN AS ADMINISTRATOR All network drives must be mapped as administrator for this to work properly

Once this is working the commands look like

mklink target_filepath source_filepath
mklink /D target_directory_path source_directory_path

