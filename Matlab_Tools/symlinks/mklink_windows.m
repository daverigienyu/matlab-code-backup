function [status, result] =  mklink_windows(targetpath, linkpath)

    if exist(targetpath,'dir')
       options = '/D';
    else
       options = '';
    end
    
    command = sprintf('mklink %s %s %s', options, linkpath, targetpath);
    
    [status, result] = system(command,'-echo');
    

end