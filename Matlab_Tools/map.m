function varargout = map(fun, varargin)

    unpack = @(x) x{:};
    
    output = cellfun( fun, varargin, 'UniformOutput', false );
    
    if nargout < numel(output)
        varargout{1} = output;
    else
        [varargout{1:nargout}] = unpack(output);
    end
end