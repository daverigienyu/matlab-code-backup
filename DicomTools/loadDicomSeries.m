function [imstack, info_arr] = loadDicomSeries(dirpath, extension)

    if nargin == 0
       dirpath =  uigetdir;
    end
    
    if nargin < 2
        extension = 'IMA';
    end
    
    filestruct = [dir(fullfile(dirpath, ['*.', lower(extension)])); ... 
                  dir(fullfile(dirpath, ['*.', upper(extension)]))];
    
    filenames = {filestruct(:).name};
    
    % Automatically determine size of image volume
    n_files = numel(filenames);
    n_slices = n_files;
    
    if n_files > 0
       first_slice = dicomread(fullfile(dirpath, filenames{1}));
       [n_rows, n_cols] = size(first_slice);
    end
    
    % Load slices into 3D array
    
    imstack = zeros([n_rows, n_cols, n_slices]);
    info_arr = [];
    
    for i=1:1:n_slices
        fprintf('\nLoading Slice %i of %i', i, n_slices);
        hdr = dicominfo(fullfile(dirpath,filenames{i}));
        info_arr = [info_arr, hdr];
        imgdata = double(dicomread(hdr));
        
        if isfield(hdr, 'RescaleSlope')
            RescaleSlope = double(hdr.RescaleSlope);
        else
            RescaleSlope = 1.0;
        end
        
        if isfield(hdr, 'RescaleIntercept')
            RescaleIntercept = double(hdr.RescaleIntercept);
        else
            RescaleIntercept = 0.0;
        end
        
        if i == n_slices
            RescaleSlope
            RescaleIntercept
        end
            
        
        imstack(:,:,i) = imgdata*RescaleSlope + RescaleIntercept;
    end
    fprintf('\n');
    
end

