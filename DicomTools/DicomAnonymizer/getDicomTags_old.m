function [T,meta, S] = getDicomTags(filepath)
% GETDICOMTAGS  create table summarizing all tag information for dicom file
%   
%   [T, meta, S] = getDicomTags(filepath) This analyzes the dicom file
%   rrrr to by filepath for all tag information. T is a MATLAB table
%   object, with detailed tag information, meta is a string with only the meta
%   information, and S is a string as returned by DICOMDISP, which contains
%   both metainformation and the tag information.
%
%   see also DICOMDISP
%

% Input Argument handling
if nargin == 0
    [filename, dirpath] = uigetfile('*.*');
    filepath = fullfile(dirpath,filename);
end


S = evalc('dicomdisp(filepath)');

matches = regexp(S,'(.*)(Location\s*Level[^\n]*)\n(-*\n)(.*)','tokens');

meta = matches{1}{1};
column_names = strsplit(strtrim(matches{1}{2}));
n_columns = numel(column_names);

tag_text = strtrim(matches{1}{4});

tag_inds = regexp(tag_text,'\d{5,}\s*\d\s*\(\d{4},\d{4}\)');
n_data_rows = length(tag_inds);

Data = cell(n_data_rows, n_columns);

for i = 1:1:n_data_rows
    
    if i < n_data_rows
        left = tag_inds(i);
        right = tag_inds(i+1);
        r = tag_text(left:right);
    else
        left = tag_inds(i);
        r = tag_text(left:end);
    end
        
    matches = regexp(r, '(\d{7})\s*(\d)\s*(\([\w,]*\))\s*([A-Z]{2})\s*(\d*\s*bytes) - (\w*)(\s.*$)','tokens');
    matches = cellfun(@strtrim, matches, 'UniformOutput',false);
    Data(i,:) = matches{:};
    
    
end

T = cell2table(Data, 'VariableNames', column_names);

end

