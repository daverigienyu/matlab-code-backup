function [T,metaInfo, S] = getDicomTags(filepath)
% GETDICOMTAGS  create table summarizing all tag information for dicom file
%   
%   [T, meta, S] = getDicomTags(filepath) This analyzes the dicom file
%   rrrr to by filepath for all tag information. T is a MATLAB table
%   object, with detailed tag information, meta is a string with only the meta
%   information, and S is a string as returned by DICOMDISP, which contains
%   both metainformation and the tag information.
%
%   see also DICOMDISP
%

% Input Argument handling
if nargin == 0
    [filename, dirpath] = uigetfile('*.*');
    filepath = fullfile(dirpath,filename);
end


S = evalc('dicomdisp(filepath)');

ind = regexp(S, 'Location Level');
meta = strtrim(S(1:(ind-1)));

columnNames = {'Location', 'Level', 'Tag', 'VR', 'Size', 'Name', 'Data'};


indStart    = regexp(S, '(?m)^\d{7}');
indStart    = indStart(1);
dataText    = S(indStart:end);

spacer = '[ \t-]*';
LocationExpr = '(?<Location>^\d{7,})';
LevelExpr    = '(?<Level>\d)';
TagExpr      = '(?<Tag>\(\w+\,\w+\))';
VRExpr       = '(?<VR>[A-Z]{2})';
SizeExpr     = '(?<Size>\d+ bytes)';
NameExpr     = '(?<Name>\S+)';
DataExpr     = '(?<Data>\S*[^\n]*)[\n$]';

data = regexp(dataText, ...
              sprintf('(?m)%s',LocationExpr,spacer,LevelExpr,spacer,TagExpr,spacer,VRExpr,spacer,SizeExpr,spacer,NameExpr,spacer,DataExpr), ...%,spacer,DataExpr), ... 
              'names');

T = struct2table(data);

%Data = cell(n_data_rows, n_columns);



%T = cell2table(Data, 'VariableNames', column_names);

end

