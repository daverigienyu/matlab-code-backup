function [blacklist] = blacklistcsv2mat(csvpath, matpath)
% BLACKLISTCSV2MAT  convert csv table to matfile with blacklist information
%   
%   blacklist = blacklist(csvpath) creates an Nx1 struct array with tags
%               that should be blacklisted. The tags are specified as
%               strings, e.g. '(0002,0001)' with the groupNum and elemNum
%
%               The input csv-file should contain only two columns. The
%               first column has the tag as a string, e.g. (0002,0001) and
%               the second column has a '1' or a '0', also as a string. The
%               1 indicates that the tag SHOULD be black listed, while the
%               0 indicates that it is safe and should be kept.
%
%

    % If no path specified, open UI for file selection
    if nargin < 1
       [filename,dirpath] = uigetfile('*.*');
       csvpath = fullfile(dirpath,filename);
    end
    
    if nargin < 2
        [~, ~, csv_ext] = fileparts(csvpath);
        matpath = strrep(csvpath, csv_ext, '.mat');
    end

    text = fileread(csvpath);
    %matches = regexp(text,'(\([^\)]*\))[^\d]*(\d)','tokens');
    matches = regexp(text,'(\([^\)]*\))[^\r\n]*(\d)','tokens');
    matches = cat(1,matches{:})
    
    ind = find(strcmp(matches(:,2),'1'));
    
    blacklist = matches(ind,1)
    
    
    
    % Writes a matfile     
    save(matpath, 'blacklist');

end

