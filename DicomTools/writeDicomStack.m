function [] = writeDicomStack(imstack, filename)

    % imstack should be n_rows x n_cols x n_slices

    if nargin == 0
       fprintf('\nwriteDicomStack(imstack, filename)\n');
    end

    maxval = double(intmax('uint16'));

    imstack_int = uint16(mat2gray(imstack)*maxval);

    [n_rows, n_cols, n_slices] = size(imstack);

    [dirpath, basename, ext] = fileparts(filename);
    
    dicomdir = fullfile(dirpath, basename); % directory for output
    
    if ~exist(dicomdir, 'dir')
        mkdir(dicomdir);
    end
    
    filename = fullfile(dicomdir, 'slice.dcm');
    
    dicomwrite(reshape(imstack_int,n_rows,n_cols,1,n_slices), filename, ...
               'MultiframeSingleFile', false)

end

