function [whiteTable, removed] = createWhiteList(T_anon, T_orig)

    n_rows = size(T_anon,1);
    
    whiteTable = [];
    removed = [];
    
    for i = 1:1:n_rows
        rowData_anon = T_anon(i,:).Data{1};
        rowOrig = T_orig(strcmp(T_orig.Tag,T_anon(i,:).Tag), :);
        try 
            rowData_orig = rowOrig.Data{1}; 
            if ~isAnonymized(rowData_anon, rowData_orig)
                whiteTable = [whiteTable; T_anon(i,:)];
            else
                removed = [removed; T_anon(i,:)];
            end 
        catch ME
            disp(ME);disp(ME.message);
        end
    end

end

function flag = isAnonymized(rowData_anon, rowData_orig)

    flag = false;
    
    %{
    if isempty(rowData_anon)
        flag = true;
    elseif strcmp(rowData_anon,'[]')
        flag = true;
    end
    
    if ~isempty(regexp(rowData_anon, '\[x*\]'))
        flag = true;
    end
    %}
    
    if ~strcmp(rowData_anon, rowData_orig)
        {rowData_anon;rowData_orig}
        flag = true;
    end
    

end