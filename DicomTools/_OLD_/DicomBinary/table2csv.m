function [] = elems2csv(Elems, filepath, delim)

    if nargin < 2
        delim = '$';
    end
    
    fid = fopen(filepath, 'w+');
        
    for i=1:1:length(Elems)
       writeElement(fid, Elems(i)); 
    end
    
    


end

function writeElement(fid, Elem)

               

        if Elem.VL < 200
            value = num2str(Elem.ValueField);
        else
            value = '';
        end
        
        s = sprintf('%i$%s$%s$%s$%i$%s$%s', ...
            Elem.pos, Elem.GroupNumber, Elem.ElementNumber, ...
            Elem.VR, Elem.VL, Elem.Name, value);
        
        fprintf(fid, '%s\n', s);
   
end