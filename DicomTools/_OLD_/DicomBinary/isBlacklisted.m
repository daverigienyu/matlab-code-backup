function flag = isBlacklisted(groupNum, elemNum, blacklist)

    query = sprintf('(%s,%s)',groupNum,elemNum);
    
    if any(cell2mat(strfind(blacklist, query)))
        flag = true;
    else
        flag = false;
    end

end