
    MF = load('VRtable.mat');
    VRtable = MF.T;
    
    mapVR = containers.Map;
    
    for i = 1:1:length(VRtable)
        [code, meaning, fmtInfo, fmt] = VRtable{i,:};
        valRep.code = code;
        valRep.meaning = meaning;
        valRep.fmtInfo = fmtInfo;
        valRep.fmt = fmt;
        mapVR(code) = valRep;
    end
        
save('mapVR.mat', 'mapVR');
