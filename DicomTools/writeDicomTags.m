function [] = writeDicomTags(inpath, outpath)

    if nargin < 2
        [dirpath, filename, ext] = fileparts(inpath);
        outpath = fullfile(dirpath, [filename '.txt']);
    end

    S = getDicomTags(inpath);
    
    fid = fopen(outpath, 'w+');
    fprintf(fid, '%s', S);
    fclose(fid);
    
end