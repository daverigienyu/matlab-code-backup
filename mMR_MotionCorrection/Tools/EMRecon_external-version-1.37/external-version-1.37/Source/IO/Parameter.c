/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "Parameter.h"


/* ############################################################################################# */
/* ### GetParameterChar ######################################################################## */
/* ############################################################################################# */
/**
 * @brief GetParameterChar
 * @param ParameterName
 * @param retVal
 */
void GetParameterChar(char *ParameterName, char *retVal)
{
    int i;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0)
        {
            sprintf(retVal,"%s",Parameter.var_parm[i].var_value);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }
}


/* ############################################################################################# */
/* ### GetParameterDouble ###################################################################### */
/* ############################################################################################# */
/**
 * @brief GetParameterDouble
 * @param ParameterName
 * @return
 */
double GetParameterDouble(char *ParameterName)
{
    int i;
    double retVal = 0.0;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0)
        {
            retVal = (double)atof(Parameter.var_parm[i].var_value);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }

    return retVal;
}


/* ############################################################################################# */
/* ### GetParameterInt ######################################################################### */
/* ############################################################################################# */
/**
 * @brief GetParameterInt
 * @param ParameterName
 * @return
 */
int GetParameterInt(char *ParameterName)
{
    int i;
    int retVal = 0;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0)
        {
            retVal = atoi(Parameter.var_parm[i].var_value);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }

    return retVal;
}


/* ############################################################################################# */
/* ### InitializeParameter ##################################################################### */
/* ############################################################################################# */
/**
 * @brief InitializeParameter
 */
void InitializeParameter()
{
    Parameter.epsilon = 0.0001;

    Parameter.subsets = 1;
    Parameter.iterations = 1;

    Parameter.fixed_subset = 0;
    Parameter.subset_order = 1;

    Parameter.convolve = 0;
    Parameter.convolutioninterval = 0;

    Parameter.fwhm = 0.0;
    Parameter.fwhm_x = -1.0;
    Parameter.fwhm_y = -1.0;
    Parameter.fwhm_z = -1.0;
}


/* ############################################################################################# */
/* ### SetParameter ############################################################################ */
/* ############################################################################################# */
/**
 * @brief SetParameter
 * @param ParameterName
 * @param ParameterValue
 */
void SetParameter(char *ParameterName, char *ParameterValue)
{
    int i;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0 || strlen(Parameter.var_parm[i].var_name) == 0)
        {
            sprintf(Parameter.var_parm[i].var_name,"%s",ParameterName);
            sprintf(Parameter.var_parm[i].var_value,"%s",ParameterValue);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }
}


/* ############################################################################################# */
/* ### UpdateParameterDouble ################################################################### */
/* ############################################################################################# */
double UpdateParameterDouble(char *ParameterName, double ParameterValue)
{
    int i;
    double retVal = ParameterValue;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0)
        {
            retVal = atof(Parameter.var_parm[i].var_value);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }
    return retVal;
}


/* ############################################################################################# */
/* ### UpdateParameterInt ###################################################################### */
/* ############################################################################################# */
/**
 * @brief UpdateParameterInt
 * @param ParameterName
 * @param ParameterValue
 * @return
 */
int UpdateParameterInt(char *ParameterName, int ParameterValue)
{
    int i;
    int retVal = ParameterValue;

    for(i=0;i<MAX_EXTERNAL_PARAMETER;i++)
    {
        if (strcmp(ParameterName,Parameter.var_parm[i].var_name) == 0)
        {
            retVal = atoi(Parameter.var_parm[i].var_value);
            i = MAX_EXTERNAL_PARAMETER;
        }
    }
    return retVal;
}


/* ############################################################################################# */
/* ### UpdateParameterList ##################################################################### */
/* ############################################################################################# */
/**
 * @brief UpdateParameterList
 */
void UpdateParameterList()
{
    /* image related parameter */
    Parameter.size_x = UpdateParameterInt("SIZE_X",Parameter.size_x);
    Parameter.size_y = UpdateParameterInt("SIZE_Y",Parameter.size_y);
    Parameter.size_z = UpdateParameterInt("SIZE_Z",Parameter.size_z);
    Parameter.imagesize = Parameter.size_x*Parameter.size_y*Parameter.size_z;

    Parameter.fov_x = UpdateParameterDouble("FOV_X",Parameter.fov_x);
    Parameter.fov_y = UpdateParameterDouble("FOV_Y",Parameter.fov_y);
    Parameter.fov_z = UpdateParameterDouble("FOV_Z",Parameter.fov_z);

    Parameter.offset_x = UpdateParameterDouble("OFFSET_X",Parameter.offset_x);
    Parameter.offset_y = UpdateParameterDouble("OFFSET_Y",Parameter.offset_y);
    Parameter.offset_z = UpdateParameterDouble("OFFSET_Z",Parameter.offset_z);

    Parameter.offset_c = UpdateParameterDouble("OFFSET_C",Parameter.offset_c);


    /* data / reconstruction related parameter */
    Parameter.datatype = UpdateParameterInt("DATATYPE",Parameter.datatype);
    Parameter.epsilon = UpdateParameterDouble("EPSILON",Parameter.epsilon);
    Parameter.fixed_subset = UpdateParameterInt("FIXED_SUBSET",Parameter.fixed_subset);
    Parameter.subsets = UpdateParameterInt("SUBSETS",Parameter.subsets);
    Parameter.subset_order = UpdateParameterInt("SUBSET_ORDER",Parameter.subset_order);
    Parameter.iterations = UpdateParameterInt("ITERATIONS",Parameter.iterations);
    Parameter.convolve = UpdateParameterInt("CONVOLVE",Parameter.convolve);
    Parameter.convolutioninterval = UpdateParameterInt("CONVOLUTIONINTERVAL",Parameter.convolutioninterval);
    Parameter.fwhm = UpdateParameterDouble("FWHM",Parameter.fwhm);
    Parameter.fwhm_x = UpdateParameterDouble("FWHM_X",Parameter.fwhm_x);
    Parameter.fwhm_y = UpdateParameterDouble("FWHM_Y",Parameter.fwhm_y);
    Parameter.fwhm_z = UpdateParameterDouble("FWHM_Z",Parameter.fwhm_z);
}
