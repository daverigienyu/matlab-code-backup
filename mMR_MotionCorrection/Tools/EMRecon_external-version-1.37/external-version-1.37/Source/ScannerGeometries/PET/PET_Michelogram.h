/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef PET_MICHELOGRAM_H
#define PET_MICHELOGRAM_H


/* emrecon includes */
#include "PET_Scanner.h"


/* struct definitions */
typedef struct
{
    short *sinogram_number, /*!< gibt für zwei ringe die entsprechende sinogramm nummer zurück */
    *sinogram_rings_lookup, /*!< gibt für ein sinogram aus dem sinogram stack zwei ringe zurück */
    *sinogram_slice, /*!< "bildschicht" zweier ringe given by s*/
    *sinogram_weight; /*!< gewichtungsfaktor für das sinogramm, ein wert pro sinogramm. */
    int span,
    max_ring_diff,
    *segments,
    sinograms,
    sinograms_in_stack,
    sensitivityevents;
    float *sinogram_mask;
    double *sinogram_slice_x_position,
    *sinogram_slice_y_position,
    *sinogram_slice_z_position;
} michelogram;


/* global variables */
michelogram Michelogram;


/* function declarations */
void pet_michelogram_axial_weight(double *input_michelogram, double *output_michelogram);
void pet_michelogram_compress_sinogram_stack(double *input_sinogram_stack, double *output_michelogram);
void pet_michelogram_disable_virtual_crystals();
long pet_michelogram_get_number_of_events_in_file();
void pet_michelogram_initialize();
void pet_michelogram_issrb(double *input_sinogram, double *output_michelogram);
void pet_michelogram_normalize(double *input_michelogram, double *input_norm, double *output_michelogram);
void pet_michelogram_read_events(int events_to_read, event *Eventvector);
void pet_michelogram_release_memory();
void pet_michelogram_setup();
void pet_michelogram_update_parameter();


#endif
