/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "PET_Siemens_mMR.h"


/* ############################################################################################# */
/* ### pet_siemens_mmr_info #################################################################### */
/* ############################################################################################# */
/**
 * @brief pet_siemens_mmr_info
 * @return
 */
char* pet_siemens_mmr_info()
{
    char *scanner_info = (char*)calloc(50,sizeof(char));
    sprintf(scanner_info,"\t\t\t  8) [PET] Siemens Biograph mMR\n");
    return scanner_info;
}


/* ############################################################################################# */
/* ### pet_siemens_mmr_rebin_32bit_data_span11 ################################################# */
/* ############################################################################################# */
/**
 * @brief pet_siemens_mmr_rebin_32bit_data_span11
 * @param input_listmode
 * @param output_prompt
 * @param output_delayed
 */
void pet_siemens_mmr_rebin_32bit_data_span11(char *input_listmode, double *output_prompt, double *output_delayed)
{
    FILE *listmodefile;
    int i, j, s, s11, ringA, ringB, current_gate, gates;
    unsigned int n;
    double *msec;
    long current_timemark = -1;
    long starttime = -1;
    long stoptime = -1;

#ifdef WINDOWS_MATLAB
    errno_t err;
    __int64 filesize, tag, tags, maxtags;

    /* existence of listmodefile was checked before */
    err = fopen_s(&listmodefile,input_listmode,"rb");
    _fseeki64(listmodefile,0,SEEK_END);
    filesize = _ftelli64(listmodefile);
    tags = filesize/4;
    _fseeki64(listmodefile,0,SEEK_SET);

    message(4,"tags: %lld\n",tags);
    message(4,"filesize: %lld\n",filesize);
#else
    long filesize, tag, tags, maxtags;

    /* existence of listmodefile was checked before */
    listmodefile = fopen(input_listmode,"rb");
    fseek(listmodefile,0,SEEK_END);
    filesize = ftell(listmodefile);
    tags = filesize/4;
    fseek(listmodefile,0,SEEK_SET);
#endif

    /* calculate number of gates */
    gates = mgf_number_of_respiratory_gates * mgf_number_of_cardiac_gates;

    /* set number of tags to process */
    maxtags = tags;
    if (GetParameterInt("MAXTAGS") > 0)
    {
        maxtags = min(maxtags,GetParameterInt("MAXTAGS"));
    }

    /* calloc space for scaling */
    msec = safe_calloc(gates,sizeof(double));

    /* initialize vector */
    for(i=0;i<gates;i++)
    {
        for(j=0;j<Michelogram.sensitivityevents;j++)
        {
            output_prompt[j+i*Michelogram.sensitivityevents] = 0.0;
            output_delayed[j+i*Michelogram.sensitivityevents] = 0.0;
        }
    }

    /* skip loop over all tags in case of invalid mgf file */
    if (mgf_number_of_entries == -1)
    {
        maxtags = 0;
    }

    /* loop over all tags */
    for (tag=0;tag<maxtags;tag++)
    {
        fread(&n,4,1,listmodefile);

        /* time marker --> PETLINK manual */
        if (n >= 2147483648 && n < 2684354560)
        {
            current_timemark = (int)(n-2147483648);
        }

        /* only use data with valid timestamp */
        if (current_timemark > -1)
        {
            /* per default gate = 1 */
            current_gate = 1;

            /* check for valid starttime */
            starttime = mgf_starttime;
            if (current_timemark < mgf_starttime)
            {
                current_gate = 0;
            }

            /* check for valid stoptime */
            if (mgf_stoptime > 0)
            {
                if (current_timemark <= mgf_stoptime)
                {
                    stoptime = current_timemark;
                }
                if (current_timemark >= mgf_stoptime)
                {
                    current_gate = 0;
                    tag = maxtags;
                }
            }
            else
            {
                stoptime = current_timemark;
            }

            /* - in case of just 1 valid entry, only use mgf_starttime and mgf_stoptime */
            /* - in case of more than 1 valid entry, use mgf_timesignal and mgf_gatingsignal */
            if (mgf_number_of_entries > 1)
            {
                if (current_gate > 0)
                {
                    /* update mgf_dataindex according to current time */
                    if (current_timemark > mgf_timesignal[mgf_dataindex])
                    {
                        mgf_dataindex++;
                    }

                    /* update current gate */
                    current_gate = mgf_gatingsignal[mgf_dataindex];
                }
            }

            /* use only counts with valid current_gate */
            if (current_gate >= 1)
            {
                /* save time for scaling */
                if (n >= 2147483648 && n < 2684354560)
                {
                    msec[current_gate-1]++;
                }

                /* delayed event --> PETLINK manual */
                /* compress data from span 1 to span 11 */
                if (n < 1073741824)
                {
                    s = n / (Sinogram.angles*Sinogram.projections);
                    ringA = Michelogram.sinogram_rings_lookup[2*s];
                    ringB = Michelogram.sinogram_rings_lookup[2*s+1];
                    s11 = Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB];
                    output_delayed[n+(s11-s)*(Sinogram.angles*Sinogram.projections)+(current_gate-1)*Michelogram.sensitivityevents] += 1.0;
                }

                /* - prompt event --> PETLINK manual */
                /* compress data from span 1 to span 11 */
                /* - keep this order (delayed before prompt) since n will be modified */
                if (n < 2147483648 && n >= 1073741824)
                {
                    n = n - 1073741824;
                    s = n / (Sinogram.angles*Sinogram.projections);
                    ringA = Michelogram.sinogram_rings_lookup[2*s];
                    ringB = Michelogram.sinogram_rings_lookup[2*s+1];
                    s11 = Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB];
                    output_prompt[n+(s11-s)*(Sinogram.angles*Sinogram.projections)+(current_gate-1)*Michelogram.sensitivityevents] += 1.0;
                }
            }
        }
    }

    /* scale data to per h */
    for(i=0;i<gates;i++)
    {
        if (msec[i] > 0.0)
        {
            for(j=0;j<Michelogram.sensitivityevents;j++)
            {
                output_prompt[j+i*Michelogram.sensitivityevents] = output_prompt[j+i*Michelogram.sensitivityevents] / msec[i] * 1000.0 * 3600.0;
                output_delayed[j+i*Michelogram.sensitivityevents] = output_delayed[j+i*Michelogram.sensitivityevents] / msec[i] * 1000.0 * 3600.0;
            }
        }
    }

    /* plot start and stoptime */
    message(4,"used starttime:\t%ld ms\n",starttime);
    message(4,"mgf_starttime:\t%ld ms\n",mgf_starttime);
    message(4,"used stoptime:\t%ld ms\n",stoptime);
    message(4,"mgf_stoptime:\t%ld ms\n",mgf_stoptime);

    /* free memory */
    free(msec);

    /* after rebinning close listmode file */
    fclose(listmodefile);
}


/* ############################################################################################# */
/* ### pet_siemens_mmr_setup ################################################################### */
/* ############################################################################################# */
/**
 * @brief pet_siemens_mmr_setup
 *
 * Geometry of Siemens mMR is based on the following documents / publications:
 *
 * 1) Interfile header
 *
 * 2) Scanner log files
 */
void pet_siemens_mmr_setup()
{
    message(5,"\nsetup siemens biograph mmr...\n");

    /* initialize */
    pet_michelogram_setup();

    /* image related parameter */
    Parameter.size_x = 344;
    Parameter.size_y = 344;
    Parameter.size_z = 127;
    Parameter.fov_x = 717.67344;
    Parameter.fov_y = 717.67344;
    Parameter.fov_z = 257.96875;
    Parameter.offset_x = -Parameter.fov_x/2.0;
    Parameter.offset_y = -Parameter.fov_y/2.0;
    Parameter.offset_z = -Parameter.fov_z/2.0;
    Parameter.offset_c = 297.5;

    /* data / reconstruction related parameter */
    Parameter.datatype = 1;
    Parameter.iterations = 3;
    Parameter.subsets = 21;

    /* scanner geometry */
    PET_Scanner.axial_blocks = 8;
    PET_Scanner.transversal_blocks = 56;

    PET_Scanner.axial_crystals_per_block = 8;
    PET_Scanner.transversal_crystals_per_block = 8;

    PET_Scanner.crystal_width_x = 20.0;
    PET_Scanner.crystal_width_y = 4.0;
    PET_Scanner.crystal_width_z = 4.0;
    PET_Scanner.crystal_gap_y = 0.0445;
    PET_Scanner.crystal_gap_z = 0.0445;

    PET_Scanner.block_offset = 0.40625;
    PET_Scanner.crystal_offset_b = 503;
    PET_Scanner.radius = 328.0;
    PET_Scanner.rotation_offset = M_PI / 56.0;

    PET_Scanner.v_crystals_between_blocks = 1;
    PET_Scanner.v_rings_between_blocks = 0;
    PET_Scanner.v_crystal_width_y = 4.0;
    PET_Scanner.v_crystal_width_z = 0.0;

    PET_Scanner.doi = 6.7;

    /* sinogram properties */
    Sinogram.projections = 344;
    Sinogram.angles = 252;

    /* michelogram properties */
    Michelogram.span = 11;
    Michelogram.max_ring_diff = 60;

    /* update parameter and create crystal geometry */
    pet_michelogram_update_parameter();
    pet_scanner_calculate_crystal_positions();
    pet_michelogram_initialize();
    pet_michelogram_disable_virtual_crystals();

    message(5,"setup siemens biograph mmr...done.\n");
}
