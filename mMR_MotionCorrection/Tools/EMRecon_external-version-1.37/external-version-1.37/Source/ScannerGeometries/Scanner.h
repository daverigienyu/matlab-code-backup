/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef SCANNER_H
#define SCANNER_H


/* emrecon includes */
#include "../Main/Tools.h"
#include "PET/PET_Scanner.h"


/* PET SCANNER */
/* - Demo */
#ifdef PET_2D_DEMO
#include "PET/Demo/PET_2D_Demo.h"
#endif

/* - GE */
#ifdef PET_GE_DISCOVERY_ST
#include "PET/GE/PET_GE_Discovery_ST.h"
#endif

/* - Siemens */
#ifdef PET_SIEMENS_INVEON
#include "PET/Siemens/PET_Siemens_Inveon.h"
#endif

#ifdef PET_SIEMENS_MMR
#include "PET/Siemens/PET_Siemens_mMR.h"
#endif

/* CT SCANNER */
#ifdef CT_SIEMENS_DEFINITION_AS
#include "CT/CT_Siemens_Definition_AS.h"
#endif


/* function declarations */
long get_number_of_events_in_file();
void read_events(long events_to_read, event *Eventvector);
void release_memory();
char* scanner_info();
void setup_scanner();


#endif
