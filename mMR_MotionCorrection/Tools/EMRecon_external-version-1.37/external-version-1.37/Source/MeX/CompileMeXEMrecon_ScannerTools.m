%% mexEMrecon_Version
if (compile_mexEMrecon_Version == 1)
    fprintf('compiling mexEMrecon_Version...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' Main/C/mexEMrecon_Version.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Main/Matlab/EMrecon_Version';
    
    fprintf('compiling mexEMrecon_Version...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_DrawCrystalGeometry
if (compile_mexEMrecon_PET_DrawCrystalGeometry == 1)
    fprintf('compiling mexEMrecon_PET_DrawCrystalGeometry...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_DrawCrystalGeometry.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_DrawCrystalGeometry_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_DrawCrystalGeometry_ND';
    
    fprintf('compiling mexEMrecon_PET_DrawCrystalGeometry...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Michelogram_AxialWeight
if (compile_mexEMrecon_PET_Michelogram_AxialWeight == 1)
    fprintf('compiling mexEMrecon_PET_Michelogram_AxialWeight...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Michelogram_AxialWeight.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Michelogram_AxialWeight';
    
    fprintf('compiling mexEMrecon_PET_Michelogram_AxialWeight...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Michelogram_CompressSinogramStack
if (compile_mexEMrecon_PET_Michelogram_CompressSinogramStack == 1)
    fprintf('compiling mexEMrecon_PET_Michelogram_CompressSinogramStack...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Michelogram_CompressSinogramStack.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Michelogram_CompressSinogramStack';
    
    fprintf('compiling mexEMrecon_PET_Michelogram_CompressSinogramStack...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Michelogram_GetMask
if (compile_mexEMrecon_PET_Michelogram_GetMask == 1)
    fprintf('compiling mexEMrecon_PET_Michelogram_GetMask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Michelogram_GetMask.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Michelogram_FillGaps';
    
    fprintf('compiling mexEMrecon_PET_Michelogram_GetMask...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Michelogram_ISSRB
if (compile_mexEMrecon_PET_Michelogram_ISSRB == 1)
    fprintf('compiling mexEMrecon_PET_Michelogram_ISSRB...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Michelogram_ISSRB.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Michelogram_ISSRB';
    
    fprintf('compiling mexEMrecon_PET_Michelogram_ISSRB...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Michelogram_Normalize
if (compile_mexEMrecon_PET_Michelogram_Normalize == 1)
    fprintf('compiling mexEMrecon_PET_Michelogram_Normalize...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Michelogram_Normalize.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Michelogram_Normalize';
    
    fprintf('compiling mexEMrecon_PET_Michelogram_Normalize...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Siemens_Inveon_Rebin
if (compile_mexEMrecon_PET_Siemens_Inveon_Rebin == 1)
    fprintf('compiling mexEMrecon_PET_Siemens_Inveon_Rebin...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Siemens_Inveon_Rebin.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Siemens_Inveon_Rebin';
    
    fprintf('compiling mexEMrecon_PET_Siemens_Inveon_Rebin...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_PET_Siemens_mMR_Rebin
if (compile_mexEMrecon_PET_Siemens_mMR_Rebin == 1)
    fprintf('compiling mexEMrecon_PET_Siemens_mMR_Rebin...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ScannerGeometries/C/mexEMrecon_PET_Siemens_mMR_Rebin.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ScannerGeometries/Matlab/EMrecon_PET_Siemens_mMR_Rebin';
    
    fprintf('compiling mexEMrecon_PET_Siemens_mMR_Rebin...done in %2.5f secs.\n',toc);
end
