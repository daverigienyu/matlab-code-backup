%% set location of source code (has to be defined once)
os_source_windows = 'H:\Projects\EMreconTNG\branches\external-version-1.37\Source\MeX';                % <----- adjust this line
os_source_linux = '/media/sf_H_DRIVE/Projects/EMreconTNG/branches/external-version-1.37/Source/Mex';   % <----- adjust this line

%% detect OS
if (ispc == 0 && isunix == 1)
    os = 'linux';
    path_to_emrecon_mex_source = os_source_linux;
elseif (ispc == 1 && isunix == 0)
    os = 'windows';
    path_to_emrecon_mex_source = os_source_windows;
else
    error('cannot detect os');
end
