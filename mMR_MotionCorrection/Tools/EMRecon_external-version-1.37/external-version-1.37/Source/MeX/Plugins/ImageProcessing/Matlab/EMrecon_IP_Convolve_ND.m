function [new_image] = EMrecon_IP_Convolve_ND(parm,image)

% EMrecon_IP_Convolve_ND(parm,image)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_IP_Convolve_ND\n');
    fprintf('Usage: new_image = EMrecon_IP_Convolve_ND(parm,image);\n\n');
    mexEMrecon_IP_Convolve;
    fprintf('\n');
    new_image = -1;
else
    vfprintf(1,parm,'EMrecon_IP_Convolve_ND...\n');
    tic;
    [new_image,dims] = mexEMrecon_IP_Convolve(parm,image(:));
    if (numel(dims) == 3)
        new_image = reshape(new_image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_IP_Convolve_ND...done. [%f sec]\n',toc);
end

end