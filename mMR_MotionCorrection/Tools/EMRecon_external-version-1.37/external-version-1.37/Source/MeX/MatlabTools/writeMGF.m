function [output_args] = writeMGF(filename,timesignal,gatingsignal,starttime,stoptime,cardiac_gates,respiratory_gates,comments)

% Writes a gating signal into a MGF file which can be used with EMrecon
% for gating of listmode files. A combination of timetag / gate number
% is used to sort the data. All events of the listmode files having a
% timestamp smaller or equal to the current timetag will be assigned to
% the corresponding gate.
%
% This file format is based on the MRF file format created by M. Dawood
% and K. Schaefers.

if (nargin ~= 8)
    output_args = 0;
    disp('writeMGF(filename,timesignal,gatingsignal,starttime,stoptime,cardiac_gates,respiratory_gates,comments)');
else
    fid = fopen(filename,'w');
    fprintf(fid,'############################################################\n\n');
    fprintf(fid,'MGF - Muenster Gating File\n\n');
    fprintf(fid,'############################################################\n\n');
    fprintf(fid,'The file contains the following list of entries:\n');
    fprintf(fid,'(1) [int32] timetag in ms\n');
    fprintf(fid,'(2) [int32] gate number (1-n)\n');
    fprintf(fid,'All listmode entries having a timestamp smaller or\n');
    fprintf(fid,'equal to the current MGF timetag belong to the corresponding\n');
    fprintf(fid,'MGF gate number. The first MGF timetag must be larger than\n');
    fprintf(fid,'the MGF_Starttime. The last entry of the MGF timetags has\n');
    fprintf(fid,'to be equal to MGF_Stoptime.\n\n');
    fprintf(fid,'The last character of the ascii header is a "semicolon".\n');
    if (numel(comments) > 0)
        fprintf(fid,'\n############################################################\n\n');
        fprintf(fid,'Comments:\n%s\n',comments);
    end
    fprintf(fid,'\n############################################################\n\n');
    fprintf(fid,'MGF_Starttime=%d\n',starttime);
    fprintf(fid,'MGF_Stoptime=%d\n',stoptime);
    fprintf(fid,'MGF_Number_of_Entries=%d\n',numel(timesignal));
    fprintf(fid,'MGF_Number_of_Cardiac_Gates=%d\n',cardiac_gates);
    fprintf(fid,'MGF_Number_of_Respiratory_Gates=%d\n\n',respiratory_gates);
    fprintf(fid,'############################################################\n\n;');
    fclose(fid);
    
    fid = fopen(filename,'ab');
    fwrite(fid,timesignal,'int32');
    fwrite(fid,gatingsignal,'int32');
    fclose(fid);
    
    output_args = 1;
end
