function [mex_emrecon_version,supported_scanner] = EMrecon_Version()

% EMrecon_Version()

if (nargout ~= 1 && nargout ~= 2)
    fprintf('\nEMrecon_Version\n');
    fprintf('Usage: [mex_emrecon_version,supported_scanner] = EMrecon_Version();\n\n');
    mexEMrecon_Version;
    fprintf('\n');
    mex_emrecon_version = -1;
    supported_scanner = -1;
else
    if (nargout == 1)
        mex_emrecon_version = mexEMrecon_Version;
    end
    if (nargout == 2)
        [mex_emrecon_version,supported_scanner] = mexEMrecon_Version;
    end
end

end