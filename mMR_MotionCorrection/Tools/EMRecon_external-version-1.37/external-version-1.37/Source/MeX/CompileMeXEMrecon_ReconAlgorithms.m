%% mexEMrecon_EM
if (compile_mexEMrecon_EM == 1)
    fprintf('compiling mexEMrecon_EM...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_EM.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_ND';
    
    fprintf('compiling mexEMrecon_EM...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_EM_Step
if (compile_mexEMrecon_EM_Step == 1)
    fprintf('compiling mexEMrecon_EM_Step...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_EM_Step.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_Step_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_Step_ND';
    
    fprintf('compiling mexEMrecon_EM_Step...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_EM_ResoReco
if (compile_mexEMrecon_EM_ResoReco == 1)
    fprintf('compiling mexEMrecon_EM_ResoReco...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_EM_ResoReco.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM_ResoReco.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_ResoReco_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_ResoReco_ND';
    
    fprintf('compiling mexEMrecon_EM_ResoReco...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_BackProj
if (compile_mexEMrecon_BackProj == 1)
    fprintf('compiling mexEMrecon_BackProj...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_BackProj.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/BackProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProj_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProj_ND';
    
    fprintf('compiling mexEMrecon_BackProj...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_BackProjAC
if (compile_mexEMrecon_BackProjAC == 1)
    fprintf('compiling mexEMrecon_BackProjAC...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_BackProjAC.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/BackProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjAC_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjAC_ND';
    
    fprintf('compiling mexEMrecon_BackProjAC...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_BackProjMask
if (compile_mexEMrecon_BackProjMask == 1)
    fprintf('compiling mexEMrecon_BackProjMask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_BackProjMask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/BackProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjMask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjMask_ND';
    
    fprintf('compiling mexEMrecon_BackProjMask...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_ForwardProj
if (compile_mexEMrecon_ForwardProj == 1)
    fprintf('compiling mexEMrecon_ForwardProj...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_ForwardProj.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/ForwardProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProj_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProj_ND';
    
    fprintf('compiling mexEMrecon_ForwardProj...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_ForwardProjAC
if (compile_mexEMrecon_ForwardProjAC == 1)
    fprintf('compiling mexEMrecon_ForwardProjAC...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_ForwardProjAC.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/ForwardProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjAC_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjAC_ND';
    
    fprintf('compiling mexEMrecon_ForwardProjAC...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_ForwardProjMask
if (compile_mexEMrecon_ForwardProjMask == 1)
    fprintf('compiling mexEMrecon_ForwardProjMask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_ForwardProjMask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/ForwardProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjMask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjMask_ND';
    
    fprintf('compiling mexEMrecon_ForwardProjMask...done in %2.5f secs.\n',toc);
end
