function [fp_data] = EMrecon_ForwardProjAC_1D(parm,image,data)

% EMrecon_ForwardProjAC_1D(parm,image,data)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_ForwardProjAC_1D\n');
    fprintf('Usage: fp_data = EMrecon_ForwardProjAC_1D(parm,image,data);\n\n');
    mexEMrecon_ForwardProjAC;
    fprintf('\n');
    fp_data = -1;
else
    vfprintf(1,parm,'EMrecon_ForwardProjAC_1D...\n');
    tic;
    fp_data = mexEMrecon_ForwardProjAC(parm,image,data);
    vfprintf(1,parm,'EMrecon_ForwardProjAC_1D...done. [%f sec]\n',toc);
end

end