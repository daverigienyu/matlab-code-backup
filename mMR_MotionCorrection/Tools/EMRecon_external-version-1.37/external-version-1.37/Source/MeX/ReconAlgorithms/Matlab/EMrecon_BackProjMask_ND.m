function [bp_image] = EMrecon_BackProjMask_ND(parm,data,mask)

% EMrecon_BackProjMask_ND(parm,data,mask)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_BackProjMask_ND\n');
    fprintf('Usage: bp_image = EMrecon_BackProjMask_ND(parm,data,mask);\n\n');
    mexEMrecon_BackProjMask;
    fprintf('\n');
    bp_image = -1;
else
    vfprintf(1,parm,'EMrecon_BackProjMask_ND...\n');
    tic;
    [bp_image,dims] = mexEMrecon_BackProjMask(parm,data,mask);
    if (numel(dims) == 3)
        bp_image = reshape(bp_image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_BackProjMask_ND...done. [%f sec]\n',toc);
end

end