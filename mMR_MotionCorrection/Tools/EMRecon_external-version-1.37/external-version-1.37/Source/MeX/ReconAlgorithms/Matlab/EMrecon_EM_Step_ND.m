function [image] = EMrecon_EM_Step_ND(parm,data,data_fp,startimage)

% EMrecon_MC_EM_Step_PM_ND(parm,data,data_fp,startimage)

if (nargin ~= 4 || nargout ~= 1)
    fprintf('\nEMrecon_EM_Step_ND\n');
    fprintf('Usage: image = EMrecon_EM_Step_ND(parm,data,data_fp,startimage);\n\n');
    mexEMrecon_EM_Step;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_Step_ND...\n');
    tic;
    [image,dims] = mexEMrecon_EM_Step(parm,data,data_fp,startimage(:));
    if (numel(dims) == 3)
        image = reshape(image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_EM_Step_ND...done. [%f sec]\n',toc);
end

end