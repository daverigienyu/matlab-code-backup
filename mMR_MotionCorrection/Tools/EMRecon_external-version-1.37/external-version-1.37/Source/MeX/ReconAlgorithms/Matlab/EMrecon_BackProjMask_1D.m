function [bp_image] = EMrecon_BackProjMask_1D(parm,data,mask)

% EMrecon_BackProjMask_1D(parm,data,mask)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_BackProjMask_1D\n');
    fprintf('Usage: bp_image = EMrecon_BackProjMask_1D(parm,data,mask);\n\n');
    mexEMrecon_BackProjMask;
    fprintf('\n');
    bp_image = -1;
else
    fprintf('EMrecon_BackProjMask_1D...\n');
    tic;
    bp_image = mexEMrecon_BackProjMask(parm,data,mask);
    fprintf('EMrecon_BackProjMask_1D...done. [%f sec]\n',toc);
end

end