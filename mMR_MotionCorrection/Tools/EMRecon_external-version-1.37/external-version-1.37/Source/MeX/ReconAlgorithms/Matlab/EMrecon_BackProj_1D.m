function [bp_image] = EMrecon_BackProj_1D(parm,data)

% EMrecon_BackProj_1D(parm,data)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_BackProj_1D\n');
    fprintf('Usage: bp_image = EMrecon_BackProj_1D(parm,data);\n\n');
    mexEMrecon_BackProj;
    fprintf('\n');
    bp_image = -1;
else
    vfprintf(1,parm,'EMrecon_BackProj_1D...\n');
    tic;
    bp_image = mexEMrecon_BackProj(parm,data);
    vfprintf(1,parm,'EMrecon_BackProj_1D...done. [%f sec]\n',toc);
end

end