function [bp_image] = EMrecon_BackProjAC_ND(parm,data,image)

% EMrecon_BackProjAC_ND(parm,data,image)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_BackProjAC_ND\n');
    fprintf('Usage: bp_image = EMrecon_BackProjAC_ND(parm,data,image);\n\n');
    mexEMrecon_BackProjAC;
    fprintf('\n');
    bp_image = -1;
else
    vfprintf(1,parm,'EMrecon_BackProjAC_ND...\n');
    tic;
    image = reshape(image,numel(image),1);
    [bp_image,dims] = mexEMrecon_BackProjAC(parm,data,image);
    if (numel(dims) == 3)
        bp_image = reshape(bp_image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_BackProjAC_ND...done. [%f sec]\n',toc);
end

end