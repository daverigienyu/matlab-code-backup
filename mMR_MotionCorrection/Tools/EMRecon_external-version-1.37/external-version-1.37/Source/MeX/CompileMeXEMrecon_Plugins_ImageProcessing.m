%% mexEMrecon_IP_Convolve
if (compile_mexEMrecon_IP_Convolve == 1)
    fprintf('compiling mexEMrecon_IP_Convolve...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' Plugins/ImageProcessing/C/mexEMrecon_IP_Convolve.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_Convolve_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_Convolve_ND';
    
    fprintf('compiling mexEMrecon_IP_Convolve...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_IP_ComputePath
if (compile_mexEMrecon_IP_ComputePath == 1)
    fprintf('compiling mexEMrecon_IP_ComputePath...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' Plugins/ImageProcessing/C/mexEMrecon_IP_ComputePath.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_ComputePath_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_ComputePath_ND';
    
    fprintf('compiling mexEMrecon_IP_ComputePath...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_IP_CutImage
if (compile_mexEMrecon_IP_CutImage == 1)
    fprintf('compiling mexEMrecon_IP_CutImage...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' Plugins/ImageProcessing/C/mexEMrecon_IP_CutImage.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_CutImage_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Plugins/ImageProcessing/Matlab/EMrecon_IP_CutImage_ND';
    
    fprintf('compiling mexEMrecon_IP_CutImage...done in %2.5f secs.\n',toc);
end
