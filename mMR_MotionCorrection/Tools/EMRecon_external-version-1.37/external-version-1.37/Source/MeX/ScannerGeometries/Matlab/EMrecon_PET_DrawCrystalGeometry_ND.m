function [crystal_geometry] = EMrecon_PET_DrawCrystalGeometry_ND(parm)

% EMrecon_PET_DrawCrystalGeometry_ND(parm)

if (nargin ~= 1 || nargout ~= 1)
    fprintf('\nEMrecon_PET_DrawCrystalGeometry_ND\n');
    fprintf('Usage: crystal_geometry = EMrecon_PET_DrawCrystalGeometry_ND(parm);\n\n');
    mexEMrecon_PET_DrawCrystalGeometry;
    fprintf('\n');
    crystal_geometry = -1;
else
    vfprintf(1,parm,'EMrecon_PET_DrawCrystalGeometry_ND...\n');
    tic;
    [crystal_geometry,dims] = mexEMrecon_PET_DrawCrystalGeometry(parm);
    if (numel(dims) == 3)
        crystal_geometry = reshape(crystal_geometry,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_PET_DrawCrystalGeometry_ND...done. [%f sec]\n',toc);
end

end