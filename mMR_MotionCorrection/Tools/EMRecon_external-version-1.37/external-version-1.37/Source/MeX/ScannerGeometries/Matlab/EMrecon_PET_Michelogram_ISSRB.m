function [michelogram] = EMrecon_PET_Michelogram_ISSRB(parm,sinogram)

% EMrecon_PET_Michelogram_ISSRB(parm,sinogram)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_PET_Michelogram_ISSRB\n');
    fprintf('Usage: michelogram = EMrecon_PET_Michelogram_ISSRB(parm,sinogram);\n\n');
    mexEMrecon_PET_Michelogram_ISSRB;
    fprintf('\n');
    michelogram = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Michelogram_ISSRB...\n');
    tic;
    michelogram = mexEMrecon_PET_Michelogram_ISSRB(parm,sinogram);
    vfprintf(1,parm,'EMrecon_PET_Michelogram_ISSRB...done. [%f sec]\n',toc);
end

end