function [prompt,delayed] = EMrecon_PET_Siemens_Inveon_Rebin(parm)

% EMrecon_PET_Siemens_Inveon_Rebin(parm)

if (nargin ~= 1 || nargout ~= 2)
    fprintf('\nEMrecon_PET_Siemens_Inveon_Rebin\n');
    fprintf('Usage: [prompt,delayed] = EMrecon_PET_Siemens_Inveon_Rebin(parm);\n\n');
    mexEMrecon_PET_Siemens_Inveon_Rebin;
    fprintf('\n');
    prompt = -1;
    delayed = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Siemens_Inveon_Rebin...\n');
    tic;
    if (isfield(parm,'SCANNERTYPE') == 0 || parm.SCANNERTYPE ~= 4)
        fprintf('EMrecon_PET_Siemens_Inveon_Rebin: parm.SCANNERTYPE=4 required\n');
        prompt = -1;
        delayed = -1;
    else
        [prompt,delayed] = mexEMrecon_PET_Siemens_Inveon_Rebin(parm);
    end
    vfprintf(1,parm,'EMrecon_PET_Siemens_Inveon_Rebin...done. [%f sec]\n',toc);
end

end