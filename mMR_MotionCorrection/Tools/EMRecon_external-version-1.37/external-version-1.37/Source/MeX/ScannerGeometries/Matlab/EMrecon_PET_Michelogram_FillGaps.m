function [new_michelogram] = EMrecon_PET_Michelogram_FillGaps(parm,michelogram)

% EMrecon_PET_Michelogram_FillGaps(parm,michelogram)

% The algorithm to fill the gaps in a michelogram (stack of sinograms)
% implemented in this function is based on the following publications:
%
% 1) Tuna et al., Interpolation for the Gap-Filling of the HRRT PET
%    Sinograms by Using the Slices in the Direction of the Radial Samples,
%    IEEE NSS Conference Record, 2009, M09-218, pp. 3273-3279

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_PET_Michelogram_FillGaps\n');
    fprintf('Usage: mask = EMrecon_PET_Michelogram_FillGaps(parm,michelogram);\n\n');
    fprintf('\n');
    new_michelogram = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Michelogram_FillGaps...\n');
    tic;
    [mask,dims] = mexEMrecon_PET_Michelogram_GetMask(parm);
    
    if (numel(michelogram) == numel(mask) && numel(dims) == 3)
        
        % reshape michelogram from 1d vector to (r,phi,s)
        mask = reshape(mask,dims(1),dims(2),dims(3));
        michelogram = reshape(michelogram,dims(1),dims(2),dims(3));
        
        % create new michelogram
        new_michelogram = zeros(size(mask));
        
        % grid for s is used for both images for all dimensions
        if (mod(dims(3),2) == 0)
            SGrid = -(dims(3)/2.0-0.5):(dims(3)/2.0-0.5);
        else
            SGrid = -((dims(3)-1)/2.0):((dims(3)-1)/2.0);
        end
        
        % grid for new (=original) phi is used for both images for all dimensions
        if (mod(dims(2),2) == 0)
            PhiGridNew = -(dims(2)/2.0-0.5):(dims(2)/2.0-0.5);
        else
            PhiGridNew = -((dims(2)-1)/2.0):((dims(2)-1)/2.0);
        end
        
        % create meshgrid
        [SNew,PhiNew] = meshgrid(SGrid,PhiGridNew);
        
        % loop over all (phi,s) planes of the michelogram
        for r=1:dims(1)
            
            % identify (non-)zero elements in slice
            phi_index = find(mask(r,:,1) > 0);
            phi_index_zero = find(mask(r,:,1) <= 0);
            
            % extract non-zero elements
            r_slice_michelogram = squeeze(michelogram(r,phi_index,:));
            
            % grid for old reduced dim phi
            if (mod(numel(phi_index),2) == 0)
                stepsize = 2*(dims(2)/2.0-0.5)/(numel(phi_index)-1);
                PhiGridOld = -(dims(2)/2.0-0.5):stepsize:(dims(2)/2.0-0.5);
            else
                stepsize = 2*((dims(2)-1)/2.0)/(numel(phi_index)-1);
                PhiGridOld = -((dims(2)-1)/2.0):stepsize:((dims(2)-1)/2.0);
            end
            
            % create new meshgrid for old coordinates
            [SOld,PhiOld] = meshgrid(SGrid,PhiGridOld);
            
            r_slice_interp = interp2(SOld,PhiOld,r_slice_michelogram,SNew,PhiNew);
            
            new_michelogram(r,phi_index_zero,:) = r_slice_interp(phi_index_zero,:);
            new_michelogram(r,phi_index,:) = michelogram(r,phi_index,:);
        end
        
        % reshape new_michelogram into 1d vector
        new_michelogram = reshape(new_michelogram,numel(new_michelogram),1);
    else
        fprintf('Michelogram dimensions do not match\n\n');
        fprintf('\n');
        new_michelogram = -1;
    end
    vfprintf(1,parm,'EMrecon_PET_Michelogram_FillGaps...done. [%f sec]\n',toc);
end
