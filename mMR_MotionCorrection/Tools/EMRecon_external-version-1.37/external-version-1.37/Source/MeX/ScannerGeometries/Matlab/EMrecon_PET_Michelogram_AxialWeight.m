function [data_weighted] = EMrecon_PET_Michelogram_AxialWeight(parm,data)

% EMrecon_PET_Michelogram_AxialWeight(parm,data)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_PET_Michelogram_AxialWeight\n');
    fprintf('Usage: data_weighted = EMrecon_PET_Michelogram_AxialWeight(parm,data);\n\n');
    mexEMrecon_PET_Michelogram_AxialWeight;
    fprintf('\n');
    data_weighted = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Michelogram_AxialWeight...\n');
    tic;
    data_weighted = mexEMrecon_PET_Michelogram_AxialWeight(parm,data);
    vfprintf(1,parm,'EMrecon_PET_Michelogram_AxialWeight...done. [%f sec]\n',toc);
end

end