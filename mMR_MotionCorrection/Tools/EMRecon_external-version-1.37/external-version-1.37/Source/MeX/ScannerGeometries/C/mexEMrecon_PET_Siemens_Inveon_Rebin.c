/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "mexEMrecon_PET_Siemens_Inveon_Rebin.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    FILE *listmodefile,*mgffile;
    int gates;
    char mgffilename[512],listmodefilename[512];
    double *output_prompt,*output_delayed;
    
    
    /* check for valid input/output parameter */
    if (nlhs != 2)
    {
        message(0,"mexEMrecon_PET_Siemens_Inveon_Rebin [%s]\n\n",EMRECON_VERSION);
    }
    else
    {
        /* initialize default parameter */
        SetParameter("LISTMODEFILENAME","");
        SetParameter("MGFFILENAME","");
        SetParameter("MAXTAGS","-1");
        
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);
        
        message(2,"mexEMrecon_PET_Siemens_Inveon_Rebin...\n");

        /* try to open mgf file */
        GetParameterChar("MGFFILENAME",mgffilename);
        if (strlen(mgffilename) > 0)
        {
            mgffile = fopen(mgffilename,"rb");
        }
        else
        {
            mgffile = 0;
        }
        
        /* try to open listmode file */
        GetParameterChar("LISTMODEFILENAME",listmodefilename);
        if (strlen(listmodefilename) > 0)
        {
            listmodefile = fopen(listmodefilename,"rb");
        }
        else
        {
            listmodefile = 0;
        }

        /* check whether both files are available */
        if (listmodefile == 0 || mgffile == 0)
        {
            if (listmodefile != 0)
            {
                fclose(listmodefile);
            }
            else
            {
                message(0,"mexEMrecon_PET_Siemens_Inveon_Rebin: Could not open parm.LISTMODEFILENAME [%s]\n",listmodefilename);
            }

            if (mgffile != 0)
            {
                fclose(mgffile);
            }
            else
            {
                message(0,"mexEMrecon_PET_Siemens_Inveon_Rebin: Could not open parm.MGFFILENAME [%s]\n",mgffilename);
            }

            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_prompt = mxGetPr(plhs[0]);
            output_prompt[0] = -1.0;
            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_delayed = mxGetPr(plhs[1]);
            output_delayed[0] = -1.0;
        }
        else
        {
            /* close open files */
            fclose(listmodefile);
            fclose(mgffile);

            /* load basic scanner setup */
            setup_scanner();

            /* read mgffile */
            gates = gt_read_mgf(mgffilename);

            /* catch invalid mgffile */
            if (gates > 0)
            {
                /* output */
                plhs[0] = mxCreateNumericMatrix(gates*get_number_of_events_in_file(),1,mxDOUBLE_CLASS,mxREAL);
                output_prompt = mxGetPr(plhs[0]);
                plhs[1] = mxCreateNumericMatrix(gates*get_number_of_events_in_file(),1,mxDOUBLE_CLASS,mxREAL);
                output_delayed = mxGetPr(plhs[1]);

                /* rebin listmode data to michelogram */
                pet_siemens_inveon_rebin_48bit_data_span3(listmodefilename,output_prompt,output_delayed);
            }
            else
            {
                plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
                output_prompt = mxGetPr(plhs[0]);
                output_prompt[0] = -1.0;
                plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
                output_delayed = mxGetPr(plhs[1]);
                output_delayed[0] = -1.0;
            }

            /* release memory */
            gt_release_memory();
        }
        /* release memory */
        release_memory();

        message(2,"mexEMrecon_PET_Siemens_Inveon_Rebin...done.\n");
    }
}
