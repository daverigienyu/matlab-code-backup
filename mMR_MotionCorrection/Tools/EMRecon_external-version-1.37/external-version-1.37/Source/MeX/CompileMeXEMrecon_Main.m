%% set cflags
cflags_windows = '-DWINDOWS_MATLAB COPTIMFLAGS=-O3 -DNDEBUG';
cflags_linux = 'CFLAGS=''$CFLAGS -w -fopenmp'' -lgomp COP/TIMFLAGS=-O3 -DNDEBUG';


%% set os_source and mex compiler options with regard to the os
if (strcmp(os,'linux') == 1)
    disp('compiling mexEMrecon for linux');
    cflags = cflags_linux;
else
    disp('compiling mexEMrecon for windows');
    cflags = cflags_windows;
end

% cd to source directory
cd(path_to_emrecon_mex_source);


%% compile mex functions
% matlab mex interface id
mex_id = 0;
matlab_id = 0;

% add main components
mex_cmd_main = ' -DMATLAB';
mex_cmd_main = [mex_cmd_main ' IO/C/mexEMrecon_Parameter.c'];
mex_cmd_main = [mex_cmd_main ' ../IO/Parameter.c'];
mex_cmd_main = [mex_cmd_main ' ../IO/IO.c'];
mex_cmd_main = [mex_cmd_main ' ../Main/Siddon.c'];
mex_cmd_main = [mex_cmd_main ' ../Main/Tools.c'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/Scanner.c'];

% add scanner definition
mex_cmd_main = [mex_cmd_main ' -DPET_SCANNER'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/PET_Scanner.c'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/PET_Michelogram.c'];
mex_cmd_main = [mex_cmd_main ' -DPET_2D_DEMO'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/Demo/PET_2D_Demo.c'];
if (add_scanner_PET_Siemens_Inveon == 1)
    mex_cmd_main = [mex_cmd_main ' -DPET_SIEMENS_INVEON'];
    mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/Siemens/PET_Siemens_Inveon.c'];
end
if (add_scanner_PET_Siemens_mMR == 1)
    mex_cmd_main = [mex_cmd_main ' -DPET_SIEMENS_MMR'];
    mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/Siemens/PET_Siemens_mMR.c'];
end
% add required plugins: Gating
mex_cmd_main = [mex_cmd_main ' ../Plugins/Gating/Gating.c'];

% enable / disable protection of scanner information
if (protect_scanner_info == 1)
    mex_cmd_main = [mex_cmd_main ' -DPROTECT_SCANNER_INFO'];
end

% setup outdir
mex_cmd_main = [mex_cmd_main ' -outdir ../../Bin'];


%% Compile ReconAlgorithms
run('CompileMeXEMrecon_ReconAlgorithms');


%% Compile Tools for different scanner types
run('CompileMeXEMrecon_ScannerTools');


%% Compile Plugins
run('CompileMeXEMrecon_Plugins');


%% list of (pure) Matlab tools
if (protect_MatlabEMreconTools == 1)
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/vfprintf';
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/writeMGF';
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/read_mMR_Normfile';
end


%% protect corresponding scripts
if (exist('../../Bin/Tools','dir') ~= 7)
    mkdir('../../Bin/Tools');
end

% protect MeX scripts
if (mex_id > 0)
    for i=1:numel(MexEMreconTools)
        fprintf('pcode %s.m\n',MexEMreconTools(i).name);
        pcode(sprintf('%s.m',MexEMreconTools(i).name));
    end
    
    % move MeX scripts
    dos('mv *.p ../../Bin');
end


%% required (pure) Matlab scripts
if (matlab_id > 0)
    for i=1:numel(MatlabEMreconTools)
        fprintf('pcode %s.m\n',MatlabEMreconTools(i).name);
        pcode(sprintf('%s.m',MatlabEMreconTools(i).name));
    end
    
    % move Matlab scripts
    dos('mv *.p ../../Bin/Tools');
end
