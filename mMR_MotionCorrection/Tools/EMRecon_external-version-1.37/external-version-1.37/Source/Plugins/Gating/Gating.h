#ifndef PLUGIN_GATING_H
#define PLUGIN_GATING_H


/* emrecon includes */
#include "../../IO/IO.h"


/* global variables */
int mgf_dataindex;
int *mgf_gatingsignal;
int mgf_starttime;
int mgf_stoptime;
int *mgf_timesignal;
int mgf_number_of_cardiac_gates;
int mgf_number_of_entries;
int mgf_number_of_respiratory_gates;


/* function declarations */
int gt_read_mgf(char *mgf_filename);
void gt_release_memory();


#endif
