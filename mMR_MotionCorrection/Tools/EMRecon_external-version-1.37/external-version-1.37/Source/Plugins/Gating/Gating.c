#include "Gating.h"


/* ############################################################################################# */
/* ### gt_read_mgf ############################################################################# */
/* ############################################################################################# */
/**
 * @brief gt_read_mgf
 * @param mgf_filename
 */
int gt_read_mgf(char *mgf_filename)
{
    int i,tag,gates;
    char tmp[255];
    char prop_name[255];
    int read_header = 1;
    int totalsize,pointersize;
    long filesize;

    FILE *mgffile;
    mgffile = fopen(mgf_filename,"rb");

    /* set gates to default value */
    gates = -1;

    /* dataindex always starts with 0 */
    mgf_dataindex = 0;

    if (mgffile == 0)
    {
        mgf_timesignal = safe_calloc(1,sizeof(int));
        mgf_timesignal[0] = -1;
        mgf_gatingsignal = safe_calloc(1,sizeof(int));
        mgf_gatingsignal[0] = -1;
        mgf_starttime = -1;
        mgf_stoptime = -1;
        mgf_number_of_cardiac_gates = -1;
        mgf_number_of_respiratory_gates = -1;
        mgf_number_of_entries = -1;
    }
    else
    {
        while (read_header == 1)
        {
            fgets(tmp,255,mgffile);
            if (strchr(tmp,'='))
            {
                totalsize = strlen(tmp);
                pointersize = strlen(strchr(tmp,'='));
                strncpy(prop_name,tmp,totalsize-pointersize);

                if (strncmp(prop_name,"MGF_Starttime",totalsize-pointersize) == 0)
                {
                    mgf_starttime = (int)atoi(strdup(strchr(tmp,'=')+1));
                    message(5,"mgf_starttime: %d\n",mgf_starttime);
                }

                if (strncmp(prop_name,"MGF_Stoptime",totalsize-pointersize) == 0)
                {
                    mgf_stoptime = (int)atoi(strdup(strchr(tmp,'=')+1));
                    message(5,"mgf_stoptime: %d\n",mgf_stoptime);
                }

                if (strncmp(prop_name,"MGF_Number_of_Entries",totalsize-pointersize) == 0)
                {
                    mgf_number_of_entries = (int)atoi(strdup(strchr(tmp,'=')+1));
                    message(5,"mgf_number_of_entries: %d\n",mgf_number_of_entries);
                }

                if (strncmp(prop_name,"MGF_Number_of_Cardiac_Gates",totalsize-pointersize) == 0)
                {
                    mgf_number_of_cardiac_gates = (int)atoi(strdup(strchr(tmp,'=')+1));
                    message(5,"mgf_number_of_cardiac_gates: %d\n",mgf_number_of_cardiac_gates);
                }

                if (strncmp(prop_name,"MGF_Number_of_Respiratory_Gates",totalsize-pointersize) == 0)
                {
                    mgf_number_of_respiratory_gates = (int)atoi(strdup(strchr(tmp,'=')+1));
                    message(5,"mgf_number_of_respiratory_gates: %d\n",mgf_number_of_respiratory_gates);
                }
            }
            if (strchr(tmp,';'))
            {
                read_header = 0;
            }
        }

        /* check for valid gates values */
        if (mgf_number_of_cardiac_gates >= 1 && mgf_number_of_respiratory_gates >= 1)
        {
            gates = mgf_number_of_cardiac_gates * mgf_number_of_respiratory_gates;
        }
        mgf_timesignal = safe_calloc(mgf_number_of_entries,sizeof(int));
        mgf_gatingsignal = safe_calloc(mgf_number_of_entries,sizeof(int));

        /* check for valid filesize */
        fseek(mgffile,0,SEEK_END);
        filesize = ftell(mgffile);
        if (filesize > mgf_number_of_entries*2*4)
        {
            fseek(mgffile,-mgf_number_of_entries*2*4,SEEK_END);
        }
        else
        {
            mgf_number_of_entries = -1;
        }

        for (i=0;i<mgf_number_of_entries;i++)
        {
            fread(&tag,sizeof(int),1,mgffile);
            /* validate each time tag */
            if (i > 0 && tag <= mgf_timesignal[i-1])
            {
                mgf_number_of_entries = -1;
            }
            mgf_timesignal[i] = tag;
        }

        for (i=0;i<mgf_number_of_entries;i++)
        {
            fread(&tag,sizeof(int),1,mgffile);
            mgf_gatingsignal[i] = tag;
            /* validate each gate tag */
            if (tag > gates)
            {
                mgf_number_of_entries = -1;
            }
        }

        fclose(mgffile);

        /* check for invalid mgf_starttime, mgf_stoptime
         * - mgf_number_of_entries=-1 skips gating function, i.e. no counts are used */
        if (mgf_starttime < 0)
        {
            mgf_number_of_entries = -1;
        }
        if (mgf_stoptime > 0 && mgf_stoptime < mgf_starttime)
        {
            mgf_number_of_entries = -1;
        }
        if (mgf_number_of_entries > 1 && mgf_starttime >= mgf_timesignal[0])
        {
            mgf_number_of_entries = -1;
        }
        if (mgf_number_of_entries > 1 && mgf_stoptime != mgf_timesignal[mgf_number_of_entries-1])
        {
            mgf_number_of_entries = -1;
        }

        /* in case of invalid mgf set gates to -1 */
        if (mgf_number_of_entries == -1)
        {
            gates = -1;
        }
    }

    return gates;
}


/* ############################################################################################# */
/* ### gt_release_memory ####################################################################### */
/* ############################################################################################# */
/**
 * @brief gt_release_memory
 */
void gt_release_memory()
{
    if (mgf_timesignal != 0)
    {
        free(mgf_timesignal);
    }
    if (mgf_gatingsignal != 0)
    {
        free(mgf_gatingsignal);
    }
}
