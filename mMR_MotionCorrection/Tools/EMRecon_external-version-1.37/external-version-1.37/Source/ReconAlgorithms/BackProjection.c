/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "BackProjection.h"


/* ############################################################################################# */
/* ### BackProjection ########################################################################## */
/* ############################################################################################# */
/**
 * @brief BackProjection
 * @param input_data
 * @param output_image
 */
void BackProjection(double *input_data, double *output_image)
{
    long i;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    /* assure that output image is zero */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1)
        {
            int j;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++)
            {
#pragma omp atomic
                output_image[Path[j].coord] += input_data[i]*Path[j].length;
            }
        }
    }
    /* event loop end */

    free(Eventvector);
    free(PathOMP);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(output_image,output_image);
    }

    cut_image(output_image);
}


/* ############################################################################################# */
/* ### BackProjectionAC ######################################################################## */
/* ############################################################################################# */
/**
 * @brief BackProjectionAC
 * @param input_data
 * @param input_image
 * @param output_image
 */
void BackProjectionAC(double *input_data, double *input_image, double *output_image)
{
    long i;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    /* assure that output image is zero */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1)
        {
            int j;
            double attenuation = 0.0;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++) {
                attenuation += input_image[Path[j].coord]*Path[j].length;
            }

            for (j=0;Path[j].coord!=-1;j++)
            {
#pragma omp atomic
                output_image[Path[j].coord] += input_data[i]*Path[j].length*exp(-attenuation/10.0);
            }
        }
    }
    /* event loop end */

    free(Eventvector);
    free(PathOMP);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(output_image,output_image);
    }

    cut_image(output_image);
}


/* ############################################################################################# */
/* ### BackProjectionMask ###################################################################### */
/* ############################################################################################# */
/**
 * @brief BackProjectionMask
 * @param input_data
 * @param input_mask
 * @param output_image
 */
void BackProjectionMask(double *input_data, double *input_mask, double *output_image)
{
    long i;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    /* assure that output image is zero */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1 && input_mask[i] > 0.0)
        {
            int j;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++)
            {
#pragma omp atomic
                output_image[Path[j].coord] += input_data[i]*input_mask[i]*Path[j].length;
            }
        }
    }
    /* event loop end */

    free(Eventvector);
    free(PathOMP);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(output_image,output_image);
    }

    cut_image(output_image);
}
