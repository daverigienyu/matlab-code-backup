%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 4;
% - setup volume where the crystals should be created
% - remember, this "FOV" has to be larger than the FOV used for
%   reconstruction, because the crystals are usually outside that FOV
% - it is reasonable to just use a single slice instead of a whole volume
%   to reduce required memory
% - in order to see the different crystals a pixel size of less than
%   0.1mm^2 is suggested

% create data for plot 1 & 2
parm.FOV_X = 200.0;
parm.FOV_Y = parm.FOV_X;
parm.FOV_Z = 150.0;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 0.0;
parm.SIZE_X = 500;
parm.SIZE_Y = parm.SIZE_X;
parm.SIZE_Z = 325;
crystal_geometry_04 = EMrecon_PET_DrawCrystalGeometry_ND(parm);

% create data for plot 3
parm.FOV_X = 200.0;
parm.FOV_Y = parm.FOV_X;
parm.FOV_Z = 1.0;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 0.0;
parm.SIZE_X = 8000;
parm.SIZE_Y = parm.SIZE_X;
parm.SIZE_Z = 1;
crystal_geometry_0025_transversal = EMrecon_PET_DrawCrystalGeometry_ND(parm);

% create data for plot 4
parm.FOV_X = 200.0;
parm.FOV_Y = 1.0;
parm.FOV_Z = 150.0;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 0.0;
parm.SIZE_X = 8000;
parm.SIZE_Y = 1;
parm.SIZE_Z = 6000;
crystal_geometry_0025_axial = EMrecon_PET_DrawCrystalGeometry_ND(parm);


%% display crystal geometrie
subplot(2,2,1);
imagesc(sum(crystal_geometry_04,3),[0 0.01]); axis image; colormap bone;

subplot(2,2,2);
imagesc(squeeze(crystal_geometry_04(:,250,:))',[0 0.01]); axis image; colormap bone;

subplot(2,2,3);
imagesc(crystal_geometry_0025_transversal(451:900,3151:3600),[0 0.01]); axis image; colormap bone;

subplot(2,2,4);
imagesc(squeeze(crystal_geometry_0025_axial(451:900,3151:3400))',[0 0.01]); axis image; colormap bone;
