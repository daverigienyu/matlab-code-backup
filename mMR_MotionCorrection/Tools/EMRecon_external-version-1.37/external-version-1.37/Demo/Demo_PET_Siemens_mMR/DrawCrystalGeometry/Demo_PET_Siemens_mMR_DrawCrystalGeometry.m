%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 8;
% - setup volume where the crystals should be created
% - remember, this "FOV" has to be larger than the FOV used for
%   reconstruction, because the crystals are usually outside that FOV
% - it is reasonable to just use a single slice instead of a whole volume
%   to reduce required memory
% - in order to see the different crystals a pixel size of around
%   less than 0.1mm^2 is suggested
parm.FOV_X = 80.0;
parm.FOV_Y = 50.0;
parm.FOV_Z = 100.0;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = 313.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 0.0;
parm.SIZE_X = 800;
parm.SIZE_Y = 500;
parm.SIZE_Z = 1;
parm.USE_VIRTUAL_CRYSTALS = 0;


%% draw different crystal setups
parm.CRYSTALS_TO_DRAW = 0;
real_crystal_geometry = EMrecon_PET_DrawCrystalGeometry_ND(parm);

parm.CRYSTALS_TO_DRAW = 1;
virtual_crystal_geometry = EMrecon_PET_DrawCrystalGeometry_ND(parm);

parm.CRYSTALS_TO_DRAW = 2;
all_crystal_geometry = EMrecon_PET_DrawCrystalGeometry_ND(parm);


%% plot results
subplot(1,3,1);
imagesc(real_crystal_geometry', [0 0.001]); title('real crystals only'); axis image; axis off; colormap bone; zoom(2.0);

subplot(1,3,2);
imagesc(virtual_crystal_geometry', [0 0.001]); title('virtual crystals only'); axis image; axis off; colormap bone; zoom(2.0);

subplot(1,3,3);
imagesc(all_crystal_geometry', [0 0.001]); title('all crystals'); axis image; axis off; colormap bone; zoom(2.0);
