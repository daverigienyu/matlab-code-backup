%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 8;
% setup listmode filename
parm.NORMFILENAME = 'D:\mmr\norm.bf'; % <----- modify this parameter !!!


% check whether data.mat from rebin example and parm.NORMFILENAME exist
if (exist('../Rebin/data.mat','file') == 2 && exist(parm.NORMFILENAME,'file') == 2)
    
    %% read all components of the norm data
    normalization = read_mMR_Normfile(parm.NORMFILENAME);
    
    %% read prompt and delayed from data.mat
    load('../Rebin/data.mat');
    
    %% process prompt data
    prompt = EMrecon_PET_Michelogram_AxialWeight(parm,prompt);
    prompt = EMrecon_PET_Michelogram_Normalize(parm,prompt,normalization);
    prompt = EMrecon_PET_Michelogram_FillGaps(parm,prompt);
    
    %% process delayed data
    delayed = EMrecon_PET_Michelogram_AxialWeight(parm,delayed);
    delayed = EMrecon_PET_Michelogram_Normalize(parm,delayed,normalization);
    delayed = EMrecon_PET_Michelogram_FillGaps(parm,delayed);
    
    %% save data to disk
    % data can be reconstructed used the EM demo
    save('data.mat','prompt','delayed');
    
    %% show results
    % reshape data and use only direct sinograms for display
    reshaped_prompt = reshape(prompt,344,252,837);
    reshaped_delayed = reshape(delayed,344,252,837);
    
    subplot(2,3,1);
    imagesc(squeeze(sum(reshaped_prompt,3))'); axis xy; axis image; title('prompt (r,phi)');
    
    subplot(2,3,2);
    imagesc(squeeze(sum(reshaped_prompt,2))'); axis xy; axis image; title('prompt (r,s)');
    
    subplot(2,3,3);
    imagesc(squeeze(sum(reshaped_prompt,1))); axis xy; axis image; title('prompt (phi,s)');
    
    subplot(2,3,4);
    imagesc(squeeze(sum(reshaped_delayed,3))'); axis xy; axis image; title('delayed (r,phi)');
    
    subplot(2,3,5);
    imagesc(squeeze(sum(reshaped_delayed,2))'); axis xy; axis image; title('delayed (r,s)');
    
    subplot(2,3,6);
    imagesc(squeeze(sum(reshaped_delayed,1))); axis xy; axis image; title('delayed (phi,s)');
    
else
    if (exist('../Rebin/data.mat','file') ~= 2)
        fprintf('../Rebin/data.mat not found. please run rebin demo first\n');
    end
    if (exist(parm.NORMFILENAME,'file') ~= 2)
        fprintf('%s not found. please adjust file name.\n',parm.NORMFILENAME);
    end
end
