%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 8;
% setup listmode filename
parm.LISTMODEFILENAME = 'D:\mmr\listmode.bf'; % <----- modify this parameter !!!
parm.MGFFILENAME = 'D:\mmr\test.mgf';         % <----- modify this parameter !!!
% setup rebin parameter
parm.MAXTAGS = -1; % <----- set to -1 for whole file; any value > 0 limits the rebinning to that amount of tags


%% create MGF file select only part of the listmode data
% setup start and stop time (in ms) for PET listmode
starttime = 0;
stoptime = -1;
% the values below just need to be initialized
timesignal = 1;
gatingsignal = 1;
cardiac_gates = 1;
respiratory_gates = 1;
comments = 'MGF example for frame based sorting of PET listmode';
writeMGF(parm.MGFFILENAME,timesignal,gatingsignal,starttime,stoptime,cardiac_gates,respiratory_gates,comments);


%% prepare data
% EMrecon_PET_Siemens_Inveon_Rebin
[prompt,delayed] = EMrecon_PET_Siemens_mMR_Rebin(parm);


%% save data to disk
% data can be further processed using the Normalization demo
save('data.mat','prompt','delayed');


%% show results
% reshape data and use only direct sinograms for display
reshaped_prompt = reshape(prompt,344,252,837);
reshaped_delayed = reshape(delayed,344,252,837);

subplot(2,3,1);
imagesc(squeeze(sum(reshaped_prompt,3))'); axis xy; axis image; title('prompt (r,phi)');

subplot(2,3,2);
imagesc(squeeze(sum(reshaped_prompt,2))'); axis xy; axis image; title('prompt (r,s)');

subplot(2,3,3);
imagesc(squeeze(sum(reshaped_prompt,1))); axis xy; axis image; title('prompt (phi,s)');

subplot(2,3,4);
imagesc(squeeze(sum(reshaped_delayed,3))'); axis xy; axis image; title('delayed (r,phi)');

subplot(2,3,5);
imagesc(squeeze(sum(reshaped_delayed,2))'); axis xy; axis image; title('delayed (r,s)');

subplot(2,3,6);
imagesc(squeeze(sum(reshaped_delayed,1))); axis xy; axis image; title('delayed (phi,s)');
