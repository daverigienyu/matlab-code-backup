function J = estimateJacobian(U,V,W)

    % A  = [ a b c 
    %        d e f
    %        g h i ]
    
    %|A| = a(ei - fh) - b(di - fg) + c(dh - eg)
    
    
    [Ux, Uy, Uz] = gradient(U);
    [Vx, Vy, Vz] = gradient(V);
    [Wx, Wy, Wz] = gradient(W);
    
    J = Ux.*(Vy.*Wz - Vz.*Wy) - ...
        Uy.*(Vx.*Wz - Vz.*Wx) + ...
        Uz.*(Vx.*Wy - Vy.*Wx);
    
    J = abs(J);
    
    

end