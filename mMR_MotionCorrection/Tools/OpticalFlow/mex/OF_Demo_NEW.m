%close all;
clear mex; clear; clc;

cd('G:\OpticalFlow\trunk\mex');
addpath('c++');
addpath('matlab');
addpath('G:\OpticalFlow\trunk\C\MoCo\Bin');

size_x = 50;
size_y = 50;
size_z = 1;

gate1 = zeros(size_x,size_y,size_z);
gate1(21:30,21:30,:) = 100;
gate2 = zeros(size_x,size_y,size_z);
gate2(31:40,21:30,:) = 100;

parm.SIZE_X = size(gate1,1);
parm.SIZE_Y = size(gate1,2);
parm.SIZE_Z = size(gate1,3);
parm.D_X = 1.0;
parm.D_Y = 1.0;
parm.D_Z = 1.0;
parm.FOV_X = parm.SIZE_X * parm.D_X;
parm.FOV_Y = parm.SIZE_Y * parm.D_Y;
parm.FOV_Z = parm.SIZE_Z * parm.D_Z;

parm.OF_ALGO = 4;
parm.LEVEL = 10;
parm.ITERATIONS = 30;
parm.OUTER_ITERATIONS = 1;
parm.ALPHA = 40.0;
parm.FWHM = 0.0;
parm.BETA1 = 2.0; % 2.0
parm.BETA2 = 1.0;
parm.SMOOTH_BEFORE_DOWNSAMPLING = 0;%1
parm.SMOOTH_AFTER_UPSAMPLING = 0;
parm.GRADIENT_THRESHOLD = 0.0;
parm.LEVEL_RATIO = 0.8;

parm.I_ITERATIONS = parm.ITERATIONS;
parm.O_ITERATIONS = parm.OUTER_ITERATIONS;

Ia = gate1;
Ib = gate2;
% scale data
scale = 100.0;
Ia = Ia - min(Ia(:));
Ia = Ia ./ max(Ia(:)) * scale;
Ib = Ib - min(Ib(:));
Ib = Ib ./ max(Ib(:)) * scale;

% calculate motion and apply correction
if (size_z == 1)
    old_tic = tic;
    [u1,v1] = OF_OpticalFlowMultiLevel_2D(parm,Ia,Ib);
    old_time = toc(old_tic);
    result1 = OF_ApplyMotionCorrection_2D(parm,Ib,u1,v1);
else
    old_tic = tic;
    [u1,v1,w1] = OF_OpticalFlowMultiLevel_3D(parm,Ia,Ib);
    old_time = toc(old_tic);
    result1 = OF_ApplyMotionCorrection_3D(parm,Ib,u1,v1,w1);
end

if (size_z == 1)
    new_tic = tic;
    [u2,v2] = OF_CalculateFlow_NEW(parm,Ia,Ib);
    new_time = toc(new_tic);
    result2 = OF_ApplyMotionCorrection_NEW(parm,Ib,u2,v2);
else
    new_tic = tic;
    [u2,v2,w2] = OF_CalculateFlow_NEW(parm,Ia,Ib);
    new_time = toc(new_tic);
    result2 = OF_ApplyMotionCorrection_NEW(parm,Ib,u2,v2,w2);
end

clc;
fprintf('time: %f\n',old_time);
fprintf('u1: %f %f\n',min(u1(:)),max(u1(:)));
fprintf('v1: %f %f\n',min(v1(:)),max(v1(:)));
if (size_z > 1)
    fprintf('w1: %f %f\n',min(w1(:)),max(w1(:)));
end
fprintf('before: %f\n',corr(Ia(:),Ib(:)));
fprintf('after: %f\n',corr(Ia(:),result1(:)));

fprintf('\ntime: %f\n',new_time);
fprintf('u2: %f %f\n',min(u2(:)),max(u2(:)));
fprintf('v2: %f %f\n',min(v2(:)),max(v2(:)));
if (size_z > 1)
    fprintf('w2: %f %f\n',min(w2(:)),max(w2(:)));
end
fprintf('before: %f\n',corr(Ia(:),Ib(:)));
fprintf('after: %f\n',corr(Ia(:),result2(:)));

diff_u = u1(:)-u2(:);
fprintf('\ndifference u: %f %f\n',min(diff_u(:)),max(diff_u(:)));
diff_v = v1(:)-v2(:);
fprintf('difference v: %f %f\n',min(diff_v(:)),max(diff_v(:)));
if (size_z > 1)
    diff_w = w1(:)-w2(:);
    fprintf('difference w: %f %f\n',min(diff_w(:)),max(diff_w(:)));
end


%
subplot(2,4,1); imagesc(sum(Ia,3))
subplot(2,4,2); imagesc(sum(result1,3))
subplot(2,4,3); imagesc(sum(result2,3))
subplot(2,4,4); imagesc(sum(Ib,3))
%
% subplot(2,4,5); imagesc(sum(gate1,3)-sum(result1,3))
% subplot(2,4,6); imagesc(sum(gate1,3)-sum(result2,3))
% subplot(2,4,7); imagesc(sum(result2,3)-sum(result1,3))
%
% % subplot(2,3,1); imagesc(Ia,[0 scale]); axis xy;
% % subplot(2,3,2); imagesc(result1,[0 scale]); axis xy;
% % subplot(2,3,3); imagesc(Ib,[0 scale]); axis xy;
% %
% % subplot(2,3,4); quiver(u,v);  axis xy;
% % subplot(2,3,5); imagesc(u); axis xy;
% % subplot(2,3,6); imagesc(v); axis xy;
%
% %save('demo_results_2D.mat','Ia','Ib','u','v');
% %save('Demo_2D_data_invers.mat','Ia','Ib','u','v');
