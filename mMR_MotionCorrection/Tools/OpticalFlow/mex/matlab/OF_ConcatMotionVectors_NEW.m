function [uc,vc,wc] = OF_ConcatMotionVectors_NEW(parm,vec1,vec2,vec3,vec4,vec5,vec6)

if (~(nargin == 5 && nargout == 2) && ~(nargin == 7 && nargout == 3))
    fprintf('\n[uc,vc,[wc]] = OF_ConcatMotionVectors_NEW(parm,vec1,vec2,vec3,vec4,[vec5,vec6])\n');
else
    fprintf('OF_ConcatMotionVectors_NEW...\n');
    tic;
    
    if (nargin == 5 && nargout == 2)
        [uc,vc] = mex_OF_ConcatMotionVectors(parm,vec1(:),vec2(:),vec3(:),vec4(:));
        
        % reshape image to old size
        uc = reshape(uc,size(vec1));
        vc = reshape(vc,size(vec1));
    end
    
    if (nargin == 7 && nargout == 3)
        [uc,vc,wc] = mex_OF_ConcatMotionVectors(parm,vec1(:),vec2(:),vec3(:),vec4(:),vec5(:),vec6(:));
        
        % reshape image to old size
        uc = reshape(uc,size(vec1));
        vc = reshape(vc,size(vec1));
        wc = reshape(wc,size(vec1));
    end
    
    fprintf('OF_ConcatMotionVectors_NEW...done. [%f sec]\n',toc);
end
