#include "mex_parse_image.h"
#include "mex_parse_parameter.h"


using namespace std;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if ((nlhs == 2 && nrhs == 5) || (nlhs == 3 && nrhs == 7))
    {
        /* parse input */
        parameter_list ParameterList = mex_parse_parameter(prhs,0);
        
        /* concat motion vectors */
        if (nlhs == 2)
        {
            image u1 = mex_parse_image(ParameterList,prhs,1);
            image v1 = mex_parse_image(ParameterList,prhs,2);
            image u2 = mex_parse_image(ParameterList,prhs,3);
            image v2 = mex_parse_image(ParameterList,prhs,4);
            
            u1.concat_motion_fields_x(u2,v2);
            v1.concat_motion_fields_y(u2,v2);
            
            /* create output */
            mex_copy_image(u1,plhs,0);
            mex_copy_image(v1,plhs,1);
        }
        else
        {
            image u1 = mex_parse_image(ParameterList,prhs,1);
            image v1 = mex_parse_image(ParameterList,prhs,2);
            image w1 = mex_parse_image(ParameterList,prhs,3);
            image u2 = mex_parse_image(ParameterList,prhs,4);
            image v2 = mex_parse_image(ParameterList,prhs,5);
            image w2 = mex_parse_image(ParameterList,prhs,5);
            
            u1.concat_motion_fields_x(u2,v2,w2);
            v1.concat_motion_fields_y(u2,v2,w2);
            w1.concat_motion_fields_z(u2,v2,w2);
            
            /* create output */
            mex_copy_image(u1,plhs,0);
            mex_copy_image(v1,plhs,1);
            mex_copy_image(w1,plhs,2);
        }
    }
    else
    {
        mexPrintf("usage:\n2D: [uc,vc] = mex_OF_ConcatMotionVectors(parm,u1,v1,u2,v2);\n");
        mexPrintf("3D: [uc,vc,wc] = mex_OF_ConcatMotionVectors(parm,u1,v1,w1,u2,v2,w2);\n");
    }
}
