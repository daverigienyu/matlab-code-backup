#include "mex_parse_parameter.h"


parameter_list mex_parse_parameter(const mxArray *prhs[])
{
    parameter_list ParameterList = mex_parse_parameter(prhs,0);
    
    return ParameterList;
}


parameter_list mex_parse_parameter(const mxArray *prhs[], int prhs_id)
{
    parameter_list ParameterList;
    
    // check whether input on rhs is a struct
    if(!mxIsStruct(prhs[prhs_id]))
    {
        mexErrMsgIdAndTxt("MATLAB:ReadFromMatlab:inputNotStruct","Input must be a structure.");
    }
    
    // loop over all fields of the parameter struct
    for (int ifield=0;ifield<mxGetNumberOfFields(prhs[prhs_id]);ifield++)
    {
        // get current field type
        mxArray *mxParam = mxGetFieldByNumber(prhs[prhs_id],0,ifield);
        
        // check whether parameter is char
        if (mxIsChar(mxParam) == 1)
        {
            // debug: printf field name
            // mexPrintf("parm [char] [%s]\n",mxGetFieldNameByNumber(prhs[prhs_id],ifield));
        }
        else
        {
            // if field is not char, assume double (array)
            double *tmpDouble;
            const mwSize *parm_size = mxGetDimensions(mxParam);
            mwSize parm_dimensions = mxGetNumberOfDimensions(mxParam);
            
            // check whether parameter contains a single values
            if (parm_dimensions == 2 && parm_size[0] == 1 && parm_size[1] == 1)
            {
                tmpDouble = mxGetPr(mxParam);
                ParameterList.add_parameter_double(mxGetFieldNameByNumber(prhs[prhs_id],ifield),tmpDouble[0]);
                
                // debug: printf field name
                // mexPrintf("parm [double] [%s] [%f]\n",mxGetFieldNameByNumber(prhs[prhs_id],ifield),tmpDouble[0]);
            }
            else
            {
                // check whether parameter contains a vector of values
                if (parm_dimensions == 2 && parm_size[0] == 1 && parm_size[1] > 1)
                {
                    tmpDouble = mxGetPr(mxParam);
                    vector<double> tmpDoubleVector;
                    for (int x=0;x<parm_size[1];x++)
                    {
                        tmpDoubleVector.push_back(tmpDouble[x]);
                    }
                    ParameterList.add_parameter_double_vector(mxGetFieldNameByNumber(prhs[prhs_id],ifield),tmpDoubleVector);
                    
                    // debug: printf field name
                    // mexPrintf("parm [vector<double>] [%s]\n",mxGetFieldNameByNumber(prhs[prhs_id],ifield));
                }
            }
        }
    }
    
    return ParameterList;
}
