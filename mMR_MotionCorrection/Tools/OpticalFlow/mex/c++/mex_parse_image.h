#include "mex.h"
#include "../../io/parameter_list.h"
#include "../../tools/image.h"


void mex_copy_image(image, mxArray*[]);
void mex_copy_image(image, mxArray*[], int);
void mex_copy_image_dim(image, mxArray*[], int);
image mex_parse_image(parameter_list, const mxArray *[], int);


using namespace std;
