#include "mex_parse_image.h"


void mex_copy_image_dim(image Image, mxArray *plhs[], int plhs_id)
{
    plhs[plhs_id] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
    double *output_dim = mxGetPr(plhs[plhs_id]);
    
    output_dim[0] = double(Image.get_size_x());
    output_dim[1] = double(Image.get_size_y());
    output_dim[2] = double(Image.get_size_z());
}


void mex_copy_image(image Image, mxArray *plhs[], int plhs_id)
{
    plhs[plhs_id] = mxCreateNumericMatrix(Image.get_imagesize(),1,mxDOUBLE_CLASS,mxREAL);
    double *output_image = mxGetPr(plhs[plhs_id]);
    
    for (int i=0;i<Image.get_imagesize();i++)
    {
        output_image[i] = Image.get_value(i);
    }
}


void mex_copy_image(image Image, mxArray *plhs[])
{
    mex_copy_image(Image,plhs,0);
}


image mex_parse_image(parameter_list ParameterList, const mxArray *prhs[], int prhs_id)
{
    /* verify that at least size_x and size_y are defined in ParameterList */
    int size_x = ParameterList.get_parameter_int("SIZE_X",-1);
    int size_y = ParameterList.get_parameter_int("SIZE_Y",-1);
    int size_z = ParameterList.get_parameter_int("SIZE_Z",1);
    
    /* actual number of events in file */
    const mwSize *image_elements = mxGetDimensions(prhs[prhs_id]);
    
    /* exit function in case of invalid size */
    if (image_elements[0] != size_x*size_y*size_z)
    {
        mexPrintf("%d:%d %d %d",image_elements[0],size_x,size_y,size_z);
        mexErrMsgIdAndTxt("MATLAB:mex_parse_image:inputinvalid","invalid image size");
    }
    
    /* create new image */
    image Image(ParameterList);
    
    /* get pointer to input image */
    double *input_image = (double*)mxGetData(prhs[prhs_id]);
    
    /* assign content */
    for (int i=0;i<Image.get_imagesize();i++)
    {
        Image.set_value(i,input_image[i]);
    }
    
    return Image;
}
