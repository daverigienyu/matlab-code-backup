function [u,v,w] = OF_CalculateFlow_NEW(parm,Ia,Ib)

u = 0;
v = 0;
w = 0;

if (~(nargin == 3) || ~(nargout == 2 || nargout == 3))
    fprintf('\n[u,v,[w]] = OF_CalculateFlow_NEW(parm,Ia,Ib)\n');
else
    fprintf('OF_CalculateFlow_NEW...\n');
    tic;
    
    if (nargin == 3 && nargout == 2)
        [u,v] = mex_OF_CalculateFlow(parm,Ia(:),Ib(:));
        
        size(Ia)
        size(u)
        
        % reshape image to old size
        u = reshape(u,size(Ia));
        v = reshape(v,size(Ia));
    end
    
    if (nargin == 3 && nargout == 3)
        [u,v,w] = mex_OF_CalculateFlow(parm,Ia(:),Ib(:));
        
        % reshape image to old size
        u = reshape(u,size(Ia));
        v = reshape(v,size(Ia));
        w = reshape(w,size(Ia));
    end
    
    fprintf('OF_CalculateFlow_NEW...done. [%f sec]\n',toc);
end
end