#include "optical_flow_algo.h"


/**
 * @brief optical_flow_algo::optical_flow_algo
 */
optical_flow_algo::optical_flow_algo()
{
}


/**
 * @brief optical_flow_algo::optical_flow_algo
 */
optical_flow_algo::optical_flow_algo(parameter_list, image, image)
{
}


/**
 * @brief optical_flow_algo::initialize
 * @param Ia
 * @param Ib
 */
void optical_flow_algo::initialize(image Ia, image Ib)
{
    //cout << "optical_flow_algo::initialize" << endl;

    /* initialize reference and floating image */
    set_Ia(Ia);
    set_Ib(Ib);

    /* initialize derivatives */
    Ix.initialize(Ia);
    Iy.initialize(Ia);
    It.initialize(Ia);

    /* initialize vectors */
    u.initialize(Ia);
    v.initialize(Ia);

    /* initialize average images */
    u_Avg.initialize(Ia);
    v_Avg.initialize(Ia);

    if (Ia.get_size_z() > 1)
    {
        Iz.initialize(Ia);
        w.initialize(Ia);
        w_Avg.initialize(Ia);
    }

    //cout << "optical_flow_algo::initialize END" << endl;
}


/**
 * @brief calculate_weighted_averages
 */
void optical_flow_algo::calculate_weighted_averages()
{
    //cout << "optical_flow_algo::calculate_weighted_averages" << endl;

    double d_x = Ia.get_d_x();
    double d_y = Ia.get_d_y();
    double d_z = Ia.get_d_z();
    int size_x = Ia.get_size_x();
    int size_y = Ia.get_size_y();
    int size_z = Ia.get_size_z();

    /* weighted averages in 2D */
    if (size_z == 1)
    {
        u_Avg.set_zero();
        v_Avg.set_zero();

#pragma omp parallel for
        for (int y=1;y<size_y-1;y++)
        {
            double value;
            int i, i1, i2, i3, i4;

            for (int x=1;x<size_x-1;x++)
            {
                i = x + y * size_x;

                i1 = (x-1) + y * size_x;
                i2 = (x+1) + y * size_x;
                i3 = x + (y-1) * size_x;
                i4 = x + (y+1) * size_x;

                value =  (u.get_value(i1) + u.get_value(i2)) / d_x;
                value += (u.get_value(i3) + u.get_value(i4)) / d_y;
                u_Avg.set_value(i,value);

                value =  (v.get_value(i1) + v.get_value(i2)) / d_x;
                value += (v.get_value(i3) + v.get_value(i4)) / d_y;
                v_Avg.set_value(i,value);
            }
        }
    }

    /* weighted averages in 3D */
    if (size_z > 1)
    {
        u_Avg.set_zero();
        v_Avg.set_zero();
        w_Avg.set_zero();

#pragma omp parallel for
        for (int z=1;z<size_z-1;z++)
        {
            for (int y=1;y<size_y-1;y++)
            {
                double value;
                int i, i1, i2, i3, i4, i5 ,i6;

                for (int x=1;x<size_x-1;x++)
                {
                    i = x + y * size_x + z * size_x * size_y;

                    i1 = (x-1) + y * size_x + z * size_x * size_y;
                    i2 = (x+1) + y * size_x + z * size_x * size_y;
                    i3 = x + (y-1) * size_x + z * size_x * size_y;
                    i4 = x + (y+1) * size_x + z * size_x * size_y;
                    i5 = x + y * size_x + (z-1) * size_x * size_y;
                    i6 = x + y * size_x + (z+1) * size_x * size_y;

                    value =  (u.get_value(i1) + u.get_value(i2)) / d_x;
                    value += (u.get_value(i3) + u.get_value(i4)) / d_y;
                    value += (u.get_value(i5) + u.get_value(i6)) / d_z;
                    u_Avg.set_value(i,value);

                    value =  (v.get_value(i1) + v.get_value(i2)) / d_x;
                    value += (v.get_value(i3) + v.get_value(i4)) / d_y;
                    value += (v.get_value(i5) + v.get_value(i6)) / d_z;
                    v_Avg.set_value(i,value);

                    value =  (w.get_value(i1) + w.get_value(i2)) / d_x;
                    value += (w.get_value(i3) + w.get_value(i4)) / d_y;
                    value += (w.get_value(i5) + w.get_value(i6)) / d_z;
                    w_Avg.set_value(i,value);
                }
            }
        }
    }

    //cout << "optical_flow_algo::calculate_weighted_averages END" << endl;
}


/**
 * @brief optical_flow_algo::calculate_partial_derivatives
 */
void optical_flow_algo::calculate_partial_derivatives()
{
    //cout << "optical_flow_algo::calculate_partial_derivatives" << endl;

    double d_x = Ia.get_d_x();
    double d_y = Ia.get_d_y();
    double d_z = Ia.get_d_z();
    int size_x = Ia.get_size_x();
    int size_y = Ia.get_size_y();
    int size_z = Ia.get_size_z();

    /* temporal derivative */
    for (int i=0;i<size_x*size_y*size_z;i++)
    {
        It.set_value(i,Ib.get_value(i)-Ia.get_value(i));
    }

    /* spatial derivative 2D */
    if (size_z == 1)
    {
#pragma omp parallel for
        for (int y=1;y<size_y-1;y++)
        {
            double value;
            int i, i1, i2;

            for (int x=1;x<size_x-1;x++)
            {
                i = x + y * size_x;

                i1 = (x-1) + y * size_x;
                i2 = (x+1) + y * size_x;
                value = (Ia.get_value(i2) - Ia.get_value(i1)) / (2.0 * d_x);
                Ix.set_value(i,value);

                i1 = x + (y-1) * size_x;
                i2 = x + (y+1) * size_x;
                value = (Ia.get_value(i2) - Ia.get_value(i1)) / (2.0 * d_y);
                Iy.set_value(i,value);
            }
        }
    }

    /* spatial derivative 3D */
    if (size_z > 1)
    {
#pragma omp parallel for
        for (int z=1;z<size_z-1;z++)
        {
            double value;
            int i, i1, i2;

            for (int y=1;y<size_y-1;y++)
            {
                for (int x=1;x<size_x-1;x++)
                {
                    i = x + y * size_x + z * size_x * size_y;

                    i1 = (x-1) + y * size_x + z * size_x * size_y;
                    i2 = (x+1) + y * size_x + z * size_x * size_y;
                    value = (Ia.get_value(i2) - Ia.get_value(i1)) / (2.0 * d_x);
                    Ix.set_value(i,value);

                    i1 = x + (y-1) * size_x + z * size_x * size_y;
                    i2 = x + (y+1) * size_x + z * size_x * size_y;
                    value = (Ia.get_value(i2) - Ia.get_value(i1)) / (2.0 * d_y);
                    Iy.set_value(i,value);

                    i1 = x + y * size_x + (z-1) * size_x * size_y;
                    i2 = x + y * size_x + (z+1) * size_x * size_y;
                    value = (Ia.get_value(i2) - Ia.get_value(i1)) / (2.0 * d_z);
                    Iz.set_value(i,value);
                }
            }
        }
    }

    //cout << "optical_flow_algo::calculate_partial_derivatives END" << endl;
}
