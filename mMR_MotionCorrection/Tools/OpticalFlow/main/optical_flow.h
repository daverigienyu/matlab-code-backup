#ifndef OPTICAL_FLOW_H
#define OPTICAL_FLOW_H


#include "optical_flow_factory.h"


using namespace std;


class optical_flow
{
private:
    optical_flow_factory Optical_Flow_Factory;
    optical_flow_algo *OpticalFlow;
    image u, v, w;

public:   
    optical_flow();
    optical_flow(parameter_list);

    image get_u() {return this->u;}
    image get_v() {return this->v;}
    image get_w() {return this->w;}

    void calculate_flow(parameter_list, image, image);
};


#endif // OPTICAL_FLOW_H
