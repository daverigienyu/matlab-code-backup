#ifndef OPTICAL_FLOW_ALGO_H
#define OPTICAL_FLOW_ALGO_H


#include "../tools/image.h"


using namespace std;


class optical_flow_algo
{
private:
    int level;

protected:
    image Ia,Ib,Ix,Iy,Iz,It;
    image u,v,w,u_Avg,v_Avg,w_Avg;

public:   
    optical_flow_algo();
    optical_flow_algo(parameter_list, image, image);

    void initialize(image, image);

    void set_Ia(image Ia) {this->Ia = Ia;}
    void set_Ib(image Ib) {this->Ib = Ib;}
    void set_Ix(image Ix) {this->Ix = Ix;}
    void set_Iy(image Iy) {this->Iy = Iy;}
    void set_Iz(image Iz) {this->Iz = Iz;}
    void set_It(image It) {this->It = It;}

    //    image get_u() {return u;}
    //    image get_v() {return v;}
    //    image get_w() {return w;}

    //    image get_u_Avg() {return u_Avg;}
    //    image get_v_Avg() {return v_Avg;}
    //    image get_w_Avg() {return w_Avg;}

    //    image get_Ia() {return Ia;}
    //    image get_Ib() {return Ib;}
    //    image get_Ix() {return Ix;}
    //    image get_Iy() {return Iy;}
    //    image get_Iz() {return Iz;}
    //    image get_It() {return It;}

    void calculate_partial_derivatives();
    void calculate_weighted_averages();

    int get_level() {return this->level;}

    image get_u() {return this->u;}
    image get_v() {return this->v;}
    image get_w() {return this->w;}

    virtual void setup(parameter_list) = 0;
    virtual void calculate_flow() = 0;
};


#endif // OPTICAL_FLOW_ALGO_H
