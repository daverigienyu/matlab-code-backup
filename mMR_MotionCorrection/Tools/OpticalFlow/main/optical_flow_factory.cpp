#include "optical_flow_factory.h"


/**
 * @brief optical_flow_factory::optical_flow_factory
 */
optical_flow_factory::optical_flow_factory()
{
}


/**
 * @brief optical_flow_factory::create_algorithm
 * @return
 */
optical_flow_algo* optical_flow_factory::create_algorithm(parameter_list ParameterList)
{
    switch (ParameterList.get_parameter_int("OF_ALGO",4)) // default is assigned here
    {
    case 4:
        return &OPTICAL_FLOW_ALGO_BNQ;
        break;
    }
}
