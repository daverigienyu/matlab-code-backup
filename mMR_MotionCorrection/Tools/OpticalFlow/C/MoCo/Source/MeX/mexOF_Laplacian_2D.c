/* global includes */
#include "mex.h"
#include "string.h"
#ifndef WINDOWS_MATLAB
#include <omp.h>
#endif


/* OF includes */
#include "mexOF_Version.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/MeX/Parameter_MEX.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int valid = 1;
    const mwSize *image_size;
    double *output_dimensions;
    double *u,*v,*u_Laplacian,*v_Laplacian;
    
    /* check for valid input/output parameter */
    if (nlhs != 3)
    {
        mexPrintf("mexOF_Laplacian_2D [%s]\n",OF_VERSION);
        mexPrintf("using EMrecon [%s]\n",EMRECON_VERSION);
    }
    else
    {
        /* initialize default parameter */
        SetParameter("SIZE_X","0");
        SetParameter("SIZE_Y","0");
        SetParameter("FOV_X","0.0");
        SetParameter("FOV_Y","0.0");
        
        
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);
        
        
        /* force values for Z to be reasonable */
        SetParameter("SIZE_Z","1");
        SetParameter("FOV_Z","1.0");
        
        
        /* update current parameter list */
        UpdateParameterList();
        
        
        /* validate x dimension */
        if (Parameter.size_x <= 0 || Parameter.fov_x <= 0)
        {
            mexPrintf("mexOF_Laplacian_2D: x Dimension error, size: [%d], fov: [%f]\n",Parameter.size_x,Parameter.fov_x);
            valid = 0;
        }
        
        /* validate y dimension */
        if (Parameter.size_y <= 0 || Parameter.fov_y <= 0)
        {
            mexPrintf("mexOF_Laplacian_2D: y Dimension error, size: [%d], fov: [%f]\n",Parameter.size_y,Parameter.fov_y);
            valid = 0;
        }
        
        /* validate image size 1 */
        image_size = mxGetDimensions(prhs[1]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_Laplacian_2D: Dimensions do not match [%d] != [%d] (u)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* validate image size 2 */
        image_size = mxGetDimensions(prhs[2]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_Laplacian_2D: Dimensions do not match [%d] != [%d] (v)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* if input data is invalid */
        if (valid == 0)
        {
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            u_Laplacian = mxGetPr(plhs[0]);
            u_Laplacian[0] = -1.0;
            
            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            v_Laplacian = mxGetPr(plhs[1]);
            v_Laplacian[0] = -1.0;
            
            plhs[2] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[2]);
            output_dimensions[0] = -1.0;
        }
        else
        {
            mexPrintf("mexOF_Laplacian_2D...\n");
            
            /* get pointer to input data */
            u = mxGetData(prhs[1]);
            v = mxGetData(prhs[2]);
            
            /* output */
            plhs[0] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            u_Laplacian = mxGetPr(plhs[0]);
            
            plhs[1] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            v_Laplacian = mxGetPr(plhs[1]);
            
            /* dimensions of the output (might be used to reshape the 1d output vector) */
            plhs[2] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[2]);
            output_dimensions[0] = (double)Parameter.size_x;
            output_dimensions[1] = (double)Parameter.size_y;
            output_dimensions[2] = (double)Parameter.size_z;
            
            /* derive laplacian in 2D */
            Laplacian_2D(u,v,u_Laplacian,v_Laplacian);
            
            mexPrintf("mexOF_Laplacian_2D...done.\n");
        }
    }
}
