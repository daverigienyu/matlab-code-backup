function [u,v,w] = OF_OpticalFlow_3D(parm,Ia,Ib)

% OF_OpticalFlow_3D(parm,Ia,Ib)

if (nargin ~= 3 || nargout ~= 3)
    fprintf('\nOF_OpticalFlow_3D\n');
    fprintf('Usage: [u,v,w] = OF_OpticalFlow_3D(parm,Ia,Ib)\n\n');
    mexOF_OpticalFlow_3D;
    fprintf('\n');
    u = -1;
    v = -1;
    w = -1;
else
    fprintf('OF_OpticalFlow_3D...\n');
    tic;
    Ia = reshape(Ia,numel(Ia),1);
    Ib = reshape(Ib,numel(Ib),1);
    [u,v,w,dims] = mexOF_OpticalFlow_3D(parm,Ia,Ib);
    u = reshape(u,dims(1),dims(2),dims(3));
    v = reshape(v,dims(1),dims(2),dims(3));
    w = reshape(w,dims(1),dims(2),dims(3));
    fprintf('OF_OpticalFlow_3D...done. [%f sec]\n',toc);
end

end