function [u,v] = OF_OpticalFlow_2D(parm,Ia,Ib)

% OF_OpticalFlow_2D(parm,Ia,Ib)

if (nargin ~= 3 || nargout ~= 2)
    fprintf('\nOF_OpticalFlow_2D\n');
    fprintf('Usage: [u,v] = OF_OpticalFlow_2D(parm,Ia,Ib)\n\n');
    mexOF_OpticalFlow_2D;
    fprintf('\n');
    u = -1;
    v = -1;
else
    fprintf('OF_OpticalFlow_2D...\n');
    tic;
    Ia = reshape(Ia,numel(Ia),1);
    Ib = reshape(Ib,numel(Ib),1);
    [u,v,dims] = mexOF_OpticalFlow_2D(parm,Ia,Ib);
    u = reshape(u,dims(1),dims(2),dims(3));
    v = reshape(v,dims(1),dims(2),dims(3));
    fprintf('OF_OpticalFlow_2D...done. [%f sec]\n',toc);
end

end