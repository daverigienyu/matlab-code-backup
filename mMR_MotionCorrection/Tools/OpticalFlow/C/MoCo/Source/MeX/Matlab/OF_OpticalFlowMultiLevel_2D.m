function [u,v] = OF_OpticalFlowMultiLevel_2D(parm,Ia,Ib)

% OF_OpticalFlowMultiLevel_2D(parm,Ia,Ib)

if (nargin ~= 3 || nargout ~= 2)
    fprintf('\nOF_OpticalFlowMultiLevel_2D\n');
    fprintf('Usage: [u,v] = OF_OpticalFlowMultiLevel_2D(parm,Ia,Ib)\n\n');
    mexOF_OpticalFlow_2D;
    fprintf('\n');
    u = -1;
    v = -1;
else
    fprintf('OF_OpticalFlowMultiLevel_2D...\n');
    of_tic = tic;
    
    % check for valid parm.LEVEL
    if (isfield(parm,'LEVEL') == 0)
        parm.LEVEL = 1;
    else
        % initialize parameter if not existing
        if (isfield(parm,'LEVEL_RATIO') == 0)
            parm.LEVEL_RATIO = 0.8;
        end
        
        if (isfield(parm,'SMOOTH_BEFORE_DOWNSAMPLING') == 0)
            parm.SMOOTH_BEFORE_DOWNSAMPLING = 2;
        end
        
        if (isfield(parm,'SMOOTH_AFTER_UPSAMPLING') == 0)
            parm.SMOOTH_AFTER_UPSAMPLING = 0;
        end
        
        % set max_level. should be calculated.
        max_level = 20;
        parm.LEVEL = max(1,min(parm.LEVEL,max_level));
    end
    
    % if only 1 level is used, run standard optical flow
    if (parm.LEVEL == 1)
        [u,v] = OF_OpticalFlow_2D(parm,Ia,Ib);
    else
        % create local copy of parameter; will be used as reference
        parm_HIGH = parm;
        
        % fixed parameters used in OF_ResampleImage
        parm_HIGH.SIZE_Z = 1;
        parm_HIGH.FOV_Z = 1.0;
        parm_HIGH.FLIP_X = 0;
        parm_HIGH.FLIP_Y = 0;
        parm_HIGH.FLIP_Z = 0;
        parm_HIGH.INTP_OFFSET_X = 0;
        parm_HIGH.INTP_OFFSET_Y = 0;
        parm_HIGH.INTP_OFFSET_Z = 0;
        
        % initialize images
        Ia_HIGH = Ia;
        Ib_HIGH = Ib;
        
        % initialize vectors
        u = zeros(size(Ia_HIGH));
        v = zeros(size(Ia_HIGH));
        
        % initialize local copy of Ib
        Ib_local = Ib_HIGH;
        
        % create filter
        G = fspecial('gaussian',[5 5]);
        
        % loop over all level
        for level=parm_HIGH.LEVEL:-1:1
            fprintf('Level %d \n',level);
            level_tic = tic;
            
            % create new parm structures
            parm_LOW = parm_HIGH;
            for l=level:-1:2
                parm_LOW.SIZE_X = parm_LOW.SIZE_X * parm.LEVEL_RATIO;
                parm_LOW.SIZE_Y = parm_LOW.SIZE_Y * parm.LEVEL_RATIO;
            end
            parm_LOW.SIZE_X = round(parm_LOW.SIZE_X);
            parm_LOW.SIZE_Y = round(parm_LOW.SIZE_Y);
            
            fprintf('%d\t%d\n',parm_HIGH.SIZE_X,parm_HIGH.SIZE_Y);
            fprintf('%d\t%d\n',parm_LOW.SIZE_X,parm_LOW.SIZE_Y);
            
            % initialize local copy of Ia
            Ia_local = Ia_HIGH;
            
            % smoothing before downsampling
            for s=1:parm.SMOOTH_BEFORE_DOWNSAMPLING
                Ia_local = imfilter(Ia_local,G,'same');
                Ib_local = imfilter(Ib_local,G,'same');
            end
            
            % downsampling of images
            Ia_local = OF_ResampleImage(parm_HIGH,parm_LOW,Ia_local);
            Ia_local(isnan(Ia_local)) = 0;
            Ib_local = OF_ResampleImage(parm_HIGH,parm_LOW,Ib_local);
            Ib_local(isnan(Ib_local)) = 0;
            
            % compute optical flow at current resolution
            [u_local,v_local] = OF_OpticalFlow_2D(parm_LOW,Ia_local,Ib_local);
            
            % upsampling of vectors
            u_local = OF_ResampleImage(parm_LOW,parm_HIGH,u_local);
            u_local(isnan(u_local)) = 0;
            v_local = OF_ResampleImage(parm_LOW,parm_HIGH,v_local);
            v_local(isnan(v_local)) = 0;
            for l=level:-1:2
                u_local = u_local / parm.LEVEL_RATIO;
                v_local = v_local / parm.LEVEL_RATIO;
            end
            
            % smoothing after upsampling
            for s=1:parm.SMOOTH_AFTER_UPSAMPLING
                u_local = imfilter(u_local,G,'same');
                v_local = imfilter(v_local,G,'same');
            end
            
            % concat motion vectors of different levels
            if (level ~= parm.LEVEL)
                [u,v] = OF_ConcatMotionVectors_2D(parm_HIGH,u,v,u_local,v_local);
            else
                u = u_local;
                v = v_local;
            end
            
            % create updated version of Ib
            if (level ~= 1)
                Ib_HIGH = zeros(parm_HIGH.SIZE_X,parm_HIGH.SIZE_Y);
                Ib_HIGH(1:parm.SIZE_X,1:parm.SIZE_Y) = Ib;
                Ib_local = OF_ApplyMotionCorrection_2D(parm_HIGH,Ib_HIGH,u,v);
            end
            
            fprintf('Level %d [%f sec]\n',level,toc(level_tic));
        end
        
        % set border pixel to zero
        %         u(1,1:parm.SIZE_Y) = 0;
        %         u(parm.SIZE_X,1:parm.SIZE_Y) = 0;
        %         u(1:parm.SIZE_X,1) = 0;
        %         u(1:parm.SIZE_X:parm.SIZE_Y) = 0;
        %
        %         v(1,1:parm.SIZE_Y) = 0;
        %         v(parm.SIZE_X,1:parm.SIZE_Y) = 0;
        %         v(1:parm.SIZE_X,1) = 0;
        %         v(1:parm.SIZE_X:parm.SIZE_Y) = 0;
    end
    
    fprintf('OF_OpticalFlowMultiLevel_2D...done. [%f sec]\n',toc(of_tic));
end

end