function [new_I] = OF_ApplyMotionCorrection_3D(parm,I,u,v,w)

% OF_ApplyMotionCorrection_3D(parm,I,u,v,w)

if (nargin ~= 5 || nargout ~= 1)
    fprintf('\nOF_ApplyMotionCorrection_3D\n');
    fprintf('Usage: [new_I] = OF_ApplyMotionCorrection_3D(parm,I,u,v,w)\n\n');
    fprintf('\n');
    new_I = -1;
else
    fprintf('OF_ApplyMotionCorrection_3D...\n');
    tic;
    
    dx = parm.FOV_X / parm.SIZE_X;
    dy = parm.FOV_Y / parm.SIZE_Y;
    dz = parm.FOV_Z / parm.SIZE_Z;
    
    if (mod(parm.SIZE_X,2) == 0)
        xgrid = -(parm.SIZE_X/2.0-0.5)*dx:dx:(parm.SIZE_X/2.0-0.5)*dx;
    else
        xgrid = -((parm.SIZE_X-1)/2.0)*dx:dx:((parm.SIZE_X-1)/2.0)*dx;
    end
    
    if (mod(parm.SIZE_Y,2) == 0)
        ygrid = -(parm.SIZE_Y/2.0-0.5)*dy:dy:(parm.SIZE_Y/2.0-0.5)*dy;
    else
        ygrid = -((parm.SIZE_Y-1)/2.0)*dy:dy:((parm.SIZE_Y-1)/2.0)*dy;
    end
    
    if (mod(parm.SIZE_Z,2) == 0)
        zgrid = -(parm.SIZE_Z/2.0-0.5)*dz:dz:(parm.SIZE_Z/2.0-0.5)*dz;
    else
        zgrid = -((parm.SIZE_Z-1)/2.0)*dz:dz:((parm.SIZE_Z-1)/2.0)*dz;
    end
    
    [a,b,c] = meshgrid(ygrid,xgrid,zgrid);
    x = u + b;
    y = v + a;
    z = w + c;
    new_I = interp3(a,b,c,I,y,x,z,'linear');
    new_I(isnan(new_I)) = 0;
    
    fprintf('OF_ApplyMotionCorrection_3D...done. [%f sec]\n',toc);
end

end