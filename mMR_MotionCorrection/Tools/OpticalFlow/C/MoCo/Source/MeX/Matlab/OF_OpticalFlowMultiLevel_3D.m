function [u,v,w] = OF_OpticalFlowMultiLevel_3D(parm,Ia,Ib)

% OF_OpticalFlowMultiLevel_3D(parm,Ia,Ib)

if (nargin ~= 3 || nargout ~= 3)
    fprintf('\nOF_OpticalFlowMultiLevel_3D\n');
    fprintf('Usage: [u,v,w] = OF_OpticalFlowMultiLevel_3D(parm,Ia,Ib)\n\n');
    mexOF_OpticalFlow_3D;
    fprintf('\n');
    u = -1;
    v = -1;
    w = -1;
else
    fprintf('OF_OpticalFlowMultiLevel_3D...\n');
    of_tic = tic;
    
    % check for valid parm.LEVEL
    if (isfield(parm,'LEVEL') == 0)
        parm.LEVEL = 1;
    else
        % initialize parameter if not existing
        if (isfield(parm,'LEVEL_RATIO') == 0)
            parm.LEVEL_RATIO = 0.8;
        end
        
        if (isfield(parm,'SMOOTH_BEFORE_DOWNSAMPLING') == 0)
            parm.SMOOTH_BEFORE_DOWNSAMPLING = 2;
        end
        
        if (isfield(parm,'SMOOTH_AFTER_UPSAMPLING') == 0)
            parm.SMOOTH_AFTER_UPSAMPLING = 0;
        end
        
        % set max_level. should be calculated.
        max_level = 20;
        parm.LEVEL = max(1,min(parm.LEVEL,max_level));
    end
    
    % if only 1 level is used, run standard optical flow
    if (parm.LEVEL == 1)
        [u,v,w] = OF_OpticalFlow_3D(parm,Ia,Ib);
    else
        % create local copy of parameter; will be used as reference
        parm_HIGH = parm;
        
        % fixed parameters used in OF_ResampleImage
        parm_HIGH.FLIP_X = 0;
        parm_HIGH.FLIP_Y = 0;
        parm_HIGH.FLIP_Z = 0;
        parm_HIGH.INTP_OFFSET_X = 0;
        parm_HIGH.INTP_OFFSET_Y = 0;
        parm_HIGH.INTP_OFFSET_Z = 0;
        
        % initialize images
        Ia_HIGH = Ia;
        Ib_HIGH = Ib;
        
        % initialize vectors
        u = zeros(size(Ia_HIGH));
        v = zeros(size(Ia_HIGH));
        w = zeros(size(Ia_HIGH));
        
        % initialize local copy of Ib
        Ib_local = Ib_HIGH;
        
        % loop over all level
        for level=parm_HIGH.LEVEL:-1:1
            fprintf('Level %d \n',level);
            level_tic = tic;
            
            % create new parm structures
            parm_LOW = parm_HIGH;
            for l=level:-1:2
                parm_LOW.SIZE_X = parm_LOW.SIZE_X * parm.LEVEL_RATIO;
                parm_LOW.SIZE_Y = parm_LOW.SIZE_Y * parm.LEVEL_RATIO;
                parm_LOW.SIZE_Z = parm_LOW.SIZE_Z * parm.LEVEL_RATIO;
            end
            parm_LOW.SIZE_X = round(parm_LOW.SIZE_X);
            parm_LOW.SIZE_Y = round(parm_LOW.SIZE_Y);
            parm_LOW.SIZE_Z = round(parm_LOW.SIZE_Z);
            
            fprintf('%d\t%d\t%d\n',parm_HIGH.SIZE_X,parm_HIGH.SIZE_Y,parm_HIGH.SIZE_Z);
            fprintf('%d\t%d\t%d\n',parm_LOW.SIZE_X,parm_LOW.SIZE_Y,parm_LOW.SIZE_Z);
            
            % initialize local copy of Ia
            Ia_local = Ia_HIGH;
            
            % smoothing before downsampling
            for s=1:parm_HIGH.SMOOTH_BEFORE_DOWNSAMPLING
                Ia_local = smooth3(Ia_local,'gaussian');
                Ib_local = smooth3(Ib_local,'gaussian');
            end
            
            % downsampling of images
            Ia_local = OF_ResampleImage(parm_HIGH,parm_LOW,Ia_local);
            Ia_local(isnan(Ia_local)) = 0;
            Ib_local = OF_ResampleImage(parm_HIGH,parm_LOW,Ib_local);
            Ib_local(isnan(Ib_local)) = 0;
            
            % compute optical flow at current resolution
            [u_local,v_local,w_local] = OF_OpticalFlow_3D(parm_LOW,Ia_local,Ib_local);
            
            % upsampling of vectors
            u_local = OF_ResampleImage(parm_LOW,parm_HIGH,u_local);
            u_local(isnan(u_local)) = 0;
            v_local = OF_ResampleImage(parm_LOW,parm_HIGH,v_local);
            v_local(isnan(v_local)) = 0;
            w_local = OF_ResampleImage(parm_LOW,parm_HIGH,w_local);
            w_local(isnan(w_local)) = 0;
            for l=level:-1:2
                u_local = u_local / parm.LEVEL_RATIO;
                v_local = v_local / parm.LEVEL_RATIO;
                w_local = w_local / parm.LEVEL_RATIO;
            end
            
            % smoothing after upsampling
            for s=1:parm_HIGH.SMOOTH_AFTER_UPSAMPLING
                u_local = smooth3(u_local,'gaussian');
                v_local = smooth3(v_local,'gaussian');
                w_local = smooth3(w_local,'gaussian');
            end
            
            % concat motion vectors of different levels
            if (level ~= parm_HIGH.LEVEL)
                [u,v,w] = OF_ConcatMotionVectors_3D(parm_HIGH,u,v,w,u_local,v_local,w_local);
            else
                u = u_local;
                v = v_local;
                w = w_local;
            end
            
            % create updated version of Ib
            if (level ~= 1)
                Ib_HIGH = Ib;
                Ib_local = OF_ApplyMotionCorrection_3D(parm_HIGH,Ib_HIGH,u,v,w);
            end
            
            fprintf('Level %d [%f sec]\n',level,toc(level_tic));
        end
        
        % set border pixel to zero
        %         u(1,1,:) = 0; v(1,1,:) = 0; w(1,1,:) = 0;
        %         u(1,:,1) = 0; v(1,:,1) = 0; w(1,:,1) = 0;
        %         u(:,1,1) = 0; v(:,1,1) = 0; w(:,1,1) = 0;
        %
        %
        %         v(1,1:parm.SIZE_Y) = 0;
        %         v(parm.SIZE_X,1:parm.SIZE_Y) = 0;
        %         v(1:parm.SIZE_X,1) = 0;
        %         v(1:parm.SIZE_X:parm.SIZE_Y) = 0;
    end
    
    fprintf('OF_OpticalFlowMultiLevel_3D...done. [%f sec]\n',toc(of_tic));
end

end