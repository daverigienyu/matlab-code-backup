function [u_Avg,v_Avg,w_Avg] = OF_Laplacian_3D(parm,u,v,w)

% OF_Laplacian_3D(parm,u,v,w)

if (nargin ~= 4 || nargout ~= 3)
    fprintf('\nOF_Laplacian_3D\n');
    fprintf('Usage: [u_Avg,v_Avg,w_Avg] = OF_Laplacian_3D(parm,u,v,w)\n\n');
    mexOF_Laplacian_3D;
    fprintf('\n');
    u_Avg = -1;
    v_Avg = -1;
    w_Avg = -1;
else
    fprintf('OF_Laplacian_3D...\n');
    tic;
    u = reshape(u,numel(u),1);
    v = reshape(v,numel(v),1);
    w = reshape(w,numel(w),1);
    [u_Avg,v_Avg,w_Avg,dims] = mexOF_Laplacian_3D(parm,u,v,w);
    u_Avg = reshape(u_Avg,dims(1),dims(2),dims(3));
    v_Avg = reshape(v_Avg,dims(1),dims(2),dims(3));
    w_Avg = reshape(w_Avg,dims(1),dims(2),dims(3));
    fprintf('OF_Laplacian_3D...done. [%f sec]\n',toc);
end

end