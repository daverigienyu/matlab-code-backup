function [new_I] = OF_ApplyMotionCorrection_2D(parm,I,u,v)

% OF_ApplyMotionCorrection_2D(parm,I,u,v)

if (nargin ~= 4 || nargout ~= 1)
    fprintf('\nOF_ApplyMotionCorrection_2D\n');
    fprintf('Usage: [new_I] = OF_ApplyMotionCorrection_2D(parm,I,u,v)\n\n');
    fprintf('\n');
    new_I = -1;
else
    fprintf('OF_ApplyMotionCorrection_2D...\n');
    tic;
    
    dx = parm.FOV_X / parm.SIZE_X;
    dy = parm.FOV_Y / parm.SIZE_Y;
    
    if (mod(parm.SIZE_X,2) == 0)
        xgrid = -(parm.SIZE_X/2.0-0.5)*dx:dx:(parm.SIZE_X/2.0-0.5)*dx;
    else
        xgrid = -((parm.SIZE_X-1)/2.0)*dx:dx:((parm.SIZE_X-1)/2.0)*dx;
    end
    
    if (mod(parm.SIZE_Y,2) == 0)
        ygrid = -(parm.SIZE_Y/2.0-0.5)*dy:dy:(parm.SIZE_Y/2.0-0.5)*dy;
    else
        ygrid = -((parm.SIZE_Y-1)/2.0)*dy:dy:((parm.SIZE_Y-1)/2.0)*dy;
    end
    
    [a,b] = meshgrid(ygrid,xgrid);
    y = v + a;
    x = u + b;
    new_I = interp2(a,b,I,y,x,'linear');
    new_I(isnan(new_I)) = 0;
    
    fprintf('OF_ApplyMotionCorrection_2D...done. [%f sec]\n',toc);
end

end