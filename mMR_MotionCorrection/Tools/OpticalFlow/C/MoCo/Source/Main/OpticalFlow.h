#ifndef OPTICALFLOW_H
#define OPTICALFLOW_H


/* Optical Flow includes */
#include "OpticalFlow_HS.h"
#include "OpticalFlow_HS_V2.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void OpticalFlow_2D(double *Ia, double *Ib, double *u, double *v);
void OpticalFlow_3D(double *Ia, double *Ib, double *u, double *v, double *w);


#endif
