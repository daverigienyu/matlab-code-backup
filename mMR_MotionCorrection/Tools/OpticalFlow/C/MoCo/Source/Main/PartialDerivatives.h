#ifndef PARTIALDERIVATIVES_H
#define PARTIALDERIVATIVES_H


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void PartialDerivatives_Spatial_2D(double *Ia, double *Ix, double *Iy);
void PartialDerivatives_Spatial_3D(double *Ia, double *Ix, double *Iy, double *Iz);


#endif
