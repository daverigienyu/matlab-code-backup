#include "PartialDerivatives.h"


void PartialDerivatives_Spatial_2D(double *Ia, double *Ix, double *Iy)
{
    int y;
    double dx,dy;

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;

    for (y=0;y<Parameter.imagesize;y++)
    {
        Ix[y] = 0.0;
        Iy[y] = 0.0;
    }

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int x;
        int i, i1, i2;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            i1 = (x-1) + y * Parameter.size_x;
            i2 = (x+1) + y * Parameter.size_x;
            Ix[i] = (Ia[i2] - Ia[i1]) / (2.0 * dx);

            i1 = x + (y-1) * Parameter.size_x;
            i2 = x + (y+1) * Parameter.size_x;
            Iy[i] = (Ia[i2] - Ia[i1]) / (2.0 * dy);
        }
    }
}


void PartialDerivatives_Spatial_3D(double *Ia, double *Ix, double *Iy, double *Iz)
{
    int z;
    double dx,dy,dz;

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;
    dz = Parameter.fov_z / (double)Parameter.size_z;

    for (z=0;z<Parameter.imagesize;z++)
    {
        Ix[z] = 0.0;
        Iy[z] = 0.0;
        Iz[z] = 0.0;
    }

#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int x,y;
        int i, i1, i2;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                Ix[i] = (Ia[i2] - Ia[i1]) / (2.0 * dx);

                i1 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                Iy[i] = (Ia[i2] - Ia[i1]) / (2.0 * dy);

                i1 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                i2 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;
                Iz[i] = (Ia[i2] - Ia[i1]) / (2.0 * dz);
            }
        }
    }
}
