#ifndef LAPLACIAN_H
#define LAPLACIAN_H


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void Laplacian_2D(double *u, double *v, double *u_Laplacian, double *v_Laplacian);
void Laplacian_3D(double *u, double *v, double *w, double *u_Laplacian, double *v_Laplacian, double *w_Laplacian);


#endif
