#ifndef OPTICALFLOW_HS_V2_H
#define OPTICALFLOW_HS_V2_H


/* Optical Flow includes */
#include "Laplacian.h"
#include "PartialDerivatives.h"
#include "WeightedAverage.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void OpticalFlow_HS_V2_2D(double *Ia, double *Ib, double *u, double *v);
void OpticalFlow_HS_V2_3D(double *Ia, double *Ib, double *u, double *v, double *w);


#endif
