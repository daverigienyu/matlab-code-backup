#include "OpticalFlow_HS.h"


void OpticalFlow_HS_2D(double *Ia, double *Ib, double *u, double *v)
{
    int iteration,y;
    double c,dx,dy;
    double *u_Avg,*v_Avg,*Ix,*Iy;
    double alpha = GetParameterDouble("ALPHA");
    
    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* pixel size for scaling */
    dx = Parameter.fov_x/(double)Parameter.size_x;
    dy = Parameter.fov_y/(double)Parameter.size_y;

    /* scaling factor */
    c = 2.0/dx + 2.0/dy;

    /* calculate partial derivatives wrt x,y */
    PartialDerivatives_Spatial_2D(Ia,Ix,Iy);
    
    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate average in neighborhood */
        WeightedAverage_2D(u,v,u_Avg,v_Avg);

        /* update u and v */
#pragma omp parallel for
        for (y=1;y<Parameter.size_y-1;y++)
        {
            int i,x;
            double denominator;

            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x;

                denominator = c * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]);

                /* update of for x and y direction */
                u[i] = u_Avg[i] - (Ix[i] * (c * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + c * (Ib[i]-Ia[i]))
                                   - (1-c) * (c * alpha + Iy[i]*Iy[i]) * u_Avg[i]) / denominator;

                v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + c * Iy[i] * v_Avg[i] + c * (Ib[i]-Ia[i]))
                                   - (1-c) * (c * alpha + Ix[i]*Ix[i]) * v_Avg[i]) / denominator;
            }
        }
    }

    /* free memory of temporary images */
    free(u_Avg);
    free(v_Avg);
    free(Ix);
    free(Iy);
}


void OpticalFlow_HS_3D(double *Ia, double *Ib, double *u, double *v, double *w)
{
    int iteration,z;
    double c,dx,dy,dz;
    double *u_Avg,*v_Avg,*w_Avg,*Ix,*Iy,*Iz;
    double alpha = GetParameterDouble("ALPHA");

    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    w_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* voxel size for scaling */
    dx = Parameter.fov_x/(double)Parameter.size_x;
    dy = Parameter.fov_y/(double)Parameter.size_y;
    dz = Parameter.fov_z/(double)Parameter.size_z;

    /* scaling factor */
    c = 2.0/dx + 2.0/dy + 2.0/dz;

    /* calculate partial derivatives wrt x,y and z */
    PartialDerivatives_Spatial_3D(Ia,Ix,Iy,Iz);

    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate average in neighborhood */
        WeightedAverage_3D(u,v,w,u_Avg,v_Avg,w_Avg);

        /* update u, v and w */
#pragma omp parallel for
        for (z=1;z<Parameter.size_z-1;z++)
        {
            int i,x,y;
            double denominator;

            for (y=1;y<Parameter.size_y-1;y++)
            {
                for (x=1;x<Parameter.size_x-1;x++)
                {
                    i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                    denominator = c * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i] + Iz[i]*Iz[i]);

                    /* update of for x,y and z direction */
                    u[i] = u_Avg[i] - (Ix[i] * (c * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                       - (1-c) * (c * alpha + Iy[i]*Iy[i] + Iz[i]*Iz[i]) * u_Avg[i]) / denominator;

                    v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + c * Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                       - (1-c) * (c * alpha + Ix[i]*Ix[i] + Iz[i]*Iz[i]) * v_Avg[i]) / denominator;

                    w[i] = w_Avg[i] - (Iz[i] * (Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + c * Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                       - (1-c) * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]) * w_Avg[i]) / denominator;
                }
            }
        }
    }
    
    /* free memory of temporary images */
    free(u_Avg);
    free(v_Avg);
    free(w_Avg);
    free(Ix);
    free(Iy);
    free(Iz);
}
