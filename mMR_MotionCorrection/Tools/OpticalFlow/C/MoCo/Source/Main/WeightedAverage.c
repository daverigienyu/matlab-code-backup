#include "WeightedAverage.h"


void WeightedAverage_2D(double *u, double *v, double *u_Avg, double *v_Avg)
{
    int y;
    double dx,dy;
    
    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;

    for (y=0;y<Parameter.imagesize;y++)
    {
        u_Avg[y] = 0.0;
        v_Avg[y] = 0.0;
    }

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int x;
        int i, i1, i2, i3, i4;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            i1 = (x-1) + y * Parameter.size_x;
            i2 = (x+1) + y * Parameter.size_x;
            i3 = x + (y-1) * Parameter.size_x;
            i4 = x + (y+1) * Parameter.size_x;

            u_Avg[i] = (u[i1] + u[i2]) / dx + (u[i3] + u[i4]) / dy;
            v_Avg[i] = (v[i1] + v[i2]) / dx + (v[i3] + v[i4]) / dy;
        }
    }
}


void WeightedAverage_3D(double *u, double *v, double *w, double *u_Avg, double *v_Avg, double *w_Avg)
{
    int z;
    double dx,dy,dz;
    
    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;
    dz = Parameter.fov_z / (double)Parameter.size_z;

    for (z=0;z<Parameter.imagesize;z++)
    {
        u_Avg[z] = 0.0;
        v_Avg[z] = 0.0;
        w_Avg[z] = 0.0;
    }

#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int x,y;
        int i, i1, i2, i3, i4, i5, i6;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i3 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i4 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i5 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                i6 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;
                
                u_Avg[i] = (u[i1] + u[i2]) / dx + (u[3] + u[4]) / dy + (u[i5] + u[i6]) / dz;
                v_Avg[i] = (v[i1] + v[i2]) / dx + (v[3] + v[4]) / dy + (v[i5] + v[i6]) / dz;
                w_Avg[i] = (w[i1] + w[i2]) / dx + (w[3] + w[4]) / dy + (w[i5] + w[i6]) / dz;
            }
        }
    }
}


void  WeightedAveragePsi_2D(double *Psi1, double *Psi2, double *u, double *v, double *u_Avg, double *v_Avg)
{
    int y;
    double dx,dy;

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;

    for (y=0;y<Parameter.imagesize;y++)
    {
        u_Avg[y] = 0.0;
        v_Avg[y] = 0.0;
    }

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int x;
        int i, i1, i2, i3, i4;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            i1 = (x-1) + y * Parameter.size_x;
            i2 = (x+1) + y * Parameter.size_x;
            i3 = x + (y-1) * Parameter.size_x;
            i4 = x + (y+1) * Parameter.size_x;

            u_Avg[i] = (((Psi2[i]+Psi2[i1])*u[i1] + (Psi2[i]+Psi2[i2])*u[i2]) / (2.0*dx)
                        + ((Psi2[i]+Psi2[i3])*u[i3] + (Psi2[i]+Psi2[i4])*u[i4]) / (2.0*dy)) / Psi1[i];
            v_Avg[i] = (((Psi2[i]+Psi2[i1])*v[i1] + (Psi2[i]+Psi2[i2])*v[i2]) / (2.0*dx)
                        + ((Psi2[i]+Psi2[i3])*v[i3] + (Psi2[i]+Psi2[i4])*v[i4]) / (2.0*dy)) / Psi1[i];
        }
    }
}


void WeightedAveragePsi_3D(double *Psi1, double *Psi2, double *u, double *v, double *w, double *u_Avg, double *v_Avg, double *w_Avg)
{
    int z;
    double dx,dy,dz;

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;
    dz = Parameter.fov_z / (double)Parameter.size_z;

    for (z=0;z<Parameter.imagesize;z++)
    {
        u_Avg[z] = 0.0;
        v_Avg[z] = 0.0;
        w_Avg[z] = 0.0;
    }

#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int x,y;
        int i, i1, i2, i3, i4, i5, i6;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i3 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i4 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i5 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                i6 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;

                u_Avg[i] = (((Psi2[i]+Psi2[i1])*u[i1] + (Psi2[i]+Psi2[i2])*u[i2]) / (2.0*dx)
                            + ((Psi2[i]+Psi2[i3])*u[i3] + (Psi2[i]+Psi2[i4])*u[i4]) / (2.0*dy)
                            + ((Psi2[i]+Psi2[i5])*u[i5] + (Psi2[i]+Psi2[i6])*u[i6]) / (2.0*dz)) / Psi1[i];
                v_Avg[i] = (((Psi2[i]+Psi2[i1])*v[i1] + (Psi2[i]+Psi2[i2])*v[i2]) / (2.0*dx)
                            + ((Psi2[i]+Psi2[i3])*v[i3] + (Psi2[i]+Psi2[i4])*v[i4]) / (2.0*dy)
                            + ((Psi2[i]+Psi2[i5])*v[i5] + (Psi2[i]+Psi2[i6])*v[i6]) / (2.0*dz)) / Psi1[i];
                w_Avg[i] = (((Psi2[i]+Psi2[i1])*w[i1] + (Psi2[i]+Psi2[i2])*w[i2]) / (2.0*dx)
                            + ((Psi2[i]+Psi2[i3])*w[i3] + (Psi2[i]+Psi2[i4])*w[i4]) / (2.0*dy)
                            + ((Psi2[i]+Psi2[i5])*w[i5] + (Psi2[i]+Psi2[i6])*w[i6]) / (2.0*dz)) / Psi1[i];
            }
        }
    }
}
