close all; clear mex; clear; clc;

addpath('Q:\labspace\mMR_MotionCorrection\Evaluation\2017-03-23-MoCo_Test\OpticalFlow\MoCo\Bin');
load('demodata.mat');

parm.SIZE_X = gate_dim(1);
parm.SIZE_Y = gate_dim(2);
parm.SIZE_Z = 1;
parm.FOV_Y = gate_dim(1) * gate_res(1);
parm.FOV_X = gate_dim(2) * gate_res(2);
parm.FOV_Z = 1;


dx = parm.FOV_X / parm.SIZE_X;
dy = parm.FOV_Y / parm.SIZE_Y;

parm.OF_ALGO = 4;
parm.LEVEL = 8;
parm.ITERATIONS = 100;
parm.OUTER_ITERATIONS = 1;
parm.ALPHA = 500.0;
parm.FWHM = 0.0;
parm.BETA1 = 100000.0;
parm.BETA2 = 1;

parm.SMOOTH_BEFORE_DOWNSAMPLING = 2;
parm.SMOOTH_AFTER_UPSAMPLING = 0;


parm.GRADIENT_THRESHOLD = 0.0;
parm.LEVEL_RATIO = 0.8;

Ia = gate1;
Ib = gate2;
% scale data
scale = 100.0;
Ia = Ia - min(Ia(:));
Ia = Ia ./ max(Ia(:)) * scale;
Ib = Ib - min(Ib(:));
Ib = Ib ./ max(Ib(:)) * scale;


if (1)
    parm.SIZE_X = 128;
    parm.SIZE_Y = 128;
    parm.SIZE_Z = 1;
    parm.FOV_Y = 128;
    parm.FOV_X = 128;
    parm.FOV_Z = 1;
    
    scale = 100.0;
    Ia = zeros(parm.SIZE_X,parm.SIZE_Y);
    Ib = zeros(parm.SIZE_X,parm.SIZE_Y);
    % y x
    Ia(51:80,51:80) = scale;
    Ib(51:80,46:75) = scale;
    % Ib(56:85,46:75) = scale;
end

if (0)
    Itmp = Ia;
    Ia = Ib;
    Ib = Itmp;
end

%G = fspecial('gaussian',[5 5]);
%Ia = imfilter(Ia,G,'same');
%Ib = imfilter(Ib,G,'same');

% calculate motion and apply correction
%[u,v] = OF_OpticalFlow_2D(parm,Ia,Ib);
[u,v] = OF_OpticalFlowMultiLevel_2D(parm,Ia,Ib);
%p = zeros(parm.SIZE_X,parm.SIZE_Y,2);
%p(:,:,1) = Ia;
%p(:,:,2) = Ib;
%[u,v] = OpticalFlow_Test(p,parm.ALPHA,parm.ITERATIONS);

result1 = OF_ApplyMotionCorrection_2D(parm,Ib,u,v);

fprintf('u: %f %f\n',min(u(:)),max(u(:)));
fprintf('v: %f %f\n',min(v(:)),max(v(:)));

fprintf('before: %f\n',corr(Ia(:),Ib(:)));
fprintf('after: %f\n',corr(Ia(:),result1(:)));


subplot(2,3,1); imagesc(Ia,[0 scale]); axis xy;
subplot(2,3,2); imagesc(result1,[0 scale]); axis xy;
subplot(2,3,3); imagesc(Ib,[0 scale]); axis xy;

subplot(2,3,4); quiver(u,v);  axis xy;
subplot(2,3,5); imagesc(u); axis xy;
subplot(2,3,6); imagesc(v); axis xy;

%save('demo_results_2D.mat','Ia','Ib','u','v');
%save('Demo_2D_data_invers.mat','Ia','Ib','u','v');
