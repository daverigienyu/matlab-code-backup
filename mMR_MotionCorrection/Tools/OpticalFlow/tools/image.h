#ifndef IMAGE_H
#define IMAGE_H


#include <stdio.h>
#include <vector>
#include <iostream>
#include <math.h>
#include <algorithm>


#include "../io/parameter_list.h"


using namespace std;


class image
{
private:
    int imagesize, size_x, size_y, size_z;
    double d_x, d_y, d_z;
    vector<double> content;

public:
    image();
    image(parameter_list);
    image(int, int, int);

    void initialize(image);
    void initialize(parameter_list);

    double get_value(int);
    double get_d_x() {return d_x;}
    double get_d_y() {return d_y;}
    double get_d_z() {return d_z;}
    int get_size_x() {return size_x;}
    int get_size_y() {return size_y;}
    int get_size_z() {return size_z;}
    int get_imagesize() {return imagesize;}

    void add_value(int, double);

    void set_zero();
    void set_value(double);
    void set_value(int, double);
    void set_value(int, int, double);
    void set_value(int, int, int, double);

    void scale(double);

    void resample(int, int);
    void resample(int, int, int);
    void resample(double);
    void resample(double, double);
    void resample(double, double, double);

    void concat_motion_fields_x(image, image);
    void concat_motion_fields_x(image, image, image);
    void concat_motion_fields_y(image, image);
    void concat_motion_fields_y(image, image, image);
    void concat_motion_fields_z(image, image);
    void concat_motion_fields_z(image, image, image);

    void apply_motion_fields(image, image);
    void apply_motion_fields(image, image, image);

    void smooth();
};


#endif // IMAGE_H
