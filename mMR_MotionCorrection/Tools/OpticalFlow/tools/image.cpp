#include "image.h"


/**
 * @brief image::image
 */
image::image()
{
    size_x = 1;
    size_y = 1;
    size_z = 1;
    imagesize = 1;

    content.push_back(0.0);

    d_x = 1.0;
    d_y = 1.0;
    d_z = 1.0;
}


/**
 * @brief image::image
 * @param ParameterList
 */
image::image(parameter_list ParameterList)
{
    size_x = ParameterList.get_parameter_int("SIZE_X",1);
    size_y = ParameterList.get_parameter_int("SIZE_Y",1);
    size_z = ParameterList.get_parameter_int("SIZE_Z",1);
    imagesize = size_x * size_y * size_z;

    for (int i=0;i<imagesize;i++)
    {
        content.push_back(0.0);
    }

    d_x = ParameterList.get_parameter_double("D_X",1.0);
    d_y = ParameterList.get_parameter_double("D_Y",1.0);
    d_z = ParameterList.get_parameter_double("D_Z",1.0);
}


/**
 * @brief image::image
 * @param size_x
 * @param size_y
 * @param size_z
 */
image::image(int size_x, int size_y, int size_z)
{
    d_x = 1.0;
    d_y = 1.0;
    d_z = 1.0;

    this->size_x = size_x;
    this->size_y = size_y;
    this->size_z = size_z;
    this->imagesize = size_x * size_y * size_z;

    for (int i=0;i<imagesize;i++)
    {
        content.push_back(0.0);
    }
}


/**
 * @brief image::initialize
 * @param I
 */
void image::initialize(image I)
{
    content.clear();

    d_x = I.get_d_x();
    d_y = I.get_d_y();
    d_z = I.get_d_z();

    size_x = I.get_size_x();
    size_y = I.get_size_y();
    size_z = I.get_size_z();
    imagesize = size_x * size_y * size_z;

    for (int i=0;i<imagesize;i++)
    {
        content.push_back(0.0);
    }
}


/**
 * @brief image::initialize
 */
void image::initialize(parameter_list ParameterList)
{
    content.clear();

    size_x = ParameterList.get_parameter_int("SIZE_X",1);
    size_y = ParameterList.get_parameter_int("SIZE_Y",1);
    size_z = ParameterList.get_parameter_int("SIZE_Z",1);
    imagesize = size_x * size_y * size_z;

    for (int i=0;i<imagesize;i++)
    {
        content.push_back(0.0);
    }

    d_x = ParameterList.get_parameter_double("D_X",1.0);
    d_y = ParameterList.get_parameter_double("D_Y",1.0);
    d_z = ParameterList.get_parameter_double("D_Z",1.0);
}


/**
 * @brief image::get_value
 * @param location
 * @return
 */
double image::get_value(int location)
{
    return content[location];
}


/**
 * @brief image::add_value
 * @param location
 * @param value
 */
void image::add_value(int location, double value)
{
    content[location] += value;
}


/**
 * @brief image::set_zero
 */
void image::set_zero()
{
    for (int i=0;i<imagesize;i++)
    {
        content[i] = 0.0;
    }
}


/**
 * @brief image::set_value
 * @param value
 */
void image::set_value(double value)
{
    for (int i=0;i<imagesize;i++)
    {
        content[i] = value;
    }
}


/**
 * @brief image::set_value
 * @param location
 * @param value
 */
void image::set_value(int location, double value)
{
    content[location] = value;
}


/**
 * @brief image::set_value
 * @param location_x
 * @param location_y
 * @param value
 */
void image::set_value(int location_x, int location_y, double value)
{
    content[location_x+location_y*size_x] = value;
}


/**
 * @brief image::set_value
 * @param location_x
 * @param location_y
 * @param location_z
 * @param value
 */
void image::set_value(int location_x, int location_y, int location_z, double value)
{
    content[location_x+location_y*size_x+location_z*size_x*size_y] = value;
}


/**
 * @brief image::scale
 * @param factor
 */
void image::scale(double factor)
{
    for (int i=0;i<imagesize;i++)
    {
        content[i] *= factor;
    }
}


/**
 * @brief image::resample
 * @param size_x
 * @param size_y
 */
void image::resample(int size_x, int size_y)
{
    double factor_x = double(size_x)/double(this->size_x);
    double factor_y = double(size_y)/double(this->size_y);

    resample(factor_x,factor_y,1.0);
}


/**
 * @brief image::resample
 * @param size_x
 * @param size_y
 * @param size_z
 */
void image::resample(int size_x, int size_y, int size_z)
{
    double factor_x = double(size_x)/double(this->size_x);
    double factor_y = double(size_y)/double(this->size_y);
    double factor_z = double(size_z)/double(this->size_z);

    resample(factor_x,factor_y,factor_z);
}


/**
 * @brief image::resample
 * @param factor
 */
void image::resample(double factor)
{
    if (size_z == 1)
    {
        resample(factor,factor,1.0);
    }
    if (size_z > 1)
    {
        resample(factor,factor,factor);
    }
}


/**
 * @brief image::resample
 * @param factor
 */
void image::resample(double factor_x, double factor_y)
{
    resample(factor_x,factor_y,1.0);
}


/**
 * @brief image::resample
 * @param factor_x
 * @param factor_y
 * @param factor_z
 */
void image::resample(double factor_x, double factor_y, double factor_z)
{
    /* make sure we have a reasonable scaling factors */
    if (factor_x < 0.0)
    {
        factor_x = 1.0;
    }
    if (factor_y < 0.0)
    {
        factor_y = 1.0;
    }
    if (factor_z < 0.0)
    {
        factor_z = 1.0;
    }

    vector<double> tmp_content;

    int new_size_x = round(size_x*factor_x);
    int new_size_y = round(size_y*factor_y);
    int new_size_z = round(size_z*factor_z);

    new_size_x = max(new_size_x,1);
    new_size_y = max(new_size_y,1);
    new_size_z = max(new_size_z,1);

    /* x direction */
    if (size_x != new_size_x)
    {
        for (int i=0;i<new_size_x*size_y*size_z;i++)
        {
            tmp_content.push_back(0.0);
        }

        double new_d_x = (size_x * d_x) / new_size_x;

        /* loop over all new bins */
#pragma omp parallel for
        for (int i=0;i<new_size_x;i++)
        {
            double c_x  = (i * new_d_x) + new_d_x / 2.0;
            int i0 = int(floor(c_x / d_x));
            int i1 = int(ceil(c_x / d_x));
            double c_x0  = (i0 * d_x) + d_x / 2.0;
            double c_x1  = (i1 * d_x) + d_x / 2.0;

            if (c_x0 > c_x)
            {
                i1 = i0;
                i0 = i0 - 1;
                c_x0  = (i0 * d_x) + d_x / 2.0;
                c_x1  = (i1 * d_x) + d_x / 2.0;
            }

            if (i0 != i1 && i0 >= 0 && i0 < size_x && i1 >= 0 && i1 < size_x)
            {
                double weight = (c_x-c_x0)/(c_x1-c_x0);

                /* update all values */
                for (int y=0;y<size_y;y++)
                {
                    for (int z=0;z<size_z;z++)
                    {
                        tmp_content[i+y*new_size_x+z*new_size_x*size_y] = content[i0+y*size_x+z*size_x*size_y] + weight * (content[i1+y*size_x+z*size_x*size_y] - content[i0+y*size_x+z*size_x*size_y]);
                    }
                }
            }
        }

        d_x = new_d_x;
        size_x = new_size_x;
        imagesize = size_x * size_y * size_z;
        content.clear();
        for (int i=0;i<imagesize;i++)
        {
            content.push_back(tmp_content[i]);
        }
        tmp_content.clear();
    }


    /* y direction */
    if (size_y != new_size_y)
    {
        for (int i=0;i<size_x*new_size_y*size_z;i++)
        {
            tmp_content.push_back(0.0);
        }

        double new_d_y = (size_y * d_y) / new_size_y;

        /* loop over all new bins */
#pragma omp parallel for
        for (int i=0;i<new_size_y;i++)
        {
            double c_y  = (i * new_d_y) + new_d_y / 2.0;
            int i0 = int(floor(c_y / d_y));
            int i1 = int(ceil(c_y / d_y));
            double c_y0  = (i0 * d_y) + d_y / 2.0;
            double c_y1  = (i1 * d_y) + d_y / 2.0;

            if (c_y0 > c_y)
            {
                i1 = i0;
                i0 = i0 - 1;
                c_y0  = (i0 * d_y) + d_y / 2.0;
                c_y1  = (i1 * d_y) + d_y / 2.0;
            }

            if (i0 != i1 && i0 >= 0 && i0 < size_y && i1 >= 0 && i1 < size_y)
            {
                double weight = (c_y-c_y0)/(c_y1-c_y0);

                /* update all values */
                for (int x=0;x<size_x;x++)
                {
                    for (int z=0;z<size_z;z++)
                    {
                        tmp_content[x+i*size_x+z*size_x*new_size_y] = content[x+i0*size_x+z*size_x*size_y] + weight * (content[x+i1*size_x+z*size_x*size_y] - content[x+i0*size_x+z*size_x*size_y]);
                    }
                }
            }
        }

        d_y = new_d_y;
        size_y = new_size_y;
        imagesize = size_x * size_y * size_z;
        content.clear();
        for (int i=0;i<imagesize;i++)
        {
            content.push_back(tmp_content[i]);
        }
        tmp_content.clear();
    }

    /* z direction */
    if (size_z != new_size_z)
    {
        for (int i=0;i<size_x*size_y*new_size_z;i++)
        {
            tmp_content.push_back(0.0);
        }

        double new_d_z = (size_z * d_z) / new_size_z;

        /* loop over all new bins */
#pragma omp parallel for
        for (int i=0;i<new_size_z;i++)
        {
            double c_z  = (i * new_d_z) + new_d_z / 2.0;
            int i0 = int(floor(c_z / d_z));
            int i1 = int(ceil(c_z / d_z));
            double c_z0  = (i0 * d_z) + d_z / 2.0;
            double c_z1  = (i1 * d_z) + d_z / 2.0;

            if (c_z0 > c_z)
            {
                i1 = i0;
                i0 = i0 - 1;
                c_z0  = (i0 * d_z) + d_z / 2.0;
                c_z1  = (i1 * d_z) + d_z / 2.0;
            }

            if (i0 != i1 && i0 >= 0 && i0 < size_z && i1 >= 0 && i1 < size_z)
            {
                double weight = (c_z-c_z0)/(c_z1-c_z0);

                /* update all values */
                for (int x=0;x<size_x;x++)
                {
                    for (int y=0;y<size_y;y++)
                    {
                        tmp_content[x+y*size_x+i*size_x*size_y] = content[x+y*size_x+i0*size_x*size_y] + weight * (content[x+y*size_x+i1*size_x*size_y] - content[x+y*size_x+i0*size_x*size_y]);
                    }
                }
            }
        }

        d_z = new_d_z;
        size_z = new_size_z;
        imagesize = size_x * size_y * size_z;
        content.clear();
        for (int i=0;i<imagesize;i++)
        {
            content.push_back(tmp_content[i]);
        }
        tmp_content.clear();
    }
}


/**
 * @brief image::concat_motion_fields_x
 * @param u
 * @param v
 */
void image::concat_motion_fields_x(image u, image v)
{
    image w = u;
    w.set_zero();

    concat_motion_fields_x(u,v,w);
}


/**
 * @brief image::concat_motion_fields_x
 * @param u
 * @param v
 * @param w
 */
void image::concat_motion_fields_x(image u, image v, image w)
{
    /* handle x dimension */
    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] += -(size_x/2.0-0.5)*d_x + x*d_x;
            }
        }
    }

    apply_motion_fields(u,v,w);

    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] -= -(size_x/2.0-0.5)*d_x + x*d_x;
            }
        }
    }
}


/**
 * @brief image::concat_motion_fields_y
 * @param u
 * @param v
 */
void image::concat_motion_fields_y(image u, image v)
{
    image w = u;
    w.set_zero();

    concat_motion_fields_y(u,v,w);
}


/**
 * @brief image::concat_motion_fields_y
 * @param u
 * @param v
 * @param w
 */
void image::concat_motion_fields_y(image u, image v, image w)
{
    /* handle y dimension */
    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] += -(size_y/2.0-0.5)*d_y + y*d_y;
            }
        }
    }

    apply_motion_fields(u,v,w);

    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] -= -(size_y/2.0-0.5)*d_y + y*d_y;
            }
        }
    }
}


/**
 * @brief image::concat_motion_fields_z
 * @param u
 * @param v
 */
void image::concat_motion_fields_z(image u, image v)
{
    image w = u;
    w.set_zero();

    concat_motion_fields_z(u,v,w);
}


/**
 * @brief image::concat_motion_fields_z
 * @param u
 * @param v
 * @param w
 */
void image::concat_motion_fields_z(image u, image v, image w)
{
    /* handle z dimension */
    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] += -(size_z/2.0-0.5)*d_z + z*d_z;
            }
        }
    }

    apply_motion_fields(u,v,w);

    for (int z=0;z<size_z;z++)
    {
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                content[x+y*size_x+z*size_x*size_y] -= -(size_z/2.0-0.5)*d_z + z*d_z;
            }
        }
    }
}


/**
 * @brief image::apply_motion_fields
 * @param u
 * @param v
 */
void image::apply_motion_fields(image u, image v)
{
    image w = image(size_x,size_y,size_z);
    apply_motion_fields(u,v,w);
}


/**
 * @brief image::apply_motion_fields
 * @param u
 * @param v
 * @param w
 */
void image::apply_motion_fields(image u, image v, image w)
{
    /* initialize with old image */
    vector<double> tmp_content;
    for (int i=0;i<imagesize;i++)
    {
        tmp_content.push_back(0.0);
    }

    /* 2D case */
    if (size_z == 1)
    {
#pragma omp parallel for
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                double length;
                double xn,yn;
                double xn2,yn2;
                double xL,xR,yL,yR;
                int coordx,coordy;

                /* motion in x-direction */
                xn = u.get_value(x+y*size_x)/d_x;
                xn2 = fabs(xn)-floor(fabs(xn));
                if (xn < 0.0)
                {
                    xL = xn2;
                    xR = 1.0-xL;
                }
                else
                {
                    xR = xn2;
                    xL = 1.0-xR;
                }

                /* motion in y-direction */
                yn = v.get_value(x+y*size_x)/d_y;
                yn2 = fabs(yn)-floor(fabs(yn));
                if (yn < 0.0)
                {
                    yL = yn2;
                    yR = 1.0-yL;
                }
                else
                {
                    yR = yn2;
                    yL = 1.0-yR;
                }

                if (x+xn >= 0.0 && x+xn <= size_x-1 && y+yn >= 0.0 && y+yn <= size_y-1)
                {
                    /* [L,L] */
                    length = xL*yL;
                    coordx = int(floor(x+xn));
                    coordy = int(floor(y+yn));

                    if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y)
                    {
                        tmp_content[x+y*size_x] += content[coordx+coordy*size_x] * length;
                    }

                    /* [R,L] */
                    length = xR*yL;
                    coordx = int(ceil(x+xn));
                    coordy = int(floor(y+yn));

                    if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y)
                    {
                        tmp_content[x+y*size_x] += content[coordx+coordy*size_x] * length;
                    }

                    /* [L,R] */
                    length = xL*yR;
                    coordx = int(floor(x+xn));
                    coordy = int(ceil(y+yn));

                    if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y)
                    {
                        tmp_content[x+y*size_x] += content[coordx+coordy*size_x] * length;
                    }

                    /* [R,R] */
                    length = xR*yR;
                    coordx = int(ceil(x+xn));
                    coordy = int(ceil(y+yn));

                    if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y)
                    {
                        tmp_content[x+y*size_x] += content[coordx+coordy*size_x] * length;
                    }
                }
            }
        }
    }

    /* 3D case */
    if (size_z > 1)
    {
#pragma omp parallel for
        for (int z=0;z<size_z;z++)
        {
            for (int y=0;y<size_y;y++)
            {
                for (int x=0;x<size_x;x++)
                {
                    double length;
                    double xn,yn,zn;
                    double xn2,yn2,zn2;
                    double xL,xR,yL,yR,zL,zR;
                    int coordx,coordy,coordz;

                    /* motion in x-direction */
                    xn = u.get_value(x+y*size_x+z*size_x*size_y)/d_x;
                    xn2 = fabs(xn)-floor(fabs(xn));
                    if (xn < 0.0)
                    {
                        xL = xn2;
                        xR = 1.0-xL;
                    }
                    else
                    {
                        xR = xn2;
                        xL = 1.0-xR;
                    }

                    /* motion in y-direction */
                    yn = v.get_value(x+y*size_x+z*size_x*size_y)/d_y;
                    yn2 = fabs(yn)-floor(fabs(yn));
                    if (yn < 0.0)
                    {
                        yL = yn2;
                        yR = 1.0-yL;
                    }
                    else
                    {
                        yR = yn2;
                        yL = 1.0-yR;
                    }

                    /* motion in z-direction */
                    zn = w.get_value(x+y*size_x+z*size_x*size_y)/d_z;
                    zn2 = fabs(zn)-floor(fabs(zn));
                    if (zn < 0.0)
                    {
                        zL = zn2;
                        zR = 1.0-zL;
                    }
                    else
                    {
                        zR = zn2;
                        zL = 1.0-zR;
                    }

                    if (x+xn >= 0.0 && x+xn <= size_x-1 && y+yn >= 0.0 && y+yn <= size_y-1 && z+zn >= 0.0 && z+zn <= size_z-1)
                    {
                        /* [L,L,L] */
                        length = xL*yL*zL;
                        coordx = int(floor(x+xn));
                        coordy = int(floor(y+yn));
                        coordz = int(floor(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [R,L,L] */
                        length = xR*yL*zL;
                        coordx = int(ceil(x+xn));
                        coordy = int(floor(y+yn));
                        coordz = int(floor(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [L,R,L] */
                        length = xL*yR*zL;
                        coordx = int(floor(x+xn));
                        coordy = int(ceil(y+yn));
                        coordz = int(floor(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [R,R,L] */
                        length = xR*yR*zL;
                        coordx = int(ceil(x+xn));
                        coordy = int(ceil(y+yn));
                        coordz = int(floor(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [L,L,R] */
                        length = xL*yL*zR;
                        coordx = int(floor(x+xn));
                        coordy = int(floor(y+yn));
                        coordz = int(ceil(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [R,L,R] */
                        length = xR*yL*zR;
                        coordx = int(ceil(x+xn));
                        coordy = int(floor(y+yn));
                        coordz = int(ceil(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [L,R,R] */
                        length = xL*yR*zR;
                        coordx = int(floor(x+xn));
                        coordy = int(ceil(y+yn));
                        coordz = int(ceil(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }

                        /* [R,R,R] */
                        length = xR*yR*zR;
                        coordx = int(ceil(x+xn));
                        coordy = int(ceil(y+yn));
                        coordz = int(ceil(z+zn));

                        if (length > 0.0 && coordx >= 0 && coordx < size_x && coordy >= 0 && coordy < size_y && coordz >= 0 && coordz < size_z)
                        {
                            tmp_content[x+y*size_x+z*size_x*size_y] += content[coordx+coordy*size_x+coordz*size_x*size_y] * length;
                        }
                    }
                }
            }
        }
    }

    content.clear();
    for (int i=0;i<imagesize;i++)
    {
        content.push_back(tmp_content[i]);
    }
    tmp_content.clear();
}


/**
 * @brief image::smooth
 */
void image::smooth()
{
    /* this version has fixed values for kernel size and sigma */
    double sigma_x = 0.65;
    double sigma_y = 0.65;
    double sigma_z = 0.65;

    int ksize_x = 3;
    int ksize_y = 3;
    int ksize_z = 3;

    vector<double> tmp_content;
    for (int i=0;i<imagesize;i++)
    {
        tmp_content.push_back(0.0);
    }

    /* convolution in x-direction */
#pragma omp parallel for
    for (int z=0;z<size_z;z++)
    {
        double sum, norm;
        for (int y=0;y<size_y;y++)
        {
            for (int x=0;x<size_x;x++)
            {
                sum = 0.0;
                norm = 0.0;
                for (int k=-(ksize_x-1)/2;k<=(ksize_x-1)/2;k++)
                {
                    if ((k+x)>=0 && (k+x)<size_x)
                    {
                        sum += exp(-(k*k*d_x*d_x)/(2.0*sigma_x*sigma_x))*content[(x+k)+y*size_x+z*size_x*size_y];
                        norm += exp(-(k*k*d_x*d_x)/(2.0*sigma_x*sigma_x));
                    }
                }
                if (norm > 0.0)
                {
                    tmp_content[x+y*size_x+z*size_x*size_y] = sum/norm;
                }
            }
        }
    }

    for (int i=0;i<imagesize;i++)
    {
        content[i] = tmp_content[i];
        tmp_content[i] = 0.0;
    }

    /* convolution in y-direction */
#pragma omp parallel for
    for (int z=0;z<size_z;z++)
    {
        double sum, norm;
        for (int x=0;x<size_x;x++)
        {
            for (int y=0;y<size_y;y++)
            {
                sum = 0.0;
                norm = 0.0;
                for (int k=-(ksize_y-1)/2;k<=(ksize_y-1)/2;k++)
                {
                    if ((k+y)>=0 && (k+y)<size_y)
                    {
                        sum += exp(-(k*k*d_y*d_y)/(2.0*sigma_y*sigma_y))*content[x+(y+k)*size_x+z*size_x*size_y];
                        norm += exp(-(k*k*d_y*d_y)/(2.0*sigma_y*sigma_y));
                    }
                }
                if (norm > 0.0)
                {
                    tmp_content[x+y*size_x+z*size_x*size_y] = sum/norm;
                }
            }
        }
    }

    for (int i=0;i<imagesize;i++)
    {
        content[i] = tmp_content[i];
        tmp_content[i] = 0.0;
    }

    /* convolution in z-direction */
#pragma omp parallel for
    for (int x=0;x<size_x;x++)
    {
        double sum, norm;
        for (int y=0;y<size_y;y++)
        {
            for (int z=0;z<size_z;z++)
            {
                sum = 0.0;
                norm = 0.0;
                for (int k=-(ksize_z-1)/2;k<=(ksize_z-1)/2;k++)
                {
                    if ((k+z)>=0 && (k+z)<size_z)
                    {
                        sum += exp(-(k*k*d_z*d_z)/(2.0*sigma_z*sigma_z))*content[x+y*size_x+(z+k)*size_x*size_y];
                        norm += exp(-(k*k*d_z*d_z)/(2.0*sigma_z*sigma_z));
                    }
                }
                if (norm > 0.0)
                {
                    tmp_content[x+y*size_x+z*size_x*size_y] = sum/norm;
                }
            }
        }
    }

    for (int i=0;i<imagesize;i++)
    {
        content[i] = tmp_content[i];
        tmp_content[i] = 0.0;
    }
}
