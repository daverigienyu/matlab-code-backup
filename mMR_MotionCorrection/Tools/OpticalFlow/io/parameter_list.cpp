#include "parameter_list.h"


/**
 * @brief parameter_list::parameter_list
 */
parameter_list::parameter_list()
{

}


/**
 * @brief parameter_list::add_parameter_double
 * @param name
 * @param value
 */
void parameter_list::add_parameter_double(string name, double value)
{
    vector_name.push_back(name);
    vector_value.push_back(parameter(value));
}


/**
 * @brief parameter_list::add_parameter_double_vector
 * @param name
 * @param value
 */
void parameter_list::add_parameter_double_vector(string name, vector<double> value)
{
    vector_name.push_back(name);
    vector_value.push_back(parameter(value));
}


/**
 * @brief parameter_list::get_parameter_double
 * @param name
 * @return
 */
double parameter_list::get_parameter_double(string name)
{
    return parameter_list::get_parameter_double(name,-1.0);
}


/**
 * @brief parameter_list::get_parameter_double
 * @param name
 * @param default_value
 * @return
 */
double parameter_list::get_parameter_double(string name, double default_value)
{
    double result = default_value;
    vector<string>::iterator iter;

    iter = find(vector_name.begin(), vector_name.end(),name);

    if (iter != vector_name.end())
    {
        int position = distance(vector_name.begin(),iter);
        result = vector_value.at(position).get_parameter_double();
    }

    return result;
}


/**
 * @brief parameter_list::get_parameter_double_vector
 * @param name
 * @return
 */
vector<double> parameter_list::get_parameter_double_vector(string name)
{
    return get_parameter_double_vector(name,-1.0,1);
}


/**
 * @brief parameter_list::get_parameter_double_vector
 * @param name
 * @param default_value
 * @param length
 * @return
 */
vector<double> parameter_list::get_parameter_double_vector(string name, double default_value, int length)
{
    vector<double> result;
    for (int i=0;i<length;i++)
    {
        result.push_back(default_value);
    }
    vector<string>::iterator iter;

    iter = find(vector_name.begin(), vector_name.end(), name);

    if (iter != vector_name.end())
    {
        int position = distance(vector_name.begin(), iter);
        result = vector_value.at(position).get_parameter_double_vector();
    }

    return result;
}


/**
 * @brief parameter_list::get_parameter_int
 * @param name
 * @return
 */
int parameter_list::get_parameter_int(string name)
{
    return parameter_list::get_parameter_int(name,-1);
}


/**
 * @brief parameter_list::get_parameter_int
 * @param name
 * @param default_value
 * @return
 */
int parameter_list::get_parameter_int(string name, int default_value)
{
    int result = default_value;
    vector<string>::iterator iter;

    iter = find(vector_name.begin(), vector_name.end(), name);

    if (iter != vector_name.end())
    {
        int position = distance(vector_name.begin(),iter);
        result = int(vector_value.at(position).get_parameter_double());
    }

    return result;
}
