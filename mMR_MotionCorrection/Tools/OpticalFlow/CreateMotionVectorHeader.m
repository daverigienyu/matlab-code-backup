% THIS HAS BEEN EDITED BY DAVE TO FIX THE FOLDER NAMING SO THAT IT REFLECTS
% THE CORRECT TARGET GATE:
%
% The naming should be like "gates_targetGate_g"
%
% CreateMotionVectorHeader.m
function [] = CreateMotionVectorHeader(targetfolder,targetGate,vecs,vecsInv)

gates = size(vecs,5);

for g=1:gates
    full_folder = sprintf('%s/vectors/%d_%d_%d', targetfolder, gates, targetGate, g);
    if (exist(full_folder,'dir') ~= 7)
        mkdir(full_folder);
    end
    
    u = vecs(:,:,:,1,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/DefFieldX.v.hdr',full_folder),u);
    v = vecs(:,:,:,2,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/DefFieldY.v.hdr',full_folder),v);
    w = vecs(:,:,:,3,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/DefFieldZ.v.hdr',full_folder),w);
    
    u = vecsInv(:,:,:,1,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/invDefFieldX.v.hdr',full_folder),u);
    v = vecsInv(:,:,:,2,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/invDefFieldY.v.hdr',full_folder),v);
    w = vecsInv(:,:,:,3,g);
    CreateMotionVectorHeader_SingleVector(sprintf('%s/invDefFieldZ.v.hdr',full_folder),w);
end

end

