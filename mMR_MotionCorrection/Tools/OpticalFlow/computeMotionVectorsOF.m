% Created by David Rigie


function [U,V,W,parm_OF] = computeMotionVectorsOF(volTarget, vol, varargin)

    %//////////////////////////////////////////////////////////////////////////
    %// -------------------- Parse Input Arguments --------------------------//
    %//////////////////////////////////////////////////////////////////////////

    p = inputParser;

    p.addRequired('volTarget', @isVolumeArray);
    p.addRequired('vol', @isVolumeArray);
    
    p.addParameter('I_ITERATIONS',               15,    @isScalarInteger);
    p.addParameter('O_ITERATIONS',               1,     @isScalarInteger);
    p.addParameter('LEVEL',                      20,    @isScalarInteger);
    p.addParameter('LEVEL_RATIO',                0.9,   @isscalar);
    p.addParameter('ALPHA',                      40.0,  @isscalar);
    p.addParameter('BETA1',                      2.0,   @isscalar);
    p.addParameter('BETA2',                      1.0,   @isscalar);
    p.addParameter('SMOOTH_BEFORE_DOWNSAMPLING', 'true',  @isTrueOrFalse);
    p.addParameter('SMOOTH_AFTER_UPSAMPLING',    'false', @isTrueOrFalse);
    
    
    p.addParameter('IMAGE_RESCALE_CONSTANT', 100,       @isscalar);
    p.addParameter('SIZE_X',                 0,         @isScalarInteger);
    p.addParameter('SIZE_Y',                 0,         @isScalarInteger);
    p.addParameter('SIZE_Z',                 0,         @isScalarInteger);
    p.addParameter('D_X',                    4.17252,   @isscalar);
    p.addParameter('D_Y',                    4.17252,   @isscalar);
    p.addParameter('D_Z',                    2.03125,   @isscalar);
   
    if ~isequal(size(volTarget), size(vol))
        error('Volumes must have the same size!');
    end
    
    p.parse(volTarget, vol, varargin{:});
    
    
    %//////////////////////////////////////////////////////////////////////////
    %// -------------------- Preprocess Input for OF-------------------------//
    %//////////////////////////////////////////////////////////////////////////
    
    parm_OF = p.Results;
    parm_OF = rmfield(parm_OF, 'volTarget');
    parm_OF = rmfield(parm_OF, 'vol');
    parm_OF = rmfield(parm_OF, 'IMAGE_RESCALE_CONSTANT');
    parm_OF.SMOOTH_BEFORE_DOWNSAMPLING = str2logical(parm_OF.SMOOTH_BEFORE_DOWNSAMPLING);
    parm_OF.SMOOTH_AFTER_UPSAMPLING = str2logical(parm_OF.SMOOTH_AFTER_UPSAMPLING);
    
    
    % Automatically set size params based on volTarget size
    if parm_OF.SIZE_X == 0
        parm_OF.SIZE_X = size(volTarget,1);
    end
    if parm_OF.SIZE_Y == 0
        parm_OF.SIZE_Y = size(volTarget,2);
    end
    if parm_OF.SIZE_Z == 0
        parm_OF.SIZE_Z = size(volTarget,3);
    end
    
    % Rescale volumes [0>>IMAGE_RESCALE_CONSTANT] and convert to double
    volTarget = double(p.Results.volTarget); 
    vol = double(p.Results.vol);
    volTarget = p.Results.IMAGE_RESCALE_CONSTANT*mat2gray(volTarget);
    vol = p.Results.IMAGE_RESCALE_CONSTANT*mat2gray(vol);
    
    %//////////////////////////////////////////////////////////////////////////
    %// -------------------- PERFORM OPTICAL FLOW ---------------------------//
    %//////////////////////////////////////////////////////////////////////////
    
    
    % Compute optical flow using C++ function
    tstart = tic;
    fprintf('\nBeginning OF Computation...');
    
    [U,V,W] = OF_CalculateFlow_NEW(parm_OF,volTarget,vol);
    
    telapsed = toc(tstart);
    fprintf('   . Finished in %f seconds.\n', telapsed);
    
    % Recale from millimeters back to unitless (voxels)
    U = U/parm_OF.D_X;
    V = V/parm_OF.D_Y;
    W = W/parm_OF.D_Z;
  
end

% Validation Functions for input data

function tf = isVolumeArray(X)
    if isnumeric(X) && (ndims(X) == 3)
        tf = true;
        return
    end
    
    tf = false;

end

function tf = isScalarInteger(x)

    if isscalar(x) && (floor(x)==x) && (x>=0)
        tf = true;
        return;
    end
    
    tf = false;
end

function tf = isTrueOrFalse(x)

    if strcmpi(x,'TRUE') || strcmpi(x,'FALSE')
        tf = true;
    else
        tf = false;
    end
        
end

function tf = str2logical(s)

    if strcmpi(strtrim(s),'TRUE')
        tf = 1;
    else
        tf = 0;
    end

end
