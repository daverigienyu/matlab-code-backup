function [] = OF_MotionEstimation_Dave(reconpath, ...
                                       targetGate, ...
                                       targetFolder, ...
                                       parm_OF_filepath)


    if nargin < 4
        parm_OF_filepath = fullfile( fileparts( mfilename('fullpath') ), ...
                                     'parm_OF_default.yaml' );
    end
                                   
    % Load parameters
    parm_OF = YAML.read(parm_OF_filepath)

    % Load voxel data (already assumed to be gridded properly)
    MF = load(reconpath);
    voxelData = MF.data;
    clear MF;
    
    dims = size(voxelData);
    numGates = dims(4);
    
    vecs = zeros([dims(1),dims(2),dims(3),3,numGates]);
    vecsInv = 0.0*vecs;
    
    volTarget = voxelData(:,:,:,targetGate);
    
    for i=1:numGates
        fprintf('estimating motion for gate [%d]...\n',i);
        gate_tic = tic;
        
        if (i ~= targetGate)
            
            vol = voxelData(:,:,:,i);
      
            [U,V,W] = computeMotionVectorsOF(volTarget, vol, parm_OF);
            vecs(:,:,:,1,i) = U;
            vecs(:,:,:,2,i) = V;
            vecs(:,:,:,3,i) = W;
            
            [U,V,W] = computeMotionVectorsOF(vol, volTarget, parm_OF);
            vecsInv(:,:,:,1,i) = U;
            vecsInv(:,:,:,2,i) = V;
            vecsInv(:,:,:,3,i) = W;
            
            fprintf('estimating motion for gate [%d]...done. [%f]\n',...
                    i,toc(gate_tic));
        end
    
    end
    
    fprintf('\nWriting vectors to interfile');
    CreateMotionVectorHeader(targetFolder, targetGate, vecs, vecsInv);
    fprintf('\n\n\n');
     
    fprintf('\nCopy parameter file to destination folder');
    copyfile(parm_OF_filepath, targetFolder);
        
end
