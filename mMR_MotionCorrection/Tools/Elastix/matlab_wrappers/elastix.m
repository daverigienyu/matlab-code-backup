function [status, result] = elastix(varargin)

    % ALL IMAGES AND MASKS SHOULD BE SPECIFIED AS PATHS TO FILES

    THISDIR = fileparts(mfilename('fullpath'))
    DEFAULT_PARM_PATH = fullfile(THISDIR, 'parameters_default.txt');
    
    %==============================================================
    %                   PARSE INPUTS
    %--------------------------------------------------------------

    p = inputParser;
    p.addOptional('fixedImage', [], @(x) exist(x, 'file')); 
    p.addOptional('movingImage', [], @(x) exist(x, 'file'));
    
    p.addParameter('outputDir', makeResultsDir());
    p.addParameter('parameterFilePath',  DEFAULT_PARM_PATH);
    p.addParameter('fixedImageMask', []); 
    p.addParameter('movingImageMask', []);
    p.addParameter('initialTransformParameterFilePath', []);
    p.addParameter('processPriority', 'normal');
    p.addParameter('maxThreads', [], @(x) isequal(floor(x(1)), x(:)));
    p.addParameter('runInBackground', false, @islogical);
    p.addParameter('debug',           false, @islogical);
    
    p.parse(varargin{:});
    Results = p.Results;
    
    if isempty(Results.fixedImage)
        Results.fixedImage = uigetpath('*.*','Select Fixed Image');
    end
    
    if isempty(Results.movingImage)
        Results.movingImage = uigetpath('*.*','Select Moving Image');
    end
    
    if strfind(Results.parameterFilePath, 'library::')
       Results.parameterFilePath = parameterLibrary(Results.parameterFilePath); 
    end
    %_______________________________________________________________%
   
    stringcat = @(varargin) cat(2, varargin{:});
    
    command = 'elastix';
    command = stringcat(command, ' -f ', Results.fixedImage);
    command = stringcat(command, ' -m ', Results.movingImage);
    command = stringcat(command, ' -out ', Results.outputDir);
    command = stringcat(command, ' -p ', Results.parameterFilePath);
    
    
    
    if ~isempty(Results.fixedImageMask)
        command = stringcat(command, ' -fMask ', Results.fixedImageMask);
    end
    
    if ~isempty(Results.movingImageMask)
        command = stringcat(command, ' -mMask ', Results.movingImageMask);
    end
    
    if ~isempty(Results.initialTransformParameterFilePath)
        command = stringcat(command, ' -t0 ', Results.initialTransformParameterFilePath);
    end
    
    if ~isempty(Results.processPriority)
        command = stringcat(command, ' -priority ', Results.processPriority);
    end
    
    if ~isempty(Results.maxThreads)
        command = stringcat(command, ' -threads ', num2str(floor(Results.maxThreads)));
    end
    
    if  Results.runInBackground
        command = stringcat(command, '&');
    end
    
    if Results.debug
        fprintf('\n\n%s\n\n', command);
        return
    end
    
    if ~exist(Results.outputDir, 'dir')
        mkdir(Results.outputDir);
    end
    
    [status, result] = system(command, '-echo');
    
    if status > 0 
        error(result);
    end
    

end

function p = makeResultsDir()

    date_string = datestr(now, 'yyyy-mm-dd__hh-MM-ss');
    p = fullfile(pwd, ['Results_', date_string]);

end