%% Run elastix registration
fixed_image = 'mrrecon_gate01_patient3.nii';
moving_image = 'mrrecon_gate25_patient3.nii';
parm.parameterFilePath = 'parameters_elastix.txt';
parm.outputDir = 'elastix_registration_result';
elastix(fixed_image, moving_image, parm);

%% Create deformation field
transformix('elastix_registration_result', ...
            fullfile('elastix_registration_result', 'TransformParameters.0.txt'), ...
            'inputImage', 'mrrecon_gate25_patient3.nii', ...
            'deformPoints', 'all');

%% do motion estimation using Thomas' code
WORLD = 'RAS';
% First put the MR data into the expected orientation

%IVpet1 = ImageVolumeNifti('Abd_MRAC_PET_BH_NAC_Images.nii');
IVpet1  = ImageVolumeDicom('Patient03_Lung_2016_09_28-Gate-1-OP-NAC');
IVmr1 = ImageVolumeNifti('mrrecon_gate01_patient3.nii');
IVmr10 = ImageVolumeNifti('mrrecon_gate25_patient3.nii');

IVpet1.coordinateSystem = WORLD;
IVmr1.coordinateSystem = WORLD;
IVmr10.coordinateSystem = WORLD;

volTarget1 = IVmr1.regridvolume2match(IVpet1);
volTarget1 = ImageVolume.reorient(volTarget1, IVpet1.dataOrdering, 'LPI');

vol1 = IVmr10.regridvolume2match(IVpet1);
vol1 = ImageVolume.reorient(vol1, IVpet1.dataOrdering, 'LPI');




%% Run optical flow
%[U,V,W] = computeMotionVectorsOF(volTarget1,vol1);
%save('motion_vectors_OF.mat','U','V','W');

% Need data ordering as LPI
OF = load('motion_vectors_OF.mat');
vol1Warped = warpVolume(vol1, OF.U, OF.V, OF.W);

VF = VectorField(fullfile('elastix_registration_result','deformationField.nii'));
VF.coordinateSystem = WORLD;
VF2 = VF.regridvectors(IVpet1);
[Ui0,Vi0,Wi0] = VF2.getIndexedVectorfield(); 

[Ui, Vi, Wi] = VectorField.reorient(Ui0, Vi0, Wi0, VF2.dataOrdering, 'LPI');
Ui = -Ui;
Vi = -Vi;


vcorr = @(x,y) corr(x(:),y(:));
vcorr(OF.U,Ui)
vcorr(OF.V,Vi)
vcorr(OF.W,Wi)


volWarpTest = warpVolume(vol1, Ui,Vi,Wi);

%% display
figure;
showim = @(x) imshow(squeeze(x(:,:,100)),[]);
subplot(2,3,1);
showim(OF.U);

subplot(2,3,2);
showim(OF.V);

subplot(2,3,3);
showim(OF.W);

subplot(2,3,4);
showim(Ui);

subplot(2,3,5);
showim(Vi);

subplot(2,3,6);
showim(Wi);
%}
figure;
imshow(squeeze(vol1(:,floor(end/2),:))',[]); hold on;
quiver(squeeze(Wi(:,floor(end/2),:))',squeeze(Ui(:,floor(end/2),:))');

