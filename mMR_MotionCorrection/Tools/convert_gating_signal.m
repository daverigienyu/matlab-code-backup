% convert gating signal for PET reconstruction queue
clear; clc;

% filename = 'Q:\labspace\mMR_MotionCorrection\Patient3_PetRecon_Demo\gating_signal_resp_5gates_nav.mat';
% filename = 'Q:\labspace\mMR_MotionCorrection\PT-Nav\results\Patient0_Wilhelm_RespCardBiased\recon2_card_10gates\card_gating_signal_navICA_10gates.mat';
filename = 'Q:\labspace\mMR_MotionCorrection\PT-Nav\results\Patient0_Wilhelm_RespCardBiased\recon5_respcard_5by5_gates\nav_ica_respCard_dual_gating_signal_5by5_gates.mat';
filename_new = sprintf('%s_converted.mat',filename(1:end-4));

load(filename);
seconds = abs(seconds);
disp([seconds(1) seconds(2)]);

return;

if (size(new_gating_signal,2) == 2)
    offset = max(new_gating_signal(:,1));
    new_gating_signal_compressed = zeros(size(new_gating_signal,1),1);
    for i=1:numel(new_gating_signal_compressed)
        new_gating_signal_compressed(i) = new_gating_signal(i,1) + offset*(new_gating_signal(i,2)-1);
    end
    new_gating_signal = new_gating_signal_compressed;
end

LM_Gate = new_gating_signal';
LM_Time = round((seconds + seconds(2)) * 1000);
disp([seconds(1) seconds(2)]);
%save(filename_new,'LM_Gate','LM_Time');
