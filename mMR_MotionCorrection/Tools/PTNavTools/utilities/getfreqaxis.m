function ff = getfreqaxis(tt)

    N = numel(tt);
    dt = tt(2)-tt(1);
    f_samp = 1.0/dt;
    
    df = f_samp/N;

    ff = (0:df:(f_samp-df)) - (f_samp - mod(N,2)*df)/2;
    
end