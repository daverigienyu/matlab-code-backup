function [U,S,V,Mu,invFun] = compressMatrix(X, numComponents)   

    % Convert Data to Double Precision for Stability
    fprintf('\r\nConverting to double... ');
    X = double(X);

    % Default to 100 components
    if nargin < 2
        numComponents = 100;
    end

    % Calculate means for data centering
    tic;
    fprintf('\r\nCentering Data... ');
    Mu = mean(X,2);
    X = bsxfun(@minus, X, Mu);
    toc;
    
    % Compute SVD using fast randomized "rSVD" algorithm
    tic;
    fprintf('\r\nComputing SVD using fast randomized algorithm... ');
    [U,S,V] = rsvd(X, numComponents);
    toc;

    % Compute statistics about compression ratio
    info = whos('X');
    sizeX = info.bytes/1e6;
    
    totalSize = 0;
    for elem = {'U','S','V','Mu'}
       info = whos(elem{:});
       sizeElem = info.bytes/1e6;
       totalSize = totalSize + sizeElem;
    end
    
    fprintf('\r\nOriginal storage size = %fM', sizeX);
    fprintf('\r\nCompressed storage size = %fM', totalSize);
    fprintf('\r\nSaved %f%%\r\n', 100*(sizeX-totalSize)/sizeX);
        
    
end
