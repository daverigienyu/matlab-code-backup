function [data] = warpMocoDicom(mocopath, vectorspath, outpath)

    pattern = '(\d+)_(\d+)_(\d+)';
    dirlist = regexpdir(vectorspath, pattern)
    [nGates, mocoGate, targetGate] = parseVectorName(dirlist(1).name);
        
    data_moco = loadDicomSeries(mocopath);
    data = zeros([size(data_moco),nGates]);
    
    data_moco = mocoFlipAxes(data_moco);
    
    getpath  = @(i,j) fullfile(vectorspath, ... 
                               strjoin({num2str(nGates), ...
                                        num2str(i), ...
                                        num2str(j)},...
                                                   '_'));
                                               
    for iGate = 1:nGates
        fprintf('\nWarping %i to %i ...', mocoGate, iGate);
        tic;
        [U,V,W] = readDefField(getpath(mocoGate, iGate), true);
        D = cat(4,V,U,W);
        datawarp = imwarp(data_moco, D);
        datawarp = flip(swapaxes(datawarp, 1, 2),3);
        data(:,:,:,iGate) = datawarp;
        toc; 
        fprintf('\n');
    end
    
    if nargin > 2
       [dname, fname, ext] =  fileparts(outpath);
       outpath = fullfile(dname, [fname, '.mat']);
       save(outpath, 'data');
    end
        
end

function Y = mocoFlipAxes(X)

    Y = flip(X, 3);
    Y = swapaxes(Y, 1, 2);

end

function [nGates, mocoGate, targetGate] = parseVectorName(filename)

   pattern = '(\d+)_(\d+)_(\d+)';
   matches = regexp(filename, pattern, 'tokens');
   [nGates, mocoGate, targetGate] = matches{1}{:};
   
   nGates     = str2num(nGates);
   mocoGate   = str2num(mocoGate);
   targetGate = str2num(targetGate);
   

end