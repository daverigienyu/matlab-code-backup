function [datawarp, D] = warpVolume(data, vectorspath, initGate, dataTarget)

    if nargin < 4
        dataTarget = false;
    end

    pattern = '(\d+)_(\d+)_(\d+)';
    dirlist = regexpdir(vectorspath, pattern);
    [nGates, ~, targetGate] = parseVectorName(dirlist(1).name);
           
    %data = mocoFlipAxes(data);
    
    getpath  = @(i,j) fullfile(vectorspath, ... 
                               strjoin({num2str(nGates), ...
                                        num2str(i), ...
                                        num2str(j)},...
                                                   '_'));
                                               
     
    fprintf('\nWarping %i to %i ...', initGate, targetGate);
    tic;
    [U,V,W] = readDefField(getpath(targetGate, initGate), true);
    D = cat(4,-U,V,W);
    datawarp = imwarp(data, D);
    %datawarp = flip(swapaxes(datawarp, 1, 2),3);
    %data = flip(swapaxes(data, 1, 2),3);
    
    if isnumeric(dataTarget)
        fprintf('\n Change in L2-dist: %d ==> %d', ... 
                norm(data(:)-dataTarget(:)), norm(datawarp(:)-dataTarget(:)));
    end
    
    toc; 
    fprintf('\n');
    
    
end

function Y = mocoFlipAxes(X)

    Y = flip(X, 3);
    Y = swapaxes(Y, 1, 2);

end

function [nGates, initGate, targetGate] = parseVectorName(filename)

   pattern = '(\d+)_(\d+)_(\d+)';
   matches = regexp(filename, pattern, 'tokens');
   [nGates, initGate, targetGate] = matches{1}{:};
   
   nGates     = str2num(nGates);
   initGate   = str2num(initGate);
   targetGate = str2num(targetGate);
   

end