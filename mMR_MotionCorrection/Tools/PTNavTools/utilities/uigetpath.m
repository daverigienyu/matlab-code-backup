function [fullpath, filterIndex] = uigetpath(varargin)
%UIGETPATH Like uigetfile but return full filepath
%   
    [filename, dirpath, filterIndex] = uigetfile(varargin{:});
    fullpath = fullfile(dirpath,filename);

end

