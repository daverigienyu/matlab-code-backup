classdef ContinuousArray
    % Creates a data structure that behaves like an ND array but allows
    % non-integer indexing. 
    %
    % For indices that are non-integers or outside of the data range
    % interpolation/extrapolation will be used.
    
    properties(SetAccess = immutable)
        dims
        ndims
        F
        isvector
    end
    
    methods
        function obj = ContinuousArray(data, extrapMethod)
            obj.dims = size(data);
            obj.ndims = ndims(data);
            obj.isvector = isvector(data);
            
            if nargin == 2
                obj.F = griddedInterpolant(data, 'linear', ...
                                            extrapMethod);
            else
                obj.F = griddedInterpolant(data, 'linear');
            end
            
        end
        
        function varargout = subsref(obj, S)
            
            for i = 1:1:numel(S.subs)
               if S.subs{i} == ':';
                  S.subs{i} = 1:obj.dims(i); 
               end
            end
            
            if strcmp(S.type, '()')
                if (numel(S.subs) < obj.ndims) && (~obj.isvector)
                    error(sprintf('Must index all %i dimensions', obj.ndims));
                end
                varargout{1} = obj.F(S.subs);
            else
                [varargout{1:nargout}] = builtin('subsref',obj,S);
            end
        end
        
        function idx = end(obj, k, n)
            idx = obj.dims(k);
        end
    end
    
end

