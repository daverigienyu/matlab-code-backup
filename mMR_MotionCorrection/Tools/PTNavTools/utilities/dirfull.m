function filelist = dirfull(dirname)

    listing = dir(dirname);
    
    if ~isdir(dirname)
        dirname = fileparts(dirname);
    end
    
    filelist = fullfile(dirname, {listing(:).name});
    

end