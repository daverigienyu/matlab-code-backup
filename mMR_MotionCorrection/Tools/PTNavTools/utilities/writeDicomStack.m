function [] = writeDicomStack(imstack, dicomdir, seriesname, metadir)

    % imstack should be n_rows x n_cols x n_slices

    if nargin == 0
       fprintf('\nwriteDicomStack(imstack, filename)\n');
    end
    
    if nargin < 3
        seriesname = 'slice';
    end
    
    if nargin < 4
       metadir = []; 
    end
    
    % Strip extensions from dicomdir
    [x,y,z] = fileparts(dicomdir);
    dicomdir = fullfile(x,y)

    maxval = double(intmax('uint16'));

    imstack_int = uint16(mat2gray(imstack)*maxval);

    [nRow, nCol, nSlice] = size(imstack);
    
    mkdir(dicomdir);

       
    
    % Zero padding slice number
    padSize = ceil(log10(nSlice));
    
    % Find paths of dicoms to extract metainformation from
    if ~isempty(metadir)
       metalist = dir(metadir);
       metalist = metalist(3:end);
       metalist = fullfile(metadir, sort({metalist.name}));     
    
       firstSliceData = dicomread(metalist{1});
       dataClass = class(firstSliceData);
    
    else
       metalist = [];
    end
    
    for iSlice = 1:1:nSlice
               
        filename = sprintf(['%s_%0', num2str(padSize), 'i.dcm'], seriesname, iSlice)
        filepath = fullfile(dicomdir, filename);
        
        if isempty(metalist)
            dicomwrite( imstack_int(:,:,iSlice), ...  
                        filepath );
        else
            metadata = dicominfo(metalist{iSlice});
            
            im = imstack_int(:,:,iSlice);
            minVal = metadata.SmallestImagePixelValue;
            maxVal = metadata.LargestImagePixelValue;
            im = cast(rescaleData(im, minVal, maxVal), dataClass);
            
            dicomwrite( im, ...  
                        filepath, ...
                        metadata);     
        end
    end

end

function y = rescaleData(x, ymin, ymax)

    ymin = double(ymin);
    ymax = double(ymax);

    yrange = ymax-ymin;
    y = mat2gray(x)*yrange - ymin;

end



