function [imstack, info_arr] = loadDicomSeries(varargin)

    % If directory is array of directories load multiple dicom stacks
    if isstruct(varargin{1})
        filelist = varargin{1};
        for i = 1:1:numel(filelist)
           dirpath = filelist(i).path;
           [imstack{i}, info_arr{i}] = loadDicomSeries(dirpath, varargin{2:end});
        end
        imstack = cat(4,imstack{:});
        return;
    end

%==========================================================================
%                           Parse Inputs
%--------------------------------------------------------------------------

    p = inputParser;
    addOptional(p, 'dirpath', '', @(x) true);
    addParameter(p, 'extension', 'auto');
    addParameter(p, 'quiet', false);
    addParameter(p, 'reslice', 'auto'); % Slice interpolation based on SliceLocation field in header
    addParameter(p, 'sortbyheader', true);
    addParameter(p, 'SUV', false);
    
    parse(p, varargin{:});
   
    if isempty(p.Results.dirpath)
        dirpath = uigetdir;
    else
        dirpath = p.Results.dirpath;
    end
    
    if strcmpi(p.Results.reslice, 'auto')
        if nargout < 2
            doReslice = true;
        else
            doReslice = false;
        end
    else
        doReslice = p.Results.reslice;
    end
            
%_________________________________________________________________________%

    filelist = dir(dirpath);
    filelist = filelist(3:end);
    filelist = fullfile(dirpath, {filelist.name});

    if strcmpi(p.Results.extension,'auto')
       fprintf('\nAutodetecting extension ...');
       ext = autodetectext(filelist); 
       fprintf('\nUsing %s', ext);
    else
        ext = p.Results.extension;
    end    
    
    indValid = ~cellfun(@isempty, regexp(filelist, ext));
    
    filelist = filelist(indValid);
          
    % Automatically determine size of image volume
    nSlice = numel(filelist);
    
    if nSlice > 0
       first_slice = dicomread(filelist{1});
       [nRow, nCol] = size(first_slice);
    end
    
    % Load slices into 3D array
    
    imstack  = zeros([nRow, nCol, nSlice]);
    clear infoarr;
        
    fprintf('\n');
        
    for i = 1:1:nSlice
        if ~p.Results.quiet
            [~, linelength] = printline('Loading Slice %3i of %3i', i, nSlice);
        end
        
        imgdata          = double(dicomread(filelist{i}));
        
        if nargout > 1
            hdr = dicominfo(filelist{i});
            info_arr(i)      = hdr;
            RescaleSlope     = double(safeloadfield(hdr, 'RescaleSlope', 1.0));
            RescaleIntercept = double(safeloadfield(hdr, 'RescaleIntercept', 0.0));

            imgdata = imgdata*RescaleSlope + RescaleIntercept;

            if p.Results.SUV == true
                imgdata = convert2SUV(imgdata, hdr);
            end
        end
        
        imstack(:,:,i) = imgdata;
        
        if i < nSlice && ~p.Results.quiet
            deleteline(linelength);
        end
        
    end
    
    if p.Results.sortbyheader
        [imstack, info_arr, slicePos] = sortDicomSlices(imstack, info_arr);
    
    
        if doReslice
            imstack = reslice(imstack, slicePos);
            fprintf('\nInterpolated image volume from %i to %i slices.\n', ... 
                    nSlice, size(imstack,3));
            if nargin > 1
                warning('dicom headers may no longer match up to pixel data. Turn off slice interpolation.');
            end
        end
    
    end
    
    fprintf('\n');
    
end

function imstack = reslice(imstack, slicePos)

    % Positions of slices extracted from dicom header
    z0 = slicePos;
    
    zmin = min(z0);
    zmax = max(z0);
    
    dz   = min(abs(diff(z0))); % Find the minimum spacing between slices
    N    = ceil(zmax-zmin)/dz + 1;
    zq   = linspace(zmin, zmax, N)'; % New slice spacing
    
    imstack = permute(imstack, [3,1,2]);
    imstack = interp1(z0, imstack, zq);
    imstack = ipermute(imstack, [3,1,2]);

end

function ext = autodetectext(filelist)

	% Returns file extension that appears the most times in filelist
	
    clear all_extensions
    for i = 1:1:numel(filelist)
       [dirpath, filename, ext] = fileparts(filelist{i});
       all_extensions{i} = ext;
    end
    
    unique_extensions = unique(all_extensions);
    
    clear counter
    for i = 1:1:numel(unique_extensions)
        counter(i) = length(find(strcmpi(unique_extensions{i}, all_extensions)));
    end
    
    [maxCount, i] = max(counter);
    
    ext = unique_extensions{i};
        
end

function [flag, linelength] = printline(varargin)
    msg = sprintf(varargin{:});
    flag = fprintf('%s', msg);
    linelength = numel(msg);
end

function flag = deleteline(linelength)
    fprintf(repmat('\b',[1, linelength]));
end

function val = safeloadfield(S, fieldName, defaultVal)

    if isfield(S, fieldName)
        val = getfield(S, fieldName);
    else
        val = defaultVal;
    end

end

function sliceArr = convert2SUV(sliceArr, sliceInfo)

    % SUV conversion bases on a script provided by Christopher Glielmi

    % scale  slice of the volume to SUV values
        % get required dicom values

        % PatientWeight in kg
        PatientWeight = sliceInfo.PatientWeight;
        
        % InjectedDose in becquerel (Bq). The conversion to millicure (mCi) follows 1mCi = 3.7x10^7 Bg.
        InjectedDose =  sliceInfo.RadiopharmaceuticalInformationSequence.Item_1.RadionuclideTotalDose / 10^3;
        halflife = sliceInfo.RadiopharmaceuticalInformationSequence.Item_1.RadionuclideHalfLife;
        
        InjectTimeHour = str2double(sliceInfo.RadiopharmaceuticalInformationSequence.Item_1.RadiopharmaceuticalStartTime(1:2));
        InjectTimeMin = str2double(sliceInfo.RadiopharmaceuticalInformationSequence.Item_1.RadiopharmaceuticalStartTime(3:4));
        
        ScanTimeHour = str2double(sliceInfo.SeriesTime(1:2));
        ScanTimeMin = str2double(sliceInfo.SeriesTime(3:4));
        
        DelayTimeSec = 60*(60*(ScanTimeHour-InjectTimeHour)+(ScanTimeMin-InjectTimeMin));
        
        if (DelayTimeSec > 12*60*60)
            DelayTimeSec = DelayTimeSec - 12*60*60;
        end
        
        Decay = 0.5^(DelayTimeSec / halflife);
              
        % volume data is stored in MBq/kg.
        sliceArr = sliceArr ./ ((InjectedDose * Decay) / PatientWeight);
    
end


