function [angles, idx] = goldenAngles(start_angle, N)

    GOLDEN_RATIO = (1 + sqrt(5))/2;
    GOLDEN_ANGLE = 180.0/GOLDEN_RATIO;
    
    angles      = (0:1:N-1)*GOLDEN_ANGLE + start_angle;
    angles 		= mod(angles, 360); % map angles to [0, 360] range
    
    [temp, idx] = sort(angles);

end