function X = reshaper(X, varargin)

    nDimsOut = numel(varargin);
    dimArr   = [varargin{:}];
    
    ind = find((dimArr == -1));
    for i = 1:1:numel(ind)
       dimArr(ind(i)) = size(X,i);
    end
    
    X = reshape(X, dimArr);
end