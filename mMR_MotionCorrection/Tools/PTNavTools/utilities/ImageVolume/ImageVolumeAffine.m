classdef ImageVolumeAffine < ImageVolume
   
    properties
    end
    
    methods
       
        function obj = ImageVolumeAffine(voxelData, S, dims)
           
            % Figure out DICOM style geometry attributes from affine
            % matrix S
            
            origin = S(1:3,4);
            
            DCM = zeros(3);
            
            xx = S(1:3,1);
            dr = norm(xx);
            DCM(:,1) = xx/dr;
            
            xx = S(1:3,2);
            dc = norm(xx);
            DCM(:,2) = xx/dc;
            
            xx = S(1:3,3);
            ds = norm(xx);
            DCM(:,3) = xx/ds;
            
            voxelSpacing = [dr,dc,ds];
            
            if ~isempty(voxelData);
                dims = size(voxelData);
            end   
    
            obj = obj@ImageVolume(voxelData, origin, DCM, voxelSpacing, dims);
            
            
        end
        
    end
    
    
    
end