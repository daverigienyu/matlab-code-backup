function [] = determineTargetGateAndRenameFolders(vectorsDirpath)

    if nargin < 1
        vectorsDirpath = uigetdir();
    end
    
    % First go through all of the motion vectors and check which one has
    % a zero displacement field
    
    newTargetGate = determineTargetGate(vectorsDirpath);
    
    renameFoldersNewTarget(vectorsDirpath, newTargetGate);

end