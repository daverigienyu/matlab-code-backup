function [nGates, refGate, gate] = parseVectorDir(dirname)

    matches = regexp(dirname, '(\d+)_(\d+)_(\d+)', 'tokens');
    
    nGates  = str2num(matches{1}{1});
    refGate = str2num(matches{1}{2});
    gate    = str2num(matches{1}{3});

end