function [newTargetGate] = determineTargetGate(vectorsDirpath)

    if nargin < 1
        vectorsDirpath = uigetdir();
    end
    
    % First go through all of the motion vectors and check which one has
    % a zero displacement field
    
    infinityNorm = @(x) max(abs(x(:)));
    l2Norm       = @(x) norm(x(:));
    
    dirlist = regexpdir(vectorsDirpath, '\d+_\d+_\d+', 1);
    
    minl2Norm = inf;
    minInfNorm = inf; 
    newTargetGate = 1;
    
    for i = 1:1:numel(dirlist)
        tic;
        dirpath = dirlist(i).path;
        [nGates, targetGate, gate] = parseVectorDir(dirlist(i).name);
                
        [U,V,W] = readDefField(dirpath, false);
        x = cat(1,U(:),V(:),W(:));
        
        nrml2 = l2Norm(x);
        nrmInf = infinityNorm(x);
        
        if nrml2 < minl2Norm
            newTargetGate = gate;
            minl2Norm = nrml2;
        end
        
        if nrmInf < minInfNorm
            minInfNorm = nrmInf;
        end
        
        fprintf('\nGate %i: %d %d', gate, nrml2, nrmInf);
        toc;
        
    end
    
    fprintf('\nI think the target gate is #%i', newTargetGate);
    
    if newTargetGate == targetGate
        fprintf('\n\nTarget gate already correctly specified by folder names.\n\n');
        return
    end
    


end