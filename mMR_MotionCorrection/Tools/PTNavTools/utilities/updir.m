function p = updir(dirpath, N)

    if nargin < 2
        N = 1;
    end

    p = dirpath;

    for i = 1:1:N
        p = fileparts(p);
    end

end