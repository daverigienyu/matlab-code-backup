function [X] = makegrid(dims, spacing)

    X = zeros(dims);

    [yStep, xStep, zStep]   = dealArr(spacing);
    [nRows, nCols, nSlices] = dealArr(dims);
    
    % Z lines
    for i = 1:yStep:nRows
        for j = 1:xStep:nCols

            X(i,j,:) = 1.0;

        end
    end

    % X lines
    for i = 1:yStep:nRows
        for k = 1:zStep:nSlices

            X(i,:,k) = 1.0;

        end
    end

    % Y lines
    for j = 1:xStep:nCols
        for k = 1:zStep:nSlices

            X(:,j,k) = 1.0;

        end
    end

end

function varargout = dealArr(x)

    for i = 1:1:numel(x)
       varargout{i} = x(i); 
    end

end