function [imstack, info_arr, Pz] = sortDicomSlices(imstack, info_arr)

    IOP = [info_arr.ImageOrientationPatient];
    n   = cross(IOP(1:3,:),IOP(4:6,:));
    IPP = [info_arr.ImagePositionPatient];
    Pz  = dot(n,IPP);
    [Pz, ind] = sort(Pz);
    imstack = imstack(:,:,ind);
    info_arr = info_arr(ind);

    for i = 1:1:numel(ind)
        info_arr(i).SliceIndex = i;
    end
    
end
    

    
    



