loadArgs = {'suv',true,'extension','.ima'};

promptstruct = {'select static', 'select moco1', 'select moco2'};
clear p
for i = 1:1:3
    p{i} = uigetdir();
end

% Load dicom stack for static reocn
IVStatic = ImageVolumeDicom(p{1}, loadArgs{:});

% Load dicom stack for moco recon
IVMoco1 = ImageVolumeDicom(p{2}, loadArgs{:});

% Load dicom stack for moco recon
IVMoco2 = ImageVolumeDicom(p{3}, loadArgs{:});


% Display both in imageviewer
vol1 = regridarray(IVStatic.voxelData, IVStatic.fov);
vol2 = regridarray(IVMoco1.voxelData, IVMoco1.fov);
vol3 = regridarray(IVMoco2.voxelData, IVMoco2.fov);
imageviewer(cat(4, vol1, vol2, vol3));







