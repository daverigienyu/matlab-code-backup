function [s, w] = linfitNav(knav, yref, numSamples)
    
    % These params seem to work okay in practice
    LSQR_TOL     = 1.0e-6;
    LSQR_MAXITER = 2000;
    
    % Useful anonymous functions
    col = @(x) x(:);
    row = @(x) x(:)';
    
    if nargin < 3
        numSamples = 5000; % randomly sample subset of navigator matrix
    end
    
    % convert navigator data to 2D matrix for fitting
    [nFE, nPar, nView, nCoil] = size(knav);
    X = permute(knav, [1, 4, 2, 3]);
    X = reshape(X, [], nPar*nView);
    X = X';
    X   = cat(2, X, ones([size(X,1),1])); % Add row of ones for bias fit
    
    % Only use indices where reference signal is not NaN
    idx  = ~isnan(yref);
    Xs    = X(idx,:);
    ys    = yref(idx);
        
    % Randomly subsample data matrix and reference signal
    [Xs, I]  = datasample(Xs, numSamples); 
    ys = col(ys(I));
    
    tic;
    fprintf('\nPerforming linear regression ...\n');
    w = lsqr(Xs, ys, LSQR_TOL, LSQR_MAXITER);
    fprintf('\n');
    toc;
    
    s = row(X*w);

    

end