function [pcasig, V, d] = pcaSignals(X, varargin)
% PCASIGNALS  decompose signal matrix into principal components
%   
%   [pcasig, V, d] = pcaSignals(X) decomposes the signal matrix X via PCA. Each row of X
%   should represent some time-signal. Usually X will be a short wide
%   matrix if it is oriented properly.
%   
%   One must also specify which eigenvalues to keep. This can be done in
%   several ways. If not specified it will default to keeping all
%   eigenvalues. One may also specify only the lastEig parameter and
%   firstEig will be 1.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig' 10)
%
%   will keep the largest 10 eigenvalues. If lastEig is a non-integer
%   between 0 and 1, e.g.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig', 1e-2)
%
%   then the eigenvalues truncated such that lastEig/firstEig >= 1e-2. In
%   other words, this example would ensure that the smallest eigenvalue
%   kept was at least 1% as large as the largest eigenvalue kept. 
%
%   For safety, the function will assume that the input matrix X is
%   oriented wrong if it is not short and wide unless the optional
%   parameter 'force' is set to true, e.g.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig', 1e-2)

%//////////////////////////////////////////////////////////////////////////
%// -------------------- Parse Input Arguments --------------------------//
%//////////////////////////////////////////////////////////////////////////

p = inputParser;

p.addRequired('X', @ismatrix);
p.addOptional('firstEig', 1, @(x) floor(x)==ceil(x));
p.addOptional('lastEig', -1, @isnumeric);
p.addParameter('force', false, @islogical);
p.addParameter('plotEigs', false, @islogical);
p.addParameter('correlation', false, @islogical);

parse(p, X, varargin{:});

X = p.Results.X;
firstEig = p.Results.firstEig;
lastEig = p.Results.lastEig;


if (size(X,1) > size(X,2)) && ~p.Results.force
   warning('Matrix is tall. It may be oriented wrong.'); 
end

%//////////////////////////////////////////////////////////////////////////
%//---------------- Begin PCA -------------------------------------------//
%//////////////////////////////////////////////////////////////////////////

% Compute covariance matrix
tic; fprintf('\n\rComputing Covariance Matrix\n\r');
if p.Results.correlation 
    Kx = corrcoef(X.');
else
    Kx = cov(X.',1);
end

toc;

% Find eigenvectors and eigenvalues
if lastEig > 1 % case 1 firstEig and lastEig are just indices
    [V,D] = eigs(Kx, lastEig);
    [V,d] = sortEig(V,D);
elseif lastEig < 0 % case 2: keep all eigs
    [V,D] = eig(Kx);
    [V,d] = sortEig(V,D);
    lastEig = length(d);
else % case 3: keep eigs based on threshold of max eig.
    [V,D] = eigs(Kx, min(50,length(Kx)-1));
    [V,d] = sortEig(V,D);
        
    if d(end)/d(firstEig) > lastEig
        fprintf('\n\rneed more than 50 eigenvalues. Computing all.\n\r');
        [V,D] = eig(Kx);
    	[V,d] = sortEig(V,D);
    end
    
    lastEig = find(d./d(firstEig) < lastEig, 1); 
   
    if isempty(lastEig)
        fprintf('\n\rThreshold cannot be met. Keeping all eigs\r\n');
        lastEig = length(d);
    end 
end

d = d(firstEig:lastEig);
V = V(:,firstEig:lastEig); 

% Compute principal components

pcasig = V.'*X;

if p.Results.plotEigs
   plot(d,'-o');
   xticks(firstEig:lastEig);
   xlabel('eigenvalue index');
   ylabel('value');
end

end
    

function [V,d] = sortEig(V,D)
% Returns sorted max -> min eigenvalues and eigenvectors. Also converts
% diagonal eigenvalue matrix D to vector d

    if ndims(D) == 2
        d = diag(D);
    else
        d = D;
    end
    
    [d,idx] = sort(d, 'descend');
    
    V = V(:,idx);

end

