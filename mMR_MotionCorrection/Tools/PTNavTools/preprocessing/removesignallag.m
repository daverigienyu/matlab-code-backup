function y = removesignallag(tref, yref, t, y)
% Fix signal so that y(tref) is maximally correlated with yref(tref)
% This introduces a shift and interpolation if necessary

    tlag = determinesignallag(t, y, tref, yref);
    y = interp1(t-tlag, y, tref, 'linear', mean(y));
    

end