function [] = compressKnav(knav, numComponents, outpath)

    if nargin < 2
       fprintf('\r\nUsing default %i components...', numComponents);
       numComponents = 100; 
    end
    
    if nargin < 3
        [filename, dirname] = uiputfile('*.*', 'Select location to save MAT file');
        outpath = fullfile(dirname,filename);
    end
    
    dims = size(knav);
    
    knav = permute(knav,[1,4,2,3]);
    knav = reshape(knav,size(knav,1)*size(knav,2),[]);
    
    [U, S, V, Mu] = compressMatrix(knav, numComponents);
    
    fprintf('\r\nSaving compressed knav... '); 
    tic;
    save(outpath, 'U','S','V','Mu','dims');
    toc;

end