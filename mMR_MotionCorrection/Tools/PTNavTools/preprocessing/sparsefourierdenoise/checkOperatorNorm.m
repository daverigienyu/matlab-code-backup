function nrm = checkOperatorNorm(Afun, x)

    y = Afun(x);
    
    nrm = norm(y(:));
    
    
end