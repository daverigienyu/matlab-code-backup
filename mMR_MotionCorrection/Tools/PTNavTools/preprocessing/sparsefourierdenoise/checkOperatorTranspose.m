function delta = checkOperatorTranspose(Afun, ATfun, x, y)

    vdot = @(x,y) x(:)'*y(:);

    x = randlike(x);
    y = randlike(y);
    
    val1 = vdot(Afun(x),y);
    val2 = vdot(x,ATfun(y));
    
    delta = 2.0*abs(val1-val2)/abs(val1+val2);
    
    fprintf('\n<Ax,y> = %E', val1);
    fprintf('\n<x,ATy> = %E', val2);
    fprintf('\nRelative Error: %E', delta);
    
    fprintf('\n\n\n');
    
end