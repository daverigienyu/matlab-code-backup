function y = randlike(x)

    if isreal(x)
        y = normrnd(0, 1, size(x));
        y = cast(y, class(x));
    else
        y = complex(randlike(real(x)), randlike(imag(x)));
    end

end