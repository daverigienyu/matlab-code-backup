function gating_signal = makeAsymmetricGatingSignal(gating_signal)

    % find max freq component (ignore DC)
    gs0     = gating_signal - mean(gating_signal); 
    [~,ind] = max(abs(fft(gs0))); 
    N       = numel(gating_signal);
    % lowpass filter gating signal at double the fund freq to get smooth
    % derivative and take the sign
    [b,a]   = butter(5, 2*ind/numel(gating_signal), 'low');
    gs_lo   = filtfilt(b,a,gating_signal);
    sgn     = sign(gradient(gs_lo));
    % Make asymmetric extension of gating signal
    gating_signal = (gating_signal - min(gating_signal)).*sgn;

end