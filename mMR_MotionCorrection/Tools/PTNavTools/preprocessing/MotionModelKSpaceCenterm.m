classdef MotionModelKSpaceCenter < MotionModel
        
    properties
        T
        tResp
        tCard
        respInd
        cardInd
        centerCol
    end
        
    methods
        
        function obj = MotionModelKSpaceCenter(twix_obj)
           obj.centerCol = twix_obj.image.centerCol(1);
           obj = obj@MotionModel(twix_obj);
        end
        
        function obj = train(obj, kdata, varargin)
            
            %==============================================================
            %                   PARSE INPUTS
            %--------------------------------------------------------------
            
            p = inputParser;
            p.addRequired('kdata');
            p.addParameter('PCAAlgorithm', 'rsvd', @ischar);
            p.addParameter('EigCutoff',    1.0e-2, @isscalar);
            p.addParameter('BSSAlgorithm', 'sobi');
            p.addParameter('fastICAParams', {'epsilon', 1e-6, ...
                                             'stabilization', 'on', ...
                                             'g', 'tanh',...
                                             'approach','symm'},...
                                             @iscell);
            
            parse(p, kdata, varargin{:});
            kdata = p.Results.kdata;

            switch upper(p.Results.PCAAlgorithm)
                case 'RSVD'
                    pcafun = @pcaSignals_rsvd;
                case 'EIG'
                    pcafun = @pcaSignals;
                otherwise
                    error('Invalid PCA Algorithm. Use rsvd or eig.');
            end
            
            switch upper(p.Results.BSSAlgorithm)
                case 'PCAOnly'
                    bssfun = @(X) X;
                case 'fastICA'
                    bssfun = @(X) fastica(X, p.Results.fastICAParams);
                case 'AMUSE'
                    bssfun = @(X) amuse(X, 1);
                case 'SOBI'
                    bssfun = @(X) sobi(X, 250);
                otherwise
                    error('Invalid BSS Algorithm');
            end
                        
            %______________________________________________________________
            
            X = obj.generate2DSignalMatrix(kdata);
            
            % Dimension reduction
            [pcasig, V, d] = pcaSignals_rsvd(X, 'lastEig', p.Results.EigCutoff);
            T1 = V';
            
            % Blind Source Separation
            [S, W] = bssfun(T1*X);
            obj.T = W*T1;
            
            % Identify Resp/Card Components
            [~, idx]  = freqRankSignals(S, obj.dt, obj.wnResp);
            obj.tResp = obj.T(idx(1), :);
            obj.respInd = idx(1);
            
            [~, idx]  = freqRankSignals(S, obj.dt, obj.wnCard);
            obj.tCard = obj.T(idx(1), :);        
            obj.cardInd = idx(1);
        end
        
        function X = generate2DSignalMatrix(obj, kdata)
            
            % slice k-space center of each readout
            [nCol, nPar, nView, nCoil] = size(kdata);
            X = kdata(obj.centerCol, :, :, :);
            
            % Take FFT along partition direction
            X = abs(fft(X, [], 2));
            
            % Reshape and squeeze singleton dimension
            X = squeeze(permute(X, 1,4,2,3]));
            X = reshape(X, nPar*nCoil, []);
            
            
            % Highpass filter and mean subtraction
            X    = double(X);
            X    = obj.filterHi(X);
            X    = bsxfun(@minus, X, mean(X,2)); 
            
        end
    end
        
        
        
        
            
            
            
end
         
    

