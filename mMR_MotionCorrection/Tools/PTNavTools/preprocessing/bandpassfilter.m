function y_filtered = bandpassfilter(y, flo, fhi)

    col = @(x) x(:);

    
    if isvector(y)
        y = col(y);
    end
    N = size(y,1);
    tt = 0.5*((1:N)-1)';
    
    ff = col(getfreqaxis(tt));
    
    min(ff)
    max(ff)
    
    mask = logical((abs(ff)>=flo).*(abs(ff)<=fhi));
    
    yf = fftshift(fft(y),1);
    
    yf = bsxfun(@times, mask, yf);
    
    y_filtered = ifft(ifftshift(yf,1));
    

end