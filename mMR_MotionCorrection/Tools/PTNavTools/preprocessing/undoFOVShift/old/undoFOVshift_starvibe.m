function [kdata_unshifted, dk] = undoFOVshift(kdata, offset, normalVector, ...
                                              osfactor, fov_read, angles)
    
    %MYSTERY_NUMBER = 0.7335;

    n_read   = size(kdata,1);
    n_views = size(kdata,3);

    %phi    = (4*360) / n_views;
    %angles = mod(0:phi:(4*360), 360);
    %angles = angles(1:n_views);
    
    %angles(2:2:end) = angles(2:2:end)+180;
    
    if nargin < 6
        angles = computeAngles2(n_views, 360, 4);
    end
        
    
    if strcmpi(checkNormalVector(normalVector), 'sagittal')
        x0     = offset.dTra;  % this is where we need to add some mysterious constant about 0.73
        y0     = offset.dCor;
        z0     = offset.dSag;
    elseif strcmpi(checkNormalVector(normalVector), 'axial') %Not tested
        x0     = -offset.dSag;
        y0     = offset.dCor;
        z0     = offset.dTra;
    elseif strcmpi(checkNormalVector(normalVector), 'coronal') %Not tested
        x0     = offset.dSag;
        y0     = offset.dTra;
        z0     = offset.dCor;
    else
        disp('unrecognized imaging geometry');
        x0 = NaN;
        y0 = NaN;
        z0 = NaN;
    end
    
     
    kdata_unshifted = zeros(size(kdata));
    
    angles = reshape(angles, 1, 1, n_views, 1);
    
    dr = x0*cosd(angles) + y0*sind(angles) + z0; % Shift in mm
    
    pixels_per_mm = n_read/(osfactor*fov_read); 
    dk = dr*pixels_per_mm; % integer shift 
    
    center = (n_read/2 + 1);
    
    f = (1:1:n_read)';
    
    phase_factor = exp( bsxfun(@times, j*2*pi()*dk, (f-center)/n_read) );
    
    kdata_unshifted = bsxfun(@times, kdata, phase_factor);
        
end


function image_plane = checkNormalVector(normalVector)

    n = [normalVector.dSag, normalVector.dCor, normalVector.dTra];
    
    if all(n == [1,0,0])
        image_plane = 'sagittal';
    elseif all(n == [0,1,0])
        image_plane = 'coronal';
    elseif all(n == [0,0,1])
        image_plane = 'axial';
    else
        disp('Error, unrecognized imaging configuration');
        image_plane = -1
        
    end
                

end

