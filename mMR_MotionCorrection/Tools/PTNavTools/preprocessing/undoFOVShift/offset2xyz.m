function [x0,y0,z0] = offset2xyz(offset, normalVector)

    dealcell = @(c) deal(c{:});

    imagingPlane = determineImagingPlane(normalVector); 
    
    switch imagingPlane
        case 'sagittal'
            [x0,y0,z0] = dealcell({offset.dTra, offset.dCor, offset.dSag});
        case 'axial'
            [x0,y0,z0] = dealcell({-offset.dSag, offset.dCor, offset.dTra});
        case 'coronal'
            [x0,y0,z0] = dealcell({offset.dSag, offset.dTra, offset.dCor});
        otherwise
            warning('Unrecognized Imaging Geometry');
            [x0,y0,z0] = dealcell({0,0,0});
    end

end

