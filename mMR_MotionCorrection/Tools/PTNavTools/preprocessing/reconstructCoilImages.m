function [im] = reconstructCoilImages(datapath, outdir);

    [dirname, filename, ext] = fileparts(datapath);
    
    if strcmpi(ext,'.MAT')
        
        MF = load(datapath);
        tryFieldNames = {'kdata', 'kdata_clean'};
        
        for i = 1:1:numel(tryFieldNames)
            f = tryFieldNames{i};
            if isfield(MF,f)
                kdata = getfield(MF,f);
                break;
            end
        end
    elseif strcmpi(ext, '.DAT')
       kdata = getkdata(datapath);
    else
        error('Invalid file extension');
    end
    
    if nargin < 2
        outdir = uigetdir(pwd, 'Location of coil images');
    end
    
    nCoil = size(kdata,4);
    
    clear im;
    nFE = size(kdata,1);
    nRow = nFE/2.0;
    nCol = nRow;
    nSlice = 48;
    
    for iCoil = 1:1:nCoil
       kdata_part = kdata(:,:,:,iCoil);
       im = gridrecon(kdata_part, [nRow, nCol, nSlice]);
       save(fullfile(outdir, sprintf('coil%i.mat', iCoil)), 'im');
    end 
    
end