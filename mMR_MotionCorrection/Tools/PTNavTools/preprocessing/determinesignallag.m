function tlag = determinesignallag(t, y , tref, yref, magnitudeOnly)
% Find the lag time tlag that maximizes the correlation between y and yref.
% For example, if y and yref are the same signal but with a phase offset,
% then y(t-tlag) = yref
%

    if nargin < 5
        magnitudeOnly = false;
    end

    x1 = t;
    y1 = y;

    x2 = tref;
    y2 = yref;

    y1int = interp1(x1,y1,x2,'linear');
    
    
    y1int(isnan(y1int)) = 0;
        
    [acor, lagArr] = xcorr(y1int, y2, 'coeff');
    
    if magnitudeOnly
        acor = abs(acor);
    end
    
    figure; plot(lagArr,acor);
    [~, I] = max(acor);
    lag = lagArr(I);
    
    
    dt = x2(2)-x2(1);
    tlag = dt*lag;
    
end