function [MF] = saveGatingSignal(raw, nGates, outpath, TR)

% raw - continuous gating signal from PCA/ICA
% nGates - the number of bins to discretize the gating signal
% outpath - the filepath for the resulting .MAT file
% TR - the repetition time in seconds (provides the spacing between gate
% numbers)

if nargin < 4
    warning('Using default TR of 0.0127 seconds.');
    TR = 0.0127;
end

tt = (1-(1:numel(raw)))*TR;

[dirname, filename, ext] = fileparts(outpath);

fullpath = fullfile(dirname,[filename '.mat']);

MF = matfile(fullpath);
MF.Properties.Writable = true;
MF.gates = nGates;
MF.raw = raw;
MF.seconds = tt;
MF.new_gating_signal = SortGatingSignalAmpl(raw, nGates);
MF.Properties.Writable = false;



end

