function [pcasig, V, d] = pcaSignals(X, varargin)
% PCASIGNALS  decompose signal matrix into principal components
%   
%   [pcasig, s] = pcaSignals(X) decomposes the signal matrix X via PCA. Each row of X
%   should represent some time-signal. Usually X will be a short wide
%   matrix if it is oriented properly.
%   
%   One must also specify which eigenvalues to keep. This can be done in
%   several ways. If not specified it will default to keeping all
%   eigenvalues. One may also specify only the lastEig parameter and
%   firstEig will be 1.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig' 10)
%
%   will keep the largest 10 eigenvalues. If lastEig is a non-integer
%   between 0 and 1, e.g.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig', 1e-2)
%
%   then the eigenvalues truncated such that lastEig/firstEig >= 1e-2. In
%   other words, this example would ensure that the smallest eigenvalue
%   kept was at least 1% as large as the largest eigenvalue kept. 
%
%   For safety, the function will assume that the input matrix X is
%   oriented wrong if it is not short and wide unless the optional
%   parameter 'force' is set to true, e.g.
%
%   pcaSignals(X, 'firstEig', 1, 'lastEig', 1e-2)

%//////////////////////////////////////////////////////////////////////////
%// -------------------- Parse Input Arguments --------------------------//
%//////////////////////////////////////////////////////////////////////////

p = inputParser;

p.addRequired('X', @ismatrix);
p.addOptional('firstEig', 1, @(x) floor(x)==ceil(x));
p.addOptional('lastEig', -1, @isnumeric);
p.addParameter('force', false, @islogical);
p.addParameter('plotEigs', false, @islogical);
p.addParameter('correlation', false, @islogical);

parse(p, X, varargin{:});

X = p.Results.X;
firstEig = p.Results.firstEig;
lastEig = p.Results.lastEig;


if (size(X,1) > size(X,2)) && ~p.Results.force
   warning('Matrix is tall. It may be oriented wrong.'); 
end

%//////////////////////////////////////////////////////////////////////////
%//---------------- Begin PCA -------------------------------------------//
%//////////////////////////////////////////////////////////////////////////


% Find eigenvectors and eigenvalues
if lastEig > 1 % case 1 firstEig and lastEig are just indices
    [U,S,V,Mu] = compressMatrix(X, lastEig);
    s = diag(S);
    d = s.*s;
elseif lastEig < 0 % case 2: keep all eigs
    [U,S,V,Mu] = compressMatrix(X,min(size(X)));
    s = diag(S);
    d = s.*s;
else % case 3: keep eigs based on threshold of max eig.
    [U,S,V,Mu] = compressMatrix(X, min(50,min(size(X))));
    s = diag(S);
    d = s.*s;
        
    if d(end)/d(firstEig) > lastEig
        fprintf('\n\rneed more than 50 eigenvalues. Computing all.\n\r');
        [U,S,V,Mu] = compressMatrix(X,min(size(X)));
        s = diag(S);
        d = s.*s;
    end
    
    lastEig = find(d./d(firstEig) < lastEig, 1); 
   
    if isempty(lastEig)
        fprintf('\n\rThreshold cannot be met. Keeping all eigs\r\n');
        lastEig = length(d);
    end 
end

d = d(firstEig:lastEig);
V = U(:,firstEig:lastEig); 

% Compute principal components

pcasig = V'*X;

if p.Results.plotEigs
   plot(d,'-o');
   xticks(firstEig:lastEig);
   xlabel('eigenvalue index');
   ylabel('value');
end

end
    



