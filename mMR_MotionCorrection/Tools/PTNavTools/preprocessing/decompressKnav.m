function [knav] = decompressKdata(knavpath)


    MF = load(knavpath);
    
    knav = decompressMatrix(MF.U, MF.S, MF.V, MF.Mu);
    
    dims = MF.dims;
    
    knav = reshape(knav, dims(1), dims(4), dims(2), dims(3));
    knav = permute(knav, [1,3,4,2]);
    knav = single(knav);


end

