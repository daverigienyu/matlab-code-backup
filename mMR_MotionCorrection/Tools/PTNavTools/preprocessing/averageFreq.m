function favg = averageFreq(y, dt)

    tt   = ((1:numel(y))-1)*dt;
    ff   = getfreqaxis(tt);
    
    yf   = abs(fftshift(fft(y)));
    
    p    = yf/sum(yf);
    
    favg = sum(p.*abs(ff));
    
    figure;
    plot(ff,p)
    

end