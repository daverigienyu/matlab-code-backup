function varargout = extractGatingSignalsPT(kdata, dt, parm_path)
               
    if nargin < 3
        warning('No param file supplied, using defaults');
        thisdir = fileparts(mfilename('fullpath'));
        parm_path = fullfile(thisdir, 'PilotToneICA.parm');
    end
    
    parm = loadtxt2struct(parm_path); 
    
    kdata = abs(fft(kdata));
    [varargout{1:nargout}] = extractGatingSignals(kdata, dt, parm);
  
end
