function [fig_final, resp, card] = selectRespCard(sourcesig, dt, varargin)

%//////////////////////////////////////////////////////////////////////////
%// ------------------ PARSE INPUT OPTIONS ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    p = inputParser;
    p.addRequired('sourcesig');
    p.addRequired('dt', @isscalar);
    p.addParameter('RespFreq', [0.1, 0.7], @checkFreqBand);
    p.addParameter('CardFreq', [0.8, 1.7], @checkFreqBand);
    p.addParameter('LowpassFilterGatingSignals', true, @islogical);
       
    parse(p, sourcesig, dt, varargin{:});

%__________________________________________________________________________


[~, resp_ind, ~] = freqRankSignals(sourcesig, dt, p.Results.RespFreq);
[~, card_ind, ~] = freqRankSignals(sourcesig, dt, p.Results.CardFreq);

resp = sourcesig(resp_ind(1),:);
card = sourcesig(card_ind(1),:);
    

if p.Results.LowpassFilterGatingSignals
    
    fcut = min(0.99, 4*findfundamentalfreq(resp));
    [b,a] = butter(5, fcut, 'low');
    resp_lp = filtfilt(b,a,resp);
    
    fcut = min(0.99, 4*findfundamentalfreq(card));
    [b,a] = butter(5, fcut, 'low');
    card_lp = filtfilt(b,a,card);
    
else
    resp_lp = resp;
    card_lp = card;
end

%//////////////////////////////////////////////////////////////////////////
%// -------------------------- FINAL PLOT -------------------------------//
%//////////////////////////////////////////////////////////////////////////

tt = dt*(1:length(resp))-dt;
fig_final = figure('Position',[500,500,1500,300]);

subplot(211);

p_resp = plot(tt, resp); hold on;
set(p_resp,'Color', [1,0.75,0.75]);

p_resp_lp = plot(tt, resp_lp); 
set(p_resp_lp,'Color', [1,0,0]);

title('Respiratory Gating Signal');
xlim([100,160]);
xlabel('time (s)');

subplot(212);

p_card = plot(tt, card); hold on;
set(p_card,'Color', [0.75,0.75,1]);

p_card_lp = plot(tt, card_lp); 
set(p_card_lp,'Color', [0,0,1]);

title('Cardiac Gating Signal');
xlim([100,130]);
xlabel('time (s)');

drawnow;
resp = resp_lp;
card = card_lp;

end

