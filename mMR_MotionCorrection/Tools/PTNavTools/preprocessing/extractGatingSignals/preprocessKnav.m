function [X, V, d] = preprocessKnav(knav, dt, varargin)

%//////////////////////////////////////////////////////////////////////////
%// ------------------ PARSE INPUT OPTIONS ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    p = inputParser;
    
    p.addRequired('knav', @checkNavDims);
    p.addRequired('dt', @isscalar);
    p.addParameter('FFT', true, @islogical);
    p.addParameter('Sum', false, @islogical);
    p.addParameter('Slice', false, @islogical);
    p.addParameter('Resample', 0, @isint);
    p.addParameter('HighpassFilter', true, @islogical);
    p.addParameter('NormalizeCoils', false, @islogical);
    p.addParameter('PCAAlgorithm', 'rsvd', @ischar);
    p.addParameter('EigCutoff', 100, @isscalar);
    complexOptions = {'concatenate', 'magnitude', 'donothing'};
    p.addParameter('HandleComplex', 'magnitude', ... 
                   @(x) any(validatestring(x,complexOptions)));
    
    parse(p, knav, dt, varargin{:});
    
    knav = p.Results.knav;
    dt = p.Results.dt;
    
    if p.Results.Resample
        resampleInterval = p.Results.Resample;
    end
    
    if p.Results.Sum && p.Results.Slice
        error('Only one of Sum/Slice can be true');
    end
    
    switch upper(p.Results.PCAAlgorithm)
        
        case 'RSVD'
            pcafun = @pcaSignals_rsvd;
        case 'EIG'
            pcafun = @pcaSignals;
        otherwise
            error('Invalid PCA Algorithm. Use rsvd or eig.');
    end
  
%//////////////////////////////////////////////////////////////////////////
%// ------------------ PREPROCESS DATA --- ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    
    [n_read, n_par, n_views, n_coils] = size(knav);
    data = double(knav);
    clear knav;

    

    NAqc = n_par*n_views;
    scanTime = dt*(NAqc-1);
    tt = (0:1:(NAqc-1))*dt;
    ff = getfreqaxis(tt);

    %% Normalize coils
   
    if p.Results.NormalizeCoils
        mytic('Normalizing coils');
        for i = 1:1:n_coils
            temp = data(:,:,:,i);
            data(:,:,:,i) = data(:,:,:,i)/std(temp(:));
        end
        mytoc;
    end

    %% Create Data Matrix

    mytic('Preprocessing Data to form 2D Signal Matrix');
    
    X = permute(data, [1,4,2,3]);
    clear data;
    
    if p.Results.FFT
        X = fft(X);
    end
    
    if p.Results.Sum
        X = sum(X,1);
    elseif p.Results.Slice
        disp('slicing data');
        [~, inds] = max(sum(X,4), [], 1);
        slice_ind = mode(inds(:))
        X = X(max(1,slice_ind-5):min(slice_ind+5,end), :, :, :);
        X = sum(X,1);
    end
    
    X = reshape(X,[],NAqc);
    
    f_nyq = 0.5/dt;
    
    if ~isreal(X)
        switch p.Results.HandleComplex
            case 'concatenate'
                X = [real(X);imag(X)];
            case 'magnitude'
                X = abs(X);
            case 'donothing'
                X = X;
        end
    end

    if p.Results.HighpassFilter
        [b,a] = butter(5, 0.1/f_nyq, 'high');
        X = filtfilt(b,a,X.').';
    end
     
    if p.Results.Resample
        X = resample(X.', 1, resampleInterval).';
        tt = linspace(0,scanTime, size(X,2));
    end

    
    
    mytoc;
    
    %% Perform PCA to get top 100 components to be used by all other algorithms
    
    [X, V, d] = pcafun(X, 'lastEig', p.Results.EigCutoff);


