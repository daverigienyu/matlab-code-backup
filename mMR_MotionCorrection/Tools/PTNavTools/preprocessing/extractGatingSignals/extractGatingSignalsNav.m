function varargout = extractGatingSignalsNav(knav, dt, parm_path)
               
    if nargin < 3
        warning('No param file supplied, using defaults');
        thisdir = fileparts(mfilename('fullpath'));
        parm_path = fullfile(thisdir, 'NavICA.parm');
    end
    
    parm = loadtxt2struct(parm_path); 
    
    [varargout{1:nargout}] = extractGatingSignals(knav, dt, parm);
  
end

