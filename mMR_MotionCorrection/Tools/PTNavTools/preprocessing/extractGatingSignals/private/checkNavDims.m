function TF = checkNavDims(knav)

    if ~isnumeric(knav);
        error('knav is not numeric');
    end
    
    if ndims(knav) < 3
        error('Expected 4D array');
    end
    
    TF = true;
    
end

