function ys = rescaleSignal2Match(y, x);

    % We find a new signal ys = a*y + b, such that
    % norm(ys - x) is minimized

    A = [dot(y,y), sum(y);...
         sum(y),   numel(y)];
     
    v = [dot(x,y); sum(x)];
    
    u =  A\v;
    
    ys = y*u(1) + u(2);
    


end

