function [Sr, ind, EE] = rankSignals(S, metricFun, varargin)

    for i = 1:1:size(S,1)
        s = S(i,:);
        EE(i) = metricFun(s, varargin{:});
    end
    
    [EE, ind] = sort(EE, 'descend');
    Sr = S(ind,:);

end