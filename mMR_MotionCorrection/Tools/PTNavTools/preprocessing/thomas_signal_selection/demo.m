load('signal_matrix.mat'); % Loads matrix X into the workspace

% X - N x L matrix where each row is a time signal with L time points


%% Rank the signals in X according to frequency content in the respiratory
% range

Wn_resp = [0.1, 0.5]; % Expected Freq. in Hz
dt      = 0.0127;     % Time spacing in seconds

Xr = freqRankSignals(X, 0.0127, Wn_resp);

%% Plot top ranked component
figure;
tt = (1:length(X))*dt - dt;
plot(tt, Xr(1,:)); % Plot of top ranked signal
xlim([100,160]);

title('top ranked component');
set(gca, 'ytick', []);
xlabel('time (s)');
set(gcf, 'Position', [193 792 1047 186]);


%% Plot all components
figure;
N = size(X,1);
for i = 1:N
    subplot(N, 1, i);
    plot(tt, Xr(i,:));
    xlim([100,160]);
    if i == 1
        title('All Components Ranked');
    end
end
set(gcf,'Position',[593 108 1133 888]);
