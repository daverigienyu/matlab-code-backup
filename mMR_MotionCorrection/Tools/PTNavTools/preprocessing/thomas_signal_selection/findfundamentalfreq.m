function f0 = findfundamentalfreq(y, dt)

    if ~exist('dt', 'var')
        dt = 0.5;
    end
    
    % Remove DC value
    y = y - mean(y);
    
    % create time and frequency axes
    tt   = ((1:numel(y)) - 1)*dt;
    ff   = getfreqaxis(tt);
    
    % Find FFT peaks
    yf   = abs(fftshift(fft(y)));
    [yf_sorted, ind] = sort(yf, 'descend');
    
    f0 = abs(ff(ind(1)));
    
end