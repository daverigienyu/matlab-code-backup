classdef MotionModelPT < MotionModel
        
    properties
        T
        tResp
        tCard
        respInd
        cardInd
    end
        
    methods
        
        function obj = MotionModelPT(twix_obj_nav)
           obj = obj@MotionModel(twix_obj_nav);
        end
        
        function obj = train(obj, kdata_pt, varargin)
            
            %==============================================================
            %                   PARSE INPUTS
            %--------------------------------------------------------------
            
            p = inputParser;
            p.addRequired('kdata_pt');
            p.addParameter('BSSAlgorithm', 'sobi');
            p.addParameter('fastICAParams', {'epsilon', 1e-6, ...
                                             'stabilization', 'on', ...
                                             'g', 'tanh',...
                                             'approach','symm'},...
                                             @iscell);
            
            parse(p, kdata_pt, varargin{:});
            knav = p.Results.kdata_pt;
            
            switch upper(p.Results.BSSAlgorithm)
                case 'PCAONLY'
                    bssfun = @(X) pcaSignals(X);
                case 'FASTICA'
                    bssfun = @(X) fastica(X, p.Results.fastICAParams{:});
                case 'AMUSE'
                    bssfun = @(X) amuse(X, 1);
                case 'SOBI'
                    bssfun = @(X) sobi(X, 250);
                otherwise
                    error('Invalid BSS Algorithm');
            end
                        
            %______________________________________________________________
            
            X = obj.generate2DSignalMatrix(kdata_pt);
            
            % Blind Source Separation
            [S, W] = bssfun(X);
            obj.T = W;
            
            % Identify Resp/Card Components
            [~, idx]  = freqRankSignals(S, obj.dt, obj.wnResp);
            obj.tResp = obj.T(idx(1), :);
            obj.respInd = idx(1);
            
            [~, idx]  = freqRankSignals(S, obj.dt, obj.wnCard);
            obj.tCard = obj.T(idx(1), :);        
            obj.cardInd = idx(1);
        end
        
        function X = generate2DSignalMatrix(obj, kdata_pt)
            % Take FFT Magnitude
            X = abs(fft(kdata_pt));
            clear kdata_pt;
            
            % Reshape into 2D signal matrix
            X    = sum(X,1);
            X    = squeeze(permute(X, [1,4,2,3]));
            X    = reshape(X, size(X,1), []);
            
            % Highpass filter and mean subtraction
            X    = double(X);
            X    = obj.filterHi(X);
            X    = bsxfun(@minus, X, mean(X,2)); 
            
        end
    end
        
        
        
        
            
            
            
end
         
    

