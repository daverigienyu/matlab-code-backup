function [new_gating_signal] = SortGatingSignalAmpl(gating_signal,gates)

if (nargin ~= 2)
    disp('[new_gating_signal] = SortGatingSignalAmpl(gating_signal,gates)');
else  
    bins = 1000000;
    
    min_gating_signal = min(gating_signal(:));
    gating_signal_mod = gating_signal - min_gating_signal;
    
    max_gating_signal_mod = max(gating_signal_mod(:));
    gating_signal_mod = gating_signal_mod / max_gating_signal_mod * (bins-1);
    
    histogram = hist(gating_signal_mod,bins);
    chistogram = cumsum(histogram);
    
    gate_number = 1;
    gate_data = zeros(bins,1);
    
    new_gating_signal = zeros(size(gating_signal));
    
    for i=1:bins
        if (chistogram(i) <= gate_number*chistogram(end)/gates)
            gate_data(i) = gate_number;
        else
            gate_number = gate_number + 1;
            gate_data(i) = gate_number;
        end
    end
    
    for i=1:size(gating_signal)
        new_gating_signal(i) = gate_data(floor(gating_signal_mod(i))+1);
    end

end