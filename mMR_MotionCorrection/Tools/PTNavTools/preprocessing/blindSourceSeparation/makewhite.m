function [Xw, Q] = makewhite(X)
%MAKEWHITE transforms the input signal matrix X so that it is "white" with
%   Signals are arranged in 2D matrix X so that:
%   size(X) = [nSignals, nTimeSamples]
%
%   Each row of output Xw will have unit variance and zero-mean. The
%   covariance of Xw should be identity.
%
%   Output Q is the whitening transform, such that Xw = QX;

    if ~strcmpi(class(X), 'DOUBLE')
        warning('Promoting input data to double precision');
        X = double(X);
    end

    % subtract mean
    X = bsxfun(@minus, X, mean(X,2));

    % Prewhiten data
    [UU,S,VV]=svd(X(:,:)',0);
    Q= pinv(S)*VV';
    Xw=sqrt(size(X,2))*Q*X(:,:);