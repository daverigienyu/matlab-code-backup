function [yresp,ycard,wresp,wcard] = projectionpursuit(X, dt)

    if ndims(X) > 2
       X = abs(fft(X));
       X = permute(X,[1,4,2,3]);
       X = reshape(X,size(X,1)*size(X,2),[]);
    end

    X = double(X);
    X = bsxfun(@minus, X, mean(X,2));

    N  = size(X,2);
    tt = 0:dt:(N-1)*dt;
    ff = getfreqaxis(tt);
    fnyq = 0.5/dt;
    
    %% resp

    Wn = [0.1, 0.6];

    fwin = ones([1,N]);
    fwin(abs(ff)<Wn(1)) = 0.0;
    fwin(abs(ff)>Wn(2)) = 0.0;

    fwin = fwin(:);

    K = @(x) ifft(ifftshift(fftshift(fft(x)).*fwin));

    A  = @(w) K(X'*w);
    AT = @(w) X*(K(w));
    ATA = @(w) AT(A(w));

    w = X(:,1);
    w = rand(size(w));

    [eigval, wresp] = poweriteration(ATA,w,20,true);

    yresp = X'*wresp;

    %% card

    Wn = [0.8, 1.7];

    fwin = ones([1,N]);
    fwin(abs(ff)<Wn(1)) = 0.0;
    fwin(abs(ff)>Wn(2)) = 0.0;

    fwin = fwin(:);

    K = @(x) ifft(ifftshift(fftshift(fft(x)).*fwin));

    A  = @(w) K(X'*w);
    AT = @(w) X*(K(w));
    ATA = @(w) AT(A(w));

    w = X(:,1);
    w = rand(size(w));

    [eigval, wcard] = poweriteration(ATA,w,20,true);

    
    dot(wcard,wresp)
    
    ycard = X'*wcard;
    
    [b,a] = butter(5,0.1/fnyq,'high');
    yresp = filtfilt(b,a,yresp);
    ycard = filtfilt(b,a,ycard);
    

end


