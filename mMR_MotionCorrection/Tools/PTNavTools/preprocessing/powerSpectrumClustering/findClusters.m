function [C] = findClusters(M, thresh)

    % M is the similarity matrix
    % 0 < thresh < 1.0

    if nargin < 2
        thresh = 0.75;
    end
    
    % Convert similarity matrix to mask
    M = M > thresh;
    
    C = {};
    I = 1:size(M,2);
    for iRow = 1:size(M,1)
        idx  = I(M(iRow,:));
        if min(idx) == iRow
            C = [C, idx];
        end
    end

    

end