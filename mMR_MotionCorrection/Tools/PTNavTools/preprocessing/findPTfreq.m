function [freq, idx] = findPTfreq(kdata, method)

    switch upper(method)
        case 'AUTO'
            [freq, idx] = findPTfreqAuto(kdata);
        case 'GUI'
            [guess, idx] = findPTfreqAuto(kdata);
            fprintf('\n\n\nYellow line indicates automated guess');
            [freq, idx] = findPTfreqGUI(kdata, guess);
    end
        

end



