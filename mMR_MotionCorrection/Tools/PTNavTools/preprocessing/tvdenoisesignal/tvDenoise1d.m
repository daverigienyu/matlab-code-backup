function denoisedSignal = tvDenoise1d(signal, tvWeight, tol, maxIter, verbose)

    grad = @(x) grad1d(x);
    div = @(x) div1d(x);

    if nargin == 5
        if strcmpi(verbose,'VERBOSE');
            verbose = true;
        end
    else
        verbose = false;
    end
    
    if nargin < 4
        maxIter = 1000;
    end
    
    if nargin < 3
        tol = std(signal)/1000;
    end
    
    if nargin < 2
        tvWeight = 0.1*std(signal);
    end
    
    originalShape = size(signal);
    signal = signal(:);
    y = signal*1.0;
    x = signal*1.0;
    xbar = x*1.0;
    z = grad(x)*0.0;
     
    sigma = 1.0/4; % dual step size
    tau   = 1.0/4; % primal step size
    
    % DEFINE PROJECTION OPERATOR FOR L1 Prox
    proj = @(x) x./max(abs(x),1);
    
    tic;
    maxDiff = inf;
    for i = 1:1:maxIter
        if verbose
            costVal = 0.5*dot((x-y),(x-y)) + tvWeight*norm(grad(x),1);
            fprintf('\r\niter %i of %i: cost = %f: maxDiff = %f', i, maxIter, costVal,maxDiff);
        end
        
        z = proj(z + sigma*grad(xbar));
        x_last = x*1.0;
        x = (tvWeight*x + tau*y + tvWeight*tau*div(z))/(tau + tvWeight);
        
        theta = sqrt(tvWeight/(tvWeight + 2*tau)); % acceleration
        %theta = 1.0;
        tau = theta*tau;
        sigma = sigma/theta;
        
        xbar = x + theta*(x - x_last); % extrapolation step
        
        maxDiff = max(abs(x-x_last));
        
        if (maxDiff < tol)
            disp(i);
            break
        end
        
    end
    fprintf('\r\n');
    toc;
    denoisedSignal = x;
    denoisedSignal = reshape(denoisedSignal,originalShape);
    
    


end