function yf = respCardBandpass(dt, y, flo, fhi)

    %////// Set Defaults ////////
    
    RESP_FREQ_MIN = 0.075;
    CARD_FREQ_MAX = 2.0;
    
    if nargin < 3
        flo = RESP_FREQ_MIN; % Hz
    if nargin < 4
        fhi = CARD_FREQ_MAX; % Hz
    end
    
    % Convert relative to nyquist
    fnyq = 0.5/dt;
    [flo, fhi] = map(@(f) f/fnyq, flo, fhi);
    
    yf = bandpassfilter(y, flo, fhi);
        
        
end