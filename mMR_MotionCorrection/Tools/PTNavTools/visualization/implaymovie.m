function [fig] = implaymovie(imstack, varargin)

    %______________________________________________________________________
    %                       Parse Inputs  
    %----------------------------------------------------------------------
    nGates = size(imstack,3);
    defaultPauseTime = 1.0/nGates;
    imstack = mat2gray(imstack);
    
    p = inputParser;
    addRequired(p,  'imstack');
    
    addParameter(p, 'nLoop',     10);
    addParameter(p, 'imViewFun', @imshow);
    addParameter(p, 'pauseTime', defaultPauseTime);
    
    parse(p, imstack, varargin{:});
    %======================================================================
    
    fig = figure();
    h = p.Results.imViewFun(imstack(:,:,1)); hold on;
    
    for i = 1:1:p.Results.nLoop
        for j = 1:1:nGates 
            title(sprintf('Frame %i', j));
            set(h,'CData',imstack(:,:,j));
            drawnow;
            pause(p.Results.pauseTime);
        end
    end


end

