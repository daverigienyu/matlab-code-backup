function [] = colorkey()

    figure;
   
    x = linspace(-1,1,128);
    y = linspace(-1,1,128);
    
    [X,Y] = meshgrid(x,y);
    
    S = mat2gray(X.^2+Y.^2);
    H = mat2gray(atan2(Y,X));
    V = ones(size(H));
    
    imshow(hsv2rgb(cat(3,H,S,V)))
    
    axis square;
    title('Color Key');

end