function frame = interpolateMotionState_old(motionMatrix, i, j)

    % First interpolate in the row direction
    
    i1 = floor(i);
    i2 = ceil(i);
    j1 = floor(j);
    j2 = ceil(j);
        
    w1 = (i2-i); w2 = (1.0-w1);
    
    frameA = w1*motionMatrix(:,:,i1,j1) + w2*motionMatrix(:,:,i2,j1);
    frameB = w1*motionMatrix(:,:,i1,j2) + w2*motionMatrix(:,:,i2,j2);
    
    
    % Then interpolate in the column direction
    
    w1 = (j2-j);
    w2 = (1.0-w1);
    
    frame = w1*frameA + w2*frameB;

end