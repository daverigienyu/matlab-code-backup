 function [fig,haxes,himg] = imageviewer(imdata)


    
    imdata = squeeze(imdata);
    
    sliceDims = repmat({1},[1, ndims(imdata)-2]);
    cmin = min(imdata(:));
    
    %mask = (imdata>0);
    %cmax = mean(imdata(mask)) + 10*std(imdata(mask));
    cmax = max(imdata(:));
    
    if nargin < 6
        clims = [cmin, cmax];
    end
    fig = figure('Visible','on', 'Position', [100,100,900,600]);
    haxes = axes('Units','pixels','Position',[170,170,800*0.65,600*0.65]);
    himg = imagesc(imdata(:,:,1), 'Parent', haxes);
    %set(gca, 'XTick', []); set(gca, 'YTick', []);
    updateImageWindow();
    imColorMap = colormap;
    autoscale  = false;
          
    btn_cycle = uicontrol('Style','pushbutton','String','Cycle Dim',...
                   'Position',[50 500 60 20], 'Callback', @callback_cycle);
   
    btn_transpose = uicontrol('Style','pushbutton','String','Transpose',...
                   'Position',[50 470 60 20], 'Callback', @callback_transpose);
               
    btn_fliplr = uicontrol('Style','pushbutton','String','Flip L/R',...
                   'Position',[50 440 60 20], 'Callback', @callback_fliplr);
               
    btn_flipud = uicontrol('Style','pushbutton','String','Flip U/D',...
                   'Position',[50 410 60 20], 'Callback', @callback_flipud);
               
    btn_flipZ = uicontrol('Style','pushbutton','String','Flip Z',...
                   'Position',[50 380 60 20], 'Callback', @callback_flipZ);
    
    popup  = uicontrol('Style', 'popup',...
           'String', {'gray', 'gray_r', 'fire','jet','hsv','hot','cool','parula'},...
           'Position', [50 350 60 20],...
           'Callback', @setmap); 
       
   chkbox_autoscale = uicontrol('Style','checkbox','String','autoscale', ...
                               'Value',0,'Position',[50 320 80 20],        ...
                                'Callback',@checkBoxCallback);
                     
   btn_saveImage = uicontrol('Style','pushbutton','String','Save Image',...
                   'Position',[50 200 60 20], 'Callback', @callback_saveImage);      

    %//////////////////////////////////////////////////////////////////////
    %// ----------- Create slider for slice/time ------------------------//
    %//////////////////////////////////////////////////////////////////////

    [n_rows, n_cols, sliceDims{:}] = size(imdata);
    size(imdata)
    numSliceDims = numel(sliceDims)
    sliceIdx = {ceil(sliceDims{1}/2), };
    updateImageDisplay();  
    
    sliderHorzSpacing = 50;    
    for i = 1:1:numSliceDims
        xpos = 725 + (i-1)*sliderHorzSpacing;
        sld_dim{i} = makeSlider([], [1, sliceDims{i}], ceil(sliceDims{i}/2), sliceDims{i}, ...
                          'Position', [xpos 160 20 400], ...  
                          'Parent', fig);
        
        addlistener(sld_dim{i}, 'ContinuousValueChange', @setSlice);
    end
    
    
    %//////////////////////////////////////////////////////////////////////
    %// ----------- Create contrast sliders  ----------------------------//
    %//////////////////////////////////////////////////////////////////////

    sld_low = makeSlider([], [cmin, cmax], cmin, 1000, ...
                         'Position', [200 100 400 20]);

    addlistener(sld_low, 'ContinuousValueChange', @setclims);

    % Add a text uicontrol to label the slider.
    txt_low = uicontrol('Style','text',...
        'Position',[200 75 400 20],...
        'String','clow');

    sld_high = makeSlider([], [cmin, cmax], cmax, 1000, ...
                          'Position', [200 50 400 20]); 

    addlistener(sld_high, 'ContinuousValueChange', @setclims);

    txt_high = uicontrol('Style','text',...
        'Position',[200 25 400 20],...
        'String','chigh');

    

    set( findall( fig, '-property', 'Units' ), 'Units', 'Normalized' )

    % Make figure visble after adding all components
    colorbar();
    setmap();
    updateSliceSliders();
    f.Visible = 'on';
    
    
    
     function updateSliceSliders()
        [n_rows, n_cols, sliceDims{:}] = size(imdata);
        sliceIdx = {ceil(sliceDims{1}/2), 1, 1};
        himg = imagesc(imdata(:,:,1,1,1), 'Parent', haxes);
        updateImageDisplay();
        updateImageWindow();
        setmap();
        for i = 1:1:numel(sliceDims)
            sld_dim{i} = makeSlider(sld_dim{i}, [1, sliceDims{i}], ...
                                    ceil(sliceDims{i}/2), sliceDims{i});
            addlistener(sld_dim{i}, 'ContinuousValueChange', @setSlice);
        end
        
        if autoscale
            axis manual;
            set(haxes, 'DataAspectRatioMode', 'auto');
        else
            axis image;
        end
        
     end
    
     function updateImageWindow()
        try
            caxis(haxes, clims); % set new minimum
        catch ME
            ; 
        end
     end
    
     function updateImageDisplay()
         frame = imdata(:,:,sliceIdx{:});
         %frame = imresize(frame, 4, 'Method', 'bilinear');
         set(himg, 'CData', frame);
         title(haxes, sprintf('slice %i, gate %i, gate %i', sliceIdx{:}));
     end

     function setclims(hObject, eventdata, handles)
         clims = [sld_low.Value, sld_high.Value];
         updateImageWindow();
     end

     function setSlice(hObject, eventdata, handles)
        for iDim = 1:1:numSliceDims
            sliceIdx{iDim} = round(sld_dim{iDim}.Value);
        end
        updateImageDisplay();
     end

     function callback_cycle(src, event)
        order = 1:ndims(imdata);
        order(1:3) = circshift(order(1:3),1);
        imdata = permute(imdata, order);
        updateSliceSliders();
     end
 
     function callback_transpose(src, event)
        order = 1:ndims(imdata);
        order(1:2) = circshift(order(1:2),1);
        imdata = permute(imdata, order);
        updateSliceSliders();
     end
 
     function callback_fliplr(src, event)
        imdata = flip(imdata, 2);
        updateSliceSliders();
     end
 
    function callback_flipud(src, event)
        imdata = flip(imdata, 1);
        updateSliceSliders();
    end

    function callback_flipZ(src, event)
        imdata = flip(imdata, 3);
        updateSliceSliders();
    end

    function callback_saveImage(src, event)
        dataFrame  = imdata(:,:,sliceIdx{:});
        imageFrame = mat2gray(dataFrame, haxes.CLim);
        X   = gray2ind(imageFrame, size(imColorMap,1));
        while max(size(X)) < 512
            X = imresize(X, 2);
        end
        
        [filename, dirpath] = uiputfile('*.jpg', 'Choose Image Path');
        [dirpath, filename, ext] = fileparts(fullfile(dirpath, filename));
        imwrite(X, imColorMap, fullfile(dirpath, [filename ext]));
        save(fullfile(dirpath, [filename '.mat']), 'dataFrame');        
    end
 
    function setmap(source,event)
            val = popup.Value;
            maps = popup.String;
            newmap = maps{val};
            
            idx = strfind(newmap, '_r');
            if ~isempty(idx)
                newmap = newmap(1:(idx-1));
                newmap = flipud(eval(newmap));
            else
                newmap = eval(newmap);
            end
            
            imColorMap = newmap;
            colormap(haxes, newmap);
            drawnow;
    end
 
     function sld = makeSlider(sld, lims, val, nSteps, varargin)
        
         if nSteps == 1
             minorStep = 1;
             majorStep = 1;
         else
             minorStep = 1.0/nSteps;
             majorStep = 1.0/nSteps;
         end
         
         if isempty(sld)         
             sld = uicontrol('Style', 'slider', ...
                              'Min', lims(1), 'Max', lims(2) + eps, 'Value', val, ...
                              'SliderStep', [minorStep, majorStep], ...
                              varargin{:});
         else
            sld.Min = lims(1);
            sld.Max = lims(2) + eps;
            sld.Value = val;
            sld.SliderStep = [minorStep, majorStep];
            varargin{:};
         end
                      
         if nSteps < 2
             sld.Enable = 'off';
             sld.Visible = 'off';
         end
         
     end
 
     function checkBoxCallback(source, event)
         autoscale = chkbox_autoscale.Value;
         updateSliceSliders();
     end

 end