function im = flowImageRep(M, varargin)

    Vx = M(:,:,1);
    Vy = M(:,:,2);
 
    H = mat2gray(atan2(Vy,Vx));
    S = mat2gray(sqrt(Vx.^2 + Vy.^2));
    V = ones(size(S));
    
    im = hsv2rgb(cat(3,H,S,V));

end