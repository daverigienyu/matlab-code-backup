function [fig, axis_list] = plotSignals(x, Y, highlight, varargin)
% Y should be arranged so that size(Y,2) = numel(x)

    if nargin == 1
        Y = x;
        x = 1;
    end

    if nargin < 3
        highlight = [];
    end
    
    if size(Y,1) > 30
       warning('signal matrix too big. Plotting only first 30 components');
       Y = Y(1:30,:);
    end
    
   
    Nmax = size(Y,1);
     
    % x can either be an array of values for the x-axis, or a 2-element
    % array specifying [x_min, x_max]
    
    if numel(x) == 1
        dx = x;
        x = ((1:size(Y,2))-1)*dx;
    end
    
    if numel(x) == 2
        x_min = x(1);
        x_max = x(2);
        x = linspace(x_min, x_max, size(Y,2));
    end

    Y = Y.';
    [num_points, num_signals] = size(Y);

    color_wheel = repmat({'k'},[1,num_signals]);
    
    if numel(highlight) == 2
        color_wheel(highlight) = {'r', 'b'};
    elseif numel(highlight) == 1
        color_wheel(highlight) = {'r'};
    end
    
    
    fig = figure;

    axis_list = [];
    color_counter = 1;
    
    for i =1:1:num_signals
        ax = subtightplot(num_signals,1,i);
        axis_list = [axis_list, ax];
        plot(x, Y(:,i), 'Color', color_wheel{i}, varargin{:});
        set(ax,'ytick',[]);
        if i < num_signals
            set(ax,'xtick',[]);
        end
    end
    
    linkaxes(axis_list, 'x');

end