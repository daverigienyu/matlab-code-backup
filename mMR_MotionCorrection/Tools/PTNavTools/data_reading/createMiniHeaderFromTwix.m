function [head] = createMiniHeaderFromTwix(twixobj)

    [head1, numInvalid1] = createMiniHeaderFromTwixOld(twixobj);    
    [head2, numInvalid2] = createMiniHeaderFromTwixVE11p(twixobj);
    
    if numInvalid1 <= numInvalid2
        fprintf('\nAssuming VB');
        head = head1;
    else
        fprintf('\nAssuming VE11C');
        head = head2;
    end       
    
end

