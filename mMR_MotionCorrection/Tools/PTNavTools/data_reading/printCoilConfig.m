function s = printCoilConfig(folderpath, pattern)

    if nargin < 2
        pattern = '\.dat';
    end

    % Find raw data files
    filelist = regexpdir(folderpath, pattern, true);
    
    % Loop through scans and print coil config
    nScans = numel(filelist)
    s = '';
    for i = 1:nScans
        s = sprintf('%s================= SCAN %i =====================\n', s, i);
        filepath = filelist(i).path;
        s = sprintf('%s%s',s, printCoilConfig1(filepath));
    end
    

end

function s = printCoilConfig1(filepath)

    [~,~,ext] = fileparts(filepath);
    
    switch upper(ext)
        case '.DAT'
            twix_map_obj = mapVBVD_Nav(filepath, 'HDR');
            C = getCoilInfo(twix_map_obj.hdr);
        case '.MAT' 
            MF = matfile(filepath);
            C = getCoilInfo(MF.hdr);
        otherwise
            error('Invalid File Extension');
    end
    
    
    nCoils = numel(C)
    s = '';
    for i = 1:nCoils
       c = C(i);
       s = sprintf('%s%s\n',s, printnestedstruct(c));
    end
    s = sprintf('%s\n',s);
end