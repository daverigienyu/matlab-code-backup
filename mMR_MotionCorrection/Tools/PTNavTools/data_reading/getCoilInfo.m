function info = getCoilInfo(hdr)

    try
        info = hdr.coilInfo';
    catch ME
       warning(ME.message);
       info = [];
       return
    end

    for iCoil = 1:numel(info)
       f = fields(info{iCoil}.sCoilElementID);
       for iField = 1:numel(f)
          info{iCoil} = setfield(info{iCoil}, f{iField}, getfield(info{iCoil}.sCoilElementID, f{iField}));
       end
       info{iCoil} = rmfield(info{iCoil}, 'sCoilElementID');
    end
    
    info = cat(1,info{:}); 
    
end
