function [twix_obj] = convertTwixObj(twix_obj, dataType)

    % This function saves all of the header and MDH information from the twix file
    % and converts it to a simple MATLAB struct.
    %
    % The actual data (nav or image) will be stored in a new field called
    % 'data'. 
    %
    % twix_obj should look like:
    %
    % twix_obj = 
    %
    %   struct with fields:
    % 
    %      hdr: [1�1 struct]
    %    image: [1�1 twix_map_obj_Nav]
    % 
    
    tic;
    fprintf('\nExtracting %s data...  ', dataType);
    
    if numel(twix_obj) > 1
        twix_obj = twix_obj{end};
        warning('Found multiple scans, skipping to the last one');
    end
    
    data = [];
    
    switch upper(dataType)
        case 'IMG'
            ecoIdx = 1;
        case 'NAV'
            ecoIdx = 2;    
    end

    idx = build_slice_idx(twix_obj, ecoIdx);
    data = squeeze( twix_obj.image( idx{:} ) );

    
    data = permute(data,[1 4 3 2]);
    fprintf('Done.\n\n');
    toc;
    
    tic;
    fprintf('\nConverting from twix_map_obj to MATLAB struct...  ');
    %twix_obj.hdr = twix_obj.hdr;
    twix_obj.image = copyAllFields(twix_obj.image);
    twix_obj = copyAllFields(twix_obj);
    twix_obj.data = data;
    twix_obj.time_milliseconds = getTimestampMilliseconds(twix_obj);
    twix_obj.time_milliseconds = twix_obj.time_milliseconds(twix_obj.image.Eco == ecoIdx); 
    fprintf('Done.\n\n');
    toc;
    
end

function idx = build_slice_idx(twix_obj, ecoIdx)

    dataDims = twix_obj.image.dataDims;
    
    clear idx;
    for i = 1:1:numel(dataDims)
       d = dataDims{i};
       if strcmpi(d,'ECO')
           idx{i} = ecoIdx;
       else
           idx{i} = ':';
       end
    end
    

end
