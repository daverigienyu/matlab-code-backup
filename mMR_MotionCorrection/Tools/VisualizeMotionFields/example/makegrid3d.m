function g = makegrid3d(dims, gridSpacing)


    g = zeros(dims);
    
    clear idx
    
    for iDim = 1:3
       idx{iDim} = gridSpacing(iDim):gridSpacing(iDim):dims(iDim);  
    end
    
    % x-lines
    g(idx{1}, :, idx{3}) = 1.0;
    
    % y-lines
    g(:, idx{2}, idx{3}) = 1.0;
    
    % z-lines
    g(idx{1}, idx{2}, :) = 1.0;
    
end