function [kx,ky,kz] = stackOfStarsTraj(nCol, nSlice, nView, nPar, fovXY, fovZ)

    [traj3d] = goldenAngleStackOfStars(nCol,nSlice,nView,nPar);
    kx = traj3d(:,1);
    ky = traj3d(:,2);
    kz = traj3d(:,3);
    
    % Initially [kmin, kmax] = [0.5, 0.5]
    kx  = kx*nCol/fovXY;
    ky  = ky*nCol/fovXY;
    kz  = kz*nSlice/fovZ;   

end