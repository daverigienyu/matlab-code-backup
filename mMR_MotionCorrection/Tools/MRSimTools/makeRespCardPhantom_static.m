function DynamicPhantom = makeRespCardPhantom()
    %% CONSTANTS DETERMINING SIZE ETC
    TORSO_X0 = 0;
    TORSO_Y0 = 0;
    TORSO_Z0 = 0;

    TORSO_AP_SIZE = 200;
    TORSO_HF_SIZE = 300;
    TORSO_LR_SIZE = 250;

    TORSO_A  = 1.0e-6;

    HEART_X0 = -75;
    HEART_Y0 = 160;
    HEART_Z0 = 0;

    HEART_AP_SIZE = 100;
    HEART_HF_SIZE = 75;
    HEART_LR_SIZE = 50;

    HEART_YAW   = -20;
    HEART_PITCH = 0;
    HEART_ROLL  = 0;

    HEART_A = 1.0e-6;

    %% SPECIFY RESPIRATORY AND CARDIAC MOTION

    RESP_AP_MOTION = 0;
    RESP_HF_MOTION = 0;
    RESP_LR_MOTION = 0;

    CARD_CONTRACTION_RATIO = 0.0;

    MF = load('physio_templates.mat');
    respSurrogate = griddedInterpolant(MF.time_milliseconds, 2*mat2gray(MF.resp)-1, 'spline');
    cardSurrogate = griddedInterpolant(MF.time_milliseconds, 2*mat2gray(MF.card)-1, 'spline');

    motionPath.x0 = @(t) 0.25*RESP_AP_MOTION*respSurrogate(t);
    motionPath.y0 = @(t) 0.25*RESP_HF_MOTION*respSurrogate(t);
    motionPath.z0 = @(t) 0.25*RESP_LR_MOTION*respSurrogate(t);

    growthPattern = @(t) 1.0 - 0.5*CARD_CONTRACTION_RATIO*(cardSurrogate(t)+1);

    %%

    S = struct();

    S.x0 = TORSO_X0;
    S.y0 = TORSO_Y0;
    S.z0 = TORSO_Z0;

    S.rx = TORSO_AP_SIZE/2;
    S.ry = TORSO_HF_SIZE/2;
    S.rz = TORSO_LR_SIZE/2;

    S.A = TORSO_A;

    TorsoObject = Ellipse(S);

    S = struct();

    S.x0 = HEART_X0;
    S.y0 = HEART_Y0;
    S.z0 = HEART_Z0;

    S.rx = HEART_AP_SIZE/2;
    S.ry = HEART_HF_SIZE/2;
    S.rz = HEART_LR_SIZE/2;

    S.alpha = HEART_YAW;
    S.beta  = HEART_PITCH;
    S.gamma = HEART_ROLL;

    S.A = HEART_A;

    HeartParams = S;

    HeartObject = Ellipse(S);

    Phant = [HeartObject, TorsoObject];

    %% Test phantom quickly
    %{
    fov = 349*2;
    target_dx  = 1.3633*2;
    N = ceil(fov/target_dx);
    dx = fov/N;
    xx = linspace(-fov/2, fov/2, N);
    kk = getfreqaxis(xx);

    [kx,ky,kz] = meshgrid(kk, kk, kk);

    fprintf('\nSampling k-space...'); 
    tic;
    val = multiObjectFT(Phant, kx, ky, kz);
    toc;

    fprintf('\niFFT Recon ...'); 
    tic;
    I   = abs(fftshift(ifftn(ifftshift(val))));
    toc;
    imageviewer(flipdim(I,1));

    %% view
    imageviewer(flipdim(I,1));
    %}

    %% Create Phantom Dynamically

    DynamicPhantom = @(t) [transformObject(TorsoObject, motionPath, @(t) 1, t), ...
                           transformObject(HeartObject, motionPath, growthPattern, t)];


end




