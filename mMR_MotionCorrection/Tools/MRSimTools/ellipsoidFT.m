
% Now to make an arbitrary ellipsoid we need
%     stretch
%     rotate
%     shift

function [val] = ellipsoidFT(kx, ky, kz, ...
                           rx, ry, rz, ...
                           alpha, beta, gamma, ...
                           x0, y0, z0)
   % Stretching Matrix
   S = diag(1.0./[rx,ry,rz]);
   
   % Rotation Matrix
   R1 = [1, 0, 0; ...
         0, cosd(gamma), -sind(gamma);...
         0, sind(gamma), cosd(gamma)];
   R2 = [cosd(beta), 0, sind(beta); ...
         0, 1, 0; ...
         -sind(beta), 0, cosd(beta)];
   R3 = [cosd(alpha), -sind(alpha), 0;...
         sind(alpha), cosd(alpha), 0; ...
         0, 0, 1];
   R = R3*R2*R1;
   
   % Overall transformation
   A = S*R;
      
   % calculate composite FT of shifted stretched rotated ellipsoid
   %shiftedSphere = @(kx,ky,kz) shiftedFT(@unitSphereFT, kx,ky,kz, x0,y0,z0);
   %val = linearTransformedFT(shiftedSphere, kx, ky, kz, A);
    linearTransformedSphereFT = @(kx,ky,kz) linearTransformedFT(@unitSphereFT, kx,ky,kz,A);
    val = shiftedFT(linearTransformedSphereFT, kx, ky, kz, x0,y0,z0);
   
end

function val = unitSphereFT(kx, ky, kz)

    K = sqrt(kx.*conj(kx) + ky.*conj(ky) + kz.*conj(kz));
    K = max(K, eps);
    
    val = (sin(2.*pi().*K) - 2.*pi().*K.*cos(2*pi().*K))./(2.*pi().^2.*K.^3);
    

end

function val = shiftedFT(fun, kx, ky, kz, x0, y0, z0)

    phaseshift = exp(-i()*2*pi*kx.*x0).*exp(-i()*2*pi*ky.*y0).*exp(-i()*2*pi*kz.*z0);
    val = phaseshift.*fun(kx,ky,kz);

end

function val = linearTransformedFT(fun, kx, ky, kz, A)

    originalShape = size(kx);
    
    AinvT = inv(A)';
    
    K      = cat(2, kx(:), ky(:), kz(:)).';
    Kt     = AinvT*K;
    
    kxt    = Kt(1,:);
    kyt    = Kt(2,:);
    kzt    = Kt(3,:);
    
    val = 1.0/abs(det(A))*fun(kxt, kyt, kzt);
    %val = fun(kxt, kyt, kzt);
    
    val = reshape(val, originalShape);

end