function [kx,ky,kz] = generateRosetteTraj(f1, f2, tt)

    if nargin == 0
       tt = linspace(0,2,704);
       tmax = max(tt);
       f1 = 2*pi()/tmax;
       f2 = 7/3*f1;
    end
        
    kx = sin(2*pi()*f1*tt).*cos(2*pi()*f2*tt);
    ky = sin(2*pi()*f1*tt).*sin(2*pi()*f2*tt);
    kz = ky*0.0;
     
end