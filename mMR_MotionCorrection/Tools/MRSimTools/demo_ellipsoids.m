kk = linspace(-5,5,128);
[kx,ky,kz] = meshgrid(kk);

dk = kk(2)-kk(1);

xmax = 0.5/dk

Ellipse1 = Ellipse()
Ellipse2 = Ellipse('rx',2,'rz',1,'x0',3,'alpha',20,'beta',20,'gamma',15);

val = multiEllipsoidFT(kx,ky,kz,[Ellipse1, Ellipse2]);

I = fftshift(abs(ifftn(ifftshift(val))));

project = @(im, dim)  squeeze(sum(im,dim));

Iproj = cat(3, project(I,1), project(I,2), project(I,3));
imageviewer(Iproj);
