function val = unitSphereFT(kx, ky, kz)

    length_k = sqrt(kx.*conj(kx) + ky.*conj(ky) + kz.*conj(kz));
    length_k = max(length_k, eps);
    
    R = 1;
    sincn = @(n,x) gamma(1 + n/2).*besselj(n/2, x).*(x/2).^(-n/2);
    val = (R^3)*(2*pi^1.5)/(3*gamma(1.5))*sincn(3,2*pi*length_k*R);

end