classdef Ellipse < PhantomPrimitive
    
    properties
        
        id 
        
        rx
        ry
        rz
        
        x0
        y0
        z0
        
        alpha
        beta
        gamma
        
        A
                        
    end
    
    methods
        function this = Ellipse(varargin)
           
        %//////////////////////////////////////////////////////////////////////////
        %// ------------------ PARSE INPUT OPTIONS ------------------------------//
        %//////////////////////////////////////////////////////////////////////////
        p = inputParser;
 
        % Location
        p.addParameter('id', 'None');
        
        % Location
        p.addParameter('x0', 0);
        p.addParameter('y0', 0);
        p.addParameter('z0', 0);
        
        % Radii
        p.addParameter('rx', 1);
        p.addParameter('ry', 1);
        p.addParameter('rz', 1);
        
        % Rotation angles (pitch, yaw, roll)
        p.addParameter('alpha', 0);
        p.addParameter('beta', 0);
        p.addParameter('gamma', 0);
        
        % Constant specifying intensity
        p.addParameter('A', 1.0);
   
        parse(p, varargin{:});
        
        this.id = p.Results.id;
        this.x0 = p.Results.x0;
        this.y0 = p.Results.y0;
        this.z0 = p.Results.z0;
        this.rx = p.Results.rx;
        this.ry = p.Results.ry;
        this.rz = p.Results.rz;
        this.alpha = p.Results.alpha;
        this.beta = p.Results.beta;
        this.gamma = p.Results.gamma;
        this.A = p.Results.A;

        end
        
        function val = measureFT(this, kx, ky, kz)

                val = this.A*ellipsoidFT(kx, ky, kz, ...
                             this.rx, this.ry, this.rz, ...
                             this.alpha, this.beta, this.gamma, ...
                             this.x0, this.y0, this.z0);
                         
        end
        
        function val = getVolume(this)
           
            val = 4/3*pi*this.rx*this.ry*this.rz;
            
        end
        
        function this = scaleVolume(this, scaleFactor)
           scaleFactor = scaleFactor^(1/3);
           this.rx = this.rx*scaleFactor;
           this.ry = this.ry*scaleFactor;
           this.rz = this.rz*scaleFactor;
        end
        
    end
    
end

