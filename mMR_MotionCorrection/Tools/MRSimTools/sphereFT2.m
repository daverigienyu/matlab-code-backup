function val = unitSphereFT(kx, ky, kz)

    length_k = sqrt(kx.*conj(kx) + ky.*conj(ky) + kz.*conj(kz));
    val = (sin(2.*pi().*length_k) - 2.*pi().*length_k.*cos(2*pi().*length_k))./(2.*pi().^2.*length_k.^3);

end