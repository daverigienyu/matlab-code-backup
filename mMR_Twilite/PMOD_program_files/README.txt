﻿ PMOD V3.7 Installation per Download
------------------------------------

Please proceed as detailed below for downloading and installing the 3.7 version
of the PMOD software.

1) Requirements
---------------
Dedicated setups for different computer platforms are available. The operating systems 
must be 64-Bit systems.  

2) Download
-----------
Please perform the following sequence of downloading and preparation steps:

1. Download the pmod-37-setup-XX.zip file corresponding to the operating system
   of your target machine.

2. Extract the files contained in pmod-37-setup-XX.zip into the installation 
   directory "pmod-37-setup-XX". Thereafter it contains three directories:
   - "java": 
     Contains a suitable Java Runtime Environement for the installation procedure.
   - "pmod": 
     Contains the installer files for PMOD, a specially prepared Java Runtime Environment
     IMPORTANT: Please DO NOT UNZIP any of the zip archives further!
   - "Setup":
     Contains the installation script in a system-specific sub-direcory.
   Also included are pdf files with the release notes, the system-specific 
   installation description, and this "README.txt".

3. Download the example database zip file "data.zip": (190MB)
   Copy "data.zip" into the sub-directory "pmod-37-setup-XX/pmod". 
   Do NOT UNZIP!

4. For licenses with the PNEURO tool only:
   Download the zip file with the knowledge base "parcellation.zip": (216MB)
   The download link can be found on the first level after support login
   in the area of the license report.
   Copy "parcellation.zip" into the sub-directory "pmod-37-setup-XX/pmod". 
   Do NOT UNZIP!

5. Download the PMOD documentation "doc.zip" (215MB) and unzip. As an alternative
   you can browse the documentation on our website in the "Support" section. The 
   direct link is http://doc37.pmod.com


3) PMOD Installation
--------------------
The installation directoy "pmod-37-setup-XX/Setup” contains a system-specific installer script 
RunSetup. Please start the "RunSetup" script and perform a standard installation. 

The installation details are described in the included installation pdf file.
Do not forget to install the USB key drivers (not necessary for trials and 
network clients).


4) Installing the PMOD License 
------------------------------
Before PMOD can be used, your dedicated license file "pstarter.lcs" must be copied 
to the appropriate location, the "Pmod3.7/system/lcs" directory 
(not necessary for network clients).

Regular users can download their license file from the PMOD website after the
personalized login into the "Support" area. 

Trial users can use the "pstarter.lcs" file obtained by e-mail.

Please make sure "pstarter.lcs" is not modified in any way:
do not open it in any program, do not rename it, and if you transfer it per FTP, use 
binary transfer. Any modified license file will not be accepted.


5) Starting PMOD 
----------------
After the above installation steps, PMOD can be started using the script "RunPmod"
created by the installer in the "Pmod3.7/Start" directory.

If the example database was not installed, an error message will be displayed. Please 
deactivate the database functionality using "Config" from the toolbar:
- Remove the check of "Use database" on USERS/SETTINGS/DATABASE. 
- Remove the check of "Start DB Transaction Server .." on DATABASE/DB TRANSACTION SERVER.
Quit with Ok and restart.


6) Troubleshooting
------------------
In the case of problems please refer to the more detailed installation descriptions 
to be found in the installation sections of the PMOD Base Functionality User Guide
http://doc37.pmod.com and send a problem report to support@pmod.com  


We thank you for using PMOD, and wish you successful research projects.

PMOD Technologies Ltd
Zurich, October, 2015

