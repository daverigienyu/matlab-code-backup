
function [] = fixMuMap()
% This script loads all of the mu map dicom images in a folder and
% This version just makes everything with mu >= FAT switch to WATER.
% Since we know our phantom is all WATER, we don't want any FAT left.

    dicomdict set dicom-dict-siemens.txt

    %=====================================================================%
    %---- INVERT ATTENUATION OF WATER/FAT --------------------------------%
    %======================================================================
        WATER_VAL = 1000;
        FAT_VAL = 854;
    
        % Ladefoged, Claes Nøhr et al. “Impact of Incorrect Tissue 
        % Classification in Dixon-Based MR-AC: Fat-Water Tissue Inversion.�? 
        % EJNMMI physics 1.1 (2014): 101. Web.
    %======================================================================
    
    
    %inputdir = uigetdir('Q:/labspace/mMR_Twilite/data');
    inputdir = uigetdir('/media/boadaf01lab/labspace/mMR_Twilite/data');
    outputdir = strrep(inputdir, 'data', 'evaluation');

    if ~exist(outputdir, 'dir')
        mkdir(outputdir);
    end
    
    files = dir(fullfile(inputdir, '*.IMA'));

    for i=1:1:numel(files)
        disp(sprintf('Processing file %i',i));
        fullpath = fullfile(inputdir, files(i).name);
        dcm_info = dicominfo(fullpath);
        dcm_data = dicomread(dcm_info);
        
        dcm_data_new = fixMuValues(dcm_data, FAT_VAL, WATER_VAL);
        
        dicomwrite(dcm_data_new, ...
                   strrep(fullpath,'data','evaluation'), ...
                   dcm_info, ...
                   'CreateMode', 'copy',...
                   'WritePrivate', true);
               
    end

end

function data_new = fixMuValues(data_old, fat_val, water_val)

    data_new = data_old;
    ind      = data_new >= fat_val;
    data_new(ind) = water_val;
    
end
