PHANTOM.PT.NYU_NEURO_BRAIN.0011.0001.2016.08.19.09.11.16.687500.283474390.IMA
0002,0002  Media Storage SOP Class UID: 1.2.840.10008.5.1.4.1.1.128 
0002,0003  Media Storage SOP Inst UID: 1.3.12.2.1107.5.2.38.51022.2016081909095046875003950
0002,0010  Transfer Syntax UID: 1.2.840.10008.1.2.1 
0002,0012  Implementation Class UID: 1.3.12.2.1107.5.2 
0002,0013  Implementation Version Name: MR_VB20P
0008,0005  Specific Character Set: ISO_IR 100
0008,0008  Image Type: ORIGINAL\PRIMARY\STATIC\AC
0008,0012  Instance Creation Date: 20160819
0008,0013  Instance Creation Time: 090954.500000 
0008,0016  SOP Class UID: 1.2.840.10008.5.1.4.1.1.128 
0008,0018  SOP Instance UID: 1.3.12.2.1107.5.2.38.51022.2016081909095046875003950
0008,0020  Study Date: 20160819
0008,0021  Series Date: 20160819
0008,0022  Acquisition Date: 20160819
0008,0023  Image Date: 20160819
0008,0030  Study Time: 081748.406000 
0008,0031  Series Time: 085721.000000 
0008,0032  Acquisition Time: 085721.000000 
0008,0033  Image Time: 090954.500000 
0008,0050  Accession Number: 
0008,0060  Modality: PT
0008,0070  Manufacturer: SIEMENS 
0008,0080  Institution Name: NYU Med Center
0008,0081  Institution Address: 1st Avenue 660,New York, NY,NYC S19E4E9B,US,10016 
0008,0090  Referring Physician's Name: HARRIS DENA E 
0008,1010  Station Name: MRC51022
0008,1030  Study Description: NYU NEURO^BRAIN 
0008,103E  Series Description: *Head_MRAC_PET_AC Images
0008,1040  Institutional Department Name: Department
0008,1050  Attending Physician's Name: 
0008,1090  Manufacturer's Model Name: Biograph_mMR
0008,1140  Referenced Image Sequence: 
0008,1150  Referenced SOP Class UID: 1.2.840.10008.5.1.4.1.1.4 
0008,1155  Referenced SOP Instance UID: 1.3.12.2.1107.5.2.38.51022.201608190852224110551280 
0008,1150  Referenced SOP Class UID: 1.2.840.10008.5.1.4.1.1.4 
0008,1155  Referenced SOP Instance UID: 1.3.12.2.1107.5.2.38.51022.2016081908522764085851304
0008,1150  Referenced SOP Class UID: 1.2.840.10008.5.1.4.1.1.4 
0008,1155  Referenced SOP Instance UID: 1.3.12.2.1107.5.2.38.51022.2016081908523114076051319
0020,000D  Study Instance UID: 1.3.12.2.1107.5.2.38.51022.30000016081811564778100000024
0020,000E  Series Instance UID: 1.3.12.2.1107.5.2.38.51022.2016081908575446080857537.0.0.0
0010,0010  Patient's Name: PHANTOM 
0010,0020  Patient ID: 1233123 
0010,0030  Patient's Birth Date: 20010101
0010,0040  Patient's Sex: O 
0010,1010  Patient's Age: 015Y
0010,1020  Patient's Size: 1.5748031516667 
0010,1030  Patient's Weight: 55.7918686071 
0018,0050  Slice Thickness: 2.03125 
0018,1000  Device Serial Number: 51022 
0018,1020  Software Versions(s): syngo MR B20P 
0018,1030  Protocol Name: Head_MRAC_PET 
0018,1181  Collimator Type: NONE
0018,1200  Date of Last Calibration: 20160819
0018,1201  Time of Last Calibration: 072126.000000 
0018,1210  Convolution Kernel: XYZGAUSSIAN2.00 
0018,1242  Actual Frame Duration: 600000
0018,5100  Patient Position: HFS 
0020,000D  Study Instance UID: 1.3.12.2.1107.5.2.38.51022.30000016081811564778100000024
0020,000E  Series Instance UID: 1.3.12.2.1107.5.2.38.51022.2016081909094915625003945
0020,0010  Study ID: 1 
0020,0011  Series Number: 11
0020,0013  Image Number: 1 
0020,0032  Image Position (Patient): -179.01927999173\-179.66434099291\129.01681005955 
0020,0037  Image Orientation (Patient): 1\0\0\0\1\0 
0020,0052  Frame of Reference UID: 1.3.12.2.1107.5.2.38.51022.2.20160819085150875.0.0.0
0020,1002  Images in Acquisition: 127 
0020,1040  Position Reference Indicator: 
0020,1041  Slice Location: 129.017 
0020,4000  Image Comments: PET AC
0028,0002  Samples per Pixel: 1
0028,0004  Photometric Interpretation: MONOCHROME2 
0028,0010  Rows: 344
0028,0011  Columns: 344
0028,0030  Pixel Spacing: 1.04313\1.04313 
0028,0051  Corrected Image: NORM\DTIM\ATTN\3SCAT\RELSC\DECY\FLEN\RANSM\XYSM\ZSM 
0028,0100  Bits Allocated: 16
0028,0101  Bits Stored: 16
0028,0102  High Bit: 15
0028,0103  Pixel Representation: 0
0028,0106  Smallest Image Pixel Value: 0
0028,0107  Largest Image Pixel Value: 130
0028,1050  Window Center: 625 
0028,1051  Window Width: 1249
0028,1052  Rescale Intercept: 0 
0028,1053  Rescale Slope: 9.6113467818232 
0028,1054  Rescale Type: US
0028,1055  Window Center & Width Explanation: Algo1 
0029,0010  ---: SIEMENS CSA HEADER
0029,0011  ---: SIEMENS MEDCOM HEADER2
0029,1008  ---: PET NUM 4 
0029,1009  ---: 20160819
0029,1018  ---: PT
0029,1019  ---: 20160819
0029,1160  ---: com 
0032,1032  Requesting Physician: HARRIS^DENA^E^^MD 
0032,1060  Requested Procedure Description: NYU NEURO BRAIN 
0040,0244  Performed Procedure Step Start Date: 20160819
0040,0245  Performed Procedure Step Start Time: 081748.500000 
0040,0253  Performed Procedure Step ID: MR20160819081748
0040,0254  Performed Procedure Step Description: NYU NEURO^BRAIN 
0054,0013  Energy Window Range Sequence: 
0054,0014  Energy Window Lower Limit: 430 
0054,0015  Energy Window Upper Limit: 610 
0054,0016  Radiopharmaceutical Information Sequence: 
0018,0031  Radiopharmaceutical: Fluorodeoxyglucose
0018,1071  Radionuclide Volume: 0 
0018,1072  Radionuclide Start Time: 083700.000000 
0018,1074  Radionuclide Total Dose: 186480000 
0018,1075  Radionuclide Half Life: 6586.2
0018,1076  Radionuclide Positron Fraction: 0.97
0054,0300  Radionuclide Code Sequence: 
0008,0100  Code Value: C-111A1 
0008,0102  Coding Scheme Designator: SRT 
0008,0104  Code Meaning: ^18^Fluorine
0054,0081  Number of Slices: 127
0054,0410  Patient Orientation Code Sequence: 
0054,0414  Patient Gantry Relationship Code Sequence: 
0054,1000  Series Type: WHOLE BODY\IMAGE
0054,1001  Units: BQML
0054,1002  Counts Source: EMISSION
0054,1100  Randoms Correction Method: DLYD
0054,1101  Attenuation Correction Method: measured
0054,1102  Decay Correction: START 
0054,1103  Reconstruction Method: OP-OSEM3i21s
0054,1104  Detector Lines of Response Used: DESCRIPTION 
0054,1105  Scatter Correction Method: Model-based 
0054,1200  Axial Acceptance: 60
0054,1201  Axial Mash: 5\6 
0054,1300  Frame Reference Time: 298421.41716721 
0054,1321  Decay Factor: 1.0319
0054,1322  Dose Calibration Factor: 1 
0054,1323  Scatter Fraction Factor: 19.2084 
0054,1330  Image Index: 1
7FE0,0010  Pixel Data: 128210

------------------------------------------------------
(Fiji Is Just) ImageJ 2.0.0-rc-41/1.50d; Java 1.6.0_24 [64-bit]; Windows NT (unknown) 6.2; 53MB of 6063MB (<1%)
 
Title: HEAD_MRAC_PET_AC_IMAGES_0011
Width:  358.8367 mm (344)
Height:  358.8367 mm (344)
Depth:  257.9693 mm (127)
Size:  29MB
Resolution:  0.9587 pixels per mm
Voxel size: 1.0431x1.0431x2.0313 mm^3
ID: -256
Bits per pixel: 16 (unsigned)
Display range: 0 - 1249
Image: 1/127 (PHANTOM.PT.NYU_NEURO_BRAIN.0011.0001.2016.08.19.09.11.16.687)
No threshold
Uncalibrated
Path: Q:\mMR_Twilite\PET_Data\Dicom\HEAD_MRAC_PET_AC_IMAGES_0011\
Screen location: 161,225 (1536x864)
Coordinate origin:  0,0,0
No overlay
No selection
