function [] = Prepare_EMrecon_mMR_Data(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('Prepare_EMrecon_mMR_Data ["%s"]',folder),5);

%% prepare path for selected EMrecon version
path_to_emrecon = mfilename('fullpath');
pindex = strfind(path_to_emrecon,'\');
path_to_emrecon = path_to_emrecon(1:pindex(end));
path_to_emrecon = [path_to_emrecon 'bin\' E7_parm.emreconversion];
addpath(path_to_emrecon);

RQ_WriteLog(RQ_parm,sprintf('using EMrecon version "%s" in folder "%s"',E7_parm.emreconversion,path_to_emrecon),6);

%% setup parameter
parm_data.SCANNERTYPE = 8;
parm_data.SIZE_X = 344;
parm_data.SIZE_Y = 344;
parm_data.SIZE_Z = 127;
parm_data.CONVOLVE = 0;
parm_data.FIXED_SUBSET = -1;
parm_data.MAX_RING_DIFF = 60;
parm_data.EMRECON_VERSION = EMrecon_Version();
parm_data.VERBOSE = 2;


%% identify filenames
% identify recon type for folder names
if (strcmp(E7_parm.recontype,'sinogram') == 1)
    recontype = '00';
end
if (strcmp(E7_parm.recontype,'listmode') == 1)
    recontype = 'LM-00';
end

% identify IF2DICOM file to extract gantry_offset and bedposition
filename_if2dicom = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\Run-05-' folder '-' recontype '-IF2Dicom.txt'];
fprintf('filename_if2dicom: %s\n',filename_if2dicom);

% identify norm file
filename_normfile = [RQ_parm.recon_path '\' folder '-Converted\' folder '-norm.n'];
fprintf('filename_normfile: %s\n',filename_normfile);

% identify scatter file
dirinfo = dir([RQ_parm.recon_path '\' folder '-Converted\debug']);
for fileid=3:numel(dirinfo)
    pos = strfind(dirinfo(fileid).name,'scatter_');
    if (pos > 0)
        pos2 = strfind(dirinfo(fileid).name,'.s');
        if (pos2 > 0)
            filename_scatter = [RQ_parm.recon_path '\' folder '-Converted\debug\' dirinfo(fileid).name];
        end
    end
end
fprintf('filename_scatter: %s\n',filename_scatter);

% identify log file to extract frame duration and scatter scaling
dirinfo = dir([RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype]);
for fileid=3:numel(dirinfo)
    pos = strfind(dirinfo(fileid).name,'log_e7_recon_');
    if (pos > 0)
        filename_log = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' dirinfo(fileid).name];
    end
end
fprintf('filename_log: %s\n',filename_log);

% identify both umaps (human and hardware) and MLAA if required
filename_umap_human = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' folder '-' recontype '-umap.v'];
fprintf('filename_umap_human: %s\n',filename_umap_human);
filename_umap_hardware = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' folder '-' recontype '-umap-hardware.v'];
fprintf('filename_umap_hardware: %s\n',filename_umap_hardware);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    filename_umap_human_mlaa = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' folder '-' recontype '-umap-mlaa_000_000_00.v'];
    fprintf('filename_umap_human_mlaa: %s\n',filename_umap_human_mlaa);
end

% identify sinogram stack (with recontype 00 uncompression is required)
if (strcmp(recontype,'00') == 1)
    filename_sinogram_stack = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' folder '-' recontype '-sino.s'];
    dos(sprintf('C:\\Siemens\\PET\\bin.win64-VA20\\intfcompr.exe -e %s.hdr --oe %s_uncompr.s.hdr',filename_sinogram_stack,filename_sinogram_stack(1:end-2)));
    filename_sinogram_stack = [filename_sinogram_stack(1:end-2) '_uncompr.s'];
end
if (strcmp(recontype,'LM-00') == 1)
    filename_sinogram_stack = [RQ_parm.recon_path '\' folder '-Converted\' folder '-' recontype '\' folder '-' recontype '-sino-0.s'];
end
fprintf('filename_sinogram_stack: %s\n',filename_sinogram_stack);


%% load data
% load norm
[normalization, normdata_scatter] = read_mMR_Normfile(filename_normfile);

% load umaps
umap_human = readimage(filename_umap_human,parm_data.SIZE_X,parm_data.SIZE_Y,parm_data.SIZE_Z,1);
umap_hardware = readimage(filename_umap_hardware,parm_data.SIZE_X,parm_data.SIZE_Y,parm_data.SIZE_Z,1);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    umap_human_mlaa = readimage(filename_umap_human_mlaa,parm_data.SIZE_X,parm_data.SIZE_Y,parm_data.SIZE_Z,1);
    umap = umap_human_mlaa + umap_hardware;
else
    umap = umap_human + umap_hardware;
end

% load sinogram stack
sinogram_stack = readimage_int16(filename_sinogram_stack,344,252,4084,2);

% load scatter data
scatterestimate = readimage(filename_scatter,344,252,127,1);


%% process data
% get scatter fraction, check for "final scatter fraction is "
valid_scatter_fraction = 0;
fid = fopen(filename_log,'r');
while (valid_scatter_fraction == 0 && ~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'final scatter fraction is ');
    if (pos > 0)
        scan_info.scatter_fraction = str2double(v(pos+26:end-1));
        valid_scatter_fraction = 1;
    end
end
fclose(fid);
fprintf('scan_info.scatter_fraction: %f\n',scan_info.scatter_fraction);

% get frame duration
valid_frame_duration = 0;
fid = fopen(filename_log,'r');
while (valid_frame_duration == 0 && ~feof(fid))
    v = fgetl(fid);
    % 1) find "frame duration="
    pos = strfind(v,'frame duration=');
    if (pos > 0)
        % 2) find " sec"
        pos2 = strfind(v,' sec');
        scan_info.frame_duration = str2double(v(pos+15:pos2-1));
        % some values are equal to 0, only take values > 0
        if (scan_info.frame_duration > 0)
            valid_frame_duration = 1;
        end
    end
end
fclose(fid);
fprintf('scan_info.frame_duration: %f\n',scan_info.frame_duration);

% get gantry_offset and bedposition
scan_info.gantry_offset_x = 0.0;
scan_info.gantry_offset_y = 0.0;
scan_info.gantry_offset_z = 0.0;
scan_info.horizontal_bed = 0.0;
scan_info.vertical_bed = 0.0;
fid = fopen(filename_if2dicom,'r');
while (~feof(fid))
    v = fgetl(fid);
    % 1) find "xx_Gantry_Offset_X"
    pos = strfind(v,'xx_Gantry_Offset_X');
    if (pos > 0)
        % 1b) find ":= "
        pos2 = strfind(v,':= ');
        scan_info.gantry_offset_x = str2double(v(pos2+3:end));
    end
    % 2) find "xx_Gantry_Offset_Y"
    pos = strfind(v,'xx_Gantry_Offset_Y');
    if (pos > 0)
        % 2b) find ":= "
        pos2 = strfind(v,':= ');
        scan_info.gantry_offset_y = str2double(v(pos2+3:end));
    end
    % 3) find "xx_Gantry_Offset_Z"
    pos = strfind(v,'xx_Gantry_Offset_Z');
    if (pos > 0)
        % 3b) find ":= "
        pos2 = strfind(v,':= ');
        scan_info.gantry_offset_z = str2double(v(pos2+3:end));
    end
    % 4) find "xx_Horizontal_Bed"
    pos = strfind(v,'xx_Horizontal_Bed');
    if (pos > 0)
        % 4b) find ":= "
        pos2 = strfind(v,':= ');
        scan_info.horizontal_bed = str2double(v(pos2+3:end));
    end
    % 5) find "xx_Vertical_Bed"
    pos = strfind(v,'xx_Vertical_Bed');
    if (pos > 0)
        % 5b) find ":= "
        pos2 = strfind(v,':= ');
        scan_info.vertical_bed = str2double(v(pos2+3:end));
    end
end
fclose(fid);
fprintf('scan_info: %f / %f / %f / %f / %f\n',scan_info.gantry_offset_x,scan_info.gantry_offset_y,scan_info.gantry_offset_z,scan_info.horizontal_bed,scan_info.vertical_bed);

% prompt data
prompt = sinogram_stack(:,:,:,1);
scan_info.prompt(1).value = sum(prompt(:));
scan_info.prompt(1).legend = 'total counts in all sinograms after resorting from listmode';
fprintf('sum(prompt): %f\n',scan_info.prompt(1).value);
prompt = EMrecon_PET_Michelogram_CompressSinogramStack(parm_data,prompt(:));
scan_info.prompt(2).value = sum(prompt(:));
scan_info.prompt(2).legend = 'total counts in all sinograms after axial compression (sinogram stack -> michelogram)';
fprintf('sum(prompt) after EMrecon_PET_Michelogram_CompressSinogramStack: %f\n',scan_info.prompt(2).value);
prompt = EMrecon_PET_Michelogram_AxialWeight(parm_data,prompt);
scan_info.prompt(3).value = sum(prompt(:));
scan_info.prompt(3).legend = 'total counts in all sinograms after axial weighting of the michelogram';
fprintf('sum(prompt) after EMrecon_PET_Michelogram_AxialWeight: %f\n',scan_info.prompt(3).value);
prompt = EMrecon_PET_Michelogram_Normalize(parm_data,prompt,normalization);
scan_info.prompt(4).value = sum(prompt(:));
scan_info.prompt(4).legend = 'total counts in all sinograms after applying normalization correction to the michelogram';
fprintf('sum(prompt) after EMrecon_PET_Michelogram_Normalize: %f\n',scan_info.prompt(4).value);
prompt = EMrecon_PET_Michelogram_FillGaps(parm_data,prompt);
scan_info.prompt(5).value = sum(prompt(:));
scan_info.prompt(5).legend = 'total counts in all sinograms after filling gaps in the michelogram';
fprintf('sum(prompt) after EMrecon_PET_Michelogram_FillGaps: %f\n',scan_info.prompt(5).value);
prompt = 3600 .* prompt ./ scan_info.frame_duration;
scan_info.prompt(6).value = sum(prompt(:));
scan_info.prompt(6).legend = 'total counts in all sinograms after scaling michelogram to 1/h';
fprintf('sum(prompt) after scaling to 1/h: %f\n',scan_info.prompt(6).value);

% delayed data
delayed = sinogram_stack(:,:,:,2);
scan_info.delayed(1).value = sum(delayed(:));
scan_info.delayed(1).legend = 'total counts in all sinograms after resorting from listmode';
fprintf('sum(delayed): %f\n',scan_info.delayed(1).value);
delayed = EMrecon_PET_Michelogram_CompressSinogramStack(parm_data,delayed(:));
scan_info.delayed(2).value = sum(delayed(:));
scan_info.delayed(2).legend = 'total counts in all sinograms after axial compression (sinogram stack -> michelogram)';
fprintf('sum(delayed) after EMrecon_PET_Michelogram_CompressSinogramStack: %f\n',scan_info.delayed(2).value);
delayed = EMrecon_PET_Michelogram_AxialWeight(parm_data,delayed);
scan_info.delayed(3).value = sum(delayed(:));
scan_info.delayed(3).legend = 'total counts in all sinograms after axial weighting of the michelogram';
fprintf('sum(delayed) after EMrecon_PET_Michelogram_AxialWeight: %f\n',scan_info.delayed(3).value);
delayed = EMrecon_PET_Michelogram_Normalize(parm_data,delayed,normalization);
scan_info.delayed(4).value = sum(delayed(:));
scan_info.delayed(4).legend = 'total counts in all sinograms after applying normalization correction to the michelogram';
fprintf('sum(delayed) after EMrecon_PET_Michelogram_Normalize: %f\n',scan_info.delayed(4).value);
delayed = EMrecon_PET_Michelogram_FillGaps(parm_data,delayed);
scan_info.delayed(5).value = sum(delayed(:));
scan_info.delayed(5).legend = 'total counts in all sinograms after filling gaps in the michelogram';
fprintf('sum(delayed) after EMrecon_PET_Michelogram_FillGaps: %f\n',scan_info.delayed(5).value);
delayed = 3600 .* delayed ./ scan_info.frame_duration;
scan_info.delayed(6).value = sum(delayed(:));
scan_info.delayed(6).legend = 'total counts in all sinograms after scaling michelogram to 1/h';
fprintf('sum(delayed) after scaling to 1/h: %f\n',scan_info.delayed(6).value);

% scatter data
scatterestimate = EMrecon_PET_Michelogram_ISSRB(parm_data,scatterestimate(:));
scatterestimate = reshape(scatterestimate,344*252,837);
for i=1:344*252
    scatterestimate(i,:) = scatterestimate(i,:) .* normdata_scatter';
end
scatterestimate = scatterestimate(:);
scatterestimate = scatterestimate / sum(scatterestimate(:)) * scan_info.scatter_fraction / 100.0 * (sum(prompt(:))-sum(delayed(:)));

% combine datasets
data = prompt;
data_fp = delayed + scatterestimate;

% perform ac correction
data_fp_ac = EMrecon_ForwardProjAC_ND(parm_data,umap,data_fp);

% create ac umap
one_vector = ones(size(data));
sensitivity_map_ac = EMrecon_BackProjAC_ND(parm_data,one_vector,umap);


%% save data
readme = ['parm_data provides information about the EMrecon version used to prepare the sinograms. ' ...
    'data contains prompt events. scatter and estimated randoms (i.e. delayed events) are stored separately. ' ...
    'data_fp_ac represents (scatterestimate+delayed) corrected for attenuation. data_fp_ac or (scatterestimate+delayed) must ' ...
    'be added to the forward projector in the corresponding (non-)ac reconstructions. all datasets have been ' ...
    'corrected using the normalization factors. umap is the sum of the human and hardware umaps and ' ...
    'was used to create data_fp_ac. it was also used to create the sensitivity_map_ac. in case of mlaa ' ...
    'umap_human_mlaa was used instead of umap_human. scan_info contains information about gantry position etc.'];
filename_emrecondata = [RQ_parm.recon_path '\' folder '-Converted\' E7_parm.anonymized_id '.mat'];
fprintf('filename_emrecondata: %s\n',filename_emrecondata);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    save(filename_emrecondata,'data','data_fp_ac','sensitivity_map_ac','scan_info','parm_data','scatterestimate','delayed','normalization','umap','umap_human','umap_human_mlaa','umap_hardware','readme');
else
    save(filename_emrecondata,'data','data_fp_ac','sensitivity_map_ac','scan_info','parm_data','scatterestimate','delayed','normalization','umap','umap_human','umap_hardware','readme');
end

%% remove path to EMrecon
rmpath(path_to_emrecon);

%% free memory and close all windows
clear('one_vector');
clear('sensitivity_map_ac');
clear('data');
clear('data_fp');
clear('data_fp_ac');
clear('prompt');
clear('delayed');
clear('scatterestimate');
clear('umap_hardware');
clear('umap_human');
if (strcmp(E7_parm.usemlaa,'1') == 1)
    clear('umap_human_mlaa');
end
clear('umap');
clear('normdata');
clear('normdata_scatter');
close all;

RQ_WriteLog(RQ_parm,sprintf('Prepare_EMrecon_mMR_Data ["%s"] END',folder),5);

end