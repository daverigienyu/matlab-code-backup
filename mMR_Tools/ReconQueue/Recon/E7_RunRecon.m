function [status] = E7_RunRecon(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon ["%s"]',folder),3);

% set default status
status = 0;

% run sinogram reconstrucion
if (strcmp('sinogram',E7_parm.recontype) == 1)
    status = E7_RunRecon_Sinogram(RQ_parm,E7_parm,folder);
end

% run whole body reconstrucion
if (strcmp('wb',E7_parm.recontype) == 1)
    status = E7_RunRecon_WholeBody(RQ_parm,E7_parm,folder);
end

% run listmode reconstrucion
if (strcmp('listmode',E7_parm.recontype) == 1)
    status = E7_RunRecon_Listmode(RQ_parm,E7_parm,folder);
end

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon [valid files "%s"] END',folder),3);
else
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon [invalid files "%s"] END',folder),3);
end

end