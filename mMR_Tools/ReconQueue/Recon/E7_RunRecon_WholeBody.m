function [status] = E7_RunRecon_WholeBody(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_WholeBody ["%s"]',folder),4);

% set default values
status = 0;
e7_version = 'VA20';

% check whether VA10 or VA20 is used
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-WB\Run-04-' folder '-WB-OP.bat'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'C:\Siemens\PET\bin.win64-');
    if (pos > 0)
        tmp_e7_version = v(pos+25:end);
        if (strfind(tmp_e7_version,'mMR'))
            e7_version = 'VA10';
        end
    end
end
fclose(fid);

% run sinogram reconstruction using VA10
if (strcmp(e7_version,'VA10'))
    status = E7_RunRecon_WholeBody_VA10(RQ_parm,E7_parm,folder);
end

% run sinogram reconstruction using VA20
if (strcmp(e7_version,'VA20'))
    status = E7_RunRecon_WholeBody_VA20(RQ_parm,E7_parm,folder);
end

% - due to the new CBI network structure it was required to check / create
%   all folders before reconstruction
if (exist(RQ_parm.data_path,'dir') ~= 7)
    mkdir(RQ_parm.data_path);
end
if (exist(RQ_parm.image_path,'dir') ~= 7)
    mkdir(RQ_parm.image_path);
end

% check for destination
% results will be stored in folder\frames_x. x is adapted.
if (strcmp(E7_parm.user,'unknown') == 1)
    user_offset = '';
else
    user_offset = [E7_parm.user '\'];
end
subdirid = 1;
while (exist([RQ_parm.image_path '\' user_offset folder '\frames_' num2str(subdirid)],'dir') ~= 0)
    subdirid = subdirid + 1;
end
image_destination = [RQ_parm.image_path '\' user_offset folder '\frames_' num2str(subdirid)];

% copy results and log files if requested
folder_list = dir([RQ_parm.recon_path '\' folder '-Converted\' folder '-WB']);
for i=3:numel(folder_list)
    if (exist([RQ_parm.recon_path '\' folder '-Converted\' folder '-WB\' folder_list(i).name],'dir') == 7)
        image_source = [RQ_parm.recon_path '\' folder '-Converted\' folder '-WB\' folder_list(i).name];
        if (strcmp(E7_parm.anonymized,'on') == 1)
            RQ_AnonymizeDicomData(RQ_parm,E7_parm,image_source,[image_destination '\' folder_list(i).name]);
        else
            dos(sprintf('xcopy %s %s\\%s /e /i',image_source,image_destination,folder_list(i).name));
        end
    end
    if (strcmp(E7_parm.log,'1') == 1)
        if (numel(strfind([RQ_parm.recon_path '\' folder '-Converted\' folder '-WB\' folder_list(i).name],'log_e7_')) > 0)
            log_source = [RQ_parm.recon_path '\' folder '-Converted\' folder '-WB\' folder_list(i).name];
            dos(sprintf('xcopy %s %s\\',log_source,image_destination));
        end
    end
end

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_WholeBody ["%s" finished] END',folder),4);
else
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_WholeBody ["%s" failed] END',folder),4);
end

end