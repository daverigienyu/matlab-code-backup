function [status] = E7_RunJSRecon(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_RunJSRecon ["%s"]',folder),3);

% set default status
status = 0;

% write config to JSRecon.txt
fid = fopen([RQ_parm.base_path '\JSRecon.txt'],'w');
fprintf(fid,'MapV18toV20 := %s\r\n',E7_parm.mapv18tov20);
fprintf(fid,'usemlaa := %s\r\n',E7_parm.usemlaa);

% in case of listmode reconstruction JSRecon.txt has to be modified
if (strcmp(E7_parm.recontype,'listmode') == 1)
    % calculate binning
    binning = E7_Listmode_AnalyzeBinning(RQ_parm,E7_parm,folder);
    fprintf(fid,'LMFrames := ');
    if (binning(1) <= 0)
        fprintf(fid,'%d:',abs(binning(1)));
        for n=2:numel(binning)-1
            fprintf(fid,'%d,',binning(n));
        end
        if (numel(binning) >= 2)
            fprintf(fid,'%d',binning(end));
        end
    else
        fprintf(fid,'0:%d',binning(1));
        for n=2:numel(binning)-1
            fprintf(fid,',%d',binning(n));
        end
        if (numel(binning) >= 2)
            fprintf(fid,',%d',binning(end));
        end
    end
    fprintf(fid,'\r\n');
end
fclose(fid);

% write binning to recon_info.txt and create binning.pdf
if (strcmp(E7_parm.recontype,'listmode') == 1)     
    E7_Listmode_CreateBinningDoc(RQ_parm,E7_parm,folder,binning);
end

% run JSRecon
dos(['cscript c:\JSRecon12\JSRecon12.js ' RQ_parm.recon_path '\' folder ' ' RQ_parm.base_path '\JSRecon.txt']);

% check whether JSRecon finished
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\JSRecon12Info.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    % this is only valid if this statement represents successfull ending...
    pos = strfind(v,'JSRecon complete!');
    if (numel(pos) > 0)
        status = 1;
    end
end
fclose(fid);

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('E7_RunJSRecon [valid files "%s"] END',folder),3);
else
    RQ_WriteLog(RQ_parm,sprintf('E7_RunJSRecon [invalid files "%s"] END',folder),3);
end

end