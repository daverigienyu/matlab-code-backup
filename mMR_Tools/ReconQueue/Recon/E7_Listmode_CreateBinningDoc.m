function [status] = E7_Listmode_CreateBinningDoc(RQ_parm,E7_parm,folder,binning)

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_CreateBinningDoc ["%s"]',folder),6);

% set default status (not used in this script so far)
status = 0;

% get listmode filename
file_list = dir([RQ_parm.recon_path '\' folder]);
for i=3:numel(file_list)
    if (numel(strfind(lower(file_list(i).name),'.dcm')) == 1)
        dinfo = dicominfo([RQ_parm.recon_path '\' folder '\' file_list(i).name]);
        if (strcmp(dinfo.ImageComments,'Listmode') == 1)
            filename = [RQ_parm.recon_path '\' folder '\' file_list(i).name];
            filename = [filename(1:end-4) '.bf'];
        end
    end
end

% extracting counts per msec from listmode file
cmdline = [RQ_parm.base_path '\Tools\LM_ExtractEvents.exe'];
cmdline = [cmdline ' ' filename ' ' RQ_parm.base_path '\events.counter'];
dos(cmdline);

% read number of timemarks from file
fid = fopen([RQ_parm.base_path '\events.counter'],'rb');
timemarks = fread(fid,1,'int32');
stoptime = timemarks-mod(timemarks,1000);

% read raw signal from disk
gating_signal_raw = fread(fid,2*stoptime,'int16');
fclose(fid);

% extract prompt and delayed
prompt = gating_signal_raw(1:2:end);
delayed = gating_signal_raw(2:2:end);

% first timemark in file is always 1(!)
gating_signal = zeros(stoptime/1000,3);

% compress data according to binning
for i=1:size(gating_signal,1)
    for j=1:1000
        gating_signal(i,1) = gating_signal(i,1) + prompt((i-1)*1000+j);
        gating_signal(i,2) = gating_signal(i,2) + delayed((i-1)*1000+j);
        gating_signal(i,3) = gating_signal(i,3) + prompt((i-1)*1000+j) + delayed((i-1)*1000+j);
    end
end

% write binning to log file
btitle = '';
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
fprintf(fid,'[%s] [E7_Listmode_CreateBinningDoc] binning=',datestr(now));
if (binning(1) <= 0)
    fprintf(fid,'%d:',abs(binning(1)));
    btitle = [btitle sprintf('%d:',abs(binning(1)))];
else
    fprintf(fid,'0:1x%d,',binning(1));
    btitle = [btitle sprintf('0:1x%d,',abs(binning(1)))];
end
old_bin = -1;
bin_count = 0;
skip_last_entry = 0;
for n=2:numel(binning)-1
    if (n == 2)
        old_bin = binning(n);
    end
    new_bin = binning(n);
    if (new_bin == old_bin)
        bin_count = bin_count + 1;
    else
        fprintf(fid,'%dx%d,',bin_count,binning(n-1));
        btitle = [btitle sprintf('%dx%d,',bin_count,binning(n-1))];
        bin_count = 1;
        old_bin = new_bin;
    end
    if (n == numel(binning)-1)
        if (binning(end-1) == binning(end))
            bin_count = bin_count + 1;
            skip_last_entry = 1;
            fprintf(fid,'%dx%d',bin_count,new_bin);
            btitle = [btitle sprintf('%dx%d',bin_count,new_bin)];
        else
            fprintf(fid,'%dx%d',bin_count,new_bin);
            btitle = [btitle sprintf('%dx%d',bin_count,new_bin)];
        end
    end
end
if (skip_last_entry == 0)
    if (numel(binning) == 2)
        fprintf(fid,'1x%d',binning(end));
        btitle = [btitle sprintf('1x%d',binning(end))];
    else
        fprintf(fid,',1x%d',binning(end));
        btitle = [btitle sprintf(',1x%d',binning(end))];
    end
end
if (binning(1) <= 0)
    fprintf(fid,' (%d:%d) [%d]',abs(binning(1)),sum(abs(binning)),stoptime/1000);
    if (strcmp(E7_parm.binning(1),'X') == 1)
        btitle = [btitle sprintf(' (%d:%d) [Tmax=%d,CPSthreshold=%.1f]',abs(binning(1)),sum(abs(binning)),stoptime/1000,str2double(E7_parm.cps))];
    else
        btitle = [btitle sprintf(' (%d:%d) [Tmax=%d]',abs(binning(1)),sum(abs(binning)),stoptime/1000)];
    end
else
    fprintf(fid,' (0:%d) [%d]',sum(abs(binning)),stoptime/1000);
    if (strcmp(E7_parm.binning(1),'X') == 1)
        btitle = [btitle sprintf(' (0:%d) [Tmax=%d,CPSthreshold=%.1f]',sum(abs(binning)),stoptime/1000,str2double(E7_parm.cps))];
    else
        btitle = [btitle sprintf(' (0:%d) [Tmax=%d]',sum(abs(binning)),stoptime/1000)];
    end
end
fprintf(fid,'\r\n');
fclose(fid);

%% create plot, write pdf and close figure
max_axis = max(gating_signal(:,3));
h = figure; orient landscape;
plot(gating_signal(:,3),'black'); hold on;
plot(gating_signal(:,1),'green');
plot(gating_signal(:,2),'red');
legend('total','prompt','delayed');
title(['Binning for [' strrep(folder,'_','\_') '] ' btitle]);
axis([0 sum(abs(stoptime/1000)) 0 1.05*max_axis]);

% draw line to control the injection time using E7_parm.cps
if (strcmp(E7_parm.binning(1),'X') == 1)
    hy = graph2d.constantline(str2double(E7_parm.cps),'Color',[.7 .7 .7]);
    changedependvar(hy,'y');
end

% one line per bin
if (binning(1) > 0)
    hx = graph2d.constantline(0,'LineWidth',1.5,'LineStyle',':','Color',[0 1 1]);
    changedependvar(hx,'x');
    hx = graph2d.constantline(abs(binning(1)),'LineStyle',':','Color',[.7 .7 .7]);
    changedependvar(hx,'x');
else
    hx = graph2d.constantline(abs(binning(1)),'LineWidth',1.5,'LineStyle',':','Color',[0 1 1]);
    changedependvar(hx,'x');
end
for i=2:numel(binning)-1
    hx = graph2d.constantline(sum(abs(binning(1:i))),'LineStyle',':','Color',[.7 .7 .7]);
    changedependvar(hx,'x');
end
hx = graph2d.constantline(sum(abs(binning(:))),'LineWidth',1.5,'LineStyle',':','Color',[0 0 1]);
changedependvar(hx,'x');

% save binning.pdf
print(h,'-dpdf',[RQ_parm.data_path '\' folder '\binning.pdf']);
close(h);

% remove events.counter
dos(['del ' RQ_parm.base_path '\events.counter']);

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_CreateBinningDoc total ["%d"]',squeeze(sum(gating_signal(:,3)))),7);
RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_CreateBinningDoc prompt ["%d"]',squeeze(sum(gating_signal(:,1)))),7);
RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_CreateBinningDoc delayed ["%d"]',squeeze(sum(gating_signal(:,2)))),7);

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_CreateBinningDoc ["%s"] END',folder),6);
