function [lasttimetag] = E7_Listmode_GetLastTimetag(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_GetLastTimetag ["%s"]',folder),7);

% - in this tool not only the last timetag is calculated. since in some
% cases the listmode file does not start with a first time index equal to 0
% it is required to correct the last timetag by this offset


% get listmode filename
file_list = dir([RQ_parm.recon_path '\' folder]);
for i=3:numel(file_list)
    if (numel(strfind(lower(file_list(i).name),'.dcm')) == 1)
        dinfo = dicominfo([RQ_parm.recon_path '\' folder '\' file_list(i).name]);
        if (strcmp(dinfo.ImageComments,'Listmode') == 1)
            filename = [RQ_parm.recon_path '\' folder '\' file_list(i).name];
            filename = [filename(1:end-4) '.bf'];
        end
    end
end

% calculate number of tags in file
fid = fopen(filename,'rb');
fseek(fid,0,1);
filesize = ftell(fid);
tags = filesize/4;
fclose(fid);

% search for first timetag
t = 0;
fid = fopen(filename,'rb');
while (t < tags)
    tag = fread(fid,1,'uint32');
    if (tag >= 2147483648 && tag < 2684354560)
        firsttimetag = tag-2147483648;
        t = tags;
    end
    t = t + 1;
end
fclose(fid);

% initialize with -1
lasttimetag = -1;

% start at the end -50000 (ie 500k pro sec as limit)
starttag = tags - 50000;
starttag = max(0,starttag);

fid = fopen(filename,'rb');
fseek(fid,starttag*4,-1);

t = starttag;
while (t < tags)
    tag = fread(fid,1,'uint32');
    if (tag >= 2147483648 && tag < 2684354560)
        lasttimetag = tag-2147483648;
    end
    t = t + 1;
end
fclose(fid);

% correct lasttimetag for firsttimetag and scale to sec
lasttimetag = floor((lasttimetag-firsttimetag)/1000.0);

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_GetLastTimetag ["%s" (%d)]',folder,lasttimetag),7);

end