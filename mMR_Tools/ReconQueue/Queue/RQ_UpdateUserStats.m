function [] = RQ_UpdateUserStats(RQ_parm,E7_parm)

RQ_WriteLog(RQ_parm,'RQ_UpdateUserStats',3);

% read user list from Stats.txt
new_user = 1;
fid = fopen([RQ_parm.base_path '\Stats.txt'],'r');

% get user
v = fgetl(fid);
pos = strfind(v,'User=');
usercount = str2double(v(pos+5:end));
if (usercount > 0)
    user(usercount).name = '';
    user(usercount).recons = 0;
end

% get recons
v = fgetl(fid);
pos = strfind(v,'TotalRecons=');
totalrecons = str2double(v(pos+12:end));

% first line is only comment
for i=0:usercount
    v = fgetl(fid);
    pos = strfind(v,'=');
    if (pos > 0)
        user(i).name = v(1:pos-1);
        user(i).recons = str2double(v(pos+1:end));
        if (strcmp(E7_parm.user,user(i).name) == 1)
            user(i).recons = user(i).recons + 1;
            new_user = 0;
        end
    end
end
fclose(fid);

% update Stats
fid = fopen([RQ_parm.base_path '\Stats.txt'],'w');
if (new_user == 1)
    fprintf(fid,'User=%d\r\n',usercount+1);
else
    fprintf(fid,'User=%d\r\n',usercount);
end
fprintf(fid,'TotalRecons=%d\r\n',totalrecons+1);
fprintf(fid,'---------------\r\n');
for i=1:usercount
    fprintf(fid,'%s=%d\r\n',user(i).name,user(i).recons);
end
if (new_user == 1)
    fprintf(fid,'%s=1\r\n',E7_parm.user);
end
fclose(fid);
clear('user');

RQ_WriteLog(RQ_parm,'RQ_UpdateUserStats END',3);

end