function [data_path] = RQ_GetInitDataPath(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_GetInitDataPath');

data_path = '';
fid = fopen([RQ_parm.base_path '\Init.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'DataPath=');
    if (pos > 0)
        data_path = v(pos+9:end);
    end
end
fclose(fid);

if (isempty(data_path))
    RQ_WriteLog(RQ_parm,'RQ_GetInitDataPath [no valid path] END');
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_GetInitDataPath ["%s"] END',data_path));
end

end
