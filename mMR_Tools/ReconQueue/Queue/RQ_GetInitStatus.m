function [status] = RQ_GetInitStatus(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_GetInitStatus');

% set default status
status = 0;

% check for status in Init.txt
fid = fopen([RQ_parm.base_path '\Init.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'Status=');
    if (pos > 0)
        status = str2double(v(pos+7:end));
    end
end
fclose(fid);

if (status == 1)
    RQ_WriteLog(RQ_parm,'RQ_GetInitStatus [queue running] END');
else
    RQ_WriteLog(RQ_parm,'RQ_GetInitStatus [queue stopped] END');
end

end