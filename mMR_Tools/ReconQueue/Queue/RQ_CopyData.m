function [] = RQ_CopyData(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('RQ_CopyData ["%s"]',folder),2);

% get all files in directory
file_list = dir([RQ_parm.data_path '\' folder]);

% make sure that the pet raw data is copied first
for i=3:numel(file_list)
    if (exist([RQ_parm.data_path '\' folder '\' file_list(i).name],'dir') == 7)
        if (numel(strfind(lower(file_list(i).name),'pet')) > 0 && numel(strfind(lower(file_list(i).name),'raw')) > 0 && numel(strfind(lower(file_list(i).name),'data')) > 0)
            pet_source = [RQ_parm.data_path '\' folder '\' file_list(i).name];
            pet_destination = [RQ_parm.recon_path '\' folder];
            dos(sprintf('xcopy %s %s /e /i',pet_source,pet_destination));
        end
    end
end

% umap is copied in a subfolder of the pet raw data
for i=3:numel(file_list)
    if (exist([RQ_parm.data_path '\' folder '\' file_list(i).name],'dir') == 7)
        if (strfind(lower(file_list(i).name),'umap') > 0)
            umap_source = [RQ_parm.data_path '\' folder '\' file_list(i).name];
            umap_destination = [pet_destination '\' file_list(i).name];
            dos(sprintf('xcopy %s %s /e /i',umap_source,umap_destination));
        end
    end
end

% write recon_info to folder
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
fprintf(fid,'[%s] [RQ_CopyData] raw data for "%s" has been copied to the reconstruction server\r\n',datestr(now),folder);
fclose(fid);

RQ_WriteLog(RQ_parm,sprintf('RQ_CopyData ["%s"] END',folder),2);

end