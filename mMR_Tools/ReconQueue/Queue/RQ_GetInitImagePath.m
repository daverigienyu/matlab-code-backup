function [image_path] = RQ_GetInitImagePath(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_GetInitImagePath');

image_path = '';
fid = fopen([RQ_parm.base_path '\Init.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'ImagePath=');
    if (pos > 0)
        image_path = v(pos+10:end);
    end
end
fclose(fid);

if (isempty(image_path))
    RQ_WriteLog(RQ_parm,'RQ_GetInitImagePath [no valid path] END');
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_GetInitImagePath ["%s"] END',image_path));
end

end
