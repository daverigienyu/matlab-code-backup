function [pause_time] = RQ_GetInitPauseTime(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_GetInitPauseTime');

pause_time = 60;
fid = fopen([RQ_parm.base_path '\Init.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'PauseTime=');
    if (pos > 0)
        pause_time = str2double(v(pos+10:end));
    end
end
fclose(fid);

if (isempty(pause_time))
    RQ_WriteLog(RQ_parm,'RQ_GetInitPauseTime [no valid time] END');
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_GetInitPauseTime ["%d"] END',pause_time));
end

end
