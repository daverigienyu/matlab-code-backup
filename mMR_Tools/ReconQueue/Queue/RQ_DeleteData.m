function [] = RQ_DeleteData(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('RQ_DeleteData ["%s"]',folder),2);

% delete files from server
dos(sprintf('rd %s\\%s /s /q',RQ_parm.recon_path,folder));
dos(sprintf('rd %s\\%s-Converted /s /q',RQ_parm.recon_path,folder));

% write recon_info to folder
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
fprintf(fid,'[%s] [RQ_DeleteData] raw data for "%s" has been removed from the reconstruction server\r\n',datestr(now),folder);
fclose(fid);

RQ_WriteLog(RQ_parm,sprintf('RQ_DeleteData ["%s"] END',folder),2);

end