function [status] = RQ_SubmitReconJob(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('RQ_SubmitReconJob ["%s"]',folder));

% write recon_info to folder - this folder is now treated as invalid
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'w');
fprintf(fid,'[%s] [RQ_SubmitReconJob] job "%s" has been submitted to the reconstruction queue\r\n',datestr(now),folder);
fclose(fid);

% copy files from folder to local ReconQueueData
RQ_CopyData(RQ_parm,folder);

% run reconstruction using the e7tools
status = RQ_RunReconJob(RQ_parm,folder);

% delete files from ReconQueueData
RQ_DeleteData(RQ_parm,folder);

% write recon_info to folder
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
fprintf(fid,'[%s] [RQ_SubmitReconJob] job "%s" finished\r\n',datestr(now),folder);
fclose(fid);

if (status == 1)
    % copy parm.txt and recon_info.txt to image_destination
    user_offset = '';
    fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'r');
    while (~feof(fid))
        v = fgetl(fid);
        
        % user
        pos = strfind(v,'user=');
        if (pos > 0)
            user_offset = [v(pos+5:end) '\'];
        end
    end
    fclose(fid);
    
    subdirid = 1;
    while (exist([RQ_parm.image_path '\' user_offset folder '\frames_' num2str(subdirid)],'dir') ~= 0)
        subdirid = subdirid + 1;
    end
    subdirid = max(1,subdirid-1);
    image_destination = [RQ_parm.image_path '\' user_offset folder '\frames_' num2str(subdirid)];
    
    dos(sprintf('xcopy %s %s\\ ',[RQ_parm.data_path '\' folder '\recon_info.txt'],image_destination));
    dos(sprintf('xcopy %s %s\\ ',[RQ_parm.data_path '\' folder '\parm.txt'],image_destination));
    
    % if binning.pdf exist, copy it to image_destination
    if (exist([RQ_parm.data_path '\' folder '\binning.pdf'],'file') == 2)
        dos(sprintf('xcopy %s %s\\ ',[RQ_parm.data_path '\' folder '\binning.pdf'],image_destination));
    end
    
    % write finished.txt to indicate the end of the reconstruction job
    fid = fopen([RQ_parm.data_path '\' folder '\finished.txt'],'a');
    fprintf(fid,'[%s] [RQ_SubmitReconJob] reconstruction for "%s" finished\r\n',datestr(now),folder);
    fclose(fid);
    
    RQ_WriteLog(RQ_parm,sprintf('RQ_SubmitReconJob [valid files "%s"] END',folder));
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_SubmitReconJob [invalid files "%s"] END',folder));
end

end