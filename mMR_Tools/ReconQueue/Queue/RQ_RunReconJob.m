function [status] = RQ_RunReconJob(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('RQ_RunReconJob ["%s"]',folder),2);

% read parameter for JSRecon / E7tools from parm.txt
E7_parm = E7_ReadParameter(RQ_parm,folder);

% write parameters used for reconstruction
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
fprintf(fid,'[%s] [RQ_RunReconJob] parameter used for the reconstruction:\r\n',datestr(now));
if (strcmp(E7_parm.user,'unknown') == 1)
    fprintf(fid,'[%s] [RQ_RunReconJob] no user specified\r\n',datestr(now));
else
    fprintf(fid,'[%s] [RQ_RunReconJob] user=%s\r\n',datestr(now),E7_parm.user);
end
fprintf(fid,'[%s] [RQ_RunReconJob] algo=%s\r\n',datestr(now),E7_parm.algo);
fprintf(fid,'[%s] [RQ_RunReconJob] is=%s,%s\r\n',datestr(now),E7_parm.is_1,E7_parm.is_2);
fprintf(fid,'[%s] [RQ_RunReconJob] w=%s\r\n',datestr(now),E7_parm.w);
fprintf(fid,'[%s] [RQ_RunReconJob] fltr=%s,%s,%s\r\n',datestr(now),E7_parm.fltr_1,E7_parm.fltr_2,E7_parm.fltr_3);
fprintf(fid,'[%s] [RQ_RunReconJob] R=%s,%s\r\n',datestr(now),E7_parm.r_1,E7_parm.r_2);
fprintf(fid,'[%s] [RQ_RunReconJob] izoom=%s\r\n',datestr(now),E7_parm.izoom);
fprintf(fid,'[%s] [RQ_RunReconJob] gpu=%s\r\n',datestr(now),E7_parm.gpu);
fprintf(fid,'[%s] [RQ_RunReconJob] usemlaa=%s\r\n',datestr(now),E7_parm.usemlaa);
fprintf(fid,'[%s] [RQ_RunReconJob] ext=%d',datestr(now),E7_parm.ext(1));
for i=2:numel(E7_parm.ext)
    fprintf(fid,',%d',E7_parm.ext(i));
end
fprintf(fid,'\r\n');
fprintf(fid,'[%s] [RQ_RunReconJob] mapv18tov20=%s\r\n',datestr(now),E7_parm.mapv18tov20);
fprintf(fid,'[%s] [RQ_RunReconJob] recontype=%s\r\n',datestr(now),E7_parm.recontype);
fprintf(fid,'[%s] [RQ_RunReconJob] scatterscaling=%s\r\n',datestr(now),E7_parm.scatterscaling);
if (strcmp(E7_parm.recontype,'listmode') == 1)
    fprintf(fid,'[%s] [RQ_RunReconJob] binning=%s\r\n',datestr(now),E7_parm.binning);
    fprintf(fid,'[%s] [RQ_RunReconJob] cps=%s\r\n',datestr(now),E7_parm.cps);
end
fprintf(fid,'[%s] [RQ_RunReconJob] anonymized=%s\r\n',datestr(now),E7_parm.anonymized);
if (strcmp(E7_parm.anonymized,'on') == 1)
    fprintf(fid,'[%s] [RQ_RunReconJob] anonymized_id=%s\r\n',datestr(now),E7_parm.anonymized_id);
end
fprintf(fid,'[%s] [RQ_RunReconJob] log=%s\r\n',datestr(now),E7_parm.log);
fprintf(fid,'[%s] [RQ_RunReconJob] EMreconData=%s\r\n',datestr(now),E7_parm.emrecondata);
fprintf(fid,'[%s] [RQ_RunReconJob] EMreconVersion=%s\r\n',datestr(now),E7_parm.emreconversion);
fclose(fid);

% update user stats
RQ_UpdateUserStats(RQ_parm,E7_parm);

% run JSRecon to convert data from DICOM (.IMA / .BF.DCM) to interfile
status = E7_RunJSRecon(RQ_parm,E7_parm,folder);

if (status == 1)
    % run e7 reconstruction depending on recontype
    status = E7_RunRecon(RQ_parm,E7_parm,folder);
end

% write parameters used for reconstruction
fid = fopen([RQ_parm.data_path '\' folder '\recon_info.txt'],'a');
if (status == 1)
    fprintf(fid,'[%s] [RQ_RunReconJob] reconstruction finished\r\n',datestr(now));
else
    fprintf(fid,'[%s] [RQ_RunReconJob] !!! reconstruction failed !!!\r\n',datestr(now));
end
fclose(fid);

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('RQ_RunReconJob ["%s" finished]',folder),2);
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_RunReconJob ["%s" failed]',folder),2);
end

end