function [] = RQ_AnonymizeDicomData(RQ_parm,E7_parm,SourceFolder,DestinationFolder)

% anonymization works in two steps:
% 1) use anonymize.exe to remove all patient relevant information from
%    public dicom fields
% 2) remove all private dicom tags which might include (even if hidden)
%    patient information
%
% - after anonymization the resulting dicom slices are copied to the
%   DestinationFolder and temporary folders are deleted
%
% - some dicom warning messages will be disables to not unnecessary flood
%   the command line if processing several folders. this is reasonable
%   since we assume to have valid dicom images before anonymizing the data
warning('off','images:dicom_add_attr:invalidAttribChar');
warning('off','images:dicom_add_attr:wrongAttribNum');

%% 1) use anonymize.exe
cmdline = [RQ_parm.base_path '\Tools\anonymize.exe'];
cmdline = [cmdline ' -i "' SourceFolder '"'];
cmdline = [cmdline ' -o "' SourceFolder '_anon"'];
cmdline = [cmdline ' -n "' E7_parm.anonymized_id '"'];
cmdline = [cmdline ' -m -p -u'];
cmdline = [cmdline ' -j "0"'];
disp(cmdline);
dos(cmdline);

%% 2) remove private fields from dicom header
% create target folder
mkdir(DestinationFolder);

% get dir info of source folder
filelist = dir([SourceFolder '_anon']);

% loop over all slices
for slice=3:numel(filelist)
    % get dicominfo and fields for current slice
    metadata = dicominfo([SourceFolder '_anon\' filelist(slice).name]);
    names = fieldnames(metadata);
    
    % loop over all fields and remove all marked as Private
    for i=1:numel(names)
        fieldname = strfind(names(i),'Private');
        if (numel(fieldname{1}) == 1)
            metadata = rmfield(metadata,names(i));
        end
        
        fieldname = strfind(names(i),'SeriesDescription');
        if (numel(fieldname{1}) == 1)
            metadata.SeriesDescription = E7_parm.anonymized_id;
        end
    end
    
    % get data for current slice from tmp folder
    data = dicomread([SourceFolder '_anon\' filelist(slice).name]);
    
    % write data and modified header for current slice
    dicomwrite(data,[DestinationFolder '\' filelist(slice).name],metadata,'CreateMode','copy');
end

% remove tmp folder
rmdir([SourceFolder '_anon'],'s');

end
