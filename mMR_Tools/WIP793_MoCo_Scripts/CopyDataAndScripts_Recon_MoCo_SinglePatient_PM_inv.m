function [] = CopyDataAndScripts_Recon_MoCo_SinglePatient_PM_inv(base_path_win_results,base_path_win_transfer,base_path_win_emrecon,gates,patient,matrix_size)

% check for target folder
if (exist(sprintf('%s',base_path_win_transfer),'dir') ~= 7)
    mkdir(sprintf('%s',base_path_win_transfer));
end

if (exist(sprintf('%s\\EMrecon',base_path_win_transfer),'dir') ~= 7)
    mkdir(sprintf('%s\\EMrecon',base_path_win_transfer));
    mkdir(sprintf('%s\\EMrecon\\Tools',base_path_win_transfer));
    dirinfo = dir(sprintf('%s\\Tools',base_path_win_emrecon));
    for i=3:numel(dirinfo)
        dos(sprintf('copy %s\\Tools\\%s %s\\EMrecon\\Tools',base_path_win_emrecon,dirinfo(i).name,base_path_win_transfer));
    end
    dirinfo = dir(sprintf('%s',base_path_win_emrecon));
    for i=3:numel(dirinfo)
        if (exist(sprintf('%s\\%s',base_path_win_emrecon,dirinfo(i).name),'dir') ~= 7)
            dos(sprintf('copy %s\\%s %s\\EMrecon',base_path_win_emrecon,dirinfo(i).name,base_path_win_transfer));
        end
    end
end

if (exist(sprintf('%s\\%s',base_path_win_transfer,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s',base_path_win_transfer,patient));
end

if (exist(sprintf('%s\\%s\\data',base_path_win_transfer,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\data',base_path_win_transfer,patient));
end

if (exist(sprintf('%s\\%s\\results',base_path_win_transfer,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\results',base_path_win_transfer,patient));
end

% copy data
if (exist(sprintf('%s\\%s\\data\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') ~= 2)
    dos(sprintf('copy %s\\%s\\data\\%s_PET_AllGates_AC_%d_%d.mat %s\\%s\\data',base_path_win_results,patient,patient,gates,matrix_size,base_path_win_transfer,patient));
end
if (exist(sprintf('%s\\%s\\data\\%s_PET_AllGates_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') ~= 2)
    dos(sprintf('copy %s\\%s\\data\\%s_PET_AllGates_%d_%d.mat %s\\%s\\data',base_path_win_results,patient,patient,gates,matrix_size,base_path_win_transfer,patient));
end
for g=1:gates
    if (exist(sprintf('%s\\%s\\data\\%s_PET_Gate_%d_%d_%d.mat',base_path_win_transfer,patient,patient,g,gates,matrix_size),'file') ~= 2)
        dos(sprintf('copy %s\\%s\\data\\%s_PET_Gate_%d_%d_%d.mat %s\\%s\\data',base_path_win_results,patient,patient,g,gates,matrix_size,base_path_win_transfer,patient));
    end
    if (exist(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected_inv.mat',base_path_win_transfer,patient,patient,g,gates,matrix_size),'file') ~= 2)
        dos(sprintf('copy %s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected_inv.mat %s\\%s\\results',base_path_win_results,patient,patient,g,gates,matrix_size,base_path_win_transfer,patient));
    end
end

% copy recon scripts
dos(sprintf('copy %s\\%s\\%s_recon_gates.m %s\\%s',base_path_win_results,patient,patient,base_path_win_transfer,patient));
dos(sprintf('copy %s\\%s\\%s_recon_mc_pm_inv.m %s\\%s',base_path_win_results,patient,patient,base_path_win_transfer,patient));
dos(sprintf('copy %s\\%s\\%s_recon_mc_pm_inv_0.m %s\\%s',base_path_win_results,patient,patient,base_path_win_transfer,patient));
dos(sprintf('copy %s\\%s\\%s_recon_mc_pm_inv_ac.m %s\\%s',base_path_win_results,patient,patient,base_path_win_transfer,patient));
dos(sprintf('copy %s\\%s\\%s_recon_mc_pm_inv_ac_0.m %s\\%s',base_path_win_results,patient,patient,base_path_win_transfer,patient));

end
