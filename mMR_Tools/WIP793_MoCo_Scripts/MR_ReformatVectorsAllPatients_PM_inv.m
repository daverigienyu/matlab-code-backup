% clean up
close all; clear mex; clear; clc;

% extend path
addpath(genpath('H:\Projects\Matlab'));
addpath(genpath('R:\koestt01labspace\Projects\MotionCorrection\WIP793'));

% set parameter
preview_only = 0;
base_path_win_data = 'R:\koestt01labspace\Data\PET\MotionCorrection\WIP793';
base_path_win_results = 'Q:\labspace\mMR_MotionCorrection\WIP793';
base_path_linux_data = '/srv/mriscan/Working-Temp/koestt01/MoCo_WIP';
reference_gate = 1;
gates = 5;
show_results = 0;

% debug
% fixed_id = [1 2 4 5 6 9 10 13 14 15 16 18 20 21 24 25 28 29 30 32];
fixed_id = 12;

% get list of patients to process
p = 1;
dirinfo = dir(base_path_win_data);
for i=3:numel(dirinfo)
    % get patient folder in base_path_data
    pos = strfind(sprintf('%s\\%s',base_path_win_data,dirinfo(i).name),'Patient_');
    if (numel(pos) > 0)
        if (exist(sprintf('%s\\%s\\Vectors',base_path_win_data,dirinfo(i).name),'dir') == 7)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
        end
    end
end

% process data sets
if (exist('patient_list','var') == 1)
    for i=1:numel(patient_list)
        if (max(fixed_id) < 0 || (min(fixed_id) > 0 && ismember(i,fixed_id) == 1))
            fprintf('%d - %s\n',i,patient_list(i).name);
            reformat_tic = tic;
            if (preview_only == 0)
                MR_ReformatVectorsSinglePatient_PM_inv(base_path_win_data,base_path_win_results,base_path_linux_data,gates,reference_gate,patient_list(i).name,show_results);
            end
            fprintf('%d - %s done. [%f sec]\n',i,patient_list(i).name,toc(reformat_tic));
        end
    end
end
