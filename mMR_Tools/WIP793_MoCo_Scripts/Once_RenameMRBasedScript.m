% Once_RenameMRBasedScript.m

id = 'V';
gates = 5;

base_path = 'R:\koestt01labspace\Data\PET\MotionCorrection\WIP793';

source = sprintf('%s\\Patient_%s\\Patient_%s_MR_GatingSignal.txt',base_path,id,id);
destination = sprintf('%s\\Patient_%s\\Patient_%s_MR_GatingSignal_%d.txt',base_path,id,id,gates);

dos(sprintf('move %s %s',source,destination));
