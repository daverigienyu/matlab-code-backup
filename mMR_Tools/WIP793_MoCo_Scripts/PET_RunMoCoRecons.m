% clean up
close all; clear mex; clear; clc;

% set parameter
base_path_win_transfer = 'Z:\Working-Temp\koestt01\MoCo_WIP';
base_path_linux_data = '/srv/mriscan/Working-Temp/koestt01/MoCo_WIP';
matrix_size = 172;

% select algorithm
run_iw_recons = 0;
run_pm_recons = 0;
run_pm_inv_recons = 0;
run_pm_v2_recons = 1;

% set type
run_0_vectors = 0;
run_1_vectors = 1;
run_ac_recons = 1;
run_nac_recons = 0;

% set gates
gates = 10;

% use second script 0/1
use_second_script = 1;

% debug
fixed_id = -[1 2 4 5 6 9 10 14 15 16 17 19 21 22 25 26 29 30 31 33];

% get list of patients to process
p = 1;
dirinfo = dir(base_path_win_transfer);
for i=3:numel(dirinfo)
    % get patient folder in base_path_win_transfer
    pos = strfind(sprintf('%s\\%s',base_path_win_transfer,dirinfo(i).name),'Patient_');
    if (numel(pos) > 0)
        patient_added = 0;
        % check whether recon script exists
        if (exist(sprintf('%s\\%s\\%s_recon_mc_pm.m',base_path_win_transfer,dirinfo(i).name,dirinfo(i).name),'file') == 2)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
            patient_added = 1;
        end
        if (patient_added == 0 && exist(sprintf('%s\\%s\\%s_recon_mc_pm_inv.m',base_path_win_transfer,dirinfo(i).name,dirinfo(i).name),'file') == 2)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
            patient_added = 1;
        end
        if (patient_added == 0 && exist(sprintf('%s\\%s\\%s_recon_mc_pm_v2.m',base_path_win_transfer,dirinfo(i).name,dirinfo(i).name),'file') == 2)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
            patient_added = 1;
        end
        if (patient_added == 0 && exist(sprintf('%s\\%s\\%s_recon_mc_iw.m',base_path_win_transfer,dirinfo(i).name,dirinfo(i).name),'file') == 2)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
            patient_added = 1;
        end
    end
end

% process data sets
if (exist('patient_list','var') == 1)
    if (use_second_script == 1)
        fid = fopen(sprintf('%s\\RunMoCoRecons2.m',base_path_win_transfer),'w');
    else
        fid = fopen(sprintf('%s\\RunMoCoRecons.m',base_path_win_transfer),'w');
    end
    for i=1:numel(patient_list)
        if (max(fixed_id) < 0 || (min(fixed_id) > 0 && ismember(i,fixed_id) == 1)) % bug!
            fprintf('%d - %s\n',i,patient_list(i).name);
            fprintf(fid,'clear mex; close all; clear; clc;\ncd(''%s/%s'');\n',base_path_linux_data,patient_list(i).name);
            fprintf(fid,'fprintf(''running %d - %s [%d/%d]\\n'');\n',i,patient_list(i).name,i,numel(patient_list));
            
            if (run_pm_recons == 1 && exist(sprintf('%s\\%s\\%s_recon_mc_pm.m',base_path_win_transfer,patient_list(i).name,patient_list(i).name),'file') == 2)
                if (run_0_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_0;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_AC_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_ac_0;\n',patient_list(i).name);
                        end
                    end
                end
                if (run_1_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_AC_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_ac;\n',patient_list(i).name);
                        end
                    end
                end
            end
            
            if (run_pm_inv_recons == 1 && exist(sprintf('%s\\%s\\%s_recon_mc_pm_inv.m',base_path_win_transfer,patient_list(i).name,patient_list(i).name),'file') == 2)
                if (run_0_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_inv_0;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_inv_ac_0;\n',patient_list(i).name);
                        end
                    end
                end
                if (run_1_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_inv;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_inv_ac;\n',patient_list(i).name);
                        end
                    end
                end
            end
            
            if (run_pm_v2_recons == 1 && exist(sprintf('%s\\%s\\%s_recon_mc_pm_v2.m',base_path_win_transfer,patient_list(i).name,patient_list(i).name),'file') == 2)
                if (run_0_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_V2_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_v2_0;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_V2_AC_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_v2_ac_0;\n',patient_list(i).name);
                        end
                    end
                end
                if (run_1_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_V2_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_v2;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_V2_AC_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_pm_v2_ac;\n',patient_list(i).name);
                        end
                    end
                end
            end
            
            if (run_iw_recons == 1 && exist(sprintf('%s\\%s\\%s_recon_mc_iw.m',base_path_win_transfer,patient_list(i).name,patient_list(i).name),'file') == 2)
                if (run_0_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_IW_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_iw_0;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_IW_AC_0_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_iw_ac_0;\n',patient_list(i).name);
                        end
                    end
                end
                if (run_1_vectors == 1)
                    if (run_nac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_IW_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_iw;\n',patient_list(i).name);
                        end
                    end
                    if (run_ac_recons == 1)
                        if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_IW_AC_%d_%d.mat',base_path_win_transfer,patient_list(i).name,patient_list(i).name,gates,matrix_size),'file') ~= 2)
                            fprintf(fid,'%s_recon_mc_iw_ac;\n',patient_list(i).name);
                        end
                    end
                end
            end
            
            fprintf(fid,'fprintf(''running %d - %s [%d/%d] done.\\n'');\n',i,patient_list(i).name,i,numel(patient_list));
            fprintf(fid,'cd(''%s'');\n\n',base_path_linux_data);
            
            fprintf('%d - %s done.\n',i,patient_list(i).name);
        end
    end
    fclose(fid);
end
