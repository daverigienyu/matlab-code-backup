% clean up
close all; clear mex; clear; clc;

% set parameter
base_path_win_transfer = 'Z:\Working-Temp\koestt01\MoCo_WIP';
base_path_linux_data = '/srv/mriscan/Working-Temp/koestt01/MoCo_WIP';
matrix_size = 172;

% use second script 0/1
use_second_script = 0;

% debug
fixed_id = -1;

% get list of patients to process
p = 1;
dirinfo = dir(base_path_win_transfer);
for i=3:numel(dirinfo)
    % get patient folder in base_path_win_transfer
    pos = strfind(sprintf('%s\\%s',base_path_win_transfer,dirinfo(i).name),'Patient_');
    if (numel(pos) > 0)
        % check whether recon script exists
        if (exist(sprintf('%s\\%s\\%s_recon_gates.m',base_path_win_transfer,dirinfo(i).name,dirinfo(i).name),'file') == 2)
            patient_list(p).name = dirinfo(i).name;
            p = p + 1;
        end
    end
end

% process data sets
if (exist('patient_list','var') == 1)
    if (use_second_script == 1)
        fid = fopen(sprintf('%s\\RunUncorrectedRecons2.m',base_path_win_transfer),'w');
    else
        fid = fopen(sprintf('%s\\RunUncorrectedRecons.m',base_path_win_transfer),'w');
    end
    for i=1:numel(patient_list)
        if (max(fixed_id) < 0 || (min(fixed_id) > 0 && ismember(i,fixed_id) == 1))
            fprintf('%d - %s\n',i,patient_list(i).name);
            fprintf(fid,'clear mex; close all; clear; clc;\ncd(''%s/%s'');\n',base_path_linux_data,patient_list(i).name);
            fprintf(fid,'fprintf(''running %d - %s [%d/%d]\\n'');\n',i,patient_list(i).name,i,numel(patient_list));
            fprintf(fid,'%s_recon_gates;\n',patient_list(i).name);
            fprintf(fid,'fprintf(''running %d - %s [%d/%d] done.\\n'');\n',i,patient_list(i).name,i,numel(patient_list));
            fprintf(fid,'cd(''%s'');\n\n',base_path_linux_data);
            
            fprintf('%d - %s done.\n',i,patient_list(i).name);
        end
    end
    fclose(fid);
end
