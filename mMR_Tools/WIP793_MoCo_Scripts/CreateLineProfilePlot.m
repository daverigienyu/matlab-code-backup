%function [] = CreateLineProfilePlot(base_path_win_results,patient)

base_path_win_results = 'R:\koestt01labspace\Projects\MotionCorrection\Boada_Nav';
%base_path_win_results = 'R:\koestt01labspace\Projects\MotionCorrection\WIP793\MoCo_WIP';
patient = 'Boada_Nav_Patient_2';
%patient = 'Patient_P';

id0 = 1;
id1 = 1;
comments = 0;

fid = fopen(sprintf('%s\\%s\\results\\line_profile.tsv',base_path_win_results,patient));
while ~feof(fid)
    cline = fgetl(fid);
    if (numel(strfind(cline,'#') > 0))
        comments = comments + 1;
    else
        if (comments >= 4)
            % entries for dataset 1
            if (comments == 4)
                pos = strfind(cline,'.');
                dataset_0(id0) = str2double(cline(pos(2)-1:end));
                id0 = id0 + 1;
            end
            % entries for dataset 2
            if (comments == 7)
                pos = strfind(cline,'.');
                dataset_1(id1) = str2double(cline(pos(2)-1:end));
                id1 = id1 + 1;
            end
        end
    end
end
fclose(fid);

fig = figure;
set(fig,'Position',[1 1 1920 1280]);
plot(dataset_0); hold on;
plot(dataset_1); hold off;
axis([1 127 0 1.05*max(dataset_1)]);
legend('uncorrected','corrected');
saveas(fig,sprintf('%s\\%s\\results\\line_profile.png',base_path_win_results,patient),'png');

%end
