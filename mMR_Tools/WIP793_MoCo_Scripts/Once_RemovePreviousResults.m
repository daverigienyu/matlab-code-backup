%
base_path_win_transfer = 'Z:\Working-Temp\koestt01\MoCo_WIP';

i = 0;
i = i + 1; patient(i).name = 'Patient_1';
i = i + 1; patient(i).name = 'Patient_6';
i = i + 1; patient(i).name = 'Patient_7';
i = i + 1; patient(i).name = 'Patient_8';
i = i + 1; patient(i).name = 'Patient_9';
i = i + 1; patient(i).name = 'Patient_10';
i = i + 1; patient(i).name = 'Patient_12';
i = i + 1; patient(i).name = 'Patient_13';
i = i + 1; patient(i).name = 'Patient_14';
i = i + 1; patient(i).name = 'Patient_18';
i = i + 1; patient(i).name = 'Patient_19';
i = i + 1; patient(i).name = 'Patient_B';
i = i + 1; patient(i).name = 'Patient_D';
i = i + 1; patient(i).name = 'Patient_F';
i = i + 1; patient(i).name = 'Patient_I';
i = i + 1; patient(i).name = 'Patient_K';
i = i + 1; patient(i).name = 'Patient_P';
i = i + 1; patient(i).name = 'Patient_Q';
i = i + 1; patient(i).name = 'Patient_S';
i = i + 1; patient(i).name = 'Patient_V';

for k=1:numel(patient)
    dirinfo = dir(sprintf('%s\\%s',base_path_win_transfer,patient(k).name));
    for i=3:numel(dirinfo)
        if (numel(strfind(dirinfo(i).name,'.m')) == 1)
            dos(sprintf('rm %s\\%s\\%s',base_path_win_transfer,patient(k).name,dirinfo(i).name));
        end
    end
    
    dirinfo = dir(sprintf('%s\\%s\\results',base_path_win_transfer,patient(k).name));
    for i=3:numel(dirinfo)
        if (numel(strfind(dirinfo(i).name,'.mat')) == 1)
            dos(sprintf('rm %s\\%s\\results\\%s',base_path_win_transfer,patient(k).name,dirinfo(i).name));
        end
    end
end
