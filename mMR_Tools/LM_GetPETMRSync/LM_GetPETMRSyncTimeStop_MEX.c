/* includes */
#include "mex.h"
#include "string.h"


/* emrecon includes */
#include "LM_GetPETMRSyncTimeStop.h"


/*  the gateway routine.  */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    if (nlhs == 1 && nrhs == 2) {
        
        long timetag_long;
        double *timetag_double;
        double *skip_events_tmp;
        int skip_events;
        
        // reading input args
        char *input_buf = mxArrayToString(prhs[0]);
        skip_events_tmp = mxGetData(prhs[1]);
        skip_events = (int)skip_events_tmp[0];
        
        // get timetag from listmode file
        timetag_long = LM_GetPETMRSyncTimeStop(input_buf,skip_events);
        
        // alloc memory for plhs and assign to timtag
        plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
        timetag_double = mxGetPr(plhs[0]);
        timetag_double[0] = (double)timetag_long;
        
        // clean up
        mxFree(input_buf);
    }
    else {
        mexPrintf("LM_GetPETMRSyncTimeStop (MEX) invalid parameter\n");
        plhs[0] = mxCreateNumericMatrix(1,1,mxSINGLE_CLASS,mxREAL);
    }
    
    return;
}
