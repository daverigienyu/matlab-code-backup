% Demo for LM_GetPETMRSyncTime_MEX
clear mex; clear; clc;

% basepath = 'Q:\labspace\mMR_MotionCorrection\RawData\WIP793F\Patient03_Lung_2016_09_28\PET';
basepath = 'g:\'; % local copy, runs forever on network share. slow implementation.
filename = '1.3.12.2.1107.5.2.38.51022.30000016092611433151500000039.bf';

% syntax: LM_GetPETMRSyncTime_MEX(filename,sync_flags_to_skip)
% returns timestamp in ms if valid flag is found, -1 otherwise
i = 1;
run = 1;
while (run == 1)
    starttime(i) = LM_GetPETMRSyncTime_MEX(sprintf('%s\\%s',basepath,filename),i-1);
    if (starttime > -1)
        stoptime(i) = LM_GetPETMRSyncTimeStop_MEX(sprintf('%s\\%s',basepath,filename),i-1);
        fprintf('scan %d, start in ms %d, stop in ms %d, duration in ms %d\n',i,starttime(i),stoptime(i),stoptime(i)-starttime(i));
        i = i + 1;
    else
        run = 0;
    end
end
