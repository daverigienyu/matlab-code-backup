function D = regexpdir(rootdir, varargin)

%==========================================================================
%                               PARSE INPUTS
%--------------------------------------------------------------------------
    p = inputParser;
    addRequired(p, 'rootdir', @isdir);
    addOptional(p, 'pattern', '.*', @ischar);
    addOptional(p, 'doRecursive', false, @islogical);
    addOptional(p, 'ignoreCase', false,  @islogical);

    p.parse(rootdir, varargin{:});
    rootdir = p.Results.rootdir;
    pattern = p.Results.pattern;
    doRecursive = p.Results.doRecursive;
    
    if p.Results.ignoreCase
        regexpfun = @regexpi;
    else
        regexpfun = @regexp;
    end
%__________________________________________________________________________

    fulllist = dir(rootdir);
    fulllist = fulllist(3:end); % Remove . and .. 
    
    ind = cellfun(@isempty, regexpfun({fulllist.name}, pattern));
    D =  fulllist(~ind);
    
    for i = 1:1:numel(D)
       D(i).path = fullfile(rootdir, D(i).name);        
    end
    
    if isempty(D)
        D = [];
    end
    
    if doRecursive
        indDir   = [fulllist.isdir]';
        dirList  = fulllist(indDir);
        
        for iDir = 1:1:numel(dirList)
           dirpath = fullfile(rootdir, dirList(iDir).name);
           Dnew = regexpdir(dirpath, varargin{:});
           D = cat(1, D(:), Dnew(:));
        end
    end
   

end