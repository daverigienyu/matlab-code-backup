function [kdata4d, traj4d, densityCompen4d] = sortGatedKdata(kdata, ...
                                                             traj, ...
                                                             densityCompen, ...
                                                             gatingSignal)

    gatingSignal(gatingSignal == -1) = nan;   
 
    [nCol, nPar, nView, nCoil] = size(kdata);
            
    reshapekdata  = @(x) reshape(x, nCol, nPar*nView, []); 
    kdata         = reshapekdata(kdata);
    traj          = reshapekdata(traj);
    densityCompen = reshapekdata(densityCompen);
    
    nGates = max(gatingSignal, [], 1); %if 2d matrix nGates = [nResp, nCard]
        
    if numel(nGates) == 1
        gatingSignal1d = gatingSignal;
        kdata4d         = cell(nGates, 1);
        traj4d          = cell(nGates, 1);
        densityCompen4d = cell(nGates, 1);
    elseif numel(nGates) == 2
        gatingSignal1d = sub2ind(nGates, gatingSignal(:,1), gatingSignal(:,2));
        kdata4d         = cell(nGates);
        traj4d          = cell(nGates);
        densityCompen4d = cell(nGates);
    else
        error('Only supports 1D or 2D gating right now...');
    end
    
   
    nGatesTotal = prod(nGates);
    
    for i = 1:1:nGatesTotal
       % extract data from gate
       idx                = logical(gatingSignal1d == i);
       chunkSize          = sum(idx);
       
       kdata4d{i}         = kdata(:, idx, :);
       traj4d{i}          = traj(:, idx, :);
       densityCompen4d{i} = densityCompen(:, idx, :);
       
       % Reshape for NUFFT operator
       flatten = @(x) reshape(x, nCol*chunkSize, []);
       kdata4d{i}         = flatten(kdata4d{i});
       traj4d{i}          = flatten(traj4d{i}).';
       densityCompen4d{i} = flatten(densityCompen4d{i});
       
    end
    
    
    
    
    
    
    
 end   





