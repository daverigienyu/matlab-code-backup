%% Load Data File
load('Q:/labspace/MR_Recon_Dave_v2/sampleData/meas_MID03068_FID282426_Abd_RadVibeNyuNavi_082016_kdata_clean.mat');

%% Perform reconstructions with different numbers of views
imarr = cell(0);
viewArr = [800, 400, 200, 100, 50];

clear specNormArr;
for i = 1:1:numel(viewArr) 
    numViews = viewArr(i);
    NVIEWS = numViews
    kdata = kdata_clean(:,:,1:NVIEWS,:);

    [nCol, nPar, nView, nCoil]  =   size(kdata);
                         
    nSlice = imageDim(3);
    
    %Generate the sampling trajectory
    [traj3d, densityCompen3d] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);
    
    if i == 1
        b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);
    end

    
    FT    = gpuNUFFTND({traj3d.'}, {densityCompen3d}, b1);
    FT.densityCompensationMode = 'asymmetric';
    
    % Apply adjoint NUFFT to filtered/densitycomp data
    kdata = {reshape(kdata, [], nCoil)};
    
    x = FT'*kdata;
    
    A = @(x) FT*x;
    AT = @(x) FT'*x;
    
    specNormArr{i} = poweriteration(A,AT,x,20);
    
end


save('specNormViewTest.mat', 'viewArr','specNormArr');
