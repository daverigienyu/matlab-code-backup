%% Load Data File
clear; clc;
load('Q:/labspace/MR_Recon_Dave_v2/sampleData/meas_MID03068_FID282426_Abd_RadVibeNyuNavi_082016_kdata_clean.mat');


%% Estimate Sensitivity Maps based on full kdata

kdata = kdata_clean;
imageDim = [256,256,48];
[nCol, nPar, nView, nCoil]  =   size(kdata);                
nSlice = imageDim(3);

[traj3d, densityCompen3d] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);

b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);
traj3d = reshape(traj3d, nCol, nPar, nView, 3);
densityCompen3d = reshape(densityCompen3d, nCol, nPar, nView);

%% Perform reconstructions with different numbers of views
viewArr = [50, 100, 200, 400, 800];

for i = 1:1:numel(viewArr)
       
    numViews = viewArr(i);
    kdataArr{i} = reshape(kdata_clean(:,:,1:numViews,:),[],nCoil);  

    %Downsample the sampling trajectory
    trajArr{i} = reshape(traj3d(:,:,1:numViews,:) , [], 3).';
    densArr{i} = reshape(densityCompen3d(:, :, 1:numViews), [], 1);
end

FT    = gpuNUFFTND(trajArr, densArr, b1);
FT.densityCompensationMode = 'asymmetric';

% Apply adjoint NUFFT to filtered/densitycomp data

im = FT'*kdataArr;
%im = FT.restore(im);
%imarr = [imarr, abs(im)];
    

%% Save the results

save('viewNumberTest.mat', 'imarr', 'viewArr');


%% Display Results

weights = 800./viewArr;

clear imarr_weighted
for i = 1:1:numel(imarr)
   imarr_weighted{i} = imarr{i}*weights(i); 
end

montage = cat(2, imarr_weighted{:});
imagesc(montage(:,:,23));


%% difference image

for i = 1:1:numel(viewArr)
    figure;
    
    im1 = imarr_weighted{i}(:,:,23);
    im2 = imarr_weighted{5}(:,:,23);

    imagesc(abs(im1-im2));
    title(sprintf('Difference Image, %i views, %i views', viewArr(i), viewArr(end)));

end