%% Load k-space data

basepath      = 'Q:/labspace/MR_Recon_Dave_v2/DualGating_TestData/Patient1_DualGatingTest'
loadfile = @(p) load(fullfile(basepath,p));

kdatafilename = 'meas_MID03068_FID282426_Abd_RadVibeNyuNavi_082016_kdata_clean.mat'
%MF = loadfile(kdatafilename);


%% Set Gating Signal File

%gatingfilename = 'resp_gating_signal_nav_5gates.mat'
%gatingfilename = 'card_gating_signal_nav_5gates.mat'
gatingfilename = 'resp_card_dual_gating_signal_5by5_gates_uniform.mat'

MFgs = loadfile(gatingfilename);


%% Perform CS Reconstruction

kdata = MF.kdata_clean;
downSampleFactor = 2;
kdata = downsamplekdata(kdata, downSampleFactor);
gatingSignal = MFgs.new_gating_signal;
tvWeight = 0.025;
imageDim = [256/downSampleFactor, 256/downSampleFactor, 48];

TV_WEIGHT_RES = 0;
L1_WEIGHT     = 0;
MAXITER       = 5;
DISPLAY       = 1;

[nFE, nPar, nView, nCoil]  =   size(kdata);
nSlice = imageDim(3);

%Generate the sampling trajectory
[traj3d, densityCompen3d] = goldenAngleStackOfStars(nFE, nSlice, nView, nPar);

% estimate coil sensitivies
b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);

% Form gated data, trajectory, etc.
[kdataArr, trajArr, densArr] = sortGatedKdata(kdata, ...
                                                    traj3d, ...
                                                    densityCompen3d, ...
                                                    gatingSignal);

% Create 1D or 2D Gradient Operator Depending on gating signal

if size(gatingSignal,2) > size(gatingSignal,1)
    warning('gating signal oriented wrong, transposing');
    gatingSignal = gatingSignal';
end

nGates = max(gatingSignal, [], 1);

if isvector(gatingSignal)
   % 1D gating, use 1D gradient
   GradOperator = Grad([imageDim, nGates], [4]);
else
   % 2D gating signal, use 2D gradient
   GradOperator = Grad([imageDim, nGates], [4,5]);
end


%% Temp DEBUG
param.y           = col(kdataArr);  
param.E           = gpuNUFFTND(trajArr, densArr, b1); 
param.y           = param.E.scaleData(param.y); % Scale data by sqrt of density compensation for use with symmetrized NUFFT
param.E.densityCompensationMode = 'symmetric'; % applies sqrt of density compensation in forward and adjoint operator
recon_cs          = param.E'*param.y;
param.TV          = GradOperator
param.TVWeight    = max(abs(recon_cs(:)))*tvWeight; %%This is the parameter you want to test
param.TVWeightRes = TV_WEIGHT_RES;
param.L1Weight    = L1_WEIGHT;
param.nite        = MAXITER;
param.display     = DISPLAY;

tic;
for n=1:3
    recon_cs = CSL1NlCg_4DRadial(recon_cs,param);
end

time=toc/60;

recon_cs = abs(recon_cs);
im = recon_cs/max(abs(recon_cs(:)));

fprintf('\r\nDone in %f minutes.\r', time);

im = param.E.restore(im);
save('dualGated_Recon_test_5by5gates.mat','im','gatingSignal','gatingfilename','tvWeight');
