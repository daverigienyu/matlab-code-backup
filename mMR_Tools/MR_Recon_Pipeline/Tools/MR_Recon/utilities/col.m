function [X] = col(x)
% Works on any size matrix or arbitrary cell arrays of arbitrary matrices

    mat2vec = @(m) m(:);

    if iscell(x)
        x = cell2mat(cellfun(mat2vec, x(:), 'UniformOutput', false));
    end

    X = x(:);
    
end
