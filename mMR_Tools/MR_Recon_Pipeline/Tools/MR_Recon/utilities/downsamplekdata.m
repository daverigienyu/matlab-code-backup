function [kdata] = downsampleKdata(kdata, downFactor)

    kdata = ifftshift(kdata, 1);
    [nFE, nPar, nView, nCoil] = size(kdata);

    nLoop = 2;

    for iLoop = 1:1:round(log(downFactor)/log(2));

        kdata = reshape(kdata, [], 2, nPar, nView, nCoil);
        kdata = squeeze(mean(kdata, 2)); 

    end

    kdata = fftshift(kdata, 1);
    
end

