function [kdata] = downsampleKdata(kdata, downFactor)

    % Downsamples along the readout direction

    nCol  = size(kdata,1);
    kdata = fftshift(kdata,1);
    
    ind1  = 1:1:(nCol/downFactor);
    
    % create cell array like {ind1, ':', ':', ':', ... } for arbitrary
    % dimensions
    II    = [{ind1}, repmat({':'}, 1, ndims(kdata)-1)];
    
    kdata = kdata(II{:});
    
    kdata = ifftshift(kdata,1);
    
end

