function y = grad1d(x, dim, dx)

    if nargin < 3
        dx = 1.0;
    end

    % Compute slice indices for arbitrary dimensions
    D         = ndims(x);
    N         = size(x,dim);
    indexing1 = repmat({':'},[1,D]);
    indexing2 = indexing1;
    
    % Perform actual difference operator
    indexing1{dim} = [2:N,N-1];
    indexing2{dim} = [1:N];
    
    y = (x(indexing1{:}) - x(indexing2{:}))/dx;
end
