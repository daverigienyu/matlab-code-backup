function kdata = loadmulticoil(dirpath)

    filelist = regexpdir(dirpath, 'coil');
    nCoil    = numel(filelist);
    
    % Load first coil data to get size 
    MF = load(filelist(1).path);
    kdata1 = MF.data;
    dims   = size(kdata1);
    kdata  = zeros([dims(1:3), nCoil]);
    kdata(:,:,:,1) = kdata1;
    clear kdata1;
    
    tic;
    for iCoil = 2:1:nCoil
       fprintf('\nLoading coil %i of %i', iCoil, nCoil);
       MF = load(filelist(iCoil).path);
       kdata(:,:,:,iCoil) = MF.data;
    end
    toc;
end