function S2 = copyAllFields(S1)

    S2 = struct();
    fieldNames = fields(S1);
    
    for iField = 1:numel(fieldNames)
        f = fieldNames{iField};
        S2 = setfield( S2, f, getfield(S1, f) );
    end
    
end