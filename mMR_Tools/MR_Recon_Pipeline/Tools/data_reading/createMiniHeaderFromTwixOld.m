function [head, numInvalid] = createMiniHeaderFromTwixOld(twix_obj)

    numInvalid = 0;
    %keyboard;
    function val = search(varargin)
       
        [val, successFlag] = getNestedField(twix_obj, varargin{:}); 
       
       if ~successFlag
           queryField = strjoin(varargin,'.');
           %warning(sprintf('Invalid field: %s', queryField));
           numInvalid = numInvalid + 1;
       end
       
    end
    
    head.osfactor              = search('hdr','Meas','flReadoutOSFactor');
    head.centerFreq            = search('hdr','Meas','lFrequency');
    
    head.fov.read              = search('hdr','MeasYaps','sAdjData','sAdjVolume','dReadoutFOV');
    head.fov.phase             = search('hdr','MeasYaps','sAdjData','sAdjVolume','dPhaseFOV');
    head.fov.slice             = search('hdr','MeasYaps','sAdjData','sAdjVolume','dThickness');
    
    head.kdata.dwellTime       = search('hdr', 'Meas', 'alDwellTime'); %nanoseconds
    head.kdata.dwellTime       = head.kdata.dwellTime(1);
    
    head.kdata.dims.nCol       = search('hdr','Config','RawCol');
    head.kdata.dims.nLin       = search('hdr','Meas','lRadialViews'); 
    head.kdata.dims.nPar       = search('hdr','Meas','NParMeas'); 
    head.kdata.dims.nCha       = search('hdr','Meas','NCha'); 
    head.scanTime              = search('hdr','Meas','TotalScanTimeSec'); 
    head.TE                    = cell2mat(search('hdr','MeasYaps','alTE'));
    head.TR                    = cell2mat(search('hdr','MeasYaps','alTR'));
    head.lPartitions           = search('hdr','Meas','lPartitions');
    head.nImagesPerSlab        = search('hdr','Meas','NoImagesPerSlab'); 
    
    head.offset.dSag           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dSag'); % These are mm
    head.offset.dCor           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dCor');
    head.offset.dTra           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dTra');
    
    head.normalVector.dSag     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dSag');
    head.normalVector.dCor     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dCor');
    head.normalVector.dTra     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dTra');
  
    head.globalTablePos.dSag     = search('hdr','Meas','GlobalTablePosSag');
    head.globalTablePos.dCor     = search('hdr','Meas','GlobalTablePosCor');
    head.globalTablePos.dTra     = search('hdr','Meas','GlobalTablePosTra');
    
    head.ImageSize.nRow           = search('hdr','Meas','NImageLins');
    head.ImageSize.nCol           = search('hdr','Meas','NImageCols');
    head.ImageSize.nSlice         = search('hdr','Meas','NImagePar');
        
    head.navigator.dwellTime   = 2500; % This is valid for Rosette as of 9/29/2016
    
    head.coilInfo              = getCoilInfo(twix_obj.hdr);
   
end

 





