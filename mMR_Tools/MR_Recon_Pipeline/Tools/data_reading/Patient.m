
classdef Patient
    %PATIENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        basedir
        Scans
    end
    
    methods
        
        function obj = Patient(basedir, loadEverything)
            
            if nargin == 1
                loadEverything = false;
            end
            
            obj.basedir = basedir;
            obj.Scans = obj.findPTNavScans(1e9, inf, loadEverything);
        end
        
        function Scans = findPTNavScans(obj, minSize, maxSize, loadEverything)     
            
            if nargin == 0
                minSize = 1e9;
                maxSize = inf;
            end
            
            searchdir = fullfile(obj.basedir, 'scanner_data', 'MR');
            listing = dir(fullfile(searchdir,'*.dat'));
            
            filterInd = logical(([listing.bytes] > minSize).*([listing.bytes] < maxSize));
            listing = listing(filterInd);
            
            Scans = {};
            
            for i=1:1:numel(listing)
                twix_path = fullfile(searchdir, listing(i).name);
                Scan = PTNavScan(twix_path, loadEverything);
                Scans = [Scans; Scan];
            end
        end
        
        
    end
    
    
    
end

