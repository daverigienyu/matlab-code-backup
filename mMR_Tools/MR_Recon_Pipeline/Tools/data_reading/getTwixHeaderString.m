function [headerString] = getTwixHeaderString(filepath)

    if nargin == 0
        [filename, pathname] = uigetfile('*.dat', 'Select TWIX', '../data/PT_2016_08_11/TWIX/');
        filepath = fullfile(pathname, filename);
    end

    headerString = 0;

    [path, name, ext] = fileparts(filepath);

    fid = fopen(filepath,'r','l','US-ASCII');

    firstInt = fread(fid,1,'uint32'); % Length of header
    secondInt = fread(fid,1,'uint32'); % ???

    %fseek(fid, MYSTERY_INTEGER1,0); 
    headerString = fread(fid,firstInt,'*char')';

    match = regexp(headerString,'<XProtocol>.*\}','match');
    headerString = match{1};

    fclose(fid);     

end