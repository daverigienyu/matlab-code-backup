function [twix_obj] = getkdata(twixpath)

    twix_obj = mapVBVD_Nav(twixpath, 'IMG');
    twix_obj = convertTwixObj(twix_obj, 'IMG');

end