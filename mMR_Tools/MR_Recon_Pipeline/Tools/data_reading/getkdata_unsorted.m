function [twixobj] = getkdata_unsorted(twixpath, readType, ecoIdx)

    %======================================================================
    %                       PARAMETER CHEKCING
    %======================================================================
    if nargin < 3
        ecoIdx = 'ALL';
    end
    
    if nargin < 2
        readType = 'IMG';
    end

    %======================================================================
    %                           FUNCTION BODY
    %======================================================================
    
    twixobj = mapVBVD_Nav(twixpath, readType); % readType is either NAV or IMG
    
    if numel(twixobj) > 1
        twixobj = twixobj{end};
    end
    
    NAcq    = twixobj.image.NAcq; % Total number of readouts
    
    % Extract readouts in acquisition order, skipping over echoes as
    % necessary
    
    if strcmpi(ecoIdx, 'ALL')
        idx = (twixobj.image.Eco == twixobj.image.Eco);
    else
        idx = (twixobj.image.Eco == ecoIdx);
    end
    
    twixobj.data = twixobj.image.unsorted(idx);
    tt = getTimestampMilliseconds(twixobj);
    twixobj.time_milliseconds = tt(idx);
    
    % Convert from twix_map_obj to ordinary struct
    twixobj.image = copyAllFields(twixobj.image);
    twixobj = copyAllFields(twixobj);
    
    
end

