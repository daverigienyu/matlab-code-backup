function [] = writeDicomFilesPETgrid(twix_obj, petpath, basepath)

    voxelData = abs(twix_obj.data);
    
    nGate = size(voxelData(:,:,:,:), 4);

    IVpet = ImageVolumeDicom(petpath);
    
    voxelDataMoco = zeros([size(IVpet.voxelData), nGate]);
    
    for iGate = 1:nGate
	dirpath = sprintf('%s_Gate%02i', basepath, iGate);
	IV  = ImageVolumeTwix(voxelData(:,:,:,iGate), twix_obj);
        IV = IV.regridvolume(IVpet); 
	IV.writeDicomStack(dirpath);	
        voxelDataMoco(:,:,:,iGate) = IV.voxelData;
    end
    
    % Save mr recon data gridded for motion estimation
    twix_obj.data = flip(swapaxes(voxelDataMoco, 1, 2), 3);
    [dirpath, filename] = fileparts(basepath);
    p = fullfile(dirpath, 'mr_recon_Moco_grid.mat');
    save(p, '-struct', 'twix_obj');

end