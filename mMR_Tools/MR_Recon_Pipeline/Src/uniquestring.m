function s = uniquestring(N)

    if nargin == 0 
        N = 10;
    end

    % Seed randperm with clock so that successive runs yield different results.
    s = RandStream('mt19937ar', 'seed' , sum(100*clock));

    alphabet = ['a':'z','A':'Z','0':'9'];
    ind      = randperm(s, numel(alphabet), N);
    s        = alphabet(ind);
end
