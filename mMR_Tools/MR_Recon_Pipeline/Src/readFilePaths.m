function [filelist] = readFilePaths(txtpath)

    fulltext = fileread(txtpath);
    lines = strsplit(fulltext,'\n');
    
    clear filelist;
    for i = 1:1:numel(lines)
       filelist{i} = strtrim(lines{i}); 
    end
    
end

