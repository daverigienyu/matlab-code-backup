classdef BeamerSlide < BeamerTemplate
   
    properties
    end
    
    methods
        
        function this = BeamerSlide(templateName)
            thisdir = fileparts(mfilename('fullpath'));
            templateDir = fullfile(thisdir, 'slideTemplates');
            
            if exist(templateName, 'file')
                templatePath = templateName;
            else
                templatePath       = fullfile(templateDir, templateName);
            end
            
            this = this@BeamerTemplate(templatePath);             
        end
        
    end
    
end