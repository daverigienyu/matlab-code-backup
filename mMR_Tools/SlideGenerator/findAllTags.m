function [skeleton, TagArr] = findAllTags(fulltext)

    
    pattern = '\\TAG{<([^>]+)>}(.*)\\TAG{</\1>}';
    skeleton = regexp(fulltext, pattern, 'split');
    tagText  = regexp(fulltext, pattern, 'match');
    
    clear TagArr
    for iTag = 1:1:numel(tagText)
        TagArr(iTag) = Tag(tagText{iTag});
    end
    
end
    