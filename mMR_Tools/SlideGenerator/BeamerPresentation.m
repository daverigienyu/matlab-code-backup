classdef BeamerPresentation < BeamerTemplate
   
    properties
        SlideArr = [];
        nSlide;
    end
    
    methods
        
        function this = BeamerPresentation(templateName)
            thisdir = fileparts(mfilename('fullpath'));
            templateDir = fullfile(thisdir, 'presentationTemplates');
            
            if exist(templateName, 'file')
                templatePath = templateName;
            else
                templatePath       = fullfile(templateDir, templateName);
            end
            
            this = this@BeamerTemplate(templatePath);             
        end
        
        function [] =  addSlide(this, slideObj)
            this.SlideArr = cat(1,this.SlideArr, slideObj);  
        end
        
        function nSlide = get.nSlide(this)
           nSlide = numel(this.SlideArr); 
        end
        
        function s = print(this)
           
            allFrames = '';
            
            for i = 1:1:numel(this.SlideArr)
               allFrames = sprintf('%s\n\n%s', allFrames, ... 
                                   this.SlideArr(i).print());
            end
            
            allFrames = sprintf('\n%s\n', allFrames);
            
            this.setTag('Frames', allFrames);
            
            s = print@BeamerTemplate(this);
            
        end
        
        function flag = write(this, outputPath)
            
            fid = fopen(outputPath, 'w+');
            flag = fprintf(fid, '%s', this.print());
            fclose(fid);
            
        end
        
    end
    
end