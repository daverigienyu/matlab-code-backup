classdef BeamerTemplate < handle
   
    properties
        templatePath
        templateString
        skeleton
        TagArr
    end
    
    methods
        
        function this = BeamerTemplate(templatePath)
            this.templatePath       = templatePath;
            this.templateString     = fileread(this.templatePath);
            [this.skeleton, this.TagArr]      = findAllTags(this.templateString);
            
        end
        
        function s = print(this)
           
            s = '';
            N = numel(this.skeleton);
            
            for i = 1:1:(N-1)
               s = strcat(s, this.skeleton{i}, this.TagArr(i).print());            
            end
            
            s = strcat(s, this.skeleton{end});
            
        end
        
        function T = getTag(this, tagName)
            ind = find(strcmp({this.TagArr.Name}, tagName));
            T = this.TagArr(ind);
        end
        
        function [] = setTag(this, tagName, tagValue)
           T = this.getTag(tagName);
           T.Value = tagValue;
        end
        
    end
    
end