function S = parse_tag_dump(filepath)

    fulltext = fileread(filepath);
    firstrow = regexp(fulltext,'\w+.*?\n','match', 'once');
    headers = strsplit(strtrim(firstrow));
    
    numCols = numel(headers);
    
    matchpattern = strjoin(repmat({'(\w+)'}, [1,numCols]), '[ ]+');
    matchpattern = strcat(matchpattern, '\S*');
    
    M = regexp(fulltext, matchpattern, 'tokens');
    M = vertcat(M{2:end});
    
    S = struct();
    for i = 1:1:numel(headers)
        h = headers{i};
        h = strrep(h, '(ms)', '');
        
        if i ~= 2
            S = setfield(S, h, str2double(M(:,i)));
        else
            S = setfield(S, h, M(:,i));
        end
    end
    
    
    
    
    
    
    
end