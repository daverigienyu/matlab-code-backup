addpath(genpath(pwd));

OVERSAMPLING_FACTOR = 2;
KERNEL_WIDTH = 3;
SECTOR_WIDTH = 8;

IMAGE_DIM = [64,64,46];

%% Data parameters
col = @(x) x(:);
k = repmat((-0.5:1/64:0.5-1/64), [1 64*64]);
k(2,:) = repmat(col(repmat((-0.5:1/64:0.5-1/64), [64 1])), [64 1]);
k(3,:) = col(repmat((-0.5:1/64:0.5-1/64), [64*64 1]));

A = gpuNUFFT(k,ones(size(k,2),1),OVERSAMPLING_FACTOR, ... 
                                 KERNEL_WIDTH, ...
                                 SECTOR_WIDTH, ...
                                 IMAGE_DIM, ...
                                 [],true);
K = @(y) A*y; Kt = @(x) A'*x;

disp('----------------');
disp('Adjoint Test');
disp('----------------');
p1 = rand(IMAGE_DIM);
p2 = rand(64^3,1);

p1 = p1 + i*rand(IMAGE_DIM);
p2 = p2 + i*rand([64^3,1]);

s1 = K(p1);
s2 = Kt(p2);
p1s2 = p1(:)' * s2(:); %Only real part would need to vanish
p2s1 = s1(:)' * p2(:);
fprintf('\np1*s2 = %E', p1s2);
fprintf('\np2*s1 = %E', p2s1);
fprintf('\np1*s2-p2*s1 = %E', p1s2-p2s1);
fprintf('\n(p1*s2-p2*s1)/p1*s2 = %E', (p1s2-p2s1)/p1s2);
