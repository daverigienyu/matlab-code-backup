project(EMrecon)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "../Bin/")

# add OpenMP support if available
find_package(OpenMP)
if(OPENMP_FOUND)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
endif()

set(SRC "Main/EMrecon.c")
set(SRC ${SRC} "Main/Tools.c")
set(SRC ${SRC} "Main/Siddon.c")

set(IO "IO/IO.c")
set(IO ${IO} "IO/Parameter.c")

set(RECONALGORITHMS "ReconAlgorithms/BackProjection.c")
set(RECONALGORITHMS ${RECONALGORITHMS} "ReconAlgorithms/ForwardProjection.c")
set(RECONALGORITHMS ${RECONALGORITHMS} "ReconAlgorithms/EM.c")

set(SRC_SCANNER "ScannerGeometries/Scanner.c")
set(SRC_SCANNER ${SRC_SCANNER} "ScannerGeometries/PET/PET_Scanner.c")

add_definitions("-DPET_2D_DEMO")
set(SRC_SCANNER ${SRC_SCANNER} "ScannerGeometries/PET/Demo/PET_2D_Demo.c")

set(SRC_SCANNER ${SRC_SCANNER} "ScannerGeometries/CT/CT_Scanner.c")
add_definitions("-DCT_SIEMENS_DEFINITION_AS")
set(SRC_SCANNER ${SRC_SCANNER} "ScannerGeometries/CT/CT_Siemens_Definition_AS.c")
add_definitions("-DCT_SIEMENS_FORCE")
set(SRC_SCANNER ${SRC_SCANNER} "ScannerGeometries/CT/CT_Siemens_Force.c")

add_executable(${PROJECT_NAME} ${SRC} ${SRC_SCANNER} ${IO} ${RECONALGORITHMS} ${PLUGINS})
target_link_libraries(${PROJECT_NAME} "m")
