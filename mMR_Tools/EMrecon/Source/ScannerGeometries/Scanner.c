/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "Scanner.h"


/* ############################################################################################# */
/* ### derive_subset_vector ############################################################## D ### */
/* ############################################################################################# */
/**
 * @brief derive_subset_vector
 * @param current_subset
 * @param subsetvector
 */
void derive_subset_vector(int angles, int current_subset, int *subsetvector)
{
    int phi,s,s2,t,skip_s2;
    int dtmp,dista,distb;
    int subset,tmp_dist,tmp_subset;
    int *tmp_subsetvector;

    /* default subset choice */
    if (Parameter.subset_order == 1)
    {
        /* subset 0 = 0 fixed */
        tmp_subsetvector = (int*)safe_calloc(Parameter.subsets,sizeof(int));
        tmp_subsetvector[0] = 0;

        for(s=1;s<Parameter.subsets;s++) {

            tmp_dist = 0;
            tmp_subset = 0;

            /* loop over s2 */
            for(s2=1;s2<Parameter.subsets;s2++) {

                /* check for possible subsets */
                skip_s2 = 0;
                for(t=0;t<=s;t++) {
                    if (s2 == tmp_subsetvector[t]) {
                        skip_s2 = 1;
                    }
                }

                /* use only valid subsets */
                if (skip_s2 == 0) {

                    /* evaluate distance measure */
                    if (tmp_subsetvector[s-1] == 0 && s2 == Parameter.subsets-1) {
                        dtmp = 1;
                    }
                    else {
                        dista = abs(tmp_subsetvector[s-1]-s2);
                        distb = abs(Parameter.subsets-dista);
                        dtmp = min(dista,distb);
                    }

                    /* check for new maximum */
                    if (dtmp > tmp_dist) {
                        if (s2 != tmp_subsetvector[s-1]) {
                            tmp_dist = dtmp;
                            tmp_subset = s2;
                        }
                    }
                }
            }
            tmp_subsetvector[s] = tmp_subset;
        }

        subset = tmp_subsetvector[current_subset];
        free(tmp_subsetvector);

        /* calculate valid angles for current_subset */
        for(phi=0;phi<angles/Parameter.subsets;phi++) {
            subsetvector[phi] = phi*Parameter.subsets+subset;
        }
    }
}


/* ############################################################################################# */
/* ### get_number_of_events_in_file ############################################################ */
/* ############################################################################################# */
/**
 * @brief get_number_of_events_in_file
 * @return
 */
long get_number_of_events_in_file()
{
    long number_of_events_in_file = -1;

    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        number_of_events_in_file = pet_2d_demo_get_number_of_events_in_file();
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        number_of_events_in_file = pet_michelogram_get_number_of_events_in_file();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        number_of_events_in_file = pet_michelogram_get_number_of_events_in_file();
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        number_of_events_in_file = ct_scanner_get_number_of_events_in_file();
        break;
#endif

#ifdef CT_SIEMENS_FORCE
    case 22:
        number_of_events_in_file = ct_scanner_get_number_of_events_in_file();
        break;
#endif
    }
    return number_of_events_in_file;
}


/* ############################################################################################# */
/* ### read_events ############################################################################# */
/* ############################################################################################# */
/**
 * @brief read_events
 * @param events_to_read
 * @param Eventvector
 */
void read_events(long events_to_read, event *Eventvector)
{
    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        pet_2d_demo_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_michelogram_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_michelogram_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_siemens_definition_as_read_events(Eventvector);
        break;
#endif

#ifdef CT_SIEMENS_FORCE
    case 22:
        ct_siemens_force_read_events(Eventvector);
        break;
#endif
    }
}


/* ############################################################################################# */
/* ### read_events_ct ########################################################################## */
/* ############################################################################################# */
/**
 * @brief read_events_ct
 * @param Eventvector
 * @param samplingindex
 */
void read_events_ct(event *Eventvector, int samplingindex_phi, int samplingindex_z)
{
    switch (Parameter.scannertype)
    {
#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_siemens_definition_as_read_events_fs(Eventvector, samplingindex_phi, samplingindex_z);
        break;
#endif

#ifdef CT_SIEMENS_FORCE
    case 22:
        ct_siemens_force_read_events_fs(Eventvector, samplingindex_phi, samplingindex_z);
        break;
#endif
    }
}

/* ############################################################################################# */
/* ### release_memory ########################################################################## */
/* ############################################################################################# */
/**
 * @brief release_memory
 */
void release_memory()
{
    switch (Parameter.scannertype)
    {
#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_michelogram_release_memory();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_michelogram_release_memory();
        break;
#endif
    }

    ParameterReleaseMemory();
}


/* ############################################################################################# */
/* ### scanner_info ############################################################################ */
/* ############################################################################################# */
/**
 * @brief scanner_info
 * @return
 */
char* scanner_info()
{
    char *PET_2D_DEMO_INFO;
    char *PET_SIEMENS_INVEON_INFO;
    char *PET_SIEMENS_MMR_INFO;
    char *CT_SIEMENS_DEFINITION_AS_INFO;
    char *CT_SIEMENS_FORCE_INFO;

    char *scanner_description = (char*)calloc(1024,sizeof(char));

#ifdef PET_2D_DEMO
    /* 1 */
    PET_2D_DEMO_INFO = pet_2d_demo_info();
    strcat(scanner_description,PET_2D_DEMO_INFO);
    free(PET_2D_DEMO_INFO);
#endif

#ifdef PET_SIEMENS_INVEON
    /* 4 */
    PET_SIEMENS_INVEON_INFO = pet_siemens_inveon_info();
    strcat(scanner_description,PET_SIEMENS_INVEON_INFO);
    free(PET_SIEMENS_INVEON_INFO);
#endif

#ifdef PET_SIEMENS_MMR
    /* 8 */
    PET_SIEMENS_MMR_INFO = pet_siemens_mmr_info();
    strcat(scanner_description,PET_SIEMENS_MMR_INFO);
    free(PET_SIEMENS_MMR_INFO);
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    /* 21 */
    CT_SIEMENS_DEFINITION_AS_INFO = ct_siemens_definition_as_info();
    strcat(scanner_description,CT_SIEMENS_DEFINITION_AS_INFO);
    free(CT_SIEMENS_DEFINITION_AS_INFO);
#endif

#ifdef CT_SIEMENS_FORCE
    /* 22 */
    CT_SIEMENS_FORCE_INFO = ct_siemens_force_info();
    strcat(scanner_description,CT_SIEMENS_FORCE_INFO);
    free(CT_SIEMENS_FORCE_INFO);
#endif

    return scanner_description;
}


/* ############################################################################################# */
/* ### setup_scanner ########################################################################### */
/* ############################################################################################# */
/**
 * @brief setup_scanner
 */
void setup_scanner()
{
    /* load scanner setup */
    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        pet_2d_demo_setup();
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_siemens_inveon_setup();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_siemens_mmr_setup();
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_siemens_definition_as_setup();
        break;
#endif

#ifdef CT_SIEMENS_FORCE
    case 22:
        ct_siemens_force_setup();
        break;
#endif

    default:
#ifdef PET_2D_DEMO
        pet_2d_demo_setup();
        Parameter.scannertype = 1;
#else
        message(0,"selected scannertype (%d) does not exist\n",Parameter.scannertype);
        exit(EXIT_FAILURE);
#endif
    }

    /* update parameter */
    UpdateParameterList();
}


/* ############################################################################################# */
/* ### update_subset_index ##################################################################### */
/* ############################################################################################# */
/**
 * @brief update_subset_index
 * @param subsetindex
 * @param subset
 */
void update_subset_index(short *subsetindex, int subset)
{
    /* load scanner setup */
    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        pet_scanner_update_subset_index(subsetindex,subset);
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        /*pet_siemens_inveon_setup();*/
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        /*pet_siemens_mmr_setup();*/
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_scanner_update_subset_index(subsetindex,subset);
        break;
#endif

#ifdef CT_SIEMENS_FORCE
    case 22:
        ct_scanner_update_subset_index(subsetindex,subset);
        break;
#endif
    }
}
