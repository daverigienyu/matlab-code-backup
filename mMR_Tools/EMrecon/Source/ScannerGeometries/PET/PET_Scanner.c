/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "PET_Scanner.h"


/* ############################################################################################# */
/* ### pet_scanner_calculate_crystal_positions ########################################## D #### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_calculate_crystal_positions
 */
void pet_scanner_calculate_crystal_positions() {

    int crystal_index;
    int ab,cab,tb,ctb;

    double theta;
    double h_y,h_z;
    double t_x,t_y,t_z;
    double s_l,s_r,t_f,t_b;
    double Ra,Rb,Rc,Rd,Re,Rf,Rg,Rh,Ri;
    double n_x,n_y,n_z,m_x,m_y,m_z,p_x,p_y,p_z;

    message(5,"calculating pet crystal positions...\n");

    if (PET_Scanner.spherical == 1 && (PET_Scanner.v_crystals_between_blocks > 0 || PET_Scanner.v_rings_between_blocks > 0)) {
        message(5,"virtual crystals not supported for spherical geometry\n");
        message(5,"calculating crystal positions...aborted.\n\n");
    }

    message(5,"axial_blocks: \t \t \t \t \t \t %d\n",PET_Scanner.axial_blocks);
    message(5,"transversal_blocks: \t \t \t \t %d\n",PET_Scanner.transversal_blocks);

    message(5,"block_offset: \t \t \t \t \t \t %f mm\n",PET_Scanner.block_offset);
    message(5,"radius: \t \t \t \t \t \t \t %f mm\n",PET_Scanner.radius);
    message(5,"rotation_offset: \t \t \t \t \t %f\n",(PET_Scanner.rotation_offset/M_PI)*180.0);

    message(5,"axial_crystals_per_block: \t \t \t %d\n",PET_Scanner.axial_crystals_per_block);
    message(5,"transversal_crystals_per_block: \t %d\n",PET_Scanner.transversal_crystals_per_block);

    message(5,"crystal_width_x: \t \t \t \t \t %f mm\n",PET_Scanner.crystal_width_x);
    message(5,"crystal_width_y: \t \t \t \t \t %f mm\n",PET_Scanner.crystal_width_y);
    message(5,"crystal_width_z: \t \t \t \t \t %f mm\n",PET_Scanner.crystal_width_z);
    message(5,"crystal_gap_y: \t \t \t \t \t \t %f mm\n",PET_Scanner.crystal_gap_y);
    message(5,"crystal_gap_z: \t \t \t \t \t \t %f mm\n",PET_Scanner.crystal_gap_z);

    if (PET_Scanner.spherical == 1) {
        message(5,"spherical: \t \t \t \t \t \t \t %d\n",PET_Scanner.spherical);
        message(5,"spherical_angle: \t \t \t \t \t %f\n",(PET_Scanner.spherical_angle/M_PI)*180.0);
    }

    if (PET_Scanner.v_crystals_between_blocks > 0) {
        message(5,"v_crystals_between_blocks: \t \t \t %d\n",PET_Scanner.v_crystals_between_blocks);
        message(5,"v_crystal_width_y: \t \t \t \t \t %f mm\n",PET_Scanner.v_crystal_width_y);
    }

    if (PET_Scanner.v_rings_between_blocks > 0) {
        message(5,"v_rings_between_blocks: \t \t \t %d\n",PET_Scanner.v_rings_between_blocks);
        message(5,"v_crystal_width_z: \t \t \t \t \t %f mm\n",PET_Scanner.v_crystal_width_z);
    }

    if (PET_Scanner.v_crystals_between_blocks > 0 || PET_Scanner.v_rings_between_blocks > 0) {
        message(5,"use_virtual_crystals: \t \t \t \t %d\n",PET_Scanner.use_virtual_crystals);
    }

    crystal_index = 0;

    PET_Scanner.rings = PET_Scanner.axial_blocks*PET_Scanner.axial_crystals_per_block;
    PET_Scanner.v_rings = (PET_Scanner.axial_blocks-1)*PET_Scanner.v_rings_between_blocks;

    PET_Scanner.crystals_per_ring = PET_Scanner.transversal_blocks*PET_Scanner.transversal_crystals_per_block;
    PET_Scanner.v_crystals_per_ring = PET_Scanner.transversal_blocks*PET_Scanner.v_crystals_between_blocks;

    PET_Crystal_Array = (pet_crystal*)safe_calloc((PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring),sizeof(pet_crystal));

    /* loop over Axial Blocks */
    for (ab=0;ab<PET_Scanner.axial_blocks;ab++)
    {
        /* loop over Crystals per Axial Block */
        for (cab=0;cab<PET_Scanner.axial_crystals_per_block+PET_Scanner.v_rings_between_blocks;cab++)
        {
            /* loop over Transversal Blocks */
            for (tb=0;tb<PET_Scanner.transversal_blocks;tb++)
            {
                /* loop over Crystals per Transversal Block */
                for (ctb=0;ctb<PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks;ctb++)
                {
                    /* catch virtual crystals after last block */
                    if (crystal_index < (PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring))
                    {
                        PET_Crystal_Array[crystal_index].virtual_crystal = 0;
                        PET_Crystal_Array[crystal_index].efficiency = 1.0;

                        h_y = (PET_Scanner.transversal_crystals_per_block*(PET_Scanner.crystal_width_y+PET_Scanner.crystal_gap_y)-PET_Scanner.crystal_gap_y)/2.0;
                        h_z = (PET_Scanner.axial_blocks*(PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z)-PET_Scanner.crystal_gap_z)+(PET_Scanner.axial_blocks-1)*PET_Scanner.block_offset)/2.0;

                        n_x = - cos(((double)tb)/((double)PET_Scanner.transversal_blocks)*2.0*M_PI+M_PI/2.0+PET_Scanner.rotation_offset);
                        n_y = + sin(((double)tb)/((double)PET_Scanner.transversal_blocks)*2.0*M_PI+M_PI/2.0+PET_Scanner.rotation_offset);
                        n_z = 0.0;

                        m_x = + n_y;
                        m_y = - n_x;
                        m_z = 0.0;

                        p_x = 0.0;
                        p_y = 0.0;
                        p_z = 1.0;

                        s_l = -h_y + ctb*(PET_Scanner.crystal_width_y+PET_Scanner.crystal_gap_y);
                        s_r = s_l + PET_Scanner.crystal_width_y;

                        t_f = -h_z + cab*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z) + ab*(PET_Scanner.block_offset+PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z));
                        t_b = t_f + PET_Scanner.crystal_width_z;


                        /* virtual crystals in axial direction */
                        if (PET_Scanner.v_rings_between_blocks > 0)
                        {

                            h_z = (PET_Scanner.axial_blocks*(PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z)-PET_Scanner.crystal_gap_z)+(PET_Scanner.axial_blocks-1)*(PET_Scanner.v_rings_between_blocks*(PET_Scanner.block_offset+PET_Scanner.v_crystal_width_z)+PET_Scanner.block_offset))/2.0;
                            t_f = -h_z + ab*(PET_Scanner.v_rings_between_blocks*(PET_Scanner.block_offset+PET_Scanner.v_crystal_width_z)+PET_Scanner.block_offset+PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z)-PET_Scanner.crystal_gap_z);

                            /* real crystals */
                            if (cab < PET_Scanner.axial_crystals_per_block) {
                                t_f += cab*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z);
                                t_b = t_f + PET_Scanner.crystal_width_z;
                            }

                            /* virtual crystals */
                            if (cab >= PET_Scanner.axial_crystals_per_block) {
                                if (PET_Scanner.use_virtual_crystals != 1) {
                                    PET_Crystal_Array[crystal_index].virtual_crystal = 1;
                                }

                                t_f += (PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z)-PET_Scanner.crystal_gap_z)+(cab-PET_Scanner.axial_crystals_per_block)*(PET_Scanner.block_offset+PET_Scanner.v_crystal_width_z)+PET_Scanner.block_offset;
                                t_b = t_f + PET_Scanner.v_crystal_width_z;
                            }
                        }


                        /* virtual crystals in transversal plane */
                        if (ctb >= PET_Scanner.transversal_crystals_per_block)
                        {
                            if (PET_Scanner.use_virtual_crystals != 1)
                            {
                                PET_Crystal_Array[crystal_index].virtual_crystal = 1;
                            }

                            h_y = (PET_Scanner.v_crystals_between_blocks*(PET_Scanner.v_crystal_width_y+PET_Scanner.crystal_gap_y)-PET_Scanner.crystal_gap_y)/2.0;

                            n_x = - cos((0.5+(double)tb)/((double)PET_Scanner.transversal_blocks)*2.0*M_PI+M_PI/2.0+PET_Scanner.rotation_offset);
                            n_y = + sin((0.5+(double)tb)/((double)PET_Scanner.transversal_blocks)*2.0*M_PI+M_PI/2.0+PET_Scanner.rotation_offset);
                            n_z = 0.0;

                            m_x = + n_y;
                            m_y = - n_x;
                            m_z = 0.0;

                            s_l = (-h_y + (ctb-PET_Scanner.transversal_crystals_per_block)*(PET_Scanner.v_crystal_width_y+PET_Scanner.crystal_gap_y));
                            s_r = s_l + PET_Scanner.v_crystal_width_y;
                        }


                        /* spherical scanner geometry */
                        if (PET_Scanner.spherical == 1)
                        {

                            h_z = (PET_Scanner.axial_crystals_per_block*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z) - PET_Scanner.crystal_gap_z)/2.0;

                            t_f = (-h_z + ((cab+ab*PET_Scanner.axial_crystals_per_block)%PET_Scanner.axial_crystals_per_block)*(PET_Scanner.crystal_width_z+PET_Scanner.crystal_gap_z));
                            t_b = t_f + PET_Scanner.crystal_width_z;

                            if ((cab+ab*PET_Scanner.axial_crystals_per_block)/PET_Scanner.axial_crystals_per_block != PET_Scanner.axial_blocks/2)
                            {
                                theta = (((cab+ab*PET_Scanner.axial_crystals_per_block)/PET_Scanner.axial_crystals_per_block)-PET_Scanner.axial_blocks/2) * PET_Scanner.spherical_angle;

                                Ra = cos(theta) + m_x*m_x*(1-cos(theta));
                                Rb = m_x*m_y*(1-cos(theta));
                                Rc = m_y*sin(theta);

                                Rd = m_x*m_y*(1-cos(theta));
                                Re = cos(theta) + m_y*m_y*(1-cos(theta));
                                Rf = -m_x*sin(theta);

                                Rg = -m_y*sin(theta);
                                Rh = m_x*sin(theta);
                                Ri = cos(theta);

                                t_x = n_x*Ra+n_y*Rb+n_z*Rc;
                                t_y = n_x*Rd+n_y*Re+n_z*Rf;
                                t_z = n_x*Rg+n_y*Rh+n_z*Ri;

                                n_x = t_x;
                                n_y = t_y;
                                n_z = t_z;

                                t_x = m_x*Ra+m_y*Rb+m_z*Rc;
                                t_y = m_x*Rd+m_y*Re+m_z*Rf;
                                t_z = m_x*Rg+m_y*Rh+m_z*Ri;

                                m_x = t_x;
                                m_y = t_y;
                                m_z = t_z;

                                t_x = p_x*Ra+p_y*Rb+p_z*Rc;
                                t_y = p_x*Rd+p_y*Re+p_z*Rf;
                                t_z = p_x*Rg+p_y*Rh+p_z*Ri;

                                p_x = t_x;
                                p_y = t_y;
                                p_z = t_z;
                            }
                        }


                        /* assign crystal coordinates */
                        PET_Crystal_Array[crystal_index].A_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * PET_Scanner.radius + m_x * s_l + p_x * t_f);
                        PET_Crystal_Array[crystal_index].A_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * PET_Scanner.radius + m_y * s_l + p_y * t_f);
                        PET_Crystal_Array[crystal_index].A_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * PET_Scanner.radius + m_z * s_l + p_z * t_f);

                        PET_Crystal_Array[crystal_index].B_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * PET_Scanner.radius + m_x * s_r + p_x * t_f);
                        PET_Crystal_Array[crystal_index].B_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * PET_Scanner.radius + m_y * s_r + p_y * t_f);
                        PET_Crystal_Array[crystal_index].B_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * PET_Scanner.radius + m_z * s_r + p_z * t_f);

                        PET_Crystal_Array[crystal_index].C_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * PET_Scanner.radius + m_x * s_l + p_x * t_b);
                        PET_Crystal_Array[crystal_index].C_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * PET_Scanner.radius + m_y * s_l + p_y * t_b);
                        PET_Crystal_Array[crystal_index].C_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * PET_Scanner.radius + m_z * s_l + p_z * t_b);

                        PET_Crystal_Array[crystal_index].D_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * PET_Scanner.radius + m_x * s_r + p_x * t_b);
                        PET_Crystal_Array[crystal_index].D_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * PET_Scanner.radius + m_y * s_r + p_y * t_b);
                        PET_Crystal_Array[crystal_index].D_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * PET_Scanner.radius + m_z * s_r + p_z * t_b);

                        PET_Crystal_Array[crystal_index].E_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_x * s_l + p_x * t_f);
                        PET_Crystal_Array[crystal_index].E_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_y * s_l + p_y * t_f);
                        PET_Crystal_Array[crystal_index].E_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_z * s_l + p_z * t_f);

                        PET_Crystal_Array[crystal_index].F_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_x * s_r + p_x * t_f);
                        PET_Crystal_Array[crystal_index].F_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_y * s_r + p_y * t_f);
                        PET_Crystal_Array[crystal_index].F_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_z * s_r + p_z * t_f);

                        PET_Crystal_Array[crystal_index].G_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_x * s_l + p_x * t_b);
                        PET_Crystal_Array[crystal_index].G_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_y * s_l + p_y * t_b);
                        PET_Crystal_Array[crystal_index].G_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_z * s_l + p_z * t_b);

                        PET_Crystal_Array[crystal_index].H_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_x * s_r + p_x * t_b);
                        PET_Crystal_Array[crystal_index].H_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_y * s_r + p_y * t_b);
                        PET_Crystal_Array[crystal_index].H_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * (PET_Scanner.radius+PET_Scanner.crystal_width_x) + m_z * s_r + p_z * t_b);

                        PET_Crystal_Array[crystal_index].P_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * (PET_Scanner.radius+PET_Scanner.doi) + m_x * (s_l + PET_Scanner.crystal_width_y/2.0) + p_x * (t_f + PET_Scanner.crystal_width_z/2.0));
                        PET_Crystal_Array[crystal_index].P_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * (PET_Scanner.radius+PET_Scanner.doi) + m_y * (s_l + PET_Scanner.crystal_width_y/2.0) + p_y * (t_f + PET_Scanner.crystal_width_z/2.0));
                        PET_Crystal_Array[crystal_index].P_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * (PET_Scanner.radius+PET_Scanner.doi) + m_z * (s_l + PET_Scanner.crystal_width_y/2.0) + p_z * (t_f + PET_Scanner.crystal_width_z/2.0));

                        PET_Crystal_Array[crystal_index].Q_x = (-2*PET_Scanner.flip_x_axis+1)*(n_x * PET_Scanner.radius + m_x * (s_l + PET_Scanner.crystal_width_y/2.0) + p_x * (t_f + PET_Scanner.crystal_width_z/2.0));
                        PET_Crystal_Array[crystal_index].Q_y = (-2*PET_Scanner.flip_y_axis+1)*(n_y * PET_Scanner.radius + m_y * (s_l + PET_Scanner.crystal_width_y/2.0) + p_y * (t_f + PET_Scanner.crystal_width_z/2.0));
                        PET_Crystal_Array[crystal_index].Q_z = (-2*PET_Scanner.flip_z_axis+1)*(n_z * PET_Scanner.radius + m_z * (s_l + PET_Scanner.crystal_width_y/2.0) + p_z * (t_f + PET_Scanner.crystal_width_z/2.0));


                        /* show crystal info if desired */
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) A: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].A_x,PET_Crystal_Array[crystal_index].A_y,PET_Crystal_Array[crystal_index].A_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) B: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].B_x,PET_Crystal_Array[crystal_index].B_y,PET_Crystal_Array[crystal_index].B_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) C: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].C_x,PET_Crystal_Array[crystal_index].C_y,PET_Crystal_Array[crystal_index].C_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) D: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].D_x,PET_Crystal_Array[crystal_index].D_y,PET_Crystal_Array[crystal_index].D_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) E: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].E_x,PET_Crystal_Array[crystal_index].E_y,PET_Crystal_Array[crystal_index].E_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) F: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].F_x,PET_Crystal_Array[crystal_index].F_y,PET_Crystal_Array[crystal_index].F_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) G: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].G_x,PET_Crystal_Array[crystal_index].G_y,PET_Crystal_Array[crystal_index].G_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) H: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].H_x,PET_Crystal_Array[crystal_index].H_y,PET_Crystal_Array[crystal_index].H_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) P: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].P_x,PET_Crystal_Array[crystal_index].P_y,PET_Crystal_Array[crystal_index].P_z);
                        message(10,"(%d, ab: %d, cab: %d, tb: %d, ctb: %d) Q: %f %f %f\n",crystal_index,ab,cab,tb,ctb,PET_Crystal_Array[crystal_index].Q_x,PET_Crystal_Array[crystal_index].Q_y,PET_Crystal_Array[crystal_index].Q_z);

                        crystal_index++;
                    }
                }
            }
        }
    }

    /* we assumed that the first crystal is always valid, ie not virtual. in order to include virtual crystals we introduce the crystal offset to obtain a valid numbering of crystals */
    if (PET_Scanner.crystal_offset_a > 0)
    {

        int crystal,ring;
        pet_crystal *tmp_PET_Crystal_Array = (pet_crystal*)safe_calloc((PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring),sizeof(pet_crystal));

        for (crystal_index=0;crystal_index<(PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring);crystal_index++)
        {
            tmp_PET_Crystal_Array[crystal_index] = PET_Crystal_Array[crystal_index];

        }

        for (crystal_index=0;crystal_index<(PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring);crystal_index++)
        {
            crystal = crystal_index % (PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring);
            ring = (crystal_index-crystal) / (PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring);
            PET_Crystal_Array[crystal_index] = tmp_PET_Crystal_Array[(crystal+PET_Scanner.crystal_offset_a)%(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)+ring*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)];
        }
        free(tmp_PET_Crystal_Array);
    }
    message(5,"calculating pet crystal positions...done.\n");
}


/* ############################################################################################# */
/* ### pet_scanner_convert_d1d2_to_rphi ################################################## D ### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_convert_d1d2_to_rphi
 * @param D1
 * @param D2
 * @param r
 * @param phi
 */
void pet_scanner_convert_d1d2_to_rphi(int D1, int D2, int *r, int *phi)
{
    int cpr = PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring;

    r[0] = ((D1 - D2 + cpr/2 + Sinogram.projections/2) + Sinogram.projections) % Sinogram.projections;

    if (D1 > D2)
    {
        r[0] = ((D2 - D1 + cpr/2 + Sinogram.projections/2) + Sinogram.projections) % Sinogram.projections;
        r[0] = Sinogram.projections - r[0];
    }

    if (abs(D1-D2)%2 == 0)
    {
        phi[0] = (((D1+D2)/2 - Sinogram.angles/2 - PET_Scanner.crystal_offset_b) + 2*Sinogram.angles) % Sinogram.angles;
    }

    if (abs(D1-D2)%2 == 1)
    {
        phi[0] = (((D1+D2+1)/2 - Sinogram.angles/2 - PET_Scanner.crystal_offset_b) + 2*Sinogram.angles) % Sinogram.angles;
    }
}


/* ############################################################################################# */
/* ### pet_scanner_convert_rphi_to_d1d2 ################################################## D ### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_convert_rphi_to_d1d2
 * @param r
 * @param phi
 * @param D1
 * @param D2
 */
void pet_scanner_convert_rphi_to_d1d2(int r, int phi, int *D1, int *D2)
{
    int cpr = PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring;

    if (r%2==0)
    {
        D1[0] = ((PET_Scanner.crystal_offset_b + phi + Sinogram.angles/2 + r/2 - cpr/4 - Sinogram.projections/4) + cpr) % cpr;
        D2[0] = ((PET_Scanner.crystal_offset_b + phi + Sinogram.angles/2 - r/2 + cpr/4 + Sinogram.projections/4) + cpr) % cpr;
    }

    if (r%2==1)
    {
        D1[0] = ((PET_Scanner.crystal_offset_b + phi + Sinogram.angles/2 + (r-1)/2 - cpr/4 - Sinogram.projections/4) + cpr) % cpr;
        D2[0] = ((PET_Scanner.crystal_offset_b + phi + Sinogram.angles/2 - (r+1)/2 + cpr/4 + Sinogram.projections/4) + cpr) % cpr;
    }
}


/* ############################################################################################# */
/* ### pet_scanner_derive_subset_vector ################################################## D ### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_derive_subset_vector
 * @param current_subset
 * @param subsetvector
 */
void pet_scanner_derive_subset_vector(int current_subset, int *subsetvector) {

    int phi,s,s2,t,skip_s2;
    int dtmp,dista,distb;
    int tmp_dist,tmp_subset;
    int *tmp_subsetvector;
    int subset,angles_per_view;

    /* default subset choice */
    if (Parameter.subset_order == 1)
    {
        /* subset 0 = 0 fixed */
        tmp_subsetvector = (int*)safe_calloc(Parameter.subsets,sizeof(int));
        tmp_subsetvector[0] = 0;

        for(s=1;s<Parameter.subsets;s++) {

            tmp_dist = 0;
            tmp_subset = 0;

            /* loop over s2 */
            for(s2=1;s2<Parameter.subsets;s2++) {

                /* check for possible subsets */
                skip_s2 = 0;
                for(t=0;t<=s;t++) {
                    if (s2 == tmp_subsetvector[t]) {
                        skip_s2 = 1;
                    }
                }

                /* use only valid subsets */
                if (skip_s2 == 0) {

                    /* evaluate distance measure */
                    if (tmp_subsetvector[s-1] == 0 && s2 == Parameter.subsets-1) {
                        dtmp = 1;
                    }
                    else {
                        dista = abs(tmp_subsetvector[s-1]-s2);
                        distb = abs(Parameter.subsets-dista);
                        dtmp = min(dista,distb);
                    }

                    /* check for new maximum */
                    if (dtmp > tmp_dist) {
                        if (s2 != tmp_subsetvector[s-1]) {
                            tmp_dist = dtmp;
                            tmp_subset = s2;
                        }
                    }
                }
            }
            tmp_subsetvector[s] = tmp_subset;
        }

        subset = tmp_subsetvector[current_subset];
        free(tmp_subsetvector);

        /* calculate valid angles for current_subset */
        for(phi=0;phi<Sinogram.angles/Parameter.subsets;phi++) {
            subsetvector[phi] = phi*Parameter.subsets+subset;
        }
    }

    /* alternative subset choice */
    if (Parameter.subset_order == 2)
    {
        /* it has to be checked upfront whether value for angles_per_view is valid */
        angles_per_view = Sinogram.angles / (4*Parameter.subsets);

        /* calculate valid angles for current_subset, use s as index for valid angle */
        s = 0;
        for(phi=0;phi<Sinogram.angles;phi++) {
            if ((phi >= (0*Sinogram.angles/4+current_subset*angles_per_view) && phi < (0*Sinogram.angles/4+(current_subset+1)*angles_per_view)) ||
                    (phi >= (1*Sinogram.angles/4+current_subset*angles_per_view) && phi < (1*Sinogram.angles/4+(current_subset+1)*angles_per_view)) ||
                    (phi >= (2*Sinogram.angles/4+current_subset*angles_per_view) && phi < (2*Sinogram.angles/4+(current_subset+1)*angles_per_view)) ||
                    (phi >= (3*Sinogram.angles/4+current_subset*angles_per_view) && phi < (3*Sinogram.angles/4+(current_subset+1)*angles_per_view)))
            {
                subsetvector[s] = phi;
                s++;
            }
        }
    }
}


/* ############################################################################################# */
/* ### pet_scanner_disable_virtual_crystals #################################################### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_disable_virtual_crystals
 */
void pet_scanner_disable_virtual_crystals()
{
}


/* ############################################################################################# */
/* ### pet_scanner_draw_crystal_geometry ####################################################### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_draw_crystal_geometry
 * @param output_image
 */
void pet_scanner_draw_crystal_geometry(double *output_image) {

    int i,j,c,r;
    int crystals_to_draw = 0;
    path_element *Path;

    setup_siddon();
    crystals_to_draw = UpdateParameterInt("CRYSTALS_TO_DRAW",crystals_to_draw);
    Path = (path_element*)(safe_calloc(Parameter.size_x+Parameter.size_y+Parameter.size_z, sizeof(path_element)));

    j = 0;
    /* loop over all crystals in each ring and draw 12 lines connecting the 8 corners of each crystal */
    for (r=0;r<PET_Scanner.rings+PET_Scanner.v_rings;r++)
    {
        for (c=0;c<PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring;c++)
        {

            if (crystals_to_draw == 2 || PET_Crystal_Array[j].virtual_crystal == crystals_to_draw)
            {

                compute_path(PET_Crystal_Array[j].A_x, PET_Crystal_Array[j].A_y, PET_Crystal_Array[j].A_z, PET_Crystal_Array[j].C_x, PET_Crystal_Array[j].C_y, PET_Crystal_Array[j].C_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].B_x, PET_Crystal_Array[j].B_y, PET_Crystal_Array[j].B_z, PET_Crystal_Array[j].D_x, PET_Crystal_Array[j].D_y, PET_Crystal_Array[j].D_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].E_x, PET_Crystal_Array[j].E_y, PET_Crystal_Array[j].E_z, PET_Crystal_Array[j].G_x, PET_Crystal_Array[j].G_y, PET_Crystal_Array[j].G_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].F_x, PET_Crystal_Array[j].F_y, PET_Crystal_Array[j].F_z, PET_Crystal_Array[j].H_x, PET_Crystal_Array[j].H_y, PET_Crystal_Array[j].H_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].E_x, PET_Crystal_Array[j].E_y, PET_Crystal_Array[j].E_z, PET_Crystal_Array[j].F_x, PET_Crystal_Array[j].F_y, PET_Crystal_Array[j].F_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].G_x, PET_Crystal_Array[j].G_y, PET_Crystal_Array[j].G_z, PET_Crystal_Array[j].H_x, PET_Crystal_Array[j].H_y, PET_Crystal_Array[j].H_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].A_x, PET_Crystal_Array[j].A_y, PET_Crystal_Array[j].A_z, PET_Crystal_Array[j].B_x, PET_Crystal_Array[j].B_y, PET_Crystal_Array[j].B_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].C_x, PET_Crystal_Array[j].C_y, PET_Crystal_Array[j].C_z, PET_Crystal_Array[j].D_x, PET_Crystal_Array[j].D_y, PET_Crystal_Array[j].D_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].E_x, PET_Crystal_Array[j].E_y, PET_Crystal_Array[j].E_z, PET_Crystal_Array[j].A_x, PET_Crystal_Array[j].A_y, PET_Crystal_Array[j].A_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].F_x, PET_Crystal_Array[j].F_y, PET_Crystal_Array[j].F_z, PET_Crystal_Array[j].B_x, PET_Crystal_Array[j].B_y, PET_Crystal_Array[j].B_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].G_x, PET_Crystal_Array[j].G_y, PET_Crystal_Array[j].G_z, PET_Crystal_Array[j].C_x, PET_Crystal_Array[j].C_y, PET_Crystal_Array[j].C_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }

                compute_path(PET_Crystal_Array[j].H_x, PET_Crystal_Array[j].H_y, PET_Crystal_Array[j].H_z, PET_Crystal_Array[j].D_x, PET_Crystal_Array[j].D_y, PET_Crystal_Array[j].D_z, Path);
                for (i=0;Path[i].coord!=-1;i++)
                {
                    output_image[Path[i].coord] += Path[i].length;
                }
            }
            j++;
        }
    }
    free(Path);
}


/* ############################################################################################# */
/* ### pet_scanner_release_memory ####################################################### D #### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_release_memory
 */
void pet_scanner_release_memory()
{
    if (PET_Crystal_Array != 0)
    {
        free(PET_Crystal_Array);
    }

    if (PET_Scanner.norm_axial_effects != 0)
    {
        free(PET_Scanner.norm_axial_effects);
    }

    if (PET_Scanner.norm_crystal_interference != 0)
    {
        free(PET_Scanner.norm_crystal_interference);
    }

    if (PET_Scanner.norm_geometry_effects != 0)
    {
        free(PET_Scanner.norm_geometry_effects);
    }
}


/* ############################################################################################# */
/* ### pet_scanner_setup ################################################################ D #### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_setup
 */
void pet_scanner_setup()
{
    PET_Scanner.axial_blocks = 0;
    PET_Scanner.transversal_blocks = 0;

    PET_Scanner.spherical = 0;
    PET_Scanner.spherical_angle = 0.0;

    PET_Scanner.use_virtual_crystals = 1;

    PET_Scanner.flip_x_axis = 0;
    PET_Scanner.flip_y_axis = 1;
    PET_Scanner.flip_z_axis = 0;

    PET_Scanner.v_crystals_between_blocks = 0;
    PET_Scanner.v_rings_between_blocks = 0;
    PET_Scanner.v_crystal_width_y = 0.0;
    PET_Scanner.v_crystal_width_z = 0.0;

    PET_Scanner.crystal_offset_a = 0;
    PET_Scanner.crystal_offset_b = 0;

    PET_Scanner.doi = 0.0;
}


/* ############################################################################################# */
/* ### pet_scanner_update_parameter ##################################################### D #### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_update_parameter
 */
void pet_scanner_update_parameter()
{
    PET_Scanner.use_virtual_crystals = UpdateParameterInt("USE_VIRTUAL_CRYSTALS",PET_Scanner.use_virtual_crystals);
}


/* ############################################################################################# */
/* ### pet_scanner_update_subset_index ######################################################### */
/* ############################################################################################# */
/**
 * @brief pet_scanner_update_subset_index
 * @param subsetindex
 * @param subset
 */
void pet_scanner_update_subset_index(short *subsetindex, int subset)
{
    int i,r,phi,s,valid_phi,sinograms;
    long events_in_file = get_number_of_events_in_file();
    int *subsetvector;

    if (Parameter.subsets > 1)
    {
        /* choose subsets */
        subsetvector = (int*)safe_calloc(Sinogram.angles/Parameter.subsets,sizeof(int));
        derive_subset_vector(Sinogram.angles,subset-1,subsetvector);

        /* this should help to handle michelograms and sinogram stacks */
        sinograms = events_in_file/Sinogram.angles/Sinogram.projections;

        for (phi=0;phi<Sinogram.angles/Parameter.subsets;phi++)
        {
            valid_phi = subsetvector[phi];

            for (s=0;s<sinograms;s++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    subsetindex[r+valid_phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] = 1;
                }
            }
        }

        free(subsetvector);
    }
    else
    {
        for (i=0;i<events_in_file;i++)
        {
            subsetindex[i] = 1;
        }
    }
}
