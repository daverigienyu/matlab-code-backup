/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef CT_SCANNER_H
#define CT_SCANNER_H


/* emrecon includes */
#include "../../IO/Parameter.h"


/* struct definitions */
typedef struct
{
    int angles,
    detector_channels,
    detector_rows,
    ffs_mode,
    readings;
    double center_channel_no_ffs,
    center_channel_ffs_even,
    center_channel_ffs_odd,
    detector_alignment,
    distance_source_rcenter,
    distance_rcenter_detector,
    fan_angle_increment,
    rotation_offset,
    slice_width,
    source_z0;
    double *fan_angle_correction;
} ct_scanner;


/* global variables */
ct_scanner CT_Scanner;


/* function declarations */
long ct_scanner_get_number_of_events_in_file();
void ct_scanner_read_events(event *Eventvector);
void ct_scanner_read_events_fs(event *Eventvector, int samplingindex_phi);
void ct_scanner_setup();
void ct_scanner_update_parameter();
void ct_scanner_update_subset_index(short *subsetindex, int subset);

#endif
