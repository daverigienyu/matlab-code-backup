/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "CT_Siemens_Force.h"


/* ############################################################################################# */
/* ### ct_siemens_force_info ################################################################### */
/* ############################################################################################# */
/**
 * @brief ct_siemens_force_info Displays scanner info.
 * @return
 */
char* ct_siemens_force_info()
{
    char *scanner_info = (char*)calloc(50,sizeof(char));
    sprintf(scanner_info,"\t\t\t 22) [CT] Siemens Somaton FORCE\n");
    return scanner_info;
}


/* ############################################################################################# */
/* ### ct_siemens_force_read_events ############################################################ */
/* ############################################################################################# */
/**
 * @brief ct_siemens_force_read_events
 * @param Eventvector
 */
void ct_siemens_force_read_events(event *Eventvector)
{
    int c,r,phi;
    double row_width;
    double *bed_position = (double*)safe_calloc(CT_Scanner.readings,sizeof(double));
    float *fan_angle_correction = (double*)safe_calloc(CT_Scanner.detector_channels*CT_Scanner.detector_rows,sizeof(double));
    float *fan_angle_correction_tmp;
    char fan_angle_correction_filename[255];
    FILE *fan_angle_correction_file;

    /* read table motion */
    GetParameterVectorDouble("BED_POSITION",bed_position,CT_Scanner.readings);

    /* fan angle correction */
    /* - this corrects for non-equidistant spacing of the fan angle */
    /* - initialized with 0, in case a valid file is found, values are overwritten */
    /* - in case of wide collimation, adjacent values are averaged - only if reasonable size is found */
    GetParameterChar("FAN_ANGLE_CORRECTION_FILENAME",fan_angle_correction_filename);
    fan_angle_correction_file = fopen(fan_angle_correction_filename,"rb");

    if (fan_angle_correction_file != 0)
    {
        /* in case of matching dimensions just read and copy values */
        if (GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS") == CT_Scanner.detector_channels && GetParameterInt("FAN_ANGLE_CORRECTION_ROWS") == CT_Scanner.detector_rows)
        {
            fan_angle_correction_tmp = (float*)safe_calloc(CT_Scanner.detector_channels*CT_Scanner.detector_rows,sizeof(float));
            fread(fan_angle_correction_tmp,sizeof(float),CT_Scanner.detector_channels*CT_Scanner.detector_rows,fan_angle_correction_file);
            for (c=0;c<CT_Scanner.detector_channels;c++)
            {
                for (r=0;r<CT_Scanner.detector_rows;r++)
                {
                    fan_angle_correction[c+r*CT_Scanner.detector_channels] = fan_angle_correction_tmp[c+r*CT_Scanner.detector_channels];
                }
            }
            free(fan_angle_correction_tmp);
        }

        /* in case of matching different collimation read and average values */
        if (GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS") == CT_Scanner.detector_channels && GetParameterInt("FAN_ANGLE_CORRECTION_ROWS") == 2*CT_Scanner.detector_rows)
        {
            fan_angle_correction_tmp = (float*)safe_calloc(GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS")*GetParameterInt("FAN_ANGLE_CORRECTION_ROWS"),sizeof(float));
            fread(fan_angle_correction_tmp,sizeof(float),GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS")*GetParameterInt("FAN_ANGLE_CORRECTION_ROWS"),fan_angle_correction_file);
            for (c=0;c<CT_Scanner.detector_channels;c++)
            {
                for (r=0;r<CT_Scanner.detector_rows;r++)
                {
                    fan_angle_correction[c+r*CT_Scanner.detector_channels] = (fan_angle_correction_tmp[c+(2*r)*CT_Scanner.detector_channels] + fan_angle_correction_tmp[c+(2*r+1)*CT_Scanner.detector_channels])/2.0;
                }
            }
            free(fan_angle_correction_tmp);
        }
        fclose(fan_angle_correction_file);
    }

    /* calculate width of rows used for given configuration */
    row_width = CT_Scanner.slice_width * (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) / CT_Scanner.distance_source_rcenter;

    /* loop over readings, rows and channels */
#pragma omp parallel for
    for (phi=0;phi<CT_Scanner.readings;phi++)
    {
        int current_channel,current_row;
        double n_x,n_y,m_x,m_y;
        double alpha,beta;
        double det_x,det_y,det_z;
        double source_x,source_y,source_z;
        double ffs_offset_x, ffs_offset_y, ffs_offset_z;

        /* define flying focal spot offset */
        if (CT_Scanner.ffs_mode == 0)
        {
            /* stationary focal spot: A-A-A-A-... */
            /* A */
            ffs_offset_x = -0.16;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 1)
        {
            /* phi (in-plane) flying focal spot: B-C-B-C-... */
            /* B */
            if (phi%2 == 0)
            {
                ffs_offset_x = +0.31;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* C */
            if (phi%2 == 1)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }
        }

        if (CT_Scanner.ffs_mode == 2)
        {
            /* combined flying focal spot: F-G-H-I-F-G-H-I-... */
            /* F */
            if (phi%4 == 0)
            {
                ffs_offset_x = 0.31;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* G */
            if (phi%4 == 1)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* H */
            if (phi%4 == 2)
            {
                ffs_offset_x = 0.31;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }

            /* I */
            if (phi%4 == 3)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }
        }

        if (CT_Scanner.ffs_mode == 3)
        {
            /* z flying focal spot: ???? */
            /* no value given for the force */
            ffs_offset_x = 0.0;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 4)
        {
            /* diagonal focal spot: D-E-D-E-... */
            /* D */
            if (phi%2 == 0)
            {
                ffs_offset_x = 0.47;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* E */
            if (phi%2 == 1)
            {
                ffs_offset_x = -0.16;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }
        }

        /* calculate rotation angle */
        /* - rotation clockwise */
        /* - rotation offset to account for starting position */
        alpha = 2.0 * M_PI / CT_Scanner.angles * (phi - (CT_Scanner.rotation_offset-1.0));
        n_x = cos(alpha);
        n_y = - sin(alpha);
        m_x = n_y;
        m_y = - n_x;

        /* source coordinates */
        /* - use source_z0 and bed_position to identify z position */
        source_x = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_x + ffs_offset_x * m_x;
        source_y = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_y + ffs_offset_x * m_y;
        source_z = CT_Scanner.source_z0 + bed_position[phi] + ffs_offset_z;

        /* calculate detector coordinates */
        for (current_row=0;current_row<CT_Scanner.detector_rows;current_row++)
        {
            det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - 0.5);

            for (current_channel=0;current_channel<CT_Scanner.detector_channels;current_channel++)
            {
                /* - current_channel indicates current detector channel in vertical direction */
                /* - rotate half the detector and shift to center of this detector (+0.5) */
                /* - we are adding detector_alignment, Siemens seems to subtract... */
                beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + 0.5 + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment + fan_angle_correction[current_channel+current_row*CT_Scanner.detector_channels]);
                det_x = n_x * CT_Scanner.distance_source_rcenter - (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * cos(alpha + beta);
                det_y = n_y * CT_Scanner.distance_source_rcenter + (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * sin(alpha + beta);

                /* store geometry in Eventvector */
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].x1 = source_x;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].y1 = source_y;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].z1 = source_z;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].x2 = det_x;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].y2 = det_y;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].z2 = det_z;
                Eventvector[current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows].valid = 1;
            }
        }
    }
    free(bed_position);
    free(fan_angle_correction);
}


/* ############################################################################################# */
/* ### ct_siemens_force_read_events_fs ######################################################### */
/* ############################################################################################# */
/**
 * @brief ct_siemens_force_read_events_fs
 * @param Eventvector
 * @param samplingindex
 */
void ct_siemens_force_read_events_fs(event *Eventvector, int samplingindex_phi, int samplingindex_z)
{
    int c,r,phi;
    double row_width;
    double *bed_position = (double*)safe_calloc(CT_Scanner.readings,sizeof(double));
    float *fan_angle_correction = (double*)safe_calloc(CT_Scanner.detector_channels*CT_Scanner.detector_rows,sizeof(double));
    float *fan_angle_correction_tmp;
    char fan_angle_correction_filename[255];
    FILE *fan_angle_correction_file;

    /* read table motion */
    GetParameterVectorDouble("BED_POSITION",bed_position,CT_Scanner.readings);

    /* fan angle correction */
    /* - this corrects for non-equidistant spacing of the fan angle */
    /* - initialized with 0, in case a valid file is found, values are overwritten */
    /* - in case of wide collimation, adjacent values are averaged - only if reasonable size is found */
    GetParameterChar("FAN_ANGLE_CORRECTION_FILENAME",fan_angle_correction_filename);
    fan_angle_correction_file = fopen(fan_angle_correction_filename,"rb");

    if (fan_angle_correction_file != 0)
    {
        /*message(0,"using FAN ANGLE CORRECTION\n");*/
        /* in case of matching dimensions just read and copy values */
        if (GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS") == CT_Scanner.detector_channels && GetParameterInt("FAN_ANGLE_CORRECTION_ROWS") == CT_Scanner.detector_rows)
        {
            fan_angle_correction_tmp = (float*)safe_calloc(CT_Scanner.detector_channels*CT_Scanner.detector_rows,sizeof(float));
            fread(fan_angle_correction_tmp,sizeof(float),CT_Scanner.detector_channels*CT_Scanner.detector_rows,fan_angle_correction_file);
            for (c=0;c<CT_Scanner.detector_channels;c++)
            {
                for (r=0;r<CT_Scanner.detector_rows;r++)
                {
                    fan_angle_correction[c+r*CT_Scanner.detector_channels] = fan_angle_correction_tmp[c+r*CT_Scanner.detector_channels];
                }
            }
            free(fan_angle_correction_tmp);
            /*message(0,"using FAN ANGLE CORRECTION [A]\n");*/
        }

        /* in case of matching different collimation read and average values */
        if (GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS") == CT_Scanner.detector_channels && GetParameterInt("FAN_ANGLE_CORRECTION_ROWS") == 2*CT_Scanner.detector_rows)
        {
            fan_angle_correction_tmp = (float*)safe_calloc(GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS")*GetParameterInt("FAN_ANGLE_CORRECTION_ROWS"),sizeof(float));
            fread(fan_angle_correction_tmp,sizeof(float),GetParameterInt("FAN_ANGLE_CORRECTION_CHANNELS")*GetParameterInt("FAN_ANGLE_CORRECTION_ROWS"),fan_angle_correction_file);
            for (c=0;c<CT_Scanner.detector_channels;c++)
            {
                for (r=0;r<CT_Scanner.detector_rows;r++)
                {
                    fan_angle_correction[c+r*CT_Scanner.detector_channels] = (fan_angle_correction_tmp[c+(2*r)*CT_Scanner.detector_channels] + fan_angle_correction_tmp[c+(2*r+1)*CT_Scanner.detector_channels])/2.0;
                }
            }
            free(fan_angle_correction_tmp);
            /*message(0,"using FAN ANGLE CORRECTION [B]\n");*/
        }
        fclose(fan_angle_correction_file);
    }
    else
    {
        /*message(0,"NOT using FAN ANGLE CORRECTION\n");*/
    }

    /* calculate width of rows used for given configuration */
    row_width = CT_Scanner.slice_width * (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) / CT_Scanner.distance_source_rcenter;

    /* loop over readings, rows and channels */
#pragma omp parallel for
    for (phi=0;phi<CT_Scanner.readings;phi++)
    {
        int index;
        double samplingoffset_z, samplingoffset_phi;
        int current_channel,current_row;
        double n_x,n_y,m_x,m_y;
        double alpha,beta;
        double det_x,det_y,det_z;
        double source_x,source_y,source_z;
        double ffs_offset_x, ffs_offset_y, ffs_offset_z;

        /* define flying focal spot offset */
        if (CT_Scanner.ffs_mode == 0)
        {
            /* stationary focal spot: A-A-A-A-... */
            /* A */
            ffs_offset_x = -0.16;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 1)
        {
            /* phi (in-plane) flying focal spot: B-C-B-C-... */
            /* B */
            if (phi%2 == 0)
            {
                ffs_offset_x = +0.31;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* C */
            if (phi%2 == 1)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }
        }

        if (CT_Scanner.ffs_mode == 2)
        {
            /* combined flying focal spot: F-G-H-I-F-G-H-I-... */
            /* F */
            if (phi%4 == 0)
            {
                ffs_offset_x = 0.31;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* G */
            if (phi%4 == 1)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* H */
            if (phi%4 == 2)
            {
                ffs_offset_x = 0.31;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }

            /* I */
            if (phi%4 == 3)
            {
                ffs_offset_x = -0.31;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }
        }

        if (CT_Scanner.ffs_mode == 3)
        {
            /* z flying focal spot: ???? */
            /* no value given for the force */
            ffs_offset_x = 0.0;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 4)
        {
            /* diagonal focal spot: D-E-D-E-... */
            /* D */
            if (phi%2 == 0)
            {
                ffs_offset_x = 0.47;
                ffs_offset_y = -1.44;
                ffs_offset_z = 0.2;
            }

            /* E */
            if (phi%2 == 1)
            {
                ffs_offset_x = -0.16;
                ffs_offset_y = 3.33;
                ffs_offset_z = -0.47;
            }
        }

        /* calculate rotation angle */
        /* - rotation clockwise */
        /* - rotation offset to account for starting position */
        alpha = 2.0 * M_PI / CT_Scanner.angles * (phi - (CT_Scanner.rotation_offset-1.0));
        n_x = cos(alpha);
        n_y = - sin(alpha);
        m_x = n_y;
        m_y = - n_x;

        /* source coordinates */
        /* - use source_z0 and bed_position to identify z position */
        source_x = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_x + ffs_offset_x * m_x;
        source_y = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_y + ffs_offset_x * m_y;
        source_z = CT_Scanner.source_z0 + bed_position[phi] + ffs_offset_z;

        /* calculate detector coordinates */
        for (current_row=0;current_row<CT_Scanner.detector_rows;current_row++)
        {
            if (Parameter.samplingfactor_z == 1)
            {
                det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - 0.5);
            }
            else
            {
                if (Parameter.samplingfactor_z%2 == 0)
                {
                    samplingoffset_z = 1.0/((double)Parameter.samplingfactor_z+1.0);
                    det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - (samplingindex_z+1)*samplingoffset_z);
                }
                else
                {
                    samplingoffset_z = 1.0/((double)Parameter.samplingfactor_z-1.0);
                    det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - samplingindex_z*samplingoffset_z);
                }
            }

            for (current_channel=0;current_channel<CT_Scanner.detector_channels;current_channel++)
            {
                /* - current_channel indicates current detector channel in vertical direction */
                /* - rotate half the detector and shift to center of this detector (+0.5) */
                /* - we are adding detector_alignment, Siemens seems to subtract... */
                if (Parameter.samplingfactor_phi == 1)
                {
                    beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + 0.5 + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment - fan_angle_correction[current_channel+current_row*CT_Scanner.detector_channels]);
                }
                else
                {
                    if (Parameter.samplingfactor_phi%2 == 0)
                    {
                        samplingoffset_phi = 1.0/((double)Parameter.samplingfactor_phi+1.0);
                        beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + (samplingindex_phi+1)*samplingoffset_phi + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment - fan_angle_correction[current_channel+current_row*CT_Scanner.detector_channels]);
                    }
                    else
                    {
                        samplingoffset_phi = 1.0/((double)Parameter.samplingfactor_phi-1.0);
                        beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + samplingindex_phi*samplingoffset_phi + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment - fan_angle_correction[current_channel+current_row*CT_Scanner.detector_channels]);
                    }
                }
                det_x = n_x * CT_Scanner.distance_source_rcenter - (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * cos(alpha + beta);
                det_y = n_y * CT_Scanner.distance_source_rcenter + (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * sin(alpha + beta);

                /* store geometry in Eventvector */
                index = current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows;
                Eventvector[index].x1 = source_x;
                Eventvector[index].y1 = source_y;
                Eventvector[index].z1 = source_z;
                Eventvector[index].x2 = det_x;
                Eventvector[index].y2 = det_y;
                Eventvector[index].z2 = det_z;
                Eventvector[index].valid = 1;
            }
        }
    }
    free(bed_position);
    free(fan_angle_correction);
}


/* ############################################################################################# */
/* ### ct_siemens_force_setup ################################################################## */
/* ############################################################################################# */
/**
 * @brief ct_siemens_force_setup Loads the scanner setup.
 *
 * All values taken from the scanner documentation provided by Karl Stierstorfer.
 */
void ct_siemens_force_setup()
{
    message(5,"setup force...\n");

    /* load default scanner setup */
    ct_scanner_setup();

    /* fixed scanner geometry */
    CT_Scanner.detector_channels = 920;
    CT_Scanner.detector_alignment = 2.125;
    CT_Scanner.distance_source_rcenter = 595.0;
    CT_Scanner.distance_rcenter_detector = 490.6;
    CT_Scanner.fan_angle_increment = 0.0542912;

    /* update current scanner parameter using external info */
    ct_scanner_update_parameter();

    message(5,"setup force...done.\n");
}
