/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "CT_Siemens_Definition_AS.h"


/* ############################################################################################# */
/* ### ct_siemens_definition_as_info ########################################################### */
/* ############################################################################################# */
/**
 * @brief ct_siemens_definition_as_info Displays scanner info.
 * @return
 */
char* ct_siemens_definition_as_info()
{
    char *scanner_info = (char*)calloc(50,sizeof(char));
    sprintf(scanner_info,"\t\t\t 21) [CT] Siemens Somaton Definition AS\n");
    return scanner_info;
}


/* ############################################################################################# */
/* ### ct_siemens_definition_as_read_events #################################################### */
/* ############################################################################################# */
/**
 * @brief ct_siemens_definition_as_read_events
 * @param Eventvector
 */
void ct_siemens_definition_as_read_events(event *Eventvector)
{
    int phi;
    double row_width;
    double *bed_position = (double*)safe_calloc(CT_Scanner.readings,sizeof(double));

    /* read table motion */
    GetParameterVectorDouble("BED_POSITION",bed_position,CT_Scanner.readings);

    /* calculate width of rows used for given configuration */
    row_width = CT_Scanner.slice_width * (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) / CT_Scanner.distance_source_rcenter;

    /* loop over readings, rows and channels */
#pragma omp parallel for
    for (phi=0;phi<CT_Scanner.readings;phi++)
    {
        int s, index;
        double samplingoffset_phi;
        int current_channel,current_row;
        double n_x,n_y,m_x,m_y;
        double alpha,beta;
        double det_x,det_y,det_z;
        double source_x,source_y,source_z;
        double ffs_offset_x, ffs_offset_y, ffs_offset_z;

        /* define flying focal spot offset */
        if (CT_Scanner.ffs_mode == 0)
        {
            /* stationary focal spot: A-A-A-A-... */
            /* A */
            ffs_offset_x = -0.2;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 1)
        {
            /* phi (in-plane) flying focal spot: C-D-C-D-... */
            /* C */
            if (phi%2 == 0)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* D */
            if (phi%2 == 1)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }
        }

        if (CT_Scanner.ffs_mode == 2)
        {
            /* combined flying focal spot: C-D-E-F-C-D-E-F-... */
            /* C */
            if (phi%4 == 0)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* D */
            if (phi%4 == 1)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* E */
            if (phi%4 == 2)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 5.45;
                ffs_offset_z = -0.66;
            }

            /* F */
            if (phi%4 == 3)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 5.45;
                ffs_offset_z = -0.66;
            }
        }

        if (CT_Scanner.ffs_mode == 3)
        {
            /* z flying focal spot: A-B-A-B-... */
            /* A */
            if (phi%2 == 0)
            {
                ffs_offset_x = -0.2;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* B */
            if (phi%2 == 1)
            {
                ffs_offset_x = 0.6;
                ffs_offset_y = 0.0;
                ffs_offset_z = -0.66;
            }
        }

        if (CT_Scanner.ffs_mode == 4)
        {
            /* diagonal focal spot: ???? */
            /* no value given for the definition as */
            ffs_offset_x = 0.0;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        /* calculate rotation angle */
        /* - rotation clockwise */
        /* - rotation offset to account for starting position */
        alpha = 2.0 * M_PI / CT_Scanner.angles * (phi - (CT_Scanner.rotation_offset-1.0));
        n_x = cos(alpha);
        n_y = - sin(alpha);
        m_x = n_y;
        m_y = - n_x;

        /* source coordinates */
        /* - use source_z0 and bed_position to identify z position */
        source_x = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_x + ffs_offset_x * m_x;
        source_y = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_y + ffs_offset_x * m_y;
        source_z = CT_Scanner.source_z0 + bed_position[phi] + ffs_offset_z;

        /* calculate detector coordinates */
        for (current_row=0;current_row<CT_Scanner.detector_rows;current_row++)
        {
            det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - 0.5);

            for (current_channel=0;current_channel<CT_Scanner.detector_channels;current_channel++)
            {
                /* - current_channel indicates current detector channel in vertical direction */
                /* - rotate half the detector and shift to center of this detector (+0.5) */
                /* - we are adding detector_alignment, Siemens seems to subtract... */
                /* store geometry in Eventvector */
                for (s=0;s<Parameter.samplingfactor_phi;s++)
                {
                    samplingoffset_phi = 1.0/((double)Parameter.samplingfactor_phi+1.0);
                    beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + (s+1)*samplingoffset_phi + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment);
                    det_x = n_x * CT_Scanner.distance_source_rcenter - (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * cos(alpha + beta);
                    det_y = n_y * CT_Scanner.distance_source_rcenter + (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * sin(alpha + beta);

                    index = current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows;
                    Eventvector[index*Parameter.samplingfactor_phi+s].x1 = source_x;
                    Eventvector[index*Parameter.samplingfactor_phi+s].y1 = source_y;
                    Eventvector[index*Parameter.samplingfactor_phi+s].z1 = source_z;
                    Eventvector[index*Parameter.samplingfactor_phi+s].x2 = det_x;
                    Eventvector[index*Parameter.samplingfactor_phi+s].y2 = det_y;
                    Eventvector[index*Parameter.samplingfactor_phi+s].z2 = det_z;
                    Eventvector[index*Parameter.samplingfactor_phi+s].valid = 1;
                }
            }
        }
    }
    free(bed_position);
}


/* ############################################################################################# */
/* ### ct_siemens_definition_as_read_events_fs ################################################# */
/* ############################################################################################# */
/**
 * @brief ct_siemens_definition_as_read_events_fs
 * @param Eventvector
 * @param samplingindex
 */
void ct_siemens_definition_as_read_events_fs(event *Eventvector, int samplingindex_phi, int samplingindex_z)
{
    int phi;
    double row_width;
    double *bed_position = (double*)safe_calloc(CT_Scanner.readings,sizeof(double));

    /* read table motion */
    GetParameterVectorDouble("BED_POSITION",bed_position,CT_Scanner.readings);

    /* calculate width of rows used for given configuration */
    row_width = CT_Scanner.slice_width * (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) / CT_Scanner.distance_source_rcenter;

    /* loop over readings, rows and channels */
#pragma omp parallel for
    for (phi=0;phi<CT_Scanner.readings;phi++)
    {
        int index;
        double samplingoffset_z, samplingoffset_phi;
        int current_channel,current_row;
        double n_x,n_y,m_x,m_y;
        double alpha,beta;
        double det_x,det_y,det_z;
        double source_x,source_y,source_z;
        double ffs_offset_x, ffs_offset_y, ffs_offset_z;

        /* define flying focal spot offset */
        if (CT_Scanner.ffs_mode == 0)
        {
            /* stationary focal spot: A-A-A-A-... */
            /* A */
            ffs_offset_x = -0.2;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        if (CT_Scanner.ffs_mode == 1)
        {
            /* phi (in-plane) flying focal spot: C-D-C-D-... */
            /* C */
            if (phi%2 == 0)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* D */
            if (phi%2 == 1)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }
        }

        if (CT_Scanner.ffs_mode == 2)
        {
            /* combined flying focal spot: C-D-E-F-C-D-E-F-... */
            /* C */
            if (phi%4 == 0)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* D */
            if (phi%4 == 1)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* E */
            if (phi%4 == 2)
            {
                ffs_offset_x = -0.4;
                ffs_offset_y = 5.45;
                ffs_offset_z = -0.66;
            }

            /* F */
            if (phi%4 == 3)
            {
                ffs_offset_x = 0.4;
                ffs_offset_y = 5.45;
                ffs_offset_z = -0.66;
            }
        }

        if (CT_Scanner.ffs_mode == 3)
        {
            /* z flying focal spot: A-B-A-B-... */
            /* A */
            if (phi%2 == 0)
            {
                ffs_offset_x = -0.2;
                ffs_offset_y = 0.0;
                ffs_offset_z = 0.0;
            }

            /* B */
            if (phi%2 == 1)
            {
                ffs_offset_x = 0.6;
                ffs_offset_y = 0.0;
                ffs_offset_z = -0.66;
            }
        }

        if (CT_Scanner.ffs_mode == 4)
        {
            /* diagonal focal spot: ???? */
            /* no value given for the definition as */
            ffs_offset_x = 0.0;
            ffs_offset_y = 0.0;
            ffs_offset_z = 0.0;
        }

        /* calculate rotation angle */
        /* - rotation clockwise */
        /* - rotation offset to account for starting position */
        alpha = 2.0 * M_PI / CT_Scanner.angles * (phi - (CT_Scanner.rotation_offset-1.0));
        n_x = cos(alpha);
        n_y = - sin(alpha);
        m_x = n_y;
        m_y = - n_x;

        /* source coordinates */
        /* - use source_z0 and bed_position to identify z position */
        source_x = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_x + ffs_offset_x * m_x;
        source_y = (CT_Scanner.distance_source_rcenter + ffs_offset_y) * n_y + ffs_offset_x * m_y;
        source_z = CT_Scanner.source_z0 + bed_position[phi] + ffs_offset_z;

        /* calculate detector coordinates */
        for (current_row=0;current_row<CT_Scanner.detector_rows;current_row++)
        {
            if (Parameter.samplingfactor_z == 1)
            {
                det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - 0.5);
            }
            else
            {
                if (Parameter.samplingfactor_z%2 == 0)
                {
                    samplingoffset_z = 1.0/((double)Parameter.samplingfactor_z+1.0);
                    det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - (samplingindex_z+1)*samplingoffset_z);
                }
                else
                {
                    samplingoffset_z = 1.0/((double)Parameter.samplingfactor_z-1.0);
                    det_z = CT_Scanner.source_z0 + bed_position[phi] + row_width * (CT_Scanner.detector_rows/2.0 - current_row - samplingindex_z*samplingoffset_z);
                }
            }

            for (current_channel=0;current_channel<CT_Scanner.detector_channels;current_channel++)
            {
                /* - current_channel indicates current detector channel in vertical direction */
                /* - rotate half the detector and shift to center of this detector (+0.5) */
                /* - we are adding detector_alignment, Siemens seems to subtract... */
                if (Parameter.samplingfactor_phi == 1)
                {
                    beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + 0.5 + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment);
                }
                else
                {
                    if (Parameter.samplingfactor_phi%2 == 0)
                    {
                        samplingoffset_phi = 1.0/((double)Parameter.samplingfactor_phi+1.0);
                        beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + (samplingindex_phi+1)*samplingoffset_phi + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment);
                    }
                    else
                    {
                        samplingoffset_phi = 1.0/((double)Parameter.samplingfactor_phi-1.0);
                        beta = 2.0*M_PI / 360.0 * ((-CT_Scanner.detector_channels/2.0 + samplingindex_phi*samplingoffset_phi + current_channel + CT_Scanner.detector_alignment) * CT_Scanner.fan_angle_increment);
                    }
                }
                det_x = n_x * CT_Scanner.distance_source_rcenter - (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * cos(alpha + beta);
                det_y = n_y * CT_Scanner.distance_source_rcenter + (CT_Scanner.distance_source_rcenter + CT_Scanner.distance_rcenter_detector) * sin(alpha + beta);

                /* store geometry in Eventvector */
                index = current_channel+current_row*CT_Scanner.detector_channels+phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows;
                Eventvector[index].x1 = source_x;
                Eventvector[index].y1 = source_y;
                Eventvector[index].z1 = source_z;
                Eventvector[index].x2 = det_x;
                Eventvector[index].y2 = det_y;
                Eventvector[index].z2 = det_z;
                Eventvector[index].valid = 1;
            }
        }
    }
    free(bed_position);
}


/* ############################################################################################# */
/* ### ct_siemens_definition_as_setup ########################################################## */
/* ############################################################################################# */
/**
 * @brief ct_siemens_definition_as_setup Loads the scanner setup.
 */
void ct_siemens_definition_as_setup()
{
    message(5,"setup definition as...\n");

    /* load default scanner setup */
    ct_scanner_setup();

    /* fixed scanner geometry */
    CT_Scanner.detector_channels = 736;
    CT_Scanner.detector_alignment = 1.125;
    CT_Scanner.distance_source_rcenter = 595.0;
    CT_Scanner.distance_rcenter_detector = 490.6;
    CT_Scanner.fan_angle_increment = 0.067864;

    /* update current scanner parameter using external info */
    ct_scanner_update_parameter();

    message(5,"setup definition as...done.\n");
}
