function [fp_data] = EMrecon_ForwardProj_ND(parm,image)

% EMrecon_ForwardProj_ND(parm,image)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_ForwardProj_ND\n');
    fprintf('Usage: fp_data = EMrecon_ForwardProj_ND(parm,image);\n\n');
    mexEMrecon_ForwardProj;
    fprintf('\n');
    fp_data = -1;
else
    vfprintf(1,parm,'EMrecon_ForwardProj_ND...\n');
    tic;
    fp_data = mexEMrecon_ForwardProj(parm,image(:));
    vfprintf(1,parm,'EMrecon_ForwardProj_ND...done. [%f sec]\n',toc);
end

end