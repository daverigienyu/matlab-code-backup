function [fp_data] = EMrecon_ForwardProjMask_1D(parm,image,mask)

% EMrecon_ForwardProjMask_1D(parm,image)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_ForwardProjMask_1D\n');
    fprintf('Usage: fp_data = EMrecon_ForwardProjMask_1D(parm,image,mask);\n\n');
    mexEMrecon_ForwardProjMask;
    fprintf('\n');
    fp_data = -1;
else
    vfprintf(1,parm,'EMrecon_ForwardProjMask_1D...\n');
    tic;
    fp_data = mexEMrecon_ForwardProjMask(parm,image,mask);
    vfprintf(1,parm,'EMrecon_ForwardProjMask_1D...done. [%f sec]\n',toc);
end

end