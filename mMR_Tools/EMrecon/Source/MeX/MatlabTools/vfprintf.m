function [] = vfprintf(level,parm,varargin)

if (nargin >= 3)
    % default verbose level for matlab is 1
    verbose = 1;
    
    % if user provides verbose level overwrite default value
    if (isfield(parm,'VERBOSE') == 1)
        verbose = parm.VERBOSE;
    end
    
    % check whether verbose level is reached
    if (verbose >= level)
        fprintf(varargin{:});
    end
end