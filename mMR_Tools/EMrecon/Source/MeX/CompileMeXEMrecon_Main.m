%% set cflags
cflags_windows = '-DWINDOWS_MATLAB COPTIMFLAGS=-O3 -DNDEBUG';
cflags_linux = 'CFLAGS=''$CFLAGS -w -fopenmp'' -lgomp COP/TIMFLAGS=-O3 -DNDEBUG';


%% set os_source and mex compiler options with regard to the os
if (strcmp(os,'linux') == 1)
    disp('compiling mexEMrecon for linux');
    cflags = cflags_linux;
else
    disp('compiling mexEMrecon for windows');
    cflags = cflags_windows;
end

% cd to source directory
cd(path_to_emrecon_mex_source);


%% compile mex functions
% matlab mex interface id
mex_id = 0;
matlab_id = 0;

% add main components
mex_cmd_main = ' -DMATLAB';
mex_cmd_main = [mex_cmd_main ' IO/C/mexEMrecon_Parameter.c'];
mex_cmd_main = [mex_cmd_main ' ../IO/Parameter.c'];
mex_cmd_main = [mex_cmd_main ' ../IO/IO.c'];
mex_cmd_main = [mex_cmd_main ' ../Main/Siddon.c'];
mex_cmd_main = [mex_cmd_main ' ../Main/Tools.c'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/Scanner.c'];

% add scanner definition
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/CT/CT_Scanner.c'];
if (add_scanner_CT_Siemens_Definition_AS == 1)
    mex_cmd_main = [mex_cmd_main ' -DCT_SIEMENS_DEFINITION_AS'];
    mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/CT/CT_Siemens_Definition_AS.c'];
end
if (add_scanner_CT_Siemens_Force == 1)
    mex_cmd_main = [mex_cmd_main ' -DCT_SIEMENS_FORCE'];
    mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/CT/CT_Siemens_Force.c'];
end
%mex_cmd_main = [mex_cmd_main ' -DPET_SCANNER'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/PET_Scanner.c'];
mex_cmd_main = [mex_cmd_main ' -DPET_2D_DEMO'];
mex_cmd_main = [mex_cmd_main ' ../ScannerGeometries/PET/Demo/PET_2D_Demo.c'];

% enable / disable protection of scanner information
if (protect_scanner_info == 1)
    mex_cmd_main = [mex_cmd_main ' -DPROTECT_SCANNER_INFO'];
end

% setup outdir
mex_cmd_main = [mex_cmd_main ' -outdir ../../Bin'];


%% Compile ReconAlgorithms
run('CompileMeXEMrecon_ReconAlgorithms');


%% Compile Tools for different scanner types
run('CompileMeXEMrecon_ScannerTools');


%% list of (pure) Matlab tools
if (protect_MatlabEMreconTools == 1)
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/vfprintf';
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/writeMGF';
    matlab_id = matlab_id + 1; MatlabEMreconTools(matlab_id).name = 'MatlabTools/read_mMR_Normfile';
end


%% protect corresponding scripts
if (exist('../../Bin/Tools','dir') ~= 7)
    mkdir('../../Bin/Tools');
end

% protect MeX scripts
if (mex_id > 0)
    for i=1:numel(MexEMreconTools)
        fprintf('pcode %s.m\n',MexEMreconTools(i).name);
        pcode(sprintf('%s.m',MexEMreconTools(i).name));
    end
    
    % move MeX scripts
    dos('mv *.p ../../Bin');
end


%% required (pure) Matlab scripts
if (matlab_id > 0)
    for i=1:numel(MatlabEMreconTools)
        fprintf('pcode %s.m\n',MatlabEMreconTools(i).name);
        pcode(sprintf('%s.m',MatlabEMreconTools(i).name));
    end
    
    % move Matlab scripts
    dos('mv *.p ../../Bin/Tools');
end
