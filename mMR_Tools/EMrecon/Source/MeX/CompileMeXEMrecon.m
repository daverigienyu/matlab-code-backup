%% clear and close
close all;
clear mex; clear; clc;


%% define compile targets
%% scanner setup
% CT
add_scanner_CT_Siemens_Definition_AS = 1;
add_scanner_CT_Siemens_Force = 1;

%% recon + tools setup
% Main
compile_mexEMrecon_Version = 1;

% ReconAlgorithms
compile_mexEMrecon_EM = 0;
compile_mexEMrecon_EM_Mask = 0;
compile_mexEMrecon_EM_Step = 0;
compile_mexEMrecon_BackProj = 1;
compile_mexEMrecon_BackProjMask = 0;
compile_mexEMrecon_ForwardProj = 1;
compile_mexEMrecon_ForwardProjMask = 0;
compile_mexEMrecon_OSEM_TV_Mask = 1;

% protect (pure) Matlab functions
protect_MatlabEMreconTools = 1;

% if set to 1, all output / log is disabled
protect_scanner_info = 0;


%% set path to corresponding source code and compiler flags
% run SetupMexEMrecon.m to setup path to the source code
run('SetupMeXEMrecon.m');


%% compile mex version of EMrecon using setup from above
run('CompileMeXEMrecon_Main.m');
