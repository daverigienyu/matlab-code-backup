/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "EM.h"


/* ############################################################################################# */
/* ### EM ###################################################################################### */
/* ############################################################################################# */
/**
 * @brief EM
 *
 * The EM/OSEM is based on the following publications:
 *
 * 1) Shepp L.A., Vardi Y., Maximum likelihood reconstruction for emission tomography, 1982,
 *    IEEE TMI, 1(2), 113-122
 *
 * 2) Hudson H.M., Larkin R.S., Accelerated image reconstruction using ordered subsets of
 *    projection data, 1994, IEEE TMI, 13(4), 601-609
 *
 * @param input_data
 * @param input_data_fp
 * @param input_sensitivity_map
 * @param input_image
 * @param output_image
 */
void EM(double *input_data, double *input_data_fp, double *input_sensitivity_map, double *input_image, double *output_image)
{
    long i;
    int k,l,subset_start,subset_stop,subiteration;
    short *subsetindex;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    double *sum = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));

    if (Parameter.fixed_subset <= 0)
    {
        subset_start = 0;
        subset_stop = Parameter.subsets;
    }
    else
    {
        subset_start = Parameter.fixed_subset-1;
        subset_stop = Parameter.fixed_subset;
    }

    /* assure that output image is initialized */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = input_image[i];
    }

    cut_image(output_image);

    /* read events - has to be done only once */
    read_events(events_in_file,Eventvector);

    /* iteration loop */
    subiteration = 0;
    for (k=0;k<Parameter.iterations;k++)
    {
        /* subset loop */
        for (l=subset_start;l<subset_stop;l++)
        {
            subiteration++;

            /* select events that should be used in current subset */
            update_subset_index(subsetindex,l+1); /* we count subsets from 1:n */

            /* event loop */
#pragma omp parallel for firstprivate(Path)
            for (i=0;i<events_in_file;i++)
            {
                if (subsetindex[i] == 1 && Eventvector[i].valid == 1 && input_data[i] > 0.0)
                {
                    int j;
                    double c = 0.0;

                    if (Path == 0)
                    {
                        Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
                    }

                    compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

                    for (j=0;Path[j].coord!=-1;j++) {
                        c += output_image[Path[j].coord]*Path[j].length;
                    }

                    c += input_data_fp[i];

                    if (c > Parameter.epsilon)
                    {
                        c = Parameter.subsets*input_data[i]/c;
                        for (j=0;Path[j].coord!=-1;j++)
                        {
#pragma omp atomic
                            sum[Path[j].coord] += c*Path[j].length;
                        }
                    }
                }
            }
            /* event loop end */

            /* image update */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                if (output_image[i] > Parameter.epsilon && sum[i] > Parameter.epsilon && input_sensitivity_map[i] > Parameter.epsilon)
                {
                    output_image[i] = output_image[i]*sum[i]/input_sensitivity_map[i];
                }
                else
                {
                    output_image[i] = 0.0;
                }
                sum[i] = 0.0;
            }
            /* image update end*/

            /* convolution with a gaussian kernel according to "convolutioninterval" and "fwhm" */
            if (Parameter.convolve == 1 && Parameter.convolutioninterval > 0 && subiteration%Parameter.convolutioninterval == 0)
            {
                convolve(output_image,output_image);
            }

            cut_image(output_image);
        }
        /* subset loop end*/
    }
    /* iteration loop end*/

    free(subsetindex);
    free(Eventvector);
    free(PathOMP);
    free(sum);
}


/* ############################################################################################# */
/* ### EM_Mask ################################################################################# */
/* ############################################################################################# */
/**
 * @brief EM_Mask
 *
 * The EM/OSEM is based on the following publications:
 *
 * 1) Shepp L.A., Vardi Y., Maximum likelihood reconstruction for emission tomography, 1982,
 *    IEEE TMI, 1(2), 113-122
 *
 * 2) Hudson H.M., Larkin R.S., Accelerated image reconstruction using ordered subsets of
 *    projection data, 1994, IEEE TMI, 13(4), 601-609
 *
 * @param input_data
 * @param input_data_fp
 * @param input_sensitivity_map
 * @param input_image
 * @param input_mask
 * @param output_image
 */
void EM_Mask(double *input_data, double *input_data_fp, double *input_sensitivity_map, double *input_image, double *input_mask, double *output_image)
{
    long i;
    int k,l,subset_start,subset_stop,subiteration;
    short *subsetindex;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    double *sum = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file, sizeof(event)));
    subsetindex = (short*)(safe_calloc(events_in_file, sizeof(short)));

    if (Parameter.fixed_subset <= 0)
    {
        subset_start = 0;
        subset_stop = Parameter.subsets;
    }
    else
    {
        subset_start = Parameter.fixed_subset-1;
        subset_stop = Parameter.fixed_subset;
    }

    /* assure that output image is initialized */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = input_image[i];
    }

    cut_image(output_image);

    /* read events - has to be done only once */
    read_events(events_in_file,Eventvector);

    /* iteration loop */
    subiteration = 0;
    for (k=0;k<Parameter.iterations;k++)
    {
        /* subset loop */
        for (l=subset_start;l<subset_stop;l++)
        {
            subiteration++;

            /* select events that should be used in current subset */
            update_subset_index(subsetindex,l+1); /* we count subsets from 1:n */

            /* event loop */
#pragma omp parallel for firstprivate(Path)
            for (i=0;i<events_in_file;i++)
            {
                if (subsetindex[i] == 1 && Eventvector[i].valid == 1 && input_mask[i] > 0.0 && input_data[i] > 0.0)
                {
                    int j;
                    double c = 0.0;

                    if (Path == 0)
                    {
                        Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
                    }

                    compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

                    for (j=0;Path[j].coord!=-1;j++) {
                        c += output_image[Path[j].coord]*input_mask[i]*Path[j].length;
                    }

                    c += input_data_fp[i];

                    if (c > Parameter.epsilon)
                    {
                        c = Parameter.subsets*input_data[i]/c;
                        for (j=0;Path[j].coord!=-1;j++)
                        {
#pragma omp atomic
                            sum[Path[j].coord] += c*input_mask[i]*Path[j].length;
                        }
                    }
                }
            }
            /* event loop end */

            /* image update */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                if (output_image[i] > Parameter.epsilon && sum[i] > Parameter.epsilon && input_sensitivity_map[i] > Parameter.epsilon)
                {
                    output_image[i] = output_image[i]*sum[i]/input_sensitivity_map[i];
                }
                else
                {
                    output_image[i] = 0.0;
                }
                sum[i] = 0.0;
            }
            /* image update end*/

            /* convolution with a gaussian kernel according to "convolutioninterval" and "fwhm" */
            if (Parameter.convolve == 1 && Parameter.convolutioninterval > 0 && subiteration%Parameter.convolutioninterval == 0)
            {
                convolve(output_image,output_image);
            }

            cut_image(output_image);
        }
        /* subset loop end*/
    }
    /* iteration loop end*/

    free(subsetindex);
    free(Eventvector);
    free(PathOMP);
    free(sum);
}


/* ############################################################################################# */
/* ### OSEM_TV_Mask ############################################################################ */
/* ############################################################################################# */
/**
 * @brief OSEM_TV_Mask
 * @param input_data
 * @param input_image
 * @param input_mask
 * @param output_image
 */
void OSEM_TV_Mask2(double *input_data, double *input_image, double *input_mask, double *output_image)
{
    long i;
    int k,l,subiteration;
    short *subsetindex;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    double *tvimg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *sensitivity_map = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *sum = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *f_blur = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    long events_in_file = get_number_of_events_in_file();

    double tv_eps = 0.00001;
    double tv_beta = 0.0;
    int tv_reso_reco = 0;
    tv_eps = UpdateParameterDouble("TV_EPS",tv_eps);
    tv_beta = UpdateParameterDouble("TV_BETA",tv_beta);
    tv_reso_reco = UpdateParameterInt("TV_RESO_RECO",tv_reso_reco);

    setup_siddon();

    Eventvector = (event*)(safe_calloc(Parameter.samplingfactor_phi*events_in_file, sizeof(event)));
    subsetindex = (short*)(safe_calloc(events_in_file, sizeof(short)));

    /* assure that output image is initialized */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = input_image[i];
        tvimg[i] = 0.0;
    }

    cut_image(output_image);

    /* read events - has to be done only once */
    read_events(events_in_file,Eventvector);

    /* iteration loop */
    subiteration = 0;
    for (k=0;k<Parameter.iterations;k++)
    {
        /* subset loop */
        for (l=0;l<Parameter.subsets;l++)
        {
            subiteration++;

            /* initialize sensitivity_map */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                sensitivity_map[i] = 0.0;
                f_blur[i] = output_image[i];
            }

            if (tv_reso_reco == 1)
            {
                convolve(output_image,f_blur);
            }

            /* select events that should be used in current subset */
            update_subset_index(subsetindex,l+1); /* we count subsets from 1:n */

            /* event loop */
#pragma omp parallel for firstprivate(Path)
            for (i=0;i<events_in_file;i++)
            {
                if (subsetindex[i] == 1 && input_mask[i] > 0.0)
                {
                    int s;

                    if (Path == 0)
                    {
                        Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
                    }

                    for (s=0;s<Parameter.samplingfactor_phi;s++)
                    {
                        int j;
                        double c = 0.0;

                        compute_path(Eventvector[i*Parameter.samplingfactor_phi+s].x1,
                                Eventvector[i*Parameter.samplingfactor_phi+s].y1,
                                Eventvector[i*Parameter.samplingfactor_phi+s].z1,
                                Eventvector[i*Parameter.samplingfactor_phi+s].x2,
                                Eventvector[i*Parameter.samplingfactor_phi+s].y2,
                                Eventvector[i*Parameter.samplingfactor_phi+s].z2,
                                Path);

                        for (j=0;Path[j].coord!=-1;j++)
                        {
                            c += f_blur[Path[j].coord]*input_mask[i]*Path[j].length;
                        }

                        /* update sensitivity_map for each subset */
                        for (j=0;Path[j].coord!=-1;j++)
                        {
#pragma omp atomic
                            sensitivity_map[Path[j].coord] += input_mask[i]*Path[j].length;
                        }

                        if (Eventvector[i].valid == 1 && input_data[i] > 0.0 && c > Parameter.epsilon)
                        {
                            c = input_data[i]/c;
                            for (j=0;Path[j].coord!=-1;j++)
                            {
#pragma omp atomic
                                sum[Path[j].coord] += c*input_mask[i]*Path[j].length;
                            }
                        }
                    }
                }
            }
            /* event loop end */

            if (tv_reso_reco == 1)
            {
                convolve(sensitivity_map,sensitivity_map);
                convolve(sum,sum);
            }

            if (tv_beta > 0.0)
            {
                tv(output_image,tvimg,tv_eps);
            }

            /* image update */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                if (output_image[i] > Parameter.epsilon && sum[i] > Parameter.epsilon && sensitivity_map[i] > Parameter.epsilon)
                {
                    if ((sensitivity_map[i] + tv_beta * tvimg[i]) > Parameter.epsilon)
                    {
                        output_image[i] = output_image[i]*sum[i]/(sensitivity_map[i] + tv_beta * tvimg[i]);
                    }
                    else
                    {
                        output_image[i] = output_image[i]*sum[i]/sensitivity_map[i];
                    }
                }
                else
                {
                    output_image[i] = 0.0;
                }
                sum[i] = 0.0;
            }
            /* image update end*/

            /* convolution with a gaussian kernel according to "convolutioninterval" and "fwhm" */
            if (Parameter.convolve == 1 && Parameter.convolutioninterval > 0 && subiteration%Parameter.convolutioninterval == 0)
            {
                convolve(output_image,output_image);
            }

            cut_image(output_image);
        }
        /* subset loop end*/
    }
    /* iteration loop end*/

    free(sensitivity_map);
    free(subsetindex);
    free(Eventvector);
    free(PathOMP);
    free(sum);
    free(f_blur);
    free(tvimg);
}


/* ############################################################################################# */
/* ### OSEM_TV_Mask ############################################################################ */
/* ############################################################################################# */
/**
 * @brief OSEM_TV_Mask
 * @param input_data
 * @param input_image
 * @param input_mask
 * @param output_image
 */
void OSEM_TV_Mask(double *input_data, double *input_image, double *input_mask, double *output_image)
{
    int sz,sphi;
    long i;
    int k,l,subiteration;
    short *subsetindex;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    double *tvimg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *sensitivity_map = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *sum = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    double *f_blur = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    long events_in_file = get_number_of_events_in_file();

    double tv_eps = 0.00001;
    double tv_beta = 0.0;
    int tv_reso_reco = 0;
    tv_eps = UpdateParameterDouble("TV_EPS",tv_eps);
    tv_beta = UpdateParameterDouble("TV_BETA",tv_beta);
    tv_reso_reco = UpdateParameterInt("TV_RESO_RECO",tv_reso_reco);

    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file, sizeof(event)));
    subsetindex = (short*)(safe_calloc(events_in_file, sizeof(short)));

    /* assure that output image is initialized */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = input_image[i];
        tvimg[i] = 0.0;
    }

    cut_image(output_image);

    /* iteration loop */
    subiteration = 0;
    for (k=0;k<Parameter.iterations;k++)
    {
        /* subset loop */
        for (l=0;l<Parameter.subsets;l++)
        {
            subiteration++;

            /* initialize sensitivity_map */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                sensitivity_map[i] = 0.0;
                f_blur[i] = output_image[i];
            }

            if (tv_reso_reco == 1)
            {
                convolve(output_image,f_blur);
            }

            /* select events that should be used in current subset */
            update_subset_index(subsetindex,l+1); /* we count subsets from 1:n */

            /* samplingfactor z loop */
            for (sz=0;sz<Parameter.samplingfactor_z;sz++)
            {
                /* samplingfactor phi loop */
                for (sphi=0;sphi<Parameter.samplingfactor_phi;sphi++)
                {
                    read_events_ct(Eventvector,sphi,sz);

                    /* event loop */
#pragma omp parallel for firstprivate(Path)
                    for (i=0;i<events_in_file;i++)
                    {
                        int j;
                        double c;

                        if (subsetindex[i] == 1 && input_mask[i] > 0.0)
                        {
                            if (Path == 0)
                            {
                                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
                            }

                            c = 0.0;
                            compute_path(Eventvector[i].x1,Eventvector[i].y1,Eventvector[i].z1,Eventvector[i].x2,Eventvector[i].y2,Eventvector[i].z2,Path);

                            for (j=0;Path[j].coord!=-1;j++)
                            {
                                c += f_blur[Path[j].coord]*input_mask[i]*Path[j].length;
                            }

                            /* update sensitivity_map for each subset */
                            for (j=0;Path[j].coord!=-1;j++)
                            {
#pragma omp atomic
                                sensitivity_map[Path[j].coord] += input_mask[i]*Path[j].length;
                            }

                            if (Eventvector[i].valid == 1 && input_data[i] > 0.0 && c > Parameter.epsilon)
                            {
                                c = input_data[i]/c;
                                for (j=0;Path[j].coord!=-1;j++)
                                {
#pragma omp atomic
                                    sum[Path[j].coord] += c*input_mask[i]*Path[j].length;
                                }
                            }
                        }
                    }
                    /* event loop end */
                }
                /* end samplingfactor phi */
            }
            /* end samplingfactor z */

            if (tv_reso_reco == 1)
            {
                convolve(sensitivity_map,sensitivity_map);
                convolve(sum,sum);
            }

            if (tv_beta > 0.0)
            {
                tv(output_image,tvimg,tv_eps);
            }

            /* image update */
#pragma omp parallel for
            for (i=0;i<Parameter.imagesize;i++)
            {
                if (output_image[i] > Parameter.epsilon && sum[i] > Parameter.epsilon && sensitivity_map[i] > Parameter.epsilon)
                {
                    if ((sensitivity_map[i] + tv_beta * tvimg[i]) > Parameter.epsilon)
                    {
                        output_image[i] = output_image[i]*sum[i]/(sensitivity_map[i] + tv_beta * tvimg[i]);
                    }
                    else
                    {
                        output_image[i] = output_image[i]*sum[i]/sensitivity_map[i];
                    }
                }
                else
                {
                    output_image[i] = 0.0;
                }
                sum[i] = 0.0;
            }
            /* image update end*/

            /* convolution with a gaussian kernel according to "convolutioninterval" and "fwhm" */
            if (Parameter.convolve == 1 && Parameter.convolutioninterval > 0 && subiteration%Parameter.convolutioninterval == 0)
            {
                convolve(output_image,output_image);
            }

            cut_image(output_image);
        }
        /* subset loop end*/
    }
    /* iteration loop end*/

    free(sensitivity_map);
    free(subsetindex);
    free(Eventvector);
    free(PathOMP);
    free(sum);
    free(f_blur);
    free(tvimg);
}


/* ############################################################################################# */
/* ### EM_Step ################################################################################# */
/* ############################################################################################# */
/**
 * @brief EM_Step
 *
 * The EM/OSEM is based on the following publications:
 *
 * 1) Shepp L.A., Vardi Y., Maximum likelihood reconstruction for emission tomography, 1982,
 *    IEEE TMI, 1(2), 113-122
 *
 * 2) Hudson H.M., Larkin R.S., Accelerated image reconstruction using ordered subsets of
 *    projection data, 1994, IEEE TMI, 13(4), 601-609
 *
 * @param input_data
 * @param input_data_fp
 * @param input_image
 * @param output_image
 */
void EM_Step(double *input_data, double *input_data_fp, double *input_image, double *output_image)
{
    long i;

    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    /* assure that output image is zero */
#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        output_image[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1 && input_data[i] > 0.0)
        {
            int j;
            double c = 0.0;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++) {
                c += input_image[Path[j].coord]*Path[j].length;
            }

            c += input_data_fp[i];

            if (c > Parameter.epsilon)
            {
                c = Parameter.subsets*input_data[i]/c;
                for (j=0;Path[j].coord!=-1;j++)
                {
#pragma omp atomic
                    output_image[Path[j].coord] += c*Path[j].length;
                }
            }
        }
    }
    /* event loop end */

    /* free memory */
    free(Eventvector);
    free(PathOMP);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(output_image,output_image);
    }

    cut_image(output_image);
}
